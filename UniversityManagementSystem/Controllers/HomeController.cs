﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UniversityManagementSystem.Areas.FinancialAccountsModule.Models;
using UniversityManagementSystem.Models;

namespace UniversityManagementSystem.Controllers
{
    public class HomeController : Controller
    {
        UniversityManagementSystemEntities db = new UniversityManagementSystemEntities();

        public ActionResult Index()
        {
            ViewBag.MainTitle = "CPanel";
            ViewBag.SubTitle = "Home Page";
            return View();
        }
        public ActionResult Unauthorized()
        {
            ViewBag.MainTitle = "CPanel";
            ViewBag.SubTitle = "Unauthorized";
            return View();
        }
        public JsonResult GetResultStdData()
        {
            var yearId = db.R_StudyYear_data.FirstOrDefault().Year_Id; 
            var data = db.Re_StudentResult_data.GroupBy(l => l.Result_AcademicStatus).Where(r=> r.FirstOrDefault().R_StudentRegistration_data.Year_Id == yearId).Select(s => new { label = s.FirstOrDefault().Result_AcademicStatus, value = s.Count() }).ToList();
            foreach (var item in data)
            {
                ChartModel row = new ChartModel
                {
                    label = item.label,
                    value = item.value
                };
            }

            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetIncomeAndExpnses()
        {
           
            List<SingeleIncomeAndExpnsesModel> SingeleIncomeAndExpnses = new List<SingeleIncomeAndExpnsesModel>();
            List<R_StudyYear_data> CurrentYear = db.R_StudyYear_data.ToList();
            foreach (var item in CurrentYear)
            {
                decimal IncomeTotal = 0;
                decimal AddfunTotal = 0;
                decimal uQTotal = 0;
                var  IncomeProfit = db.Fee_TuitionFeesDescribtion.Where(u => u.TFD_FeesDate > item.YearStart && u.TFD_FeesDate < item.YearEnd).Select(x=>x.TFD_TuitionFee).ToList();
                if (IncomeProfit != null)
                {
                    foreach (var Income in IncomeProfit)
                    {
                        IncomeTotal = IncomeTotal + Income;
                    }
                }
            var  Addfun = db.AddFunds.Where(u => u.fundHistory > item.YearStart && u.fundHistory < item.YearEnd).Select(x => x.funAmount).ToList();
                if (Addfun !=null)
                {
                    foreach (var fun in Addfun)
                    {
                        AddfunTotal =Convert.ToDecimal( AddfunTotal + fun);
                    }
                }
                var Inv = db.Invoices.Where(u => u.INDate > item.YearStart && u.INDate < item.YearEnd).ToList();
                foreach (var Invitem in Inv)
                {
                    var InvoicesDetails = db.InvoicesDetails.Where(x => x.INnumber == Invitem.INnumber).ToList();
                    foreach (var Ditem in InvoicesDetails)
                    {
                        var TIN = db.InvoicesDetails.Find(Ditem.idID);
                        decimal UnitPrice = Convert.ToDecimal(TIN.IdUnitPrice);
                        int Quantity = Convert.ToInt32(TIN.IdQuantity);
                        uQTotal = uQTotal + (UnitPrice * Quantity);
                    }
                }
                SingeleIncomeAndExpnsesModel Singele = new SingeleIncomeAndExpnsesModel()
                {
                    YearList=item.Year_Name,
                    ExpnsesList=uQTotal,
                    IncomeList=Convert.ToDecimal(IncomeTotal + AddfunTotal)
                };
                SingeleIncomeAndExpnses.Add(Singele);

            }
           

            IncomeAndExpnsesModel row = new IncomeAndExpnsesModel
            {
                YearList = SingeleIncomeAndExpnses.Select(x=>x.YearList).ToList(),
                IncomeList = SingeleIncomeAndExpnses.Select(x => x.IncomeList).ToList(),
                ExpnsesList = SingeleIncomeAndExpnses.Select(x => x.ExpnsesList).ToList()
            };
            return Json(row, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetProfit()
        {
            
            List<Profit> ProfitList = new List<Profit>();
            List<R_StudyYear_data> CurrentYear = db.R_StudyYear_data.ToList();
            foreach (var item in CurrentYear)
            {
                decimal IncomeTotal = 0;
                decimal AddfunTotal = 0;
                decimal uQTotal = 0;
                var   IncomeProfit = db.Fee_TuitionFeesDescribtion.Where(u => u.TFD_FeesDate > item.YearStart && u.TFD_FeesDate < item.YearEnd).Select(x=> x.TFD_TuitionFee).ToList();
                if (IncomeProfit != null)
                {
                    foreach (var Income in IncomeProfit)
                    {
                        IncomeTotal = IncomeTotal + Income;
                    }
                }
                var Addfun = db.AddFunds.Where(u => u.fundHistory > item.YearStart && u.fundHistory < item.YearEnd).Select(x=>x.funAmount).ToList();
                if (Addfun != null)
                {
                    foreach (var fun in Addfun)
                    {
                        AddfunTotal = Convert.ToDecimal(AddfunTotal + fun);
                    }
                }
                var Inv = db.Invoices.Where(u => u.INDate > item.YearStart && u.INDate < item.YearEnd).ToList();
                foreach (var Invitem in Inv)
                {
                    var InvoicesDetails = db.InvoicesDetails.Where(x => x.INnumber == Invitem.INnumber).ToList();
                    foreach (var Ditem in InvoicesDetails)
                    {
                        var TIN = db.InvoicesDetails.Find(Ditem.idID);
                        decimal UnitPrice = Convert.ToDecimal(TIN.IdUnitPrice);
                        int Quantity = Convert.ToInt32(TIN.IdQuantity);
                        uQTotal = uQTotal + (UnitPrice * Quantity);
                    }
                }
                decimal v = Convert.ToDecimal(IncomeTotal + AddfunTotal);
                Profit allProfit = new Profit()
                {
                    yearsLable=item.Year_Name,
                    value= v- uQTotal
                };

                ProfitList.Add(allProfit);
            }
            ProfitModel row = new ProfitModel
            {
                YearList = ProfitList.Select(x => x.yearsLable).ToList(),
                ProfitList = ProfitList.Select(x => x.value).ToList()
            };
            return Json(row, JsonRequestBehavior.AllowGet);
        }
       
    }
}

