﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using UniversityManagementSystem.Models;
using Microsoft.AspNet.Identity.EntityFramework;

namespace UniversityManagementSystem.Controllers
{

    [CustomAuthorize(Roles = "Admin")]
    public class ManageController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        private ApplicationDbContext context = new ApplicationDbContext();
        UniversityManagementSystemEntities UDB = new UniversityManagementSystemEntities();

        public ManageController()
        {
        }

        public ManageController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set 
            { 
                _signInManager = value; 
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        [HttpGet]

        // GET: User List 
        public ActionResult AdminsList()
        {
            ViewBag.MainTitle = "User Managment";
            ViewBag.SubTitle = "Users & Roles";
            var List = UDB.AspNetUsers.Where(u => u.AspNetRoles.Count() > 0).ToList();
            ViewBag.Roles = UDB.AspNetRoles.ToList();
            return View(List);
        }

        [HttpGet]
        public ActionResult NewUser()
        {
            ViewBag.MainTitle = "User Managment";
            ViewBag.SubTitle = "New User";
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> NewUser(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var msg = "";
                var user = new ApplicationUser { UserName = model.UserName, Email = model.Email, PhoneNumber = model.Phone };
                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    msg = "saved Successfully";
                    return RedirectToAction("AdminsList", "Manage", new { Message = msg });
                }
                AddErrors(result);
            }
            // If we got this far, something failed, redisplay form
            return View(model);
        }

        // GET: /Manage/SetPassword
        public ActionResult SetPassword(string UserId)
        {
            if (!string.IsNullOrWhiteSpace(UserId))
            {
                ViewBag.MainTitle = "User Managment";
                ViewBag.SubTitle = "تغير كلمة المرور";
                ViewBag.UserId = UserId;
                return View();
            }
            else
            {
                return RedirectToAction("AdminsList");
            }
        }

        // POST: /Manage/SetPassword
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SetPassword(SetPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {

                await UserManager.RemovePasswordAsync(model.UserId);
                var result = await UserManager.AddPasswordAsync(model.UserId, model.NewPassword);
                if (result.Succeeded)
                {
                    return RedirectToAction("AdminsList", "Manage", new { Message = ManageMessageId.SetPasswordSuccess });
                }
                AddErrors(result);
            }
            // If we got this far, something failed, redisplay form
            return View(model);
        }
        
        // GET: /Manage/Edit User Info
        [HttpGet]
        public ActionResult EditUser(string UserId)
        {
            if (UserId != "")
            {
                ViewBag.MainTitle = "User Managment";
                ViewBag.SubTitle = "Edit admin data";
                var data = UDB.AspNetUsers.Find(UserId);
                UpdateViewModel um = new UpdateViewModel();
                um.UserName = data.UserName;
                um.Email = data.Email;
                um.PhoneNumber = data.PhoneNumber;
                return View(um);
            }
            else
            {
                return RedirectToAction("AdminsList");
            }
        }
        
        // POST: /Manage/Edit User Info
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditUser(UpdateViewModel user)
        {
            string msg = "";
            AspNetUser U = UDB.AspNetUsers.Find(user.UserId);
            U.UserName = user.UserName;
            U.PhoneNumber = user.PhoneNumber;
            U.Email = user.Email;
            UDB.Entry(U).State = System.Data.Entity.EntityState.Modified;
            int don = UDB.SaveChanges();
            if (don > 0)
            {
                msg = "saved Successfully";
                return RedirectToAction("AdminsList", "Manage", new { Message = msg });
            }
            else
            {
                return View();
            }
        }
        
        // : /Manage/Remove User Info
        public ActionResult RemoveUser(string UserId)
        {
            try
            {
                string msg = "";
                AspNetUser u = UDB.AspNetUsers.Find(UserId);
                UDB.AspNetUsers.Remove(u);
                int don = UDB.SaveChanges();
                if (don > 0)
                {
                    msg = "deleted Successfully";
                }
                return RedirectToAction("AdminsList", "Manage", new { Message = msg });
            }
            catch (Exception ex)
            {
                return RedirectToAction("AdminsList", "Manage", new { Message = ex });
            }
        }
        
        // GET: /Manage/Create User Role
        [HttpGet]
        public ActionResult CreateUserRole(string UserName)
        {
            try
            {

                if (!string.IsNullOrWhiteSpace(UserName))
                {
                    ViewBag.MainTitle = "User Managment";
                    ViewBag.SubTitle = "Add role to admin";
                    ViewBag.UserName = UserName;
                    ViewBag.RolesList = context.Roles.OrderBy(r => r.Name).ToList().Select(rr =>
                    new SelectListItem { Value = rr.Name.ToString(), Text = rr.Name }).ToList();

                    ApplicationUser user = context.Users.Where(u => u.UserName.Equals(UserName, StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();
                    var manager = new ApplicationUserManager(new UserStore<ApplicationUser>(context));
                    ViewBag.UserRoles = manager.GetRoles(user.Id);

                    return View();
                }
                else
                {
                    return RedirectToAction("AdminsList");
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("AdminsList", "Manage", new { Message = ex });
            }
        }
        
        // POST: /Manage/Create User Role
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateUserRole(CreateUserRole CRole)
        {
            var msg = "";
            try
            {
                ApplicationUser user = context.Users.Where(u => u.UserName.Equals(CRole.UserName, StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();
                var Manager = new ApplicationUserManager(new UserStore<ApplicationUser>(context));

                if (Manager.IsInRole(user.Id, CRole.RoleName))
                {
                    msg = "Role is Already exists";
                    return RedirectToAction("AdminsList", "Manage", new { Message = msg });
                }
                else
                {
                    Manager.AddToRole(user.Id, CRole.RoleName);
                    msg = " User: " + CRole.UserName + " Have New Role: " + CRole.RoleName;
                    return RedirectToAction("AdminsList", "Manage", new { Message = msg });
                }

            }
            catch (Exception ex)
            {
                return RedirectToAction("AdminsList", "Manage", new { Message = ex });
            }
        }
        
        // : /Manage/Remove User Role
        public ActionResult RemoveUserRole(string UserName, string Role)
        {
            try
            {
                var manager = new ApplicationUserManager(new UserStore<ApplicationUser>(context));
                ApplicationUser user = context.Users.Where(u => u.UserName.Equals(UserName, StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();
                if (manager.IsInRole(user.Id, Role))
                {
                    manager.RemoveFromRole(user.Id, Role);
                    return RedirectToAction("AdminsList", "Manage", new { Message = "Deleted successfully" });
                }
                else
                {
                    return RedirectToAction("AdminsList", "Manage", new { Message = "this user doesn't velong to selected role" });
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("AdminsList", "Manage", new { Message = ex });
            }
        }
        /*---------------------------------------- End Users ----------------------------------------*/

        [HttpGet]
        public ActionResult NewRole()
        {

            ViewBag.MainTitle = "User Managment";
            ViewBag.SubTitle = "New Role";
            return View();

        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult NewRole(FormCollection coll)
        {
            try
            {
                context.Roles.Add(new IdentityRole()
                {
                    Name = coll["Name"]
                });
                int don = context.SaveChanges();
                if (don > 0)
                {
                    ViewBag.MainTitle = "User Managment";
                    ViewBag.SubTitle = "New Role";
                    ViewBag.RoleMessage = "Saved Successfully";
                    return View();
                }
                else
                {
                    ViewBag.MainTitle = "User Managment";
                    ViewBag.SubTitle = "New Role";
                    return View();
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("AdminsList", "Manage", new { Message = ex });
            }
        }
        [HttpGet]
        public ActionResult EditRole(string RoleId)
        {
            if (RoleId != null)
            {
                ViewBag.MainTitle = "User Managment";
                ViewBag.SubTitle = "Edit Role";

                var GetData = context.Roles.Find(RoleId);
                return View(GetData);
            }
            else
            {
                return RedirectToAction("AdminsList");
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditRole(IdentityRole Role)
        {
            try
            {
                context.Entry(Role).State = System.Data.Entity.EntityState.Modified;
                int don = context.SaveChanges();
                if (don > 0)
                {
                    ViewBag.MainTitle = "User Managment";
                    ViewBag.SubTitle = "New Role";
                    ViewBag.RoleMessage = "Saved Successfully";
                    return View();
                }
                else
                {
                    ViewBag.MainTitle = "User Managment";
                    ViewBag.SubTitle = "New Role";
                    return View();
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("AdminsList", "Manage", new { Message = ex });
            }
        }
        public ActionResult RemoveRole(string RoleId)
        {
            var msg = "";
            try
            {
                if (RoleId != null)
                {
                    AspNetRole r = UDB.AspNetRoles.Find(RoleId);
                    UDB.AspNetRoles.Remove(r);
                    UDB.SaveChanges();
                    msg = "Removed Successfully";
                    return RedirectToAction("AdminsList", new { Message = msg });
                }
                else
                {
                    return RedirectToAction("AdminsList");
                }
            }
            catch (Exception ex)
            {
                msg = "Thay Error" + ex;
                return RedirectToAction("", new { Message = msg });
            }
        }
        /*---------------------------------------- End Rolse ----------------------------------------*/
        //
        // GET: /Manage/Index
        public async Task<ActionResult> Index(ManageMessageId? message)
        {
            ViewBag.StatusMessage =
                message == ManageMessageId.ChangePasswordSuccess ? "Your password has been changed."
                : message == ManageMessageId.SetPasswordSuccess ? "Your password has been set."
                : message == ManageMessageId.SetTwoFactorSuccess ? "Your two-factor authentication provider has been set."
                : message == ManageMessageId.Error ? "An error has occurred."
                : message == ManageMessageId.AddPhoneSuccess ? "Your phone number was added."
                : message == ManageMessageId.RemovePhoneSuccess ? "Your phone number was removed."
                : "";

            var userId = User.Identity.GetUserId();
            var model = new IndexViewModel
            {
                HasPassword = HasPassword(),
                PhoneNumber = await UserManager.GetPhoneNumberAsync(userId),
                TwoFactor = await UserManager.GetTwoFactorEnabledAsync(userId),
                Logins = await UserManager.GetLoginsAsync(userId),
                BrowserRemembered = await AuthenticationManager.TwoFactorBrowserRememberedAsync(userId)
            };
            return View(model);
        }

        //
        // POST: /Manage/RemoveLogin
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> RemoveLogin(string loginProvider, string providerKey)
        {
            ManageMessageId? message;
            var result = await UserManager.RemoveLoginAsync(User.Identity.GetUserId(), new UserLoginInfo(loginProvider, providerKey));
            if (result.Succeeded)
            {
                var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
                if (user != null)
                {
                    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                }
                message = ManageMessageId.RemoveLoginSuccess;
            }
            else
            {
                message = ManageMessageId.Error;
            }
            return RedirectToAction("ManageLogins", new { Message = message });
        }

        //
        // GET: /Manage/AddPhoneNumber
        public ActionResult AddPhoneNumber()
        {
            return View();
        }

        //
        // POST: /Manage/AddPhoneNumber
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddPhoneNumber(AddPhoneNumberViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            // Generate the token and send it
            var code = await UserManager.GenerateChangePhoneNumberTokenAsync(User.Identity.GetUserId(), model.Number);
            if (UserManager.SmsService != null)
            {
                var message = new IdentityMessage
                {
                    Destination = model.Number,
                    Body = "Your security code is: " + code
                };
                await UserManager.SmsService.SendAsync(message);
            }
            return RedirectToAction("VerifyPhoneNumber", new { PhoneNumber = model.Number });
        }

        //
        // POST: /Manage/EnableTwoFactorAuthentication
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EnableTwoFactorAuthentication()
        {
            await UserManager.SetTwoFactorEnabledAsync(User.Identity.GetUserId(), true);
            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (user != null)
            {
                await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
            }
            return RedirectToAction("Index", "Manage");
        }

        //
        // POST: /Manage/DisableTwoFactorAuthentication
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DisableTwoFactorAuthentication()
        {
            await UserManager.SetTwoFactorEnabledAsync(User.Identity.GetUserId(), false);
            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (user != null)
            {
                await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
            }
            return RedirectToAction("Index", "Manage");
        }

        //
        // GET: /Manage/VerifyPhoneNumber
        public async Task<ActionResult> VerifyPhoneNumber(string phoneNumber)
        {
            var code = await UserManager.GenerateChangePhoneNumberTokenAsync(User.Identity.GetUserId(), phoneNumber);
            // Send an SMS through the SMS provider to verify the phone number
            return phoneNumber == null ? View("Error") : View(new VerifyPhoneNumberViewModel { PhoneNumber = phoneNumber });
        }

        //
        // POST: /Manage/VerifyPhoneNumber
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyPhoneNumber(VerifyPhoneNumberViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var result = await UserManager.ChangePhoneNumberAsync(User.Identity.GetUserId(), model.PhoneNumber, model.Code);
            if (result.Succeeded)
            {
                var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
                if (user != null)
                {
                    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                }
                return RedirectToAction("Index", new { Message = ManageMessageId.AddPhoneSuccess });
            }
            // If we got this far, something failed, redisplay form
            ModelState.AddModelError("", "Failed to verify phone");
            return View(model);
        }

        //
        // POST: /Manage/RemovePhoneNumber
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> RemovePhoneNumber()
        {
            var result = await UserManager.SetPhoneNumberAsync(User.Identity.GetUserId(), null);
            if (!result.Succeeded)
            {
                return RedirectToAction("Index", new { Message = ManageMessageId.Error });
            }
            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (user != null)
            {
                await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
            }
            return RedirectToAction("Index", new { Message = ManageMessageId.RemovePhoneSuccess });
        }

        //
        // GET: /Manage/ChangePassword
        public ActionResult ChangePassword()
        {
            return View();
        }

        //
        // POST: /Manage/ChangePassword
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword, model.NewPassword);
            if (result.Succeeded)
            {
                var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
                if (user != null)
                {
                    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                }
                return RedirectToAction("Index", new { Message = ManageMessageId.ChangePasswordSuccess });
            }
            AddErrors(result);
            return View(model);
        }

        //
        // GET: /Manage/ManageLogins
        public async Task<ActionResult> ManageLogins(ManageMessageId? message)
        {
            ViewBag.StatusMessage =
                message == ManageMessageId.RemoveLoginSuccess ? "The external login was removed."
                : message == ManageMessageId.Error ? "An error has occurred."
                : "";
            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (user == null)
            {
                return View("Error");
            }
            var userLogins = await UserManager.GetLoginsAsync(User.Identity.GetUserId());
            var otherLogins = AuthenticationManager.GetExternalAuthenticationTypes().Where(auth => userLogins.All(ul => auth.AuthenticationType != ul.LoginProvider)).ToList();
            ViewBag.ShowRemoveButton = user.PasswordHash != null || userLogins.Count > 1;
            return View(new ManageLoginsViewModel
            {
                CurrentLogins = userLogins,
                OtherLogins = otherLogins
            });
        }

        //
        // POST: /Manage/LinkLogin
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LinkLogin(string provider)
        {
            // Request a redirect to the external login provider to link a login for the current user
            return new AccountController.ChallengeResult(provider, Url.Action("LinkLoginCallback", "Manage"), User.Identity.GetUserId());
        }

        //
        // GET: /Manage/LinkLoginCallback
        public async Task<ActionResult> LinkLoginCallback()
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync(XsrfKey, User.Identity.GetUserId());
            if (loginInfo == null)
            {
                return RedirectToAction("ManageLogins", new { Message = ManageMessageId.Error });
            }
            var result = await UserManager.AddLoginAsync(User.Identity.GetUserId(), loginInfo.Login);
            return result.Succeeded ? RedirectToAction("ManageLogins") : RedirectToAction("ManageLogins", new { Message = ManageMessageId.Error });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && _userManager != null)
            {
                _userManager.Dispose();
                _userManager = null;
            }

            base.Dispose(disposing);
        }

#region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private bool HasPassword()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            if (user != null)
            {
                return user.PasswordHash != null;
            }
            return false;
        }

        private bool HasPhoneNumber()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            if (user != null)
            {
                return user.PhoneNumber != null;
            }
            return false;
        }

        public enum ManageMessageId
        {
            AddPhoneSuccess,
            ChangePasswordSuccess,
            SetTwoFactorSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
            RemovePhoneSuccess,
            Error
        }

#endregion
    }
}