﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UniversityManagementSystem.Models;

[assembly: OwinStartupAttribute(typeof(UniversityManagementSystem.Startup))]
namespace UniversityManagementSystem
{
    public partial class Startup
    {
        ApplicationDbContext db = new ApplicationDbContext();
        UniversityManagementSystemEntities MDB = new UniversityManagementSystemEntities();
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            createRolesandUsers();
            AdminAccount();
        }
        public void AdminAccount()
        {
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));
            var UserAdmin = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
            var User = new ApplicationUser();
            User.Email = "darfuresco@gmail.com";
            User.UserName = "Admin";
            var check = UserAdmin.Create(User, "Admin@123");
            if (check.Succeeded)
            {
                if (roleManager.RoleExists("Admin"))
                {
                    var result1 = UserAdmin.AddToRole(User.Id, "Admin");

                }

                int Year = DateTime.Now.Year;

                R_StudyYear_data Yeardat = MDB.R_StudyYear_data.SingleOrDefault(u => u.Year_Number == Year);
                if (Yeardat == null)
                {
                    R_StudyYear_data SYD = new R_StudyYear_data()
                    {
                        Year_Number = Year,
                        Year_Name = Convert.ToString(Year) + "-" + Convert.ToString(Year + 1)
                    };
                    MDB.R_StudyYear_data.Add(SYD);
                    MDB.SaveChanges();

                }
                Teaches();
            }
        }
        private void createRolesandUsers()
        {
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));
            if (!roleManager.RoleExists("accounting"))
            {
                var role = new IdentityRole();
                role.Id = ID();
                role.Name = "accounting";
                roleManager.Create(role);
            }
            if (!roleManager.RoleExists("Admin"))
            {
                var role = new IdentityRole();
                role.Id = ID();
                role.Name = "Admin";
                roleManager.Create(role);
            }
            if (!roleManager.RoleExists("Gate User"))
            {
                var role = new IdentityRole();
                role.Id = ID();
                role.Name = "Gate User";
                roleManager.Create(role);
            }
        }

        private void Teaches()
        {
            List<string> T = new List<string>();
            T.Add("Co-professor");
            T.Add("Professor");
            T.Add("Assistant Professor");
            T.Add("Lecturer");
            T.Add("Teaching Assistant");
            T.Add("Collaborator");
            foreach (var item in T)
            {
                Designation De = new Designation
                {
                    Title = item
                };
                MDB.Designations.Add(De);
                MDB.SaveChanges();
            }
        }
        public string ID()
        {
            StringBuilder builder = new StringBuilder();
            Enumerable
               .Range(65, 26)
                .Select(e => ((char)e).ToString())
                .Concat(Enumerable.Range(97, 26).Select(e => ((char)e).ToString()))
                .Concat(Enumerable.Range(0, 10).Select(e => e.ToString()))
                .OrderBy(e => Guid.NewGuid())
                .Take(11)
                .ToList().ForEach(e => builder.Append(e));
            string id = builder.ToString();
            return id;
        }
    }
}
