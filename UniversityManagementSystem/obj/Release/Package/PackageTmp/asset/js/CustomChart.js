﻿(function (jQuery) {
    // @*[Chart]Student Result Academic Status*@
    function GetResultStdData()
    {
        var doughnutData = [];
        var SumValue;
        $.ajax({
            type: "GET",
            url: "/Home/GetResultStdData",
            dataType: "",
            success: function (data) {
                $.each(data, function (index, val) {
                    doughnutData.push({
                        value: val.value,
                        color: "#129352",
                        highlight: "#15BA67",
                        label: val.label
                    });
                    SumValue = SumValue + val.value;
                });
                var ctx = $(".doughnut-chart")[0].getContext("2d");
                window.myDoughnut = new Chart(ctx).Doughnut(doughnutData, {
                    //responsive: true,
                    showTooltips: true,
                    responsive: true,
                    title: { display: true, text: 'studant data' },
                    legend: { position: 'bottom' }
                });
                $("#StdTotal").val(SumValue);
            },
            error: function (data) {

            }
        });
    };

    // @*[Chart]Income And Expnses*@
    function GetIncomeAndExpnses() {
       
        $.ajax({
            type: "GET",
            url: "/Home/GetIncomeAndExpnses",
            dataType: "",
            success: function (data) {
               var barChartData = {
                   labels: data.YearList,
                    datasets: [{
                        label: "Income data",
                        fillColor: "rgba(21,186,103,0.4)",
                        strokeColor: "rgba(220,220,220,0.8)",
                        highlightFill: "rgba(21,186,103,0.2)",
                        highlightStroke: "rgba(21,186,103,0.2)",
                        data: data.IncomeList
                    },
                    {
                        label: "Expnses data",
                        fillColor: "rgba(21,113,186,0.5)",
                        strokeColor: "rgba(151,187,205,0.8)",
                        highlightFill: "rgba(21,113,186,0.2)",
                        highlightStroke: "rgba(21,113,186,0.2)",
                        data: data.ExpnsesList
                    }]
                };

                var ctx3 = $(".bar-chart")[0].getContext("2d");
                window.myLine = new Chart(ctx3).Bar(barChartData, {
                    responsive: true,
                    showTooltips: true,
                    title: { display: true, text: 'Income And Expnses' },
                    legend: { position: 'bottom' }
                });
            },
            error: function (data) {

            }
        });
    }

    // @*[Chart]Profit*@
    function GetProfit() {

        $.ajax({
            type: "GET",
            url: "/Home/GetProfit",
            dataType: "",
            success: function (data) {
                var lineChartData = {
                    labels: data.YearList,
                    datasets: [{
                        label: "Profit data",
                        fillColor: "rgba(21,186,103,0.4)",
                        strokeColor: "rgba(220,220,220,1)",
                        pointColor: "rgba(66,69,67,0.3)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(220,220,220,1)",
                        data: data.ProfitList
                    }]
                };

                var ctx2 = $(".line-chart")[0].getContext("2d");
                window.myLine = new Chart(ctx2).Line(lineChartData, {
                    responsive: true,
                    showTooltips: true,
                    multiTooltipTemplate: "<%= value %>",
                    maintainAspectRatio: false
                });
            },
            error: function (data) {

            }
        });
    }
    Chart.defaults.global.pointHitDetectionRadius = 1;
    Chart.defaults.global.customTooltips = function (tooltip) {
        var tooltipEl = $('#chartjs-tooltip');
        if (!tooltip) {
            tooltipEl.css({
                opacity: 0
            });
            return;
        }

        tooltipEl.removeClass('above below');
        tooltipEl.addClass(tooltip.yAlign);

        var innerHtml = '';
        if (undefined !== tooltip.labels && tooltip.labels.length) {
            for (var i = tooltip.labels.length - 1; i >= 0; i--) {
                innerHtml += [
                    '<div class="chartjs-tooltip-section">',
                    '   <span class="chartjs-tooltip-key" style="background-color:' + tooltip.legendColors[i].fill + '"></span>',
                    '   <span class="chartjs-tooltip-value">' + tooltip.labels[i] + '</span>',
                    '</div>'
                ].join('');
            }
            tooltipEl.html(innerHtml);
        }

        tooltipEl.css({
            opacity: 1,
            left: tooltip.chart.canvas.offsetLeft + tooltip.x + 'px',
            top: tooltip.chart.canvas.offsetTop + tooltip.y + 'px',
            fontFamily: tooltip.fontFamily,
            fontSize: tooltip.fontSize,
            fontStyle: tooltip.fontStyle
        });
    };
    var randomScalingFactor = function () {
        return Math.round(Math.random() * 100);
    };

    

    window.onload = function () {
        GetResultStdData();
        GetIncomeAndExpnses();
        GetProfit();
    };
    //  end:  Chart =============
})(jQuery);