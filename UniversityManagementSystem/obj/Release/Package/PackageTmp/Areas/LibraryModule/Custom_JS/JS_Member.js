﻿jQuery(function ($) {
    // add new About
    $("#add").click(function () {
        $(".modal-title").text("Add New Book")
        $("#form_data")[0].reset();
    });

    //Update About
    $(document).on('click', '.update', function () {
        var id = $(this).attr("id");

        var MemberName = $('#' + id).children('td[data-target=MemberName]').text();
        var MemberPhone = $('#' + id).children('td[data-target=MemberPhone]').text();
        var MemberEmail = $('#' + id).children('td[data-target=MemberEmail]').text();

        $("#MemberId").val(id);
        $("#MemberName").val(MemberName);
        $("#MemberPhone").val(MemberPhone);
        $("#MemberEmail").val(MemberEmail);

        $(".modal-title").text("Edit Current Data");
    });

    // new Player And Update current selecte Player
    $("#form_data").submit(function (event) {
        event.preventDefault();
        $('#form_data').validate({
            rules: {
                MemberName: {
                    required: true
                }, MemberPhone: {
                    required: true,
                    number: true
                }, MemberEmail: {
                    required: true,
                    email: true
                }
            },
            messages: {
                MemberName: "book name is Required",
                MemberPhone:{
                    required: "book Phone is Required",
                    number: "number"
                },
                MemberEmail: {
                    required: "book Email is Required",
                    email:"not like email"
                }
            },
            errorPlacement: function (error, element) { error.addClass("help-block"); element.parents(".form-group").addClass("has-feedback"); error.insertAfter(element); },
            highlight: function (element) { $(element).parents(".form-group").addClass("has-error").removeClass("has-success"); },
            unhighlight: function (element, errorClass, validClass) { $(element).parents(".form-group").addClass("has-success").removeClass("has-error"); },
            submitHandler: function (form) {
                $("#Loding").show();
                var data = new FormData(form);
                $.ajax({
                    type: 'POST',
                    url: '/LibraryModule/Member/MemberAction',
                    data: data,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        if (data === "The operation was successful") {
                            $("#form_data")[0].reset();
                            $("#Modal_View").modal('hide');
                            $("#Loding").hide();
                            alert(data);
                            location.reload();
                        }
                        else {
                            $("#Loding").hide();
                            alert(data);
                        }
                    },
                    error: function (data) {
                        alert(data);
                    }
                });
            }
        });
    });

    $(document).on('click', '.delete', function () {
        var id = $(this).attr("id");
        if (confirm("Do you Delete the record?")) {
            $.ajax({
                type: 'POST',
                url: '/LibraryModule/Member/Delete',
                data: { Id: id },
                success: function (data) {
                    if (data == "The operation was successful") {
                        $("#form_data")[0].reset();
                        alert(data);
                        location.reload();
                    }
                    else {
                        alert(data);
                    }
                },
                error: function (data) {
                    alert(data);
                }
            });
        }
        else {
            return false;
        }
    });

    //initiate dataTables plugin
    var myTable =
        $('#Member_datatables')
            .DataTable({
                bAutoWidth: false,
                "aaSorting": []
            });
});