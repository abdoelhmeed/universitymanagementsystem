﻿jQuery(function ($) {
    // add new About
    $("#add").click(function () {
        $(".modal-title").text("Add New Book")
        $("#form_data")[0].reset();
    });

    //Update About
    $(document).on('click', '.update', function () {
        var id = $(this).attr("id");

        var BookNo = $('#' + id).children('td[data-target=BookNo]').text();
        var BookName = $('#' + id).children('td[data-target=BookName]').text();
        var BookAuthor = $('#' + id).children('td[data-target=BookAuthor]').text();
        var BookPublisher = $('#' + id).children('td[data-target=BookPublisher]').text();
        var BookNumberOfCopy = $('#' + id).children('td[data-target=BookNumberOfCopy]').text();

        $("#BookNo").val(id);
        $("#BookName").val(BookName);
        $("#BookAuthor").val(BookAuthor);
        $("#BookPublisher").val(BookPublisher);
        $("#BookNumberOfCopy").val(BookNumberOfCopy);

        $(".modal-title").text("Edit Current Data");
    });

    // new Player And Update current selecte Player
    $("#form_data").submit(function (event) {
        event.preventDefault();
        $('#form_data').validate({
            rules: {
                BookName: {
                    required: true
                }, BookAuthor: {
                    required: true
                }, BookPublisher: {
                    required: true
                }, BookNumberOfCopy: {
                    required: true
                }
            },
            messages: {
                BookName: "book name is Required",
                BookAuthor: "book Author is Required",
                BookPublisher: "book Publisher is Required",
                BookNumberOfCopy: "Number of Copy is Required",
            },
            errorPlacement: function (error, element) { error.addClass("help-block"); element.parents(".form-group").addClass("has-feedback"); error.insertAfter(element); },
            highlight: function (element) { $(element).parents(".form-group").addClass("has-error").removeClass("has-success"); },
            unhighlight: function (element, errorClass, validClass) { $(element).parents(".form-group").addClass("has-success").removeClass("has-error"); },
            submitHandler: function (form) {
                $("#Loding").show();
                var data = new FormData(form);
                $.ajax({
                    type: 'POST',
                    url: '/LibraryModule/Book/BookAction',
                    data: data,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        if (data === "The operation was successful") {
                            $("#form_data")[0].reset();
                            $("#Modal_View").modal('hide');
                            $("#Loding").hide();
                            alert(data);
                            location.reload();
                        }
                        else {
                            $("#Loding").hide();
                            alert(data);
                        }
                    },
                    error: function (data) {
                        alert(data);
                    }
                });
            }
        });
    });

    $(document).on('click', '.delete', function () {
        var id = $(this).attr("id");
        if (confirm("Do you Delete the record?")) {
            $.ajax({
                type: 'POST',
                url: '/LibraryModule/Book/Delete',
                data: { Id: id },
                success: function (data) {
                    if (data == "The operation was successful") {
                        $("#form_data")[0].reset();
                        alert(data);
                        location.reload();
                    }
                    else {
                        alert(data);
                    }
                },
                error: function (data) {
                    alert(data);
                }
            });
        }
        else {
            return false;
        }
    });

    //initiate dataTables plugin
    var myTable =
        $('#Book_datatables')
            .DataTable({
                bAutoWidth: false,
                "aaSorting": []
            });
});