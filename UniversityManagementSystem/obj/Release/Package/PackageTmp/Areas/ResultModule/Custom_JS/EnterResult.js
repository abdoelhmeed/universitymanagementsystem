﻿
$(document).ready(function (e) {
    //Add Student degree
    $(document).on('change', '.StdDegree', function () {
        var id = $(this).attr("id");
        var value = $(this).val();
        if (!isNaN(value)) {
            $('#' + id).children('td[data-target=Degree]').text(value);
            if ($('#' + id).children('td[data-target=CStatus]').text() == "") {
                $('#' + id).children('td[data-target=CStatus]').text("Present");
            }
        }
        else {
            alert("enter Numeric number");
            $('#' + id).children('td[data-target=Degree]').text("0");
        }
    });
    //Add Student degree
    $(document).on('change', '.CourseStatus', function () {
        var id = $(this).attr("id");
        var value = $(this).val();
        $('#' + id).children('td[data-target=CStatus]').text(value);

    });

    //save result degree of Student
    $("#EnterResultBtn").click(function () {
        $("#Loding").show();
        debugger
        var CountEmpty = 0;
        var SubjectArr = [];
        SubjectArr.length = 0;

        $.each($("#Result_td tbody tr"), function () {
            if ($(this).find('td:eq(3)').html() == "" || $(this).find('td:eq(4)').html() == "") {
                CountEmpty = CountEmpty + 1;
            }
            else {

                SubjectArr.push({
                    StdId: $(this).find('td:eq(0)').html(),
                    SR_Id: $(this).find('td:eq(1)').html(),
                    StdDegree: $(this).find('td:eq(3)').html(),
                    CourseStatus: $(this).find('td:eq(4)').html()
                });
            }
        });

        if (CountEmpty >= 1) {
            alert("Be sure to include the class and status of each student");
        }
        else {

            var data = {
                Year_Id: $("#Year_Id").val(),
                Department_Id: $("#Department_Id").val(),
                Semester_Id: $("#Semester_Id").val(),
                batch_Id: $("#batch_Id").val(),
                Course_Id: $("#Course_Id").val(),
                StdList: SubjectArr
            };

            $.ajax({
                type: "POST",
                url: "/ResultModule/Result/InsertResultData",
                data: data,
                success: function (response) {
                    $("#Loding").hide();
                    $('#Modal_View').modal('hide');
                    alert(response);
                }
            });
        }
    });

    //
    $("#CalculationResultBtn").click(function () {
        $("#Loding").show();
        debugger
        var SubjectArr = [];
        SubjectArr.length = 0;
        $.each($("#Result_td tbody tr"), function () {
            SubjectArr.push({
                SR_Id: $(this).find('td:eq(0)').html(),
                StdId: $(this).find('td:eq(1)').html(),
            });
        });
        var data = {
            Year_Id: $("#Year_Id").val(),
            Department_Id: $("#Department_Id").val(),
            Semester_Id: $("#Semester_Id").val(),
            batch_Id: $("#batch_Id").val(),
            Course_Id: $("#Course_Id").val(),
            StdList: SubjectArr
        };

        $.ajax({
            type: "POST",
            url: "/ResultModule/Result/CalculationSemesterResult",
            data: data,
            success: function (response) {
                $("#Loding").hide();
                $('#Modal_View').modal('hide');
                alert(response);
            }
        })

    })

    //SupplementsAndAlternativesResult
    $("#CalculationSupplementsAndAlternativesResultBtn").click(function () {
        $("#Loding").show();
        debugger
        var SubjectArr = [];
        SubjectArr.length = 0;

        $.each($("#Result_td tbody tr"), function () {
            SubjectArr.push({
                StdId: $(this).find('td:eq(0)').html(),
                SR_Id: $(this).find('td:eq(1)').html(),
                StdDegree: $(this).find('td:eq(3)').html(),
                CourseStatus: $(this).find('td:eq(4)').html(),
            });
        });

        var data = {
            Year_Id: $("#Year_Id").val(),
            Department_Id: $("#Department_Id").val(),
            Semester_Id: $("#Semester_Id").val(),
            batch_Id: $("#batch_Id").val(),
            Course_Id: $("#Course_Id").val(),
            StdList: SubjectArr
        };
        $.ajax({
            type: "POST",
            url: "/ResultModule/SupplementsAndAlternativesResult/InsertSupplementsAndAlternativesData",
            data: data,
            success: function (response) {
                $("#Loding").hide();
                $('#Modal_View').modal('hide');
                alert(response);
                location.reload();
            },
            error: function (response) {
                $("#Loding").hide();
                $('#Modal_View').modal('hide');
                alert(response);
            }
        })
    });

    //Semester drop down
    $(document).on('change', '#StdNumber', function () {
        var id = $(this).val();

        $.ajax({
            url: "/ResultModule/ResultReport/SemestersList",
            type: "GET",
            data: { Id: id },
            success: function (data) {
                $('#SemesterId').html("");
                var _options = ""
                _options += ('<option value=" ">-- select option --</option>');
                $.each(data, function (i, value) {
                    _options += ('<option value="' + value.Semester_Id + '">' + value.Semester_Id + " | " + value.Semester_Name + '</option>');
                });
                $('#SemesterId').append(_options);
            },
            error: function (data) {
                alert(data);
            }
        });

    });

    //batch drop down
    $(document).on('change', '#Department_Id', function () {
        var id = $(this).val();
        $.ajax({
            url: "/AcademicAffairsModule/AcademicSitting/BatchByDeptId",
            type: "GET",
            data: { Id: id },
            success: function (data) {
                $('#batch_Id').html("");
                var _options = ""
                _options += ('<option value=" ">-- select batch number --</option>');
                $.each(data, function (i, value) {
                    _options += ('<option value="' + value.batch_Id + '">' + value.batch_Name + '</option>');
                });
                $('#batch_Id').append(_options);
            },
            error: function (data) {
                alert(data);
            }
        });
    });

    //Course  drop down
    $(document).on('change', '#Semester_Id', function () {
        var sid = $(this).val();
        var did = $("#Department_Id").val();
        $.ajax({
            url: "/ResultModule/Result/GetCourses",
            type: "GET",
            data: { SId: sid, DId: did},
            success: function (data) {
                $('#Course_Id').html("");
                var _options = ""
                _options += ('<option value=" ">-- select Course number --</option>');
                $.each(data, function (i, value) {
                    _options += ('<option value="' + value.Course_Id + '">' + value.Course_Name + '</option>');
                });
                $('#Course_Id').append(_options);
            },
            error: function (data) {
                alert(data);
            }
        });
    });

    //#Get All Result of Studant ,Get Semester Studant Result
    $(document).on('click', '#Get_AllStudantResult ,#Get_SemesterStudantResult ,#BoardResultBtn ,#BoardResultBtn', function () {
        $("#Loding").show();
        setTimeout(function () { $("#Loding").hide(); }, 8000);
    });

    //
    function IsNumeric(input) {
        var RE = /^-{0,1}\d*\.{0,1}\d+$/;
        return (RE.test(input));
    }
});
