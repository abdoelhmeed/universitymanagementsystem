﻿jQuery(function ($) {


    // new Player And Update current selecte Player
    $("#form_data").submit(function (event) {
        event.preventDefault();
        $('#form_data').validate({
            rules: {
                IdProductName: {
                    required: true
                }, IdSupplier: {
                    required: true
                }, IdQuantity: {
                    required: true,
                    number: true
                },
                IdUnitPrice: {
                    required: true,
                    number: true
                },
                IdNumber: {
                    required: true,
                    number: true
                }
            },
            messages: {
                IdProductName: "product Name is Required",
                IdSupplier: "Supplier Details is Required",
                IdQuantity: "Quantity is Required and Type Number",
                IdUnitPrice: "Unit Price is Required  and Type Number",
                IdNumber: "IdNumber is Required  and Type Number",

            },
            errorPlacement: function (error, element) { error.addClass("help-block"); element.parents(".form-group").addClass("has-feedback"); error.insertAfter(element); },
            highlight: function (element) { $(element).parents(".form-group").addClass("has-error").removeClass("has-success"); },
            unhighlight: function (element, errorClass, validClass) { $(element).parents(".form-group").addClass("has-success").removeClass("has-error"); },
            submitHandler: function (form) {
                $("#Loding").show();
                var data = new FormData(form);
                $.ajax({
                    type: 'POST',
                    url: '/FinancialAccountsModule/Invoices/newInvoices',
                    data: data,
                    contentType: false,
                    processData: false,
                    success: function (mss) {
                        location.href = "/FinancialAccountsModule/Invoices/AddInvoices?orID=" + mss;
                    },
                    error: function (data) {
                        alert("Sorry, try again");
                    }
                });
            }
        });
    });


});

