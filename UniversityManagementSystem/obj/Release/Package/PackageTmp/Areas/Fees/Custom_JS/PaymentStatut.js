﻿jQuery(function ($) {
    // add new About
    $("#add").click(function () {
        $(".modal-title").text("Add New Book")
        $("#form_data")[0].reset();
    });

    //Update About
    $(document).on('click', '.update', function () {
        var id = $(this).attr("id");

        var Payment_Id = $.trim( $('#' + id).children('td[data-target=Payment_Id]').text());
        var Payment_Name = $.trim($('#' + id).children('td[data-target=Payment_Name]').text());
        var Payment_Value = $.trim( $('#' + id).children('td[data-target=Payment_Value]').text());
        var Payment_percent = $.trim($('#' + id).children('td[data-target=Payment_percent]').text());
        

        $("#Payment_Id").val(id);
        $("#Payment_Name").val(Payment_Name);
        $("#Payment_Value").val(Payment_Value);
        $("#Payment_percent").val(Payment_percent);
        

        $(".modal-title").text("Edit Current Data");
    });

    // new Player And Update current selecte Player
    $("#form_data").submit(function (event) {
        event.preventDefault();
        $('#form_data').validate({
            rules: {
                Payment_Name: {
                    required: true
                }, Payment_Value: {
                    required: true
                }, Payment_percent: {
                    required: true
                }
            },
            messages: {
                Payment_Name: "Payment Name is Required",
                Payment_Value: "Payment Value is Required",
                Payment_percent: "Payment percent is Required",
               
            },
            errorPlacement: function (error, element) { error.addClass("help-block"); element.parents(".form-group").addClass("has-feedback"); error.insertAfter(element); },
            highlight: function (element) { $(element).parents(".form-group").addClass("has-error").removeClass("has-success"); },
            unhighlight: function (element, errorClass, validClass) { $(element).parents(".form-group").addClass("has-success").removeClass("has-error"); },
            submitHandler: function (form) {
                $("#Loding").show();
                var data = new FormData(form);
                $.ajax({
                    type: 'POST',
                    url: '/Fees/PaymentStatus/Create',
                    data: data,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        if (data === "Special Case is Saved" || data === "Special Case is Updated ") {
                            $("#form_data")[0].reset();
                            $("#Modal_View").modal('hide');
                            $("#Loding").hide();
                            alert(data);
                            window.location.href = "/Fees/PaymentStatus/index";
                        }
                        else {
                            $("#Loding").hide();
                            alert(data);
                        }
                    },
                    error: function (data) {
                        alert(data);
                    }
                });
            }
        });
    });

    $(document).on('click', '.delete', function () {
        var id = $(this).attr("id");
        if (confirm("Do you Delete the record?")) {
            $.ajax({
                type: 'POST',
                url: '/Fees/PaymentStatus/Delete',
                data: { Id: id },
                success: function (data) {
                    if (data === "Deleted") {
                        $("#form_data")[0].reset();
                        alert(data);
                        window.location.href = "/Fees/PaymentStatus/index";
                    }
                    else {
                        alert(data);
                    }
                },
                error: function (data) {
                    alert(data);
                }
            });
        }
        else {
            return false;
        }
    });

    //initiate dataTables plugin
    var myTable =
        $('#Book_datatables')
            .DataTable({
                bAutoWidth: false,
                "aaSorting": []
            });
});