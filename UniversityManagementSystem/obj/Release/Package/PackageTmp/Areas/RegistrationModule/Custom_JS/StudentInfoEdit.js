﻿jQuery(function ($) {
    // new Player And Update current selecte Player
    $("#MyForm").submit(function (event) {
        event.preventDefault();
        $('#MyForm').validate({
            rules: {
                Student_Number: {
                    required: true
                },
                Student_FullName: {
                    required: true
                }, Student_Nationality: {
                    required: true
                }, Student_PassportOrNationalNo: {
                    required: true
                }
                , Student_Sex: {
                    required: true
                }, Student_PlaceOfBirth: {
                    required: true
                }, Student_NativeLanguage: {
                    required: true
                }, Student_PresentAddress: {
                    required: true
                }, Student_Tel: {
                    required: true,
                    number: true,
                    minlength: 9,
                    maxlength: 9
                }, Student_Email: {
                    email: true,
                    required: true
                }, Student_PermanantAddress: {
                    required: true
                }, Student_EmerInSName: {
                    required: true
                }, Student_EmerInSEmail: {
                    email: true,
                    required: true
                }, Student_EmerInSTel: {
                    required: true,
                    number: true,
                    minlength: 9,
                    maxlength: 9
                }, Student_Nameofcertificate: {
                    required: true
                }, Student_CertificateDate: {
                    required: true
                }, Student_GeneralPercentage: {
                    required: true
                    , number: true
                }

                , Student_College: {
                    required: true
                }

                , Department: {
                    required: true
                }
            },
            messages: {
                Student_Number: "Student Number is Required",
                Student_FullName: "Student FullName is Required",
                Student_PassportOrNationalNo: "Passport Or NationalNo Publisher is Required",
                Student_Nationality: "Student Nationality is Required",
                Student_Sex: "Student Gender is Required",
                Student_PlaceOfBirth: "Student Place Of Birth is Required",
                Student_NativeLanguage: "Student Native Language is Required",
                Student_PresentAddress: "Student Native Language is Required",
                Student_Tel: "Student Tel is Required,Type is number, length  9",
                Student_Email: "Student Email is Required, Type is Email",
                Student_PermanantAddress: "Permanant Address is Required",
                Student_EmerInSName: "Name is Required",
                Student_EmerInSEmail: "Email Required ,Type is Email",
                Student_EmerInSTel: "Tel is Required ,Type is number, length  9",
                Student_Nameofcertificate: "Name Certificate is Required",
                Student_CertificateDate: "Certificate Date is Required ,Type is date",
                Student_GeneralPercentage: "General Percentage Required",
                Student_College: "College is Required",
                Department: "Department is Required",
            },
            errorPlacement: function (error, element) { error.addClass("help-block"); element.parents(".form-group").addClass("has-feedback"); error.insertAfter(element); },
            highlight: function (element) { $(element).parents(".form-group").addClass("has-error").removeClass("has-success"); },
            unhighlight: function (element, errorClass, validClass) { $(element).parents(".form-group").addClass("has-success").removeClass("has-error"); },
            submitHandler: function (form) {
                $("#Loding").show();
                var data = new FormData(form);
                $.ajax({
                    type: 'POST',
                    url: '/RegistrationModule/StudentInfo/Edit',
                    data: data,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        if (data === " Student Edit") {
                            $("#MyForm")[0].reset();
                            alert(data);
                            window.location.href = "/RegistrationModule/StudentInfo/index";
                        }
                        else {
                            $("#Loding").hide();
                            alert(data);
                        }
                    },
                    error: function (data) {
                        alert(data);
                    }
                });
            }
        });
    });
});