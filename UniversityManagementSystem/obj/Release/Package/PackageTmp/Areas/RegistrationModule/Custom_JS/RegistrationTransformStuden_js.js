﻿jQuery(function ($) {
    // new Player And Update current selecte Player
    $("#MyForm").submit(function (event) {
        event.preventDefault();
        $('#MyForm').validate({
            rules: {
                Year_Id: {
                    required: true
                },
                Semester_Id: {
                    required: true
                },
               
                batch_Id: {
                    required: true
                }
            },
            messages: {
                Year_Id: "Select Year",
                Semester_Id: "Select Semester",
               
                batch_Id: "Select Batch",

            },

            errorPlacement: function (error, element) { error.addClass("help-block"); element.parents(".form-group").addClass("has-feedback"); error.insertAfter(element); },
            highlight: function (element) { $(element).parents(".form-group").addClass("has-error").removeClass("has-success"); },
            unhighlight: function (element, errorClass, validClass) { $(element).parents(".form-group").addClass("has-success").removeClass("has-error"); },
            submitHandler: function (form) {
                var data = new FormData(form);
                $.ajax({
                    type: 'POST',
                    url: '/RegistrationModule/RegistrationNewTransformStudent/RegistrationTransformStuden',
                    data: data,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        alert(data);
                    },
                    error: function (data) {
                        alert(data);
                    }
                });
            }
        });
    });

});