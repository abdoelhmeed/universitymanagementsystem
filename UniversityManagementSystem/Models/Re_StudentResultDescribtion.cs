//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UniversityManagementSystem.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Re_StudentResultDescribtion
    {
        public int SRD_Id { get; set; }
        public double SRD_CourseGrade { get; set; }
        public double SRD_PointsNumber { get; set; }
        public string SRD_Pointsletter { get; set; }
        public int SRD_ExamSit { get; set; }
        public string SRD_Status { get; set; }
        public int Result_Id { get; set; }
        public int CATYId { get; set; }
    
        public virtual C_CourseAssignToYear_data C_CourseAssignToYear_data { get; set; }
        public virtual Re_StudentResult_data Re_StudentResult_data { get; set; }
    }
}
