//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UniversityManagementSystem.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class R_StudentRegistration_data
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public R_StudentRegistration_data()
        {
            this.Re_StudentResult_data = new HashSet<Re_StudentResult_data>();
            this.Re_StudentSupplementsAndAlternativesResult_data = new HashSet<Re_StudentSupplementsAndAlternativesResult_data>();
        }
    
        public int SR_Id { get; set; }
        public int Student_Id { get; set; }
        public int Year_Id { get; set; }
        public int Semester_Id { get; set; }
        public int Department_Id { get; set; }
        public string User_Id { get; set; }
        public int batch_Id { get; set; }
        public Nullable<bool> Registered { get; set; }
        public string Registr_Status { get; set; }
    
        public virtual AspNetUser AspNetUser { get; set; }
        public virtual R_BatchNumber_data R_BatchNumber_data { get; set; }
        public virtual R_Semester_data R_Semester_data { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Re_StudentResult_data> Re_StudentResult_data { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Re_StudentSupplementsAndAlternativesResult_data> Re_StudentSupplementsAndAlternativesResult_data { get; set; }
        public virtual R_Departments_data R_Departments_data { get; set; }
        public virtual R_StudyYear_data R_StudyYear_data { get; set; }
        public virtual R_StudentInfo_data R_StudentInfo_data { get; set; }
    }
}
