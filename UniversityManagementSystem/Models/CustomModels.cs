﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UniversityManagementSystem.Models
{
    public class ChartModel
    {
        public string label { get; set; }
        public int value { get; set; }
    }

     public class IncomeAndExpnsesModel
    {
        public List<string> YearList { get; set; }
        public List<decimal> IncomeList { get; set; }
        public List<decimal> ExpnsesList { get; set; }
    }

    public class SingeleIncomeAndExpnsesModel
    {
        public string YearList { get; set; }
        public decimal IncomeList { get; set; }
        public decimal ExpnsesList { get; set; }
    }
    public class ProfitModel
    {
        public List<string> YearList { get; set; }
        public List<decimal> ProfitList { get; set; }

    }

    public class Profit
    {
        public string yearsLable { get; set; }
        public decimal value { get; set; }
    }
}