//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UniversityManagementSystem.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class attachment
    {
        public int attachment_Id { get; set; }
        public Nullable<int> Student_id { get; set; }
        public byte[] attachfile { get; set; }
        public string description { get; set; }
    
        public virtual R_StudentInfo_data R_StudentInfo_data { get; set; }
    }
}
