﻿using System.Web.Mvc;

namespace UniversityManagementSystem.Areas.HealthStatusModule
{
    public class HealthStatusModuleAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "HealthStatusModule";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "HealthStatusModule_default",
                "HealthStatusModule/{controller}/{action}/{id}",
                new { Controller= "HealthStatusModule", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}