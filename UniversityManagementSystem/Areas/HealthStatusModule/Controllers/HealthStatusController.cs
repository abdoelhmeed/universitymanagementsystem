﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using UniversityManagementSystem.Areas.HealthStatusModule.Models;
using UniversityManagementSystem.Models;

namespace UniversityManagementSystem.Areas.HealthStatusModule.Controllers
{
    [Authorize]
    public class HealthStatusController : Controller
    {
        private UniversityManagementSystemEntities db = new UniversityManagementSystemEntities();

        // GET: HealthStatusModule/HealthStatus
        public ActionResult Index()
        {
            ViewBag.MainTitle = "University Setting Module";
            ViewBag.SubTitle = "Student Health Status";
            var r_HealthStatus_data = db.R_HealthStatus_data.Include(r => r.R_StudentInfo_data);
            return View(r_HealthStatus_data.ToList());
        }

        // GET: HealthStatusModule/HealthStatus/Details/5
        public ActionResult Details(int? id)
        {
            ViewBag.MainTitle = "University Setting Module";
            ViewBag.SubTitle = "Student Health Status";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            R_HealthStatus_data r_HealthStatus_data = db.R_HealthStatus_data.Find(id);
            if (r_HealthStatus_data == null)
            {
                return HttpNotFound();
            }
            return View(r_HealthStatus_data);
        }

        // GET: HealthStatusModule/HealthStatus/Create
        public ActionResult Create( string StudentNumber)
        {
            ViewBag.MainTitle = "University Setting Module";
            ViewBag.SubTitle = "Student Health Status";
            R_StudentInfo_data Student = db.R_StudentInfo_data.FirstOrDefault(u => u.Student_Number == StudentNumber);
            Health model = new Health();
            if (Student !=null)
            {
              
                model.Student_Id = Student.Student_Id;
                model.Student_Name = Student.Student_FullName;
                ViewBag.Mss = null;
                return View(model);
            }
            else
            {
                ViewBag.Mss = "Student Number Not True";
                return View();
            }
            
          
          
        }

        
        [HttpPost]
        public ActionResult Create(Health model)
        {
            ViewBag.MainTitle = "University Setting Module";
            ViewBag.SubTitle = "Student Health Status";
          
            if (model !=null)
            {
                R_HealthStatus_data health = new R_HealthStatus_data();
                health.Student_Id = model.Student_Id;
                health.Health_StudentHeight = model.Health_StudentHeight;
                health.Health_StudentWeight = model.Health_StudentWeight;
                health.Health_Vision = model.Health_Vision;
                health.Health_BloodPressure = model.Health_BloodPressure;
                health.Health_BloodType = model.Health_BloodType;
                health.Health_CorrectedVision = model.Health_CorrectedVision;
                health.Health_DiseasesAndDisorders = model.Health_DiseasesAndDisorders;


                db.R_HealthStatus_data.Add(health);
              int Val  =db.SaveChanges();
                if (Val > 0)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    ViewBag.msgError = "Health Status Not Save";
                    return View(model);
                }
                
            }
            else
            {
                ViewBag.msgError = "Student ture";
                return View(model);
            }
                
            

          
        }

        // GET: HealthStatusModule/HealthStatus/Edit/5
        [HttpGet]
        public ActionResult Edit(int? id)
        {
            ViewBag.MainTitle = "University Setting Module";
            ViewBag.SubTitle = "Student Health Status";
            R_HealthStatus_data model = db.R_HealthStatus_data.Find(id);
            Health health = new Health();
            if (model != null)
            {  
                health.Student_Id = model.Student_Id;
                health.Health_Id = model.Health_Id;
                health.Student_Name = model.R_StudentInfo_data.Student_FullName;
                health.Health_StudentHeight = model.Health_StudentHeight;
                health.Health_StudentWeight = model.Health_StudentWeight;
                health.Health_Vision = model.Health_Vision;
                health.Health_BloodPressure = model.Health_BloodPressure;
                health.Health_BloodType = model.Health_BloodType;
                health.Health_CorrectedVision = model.Health_CorrectedVision;
                health.Health_DiseasesAndDisorders = model.Health_DiseasesAndDisorders;
            }
            
            return View(health);
        }

        // POST: HealthStatusModule/HealthStatus/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public ActionResult Edit(Health model)
        {
            ViewBag.MainTitle = "University Setting Module";
            ViewBag.SubTitle = "Student Health Status";

            if (model != null)
            {
                R_HealthStatus_data health = new R_HealthStatus_data();
                health.Student_Id = model.Student_Id;
                health.Health_Id = model.Health_Id;
                health.Health_StudentHeight = model.Health_StudentHeight;
                health.Health_StudentWeight = model.Health_StudentWeight;
                health.Health_Vision = model.Health_Vision;
                health.Health_BloodPressure = model.Health_BloodPressure;
                health.Health_BloodType = model.Health_BloodType;
                health.Health_CorrectedVision = model.Health_CorrectedVision;
                health.Health_DiseasesAndDisorders = model.Health_DiseasesAndDisorders;


                db.Entry(health).State = EntityState.Modified;
                int Val = db.SaveChanges();
                if (Val > 0)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    ViewBag.msgError = "Health Status Not Update";
                    return View(model);
                }

            }
            else
            {
                ViewBag.msgError = "Student ture";
                return View(model);
            }
        }

        // GET: HealthStatusModule/HealthStatus/Delete/5
        public ActionResult Delete(int? id)
        {
            ViewBag.MainTitle = "University Setting Module";
            ViewBag.SubTitle = "Student Health Status";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            R_HealthStatus_data r_HealthStatus_data = db.R_HealthStatus_data.Find(id);
            if (r_HealthStatus_data == null)
            {
                return HttpNotFound();
            }
            return View(r_HealthStatus_data);
        }

        // POST: HealthStatusModule/HealthStatus/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            R_HealthStatus_data r_HealthStatus_data = db.R_HealthStatus_data.Find(id);
            db.R_HealthStatus_data.Remove(r_HealthStatus_data);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
