﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace UniversityManagementSystem.Areas.HealthStatusModule.Models
{
    public class Health
    {
        public int Health_Id { get; set; }
        [Required(ErrorMessage = "BloodType is Required")]
        public string Health_BloodType { get; set; }
        [Required(ErrorMessage = "Student Height is Required")]
        public Nullable<int> Health_StudentHeight { get; set; }
        [Required(ErrorMessage = "Student Weight is Required")]
        public Nullable<int> Health_StudentWeight { get; set; }
        [Required(ErrorMessage = "Diseases And Disorders is Required")]
        public string Health_DiseasesAndDisorders { get; set; }
        [Required(ErrorMessage = "Blood Pressure is Required")]
        public Nullable<int> Health_BloodPressure { get; set; }
        [Required(ErrorMessage = "Vision  is Required")]
        public string Health_Vision { get; set; }
        [Required(ErrorMessage = "Corrected Vision  is Required")]
        public string Health_CorrectedVision { get; set; }
        [Required(ErrorMessage = "Eyes  is Required")]
        public string Health_Eyes { get; set; }
        public int Student_Id { get; set; }
        public string Student_Name { get; set; }
    }
}