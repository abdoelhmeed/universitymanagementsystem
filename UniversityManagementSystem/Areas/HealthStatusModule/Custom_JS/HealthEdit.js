﻿jQuery(function ($) {
    // new Player And Update current selecte Player
    $("#MyForm").submit(function (event) {
        event.preventDefault();
        $('#MyForm').validate({
            rules: {
                Health_BloodType: {
                    required: true
                },
                Health_StudentHeight: {
                    required: true,
                    number: true

                }, Health_StudentWeight: {
                    required: true,
                    number: true

                }, Health_DiseasesAndDisorders: {
                    required: true,


                }, Health_BloodPressure: {
                    required: true,
                    number: true

                }, Health_Eyes: {
                    required: true,


                },
                Health_Vision: {
                    required: true,


                }, Health_CorrectedVision: {
                    required: true,


                },
            },
            messages: {
                Health_BloodType: "Student Blood Type is Required",
                Health_StudentHeight: "Student Height is Required",
                Health_StudentWeight: "Student Weight is Required",
                Health_DiseasesAndDisorders: "Student Diseases And Disorders is Required",
                Health_BloodPressure: "Student Blood Pressure is Required",
                Health_Eyes: "Student Health Eyes is Required",
                Health_Vision: "Student Health Vision is Required",
                Health_CorrectedVision: "Student CorrectedVision is Required"

            },
            errorPlacement: function (error, element) { error.addClass("help-block"); element.parents(".form-group").addClass("has-feedback"); error.insertAfter(element); },
            highlight: function (element) { $(element).parents(".form-group").addClass("has-error").removeClass("has-success"); },
            unhighlight: function (element, errorClass, validClass) { $(element).parents(".form-group").addClass("has-success").removeClass("has-error"); },
            submitHandler: function (form) {
                $("#Loding").show();
                var data = new FormData(form);
                $.ajax({
                    type: 'POST',
                    url: '/HealthStatusModule/HealthStatus/Edit',
                    data: data,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        if (data === "The operation was successful") {
                            $("#MyForm")[0].reset();
                            alert(data);
                            location.reload();
                        }
                        else {
                            $("#Loding").hide();
                            alert(data);
                        }
                    },
                    error: function (data) {
                        alert(data);
                    }
                });
            }
        });
    });
});