﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UniversityManagementSystem.Areas.LibraryModule.Models
{
    public class BookIssueModel
    {
        public int IssueId { get; set; }
        public int BookNo { get; set; }
        public string BookName { get; set; }
        public int MemberId { get; set; }
        public string MemberName { get; set; }
        public System.DateTime IssueDate { get; set; }
        public Nullable<System.DateTime> DeliveryDate { get; set; }
        public Nullable<bool> IsReceived { get; set; }
    }
}