﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UniversityManagementSystem.Areas.LibraryModule.Models
{
    public class BookModel
    {
        public int BookNo { get; set; }
        public string BookName { get; set; }
        public string BookAuthor { get; set; }
        public string BookPublisher { get; set; }
        public bool BookisAvail { get; set; }
        public int BookNumberOfCopy { get; set; }
    }
}