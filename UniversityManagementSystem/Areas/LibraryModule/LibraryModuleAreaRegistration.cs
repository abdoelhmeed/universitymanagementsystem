﻿using System.Web.Mvc;

namespace UniversityManagementSystem.Areas.LibraryModule
{
    public class LibraryModuleAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "LibraryModule";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "LibraryModule_default",
                "LibraryModule/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}