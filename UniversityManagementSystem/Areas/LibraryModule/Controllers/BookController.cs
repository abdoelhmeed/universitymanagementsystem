﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UniversityManagementSystem.Models;
using UniversityManagementSystem.Areas.LibraryModule.Models;

namespace UniversityManagementSystem.Areas.LibraryModule.Controllers
{
    [Authorize]
    public class BookController : Controller
    {
        UniversityManagementSystemEntities UDB = new UniversityManagementSystemEntities();
        public int don = 0;
        public string msg = "";
        // GET: LibraryModule/Book
        [HttpGet]
        public ActionResult Index()
        {
            List<BookModel> BookList = new List<BookModel>(); 
            ViewBag.MainTitle = "Library Module";
            ViewBag.SubTitle = "Book";
            var data = UDB.Library_Book.ToList();

            foreach (var item in data)
            {
                BookModel model = new BookModel
                {
                    BookNo = item.BookNo,
                    BookName = item.BookName,
                    BookAuthor = item.BookAuthor,
                    BookisAvail = item.BookisAvail,
                    BookPublisher = item.BookPublisher,
                    BookNumberOfCopy = item.BookNumberOfCopy
                };
                BookList.Add(model);
            }
            return View(BookList.ToList());
        }

        // BOST: LibraryModule/Book/Action
        [HttpPost]
        public JsonResult BookAction(BookModel data)
        {
            try
            {
                var isNew = UDB.Library_Book.Any(b => b.BookNo == data.BookNo);
                if(isNew == false)
                {
                    return AddNewBook(data);
                }
                else
                {
                    return EditCurrentBook(data);
                }
            }
            catch (Exception ex)
            {
                msg = "an error occurred :" + ex;
                return Json(msg, JsonRequestBehavior.AllowGet);
            }
        }

        // POST: LibraryModule/Book/Delete/5
        [HttpPost]
        public JsonResult Delete(int id)
        {
            try
            {
                Library_Book b = UDB.Library_Book.Find(id);
                UDB.Library_Book.Remove(b);
                UDB.SaveChanges();
                don = UDB.SaveChanges();
                if (don > 0)
                {
                    msg = "The operation was successful";
                    return Json(msg, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    msg = "The operation was not successful";
                    return Json(msg, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                msg = "an error occurred :" + ex;
                return Json(msg, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult AddNewBook(BookModel data)
        {
            var isExist = UDB.Library_Book.Any(b=>b.BookName == data.BookName);
            if (isExist == true)
            {
                msg = "the record is Already entered";
                return Json(msg, JsonRequestBehavior.AllowGet);
            }
            else
            {
                Library_Book LB = new Library_Book();
                LB.BookName = data.BookName;
                LB.BookAuthor = data.BookAuthor;
                LB.BookPublisher = data.BookPublisher;
                LB.BookisAvail = true;
                LB.BookNumberOfCopy = data.BookNumberOfCopy;
                UDB.Library_Book.Add(LB);
                don = UDB.SaveChanges();
                if(don > 0)
                {
                    msg = "The operation was successful";
                    return Json(msg, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    msg = "The operation was not successful";
                    return Json(msg, JsonRequestBehavior.AllowGet);
                }
            }
        }

        public JsonResult EditCurrentBook(BookModel data)
        {
            var isExist = UDB.Library_Book.Any(b => b.BookNo != data.BookNo  && b.BookName == data.BookName);
            if (isExist == true)
            {
                msg = "the record is Already entered";
                return Json(msg, JsonRequestBehavior.AllowGet);
            }
            else
            {
                Library_Book LB = UDB.Library_Book.Find(data.BookNo);
                LB.BookName = data.BookName;
                LB.BookAuthor = data.BookAuthor;
                LB.BookPublisher = data.BookPublisher;
                LB.BookisAvail = true;
                LB.BookNumberOfCopy = data.BookNumberOfCopy;
                UDB.Entry(LB).State = System.Data.Entity.EntityState.Modified;
                don = UDB.SaveChanges();
                if (don > 0)
                {
                    msg = "The operation was successful";
                    return Json(msg, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    msg = "The operation was not successful";
                    return Json(msg, JsonRequestBehavior.AllowGet);
                }
            }
        }
    }
}
