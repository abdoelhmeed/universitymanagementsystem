﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UniversityManagementSystem.Models;
using UniversityManagementSystem.Areas.LibraryModule.Models;
namespace UniversityManagementSystem.Areas.LibraryModule.Controllers
{
    [Authorize]
    public class MemberController : Controller
    {
        UniversityManagementSystemEntities UDB = new UniversityManagementSystemEntities();
        private int don;
        private string msg;

        // GET: LibraryModule/Member
        public ActionResult Index()
        {
            ViewBag.MainTitle = "LibraryModule";
            ViewBag.SubTitle = "Member";
            List<MemberModel> MemberList = new List<MemberModel>();
            var data = UDB.Library_Member.ToList();

            foreach (var item in data)
            {

                MemberModel model = new MemberModel
                {
                    MemberId = item.MemberId,
                    MemberName = item.MemberName,
                    MemberPhone = item.MemberPhone,
                    MemberEmail = item.MemberEmail,
                };
                MemberList.Add(model);
            }
            return View(MemberList.ToList());
        }

        // BOST: LibraryModule/Book/Action
        [HttpPost]
        public JsonResult MemberAction(MemberModel data)
        {
            try
            {
                var isNew = UDB.Library_Book.Any(b => b.BookNo == data.MemberId);
                if (isNew == false)
                {
                    return AddCurrentMember(data);
                }
                else
                {
                    return EditCurrentMember(data);
                }
            }
            catch (Exception ex)
            {
                msg = "an error occurred :" + ex;
                return Json(msg, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: LibraryModule/Member/Delete/5
        [HttpPost]
        public JsonResult Delete(int id)
        {
            try
            {
                Library_Member m = UDB.Library_Member.Find(id);
                UDB.Library_Member.Remove(m);
                UDB.SaveChanges();
                don = UDB.SaveChanges();
                if (don > 0)
                {
                    msg = "operation was successful";
                    return Json(msg, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    msg = "operation was not successful";
                    return Json(msg, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                msg = "an error occurred :" + ex;
                return Json(msg, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult AddCurrentMember(MemberModel data)
        {
            var isExist = UDB.Library_Book.Any(b => b.BookName == data.MemberName);
            if (isExist == true)
            {
                msg = "the record is Already entered";
                return Json(msg, JsonRequestBehavior.AllowGet);
            }
            else
            {
                Library_Member LM = new Library_Member();
                LM.MemberName = data.MemberName;
                LM.MemberEmail = data.MemberEmail;
                LM.MemberPhone = data.MemberPhone;
                UDB.Library_Member.Add(LM);
                don = UDB.SaveChanges();
                if (don > 0)
                {
                    msg = "The operation was successful";
                    return Json(msg, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    msg = "The operation was not successful";
                    return Json(msg, JsonRequestBehavior.AllowGet);
                }
            }
        }

        public JsonResult EditCurrentMember(MemberModel data)
        {
            var isExist = UDB.Library_Member.Any(b => b.MemberId != data.MemberId && b.MemberName == data.MemberName);
            if (isExist == true)
            {
                msg = "the record is Already entered";
                return Json(msg, JsonRequestBehavior.AllowGet);
            }
            else
            {
                Library_Member LM = UDB.Library_Member.Find(data.MemberId);
                LM.MemberName = data.MemberName;
                LM.MemberEmail = data.MemberEmail;
                LM.MemberPhone = data.MemberPhone;
                UDB.Entry(LM).State = System.Data.Entity.EntityState.Modified;
                don = UDB.SaveChanges();
                if (don > 0)
                {
                    msg = "The operation was successful";
                    return Json(msg, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    msg = "The operation was not successful";
                    return Json(msg, JsonRequestBehavior.AllowGet);
                }
            }
        }
    }
}
