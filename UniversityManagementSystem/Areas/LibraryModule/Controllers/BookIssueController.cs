﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using UniversityManagementSystem.Models;
using UniversityManagementSystem.Areas.LibraryModule.Models;
using System;

namespace UniversityManagementSystem.Areas.LibraryModule.Controllers
{
    [Authorize]
    public class BookIssueController : Controller
    {
        UniversityManagementSystemEntities UDB = new UniversityManagementSystemEntities();
        private string msg;
        private int don;

        // GET: LibraryModule/BookIssue
        public ActionResult Index()
        {
            List<BookIssueModel> ListModel = new List<BookIssueModel>();
            ViewBag.MainTitle = "Library Module";
            ViewBag.SubTitle = "Book Issue";
            var data = UDB.Library_Issue.ToList();
            foreach (var item in data)
            {
                BookIssueModel model = new BookIssueModel
                {
                    IssueId = item.IssueId,
                    BookNo = item.BookNo,
                    BookName = item.Library_Book.BookName,
                    MemberId = item.MemberId,
                    MemberName = item.Library_Member.MemberName,
                    IssueDate = item.IssueDate,
                    DeliveryDate = item.DeliveryDate,
                    IsReceived = item.IsReceived
                };
                ListModel.Add(model);
            }
            return View(ListModel.ToList());
        }
        [HttpPost]
        public JsonResult BookIssueAction(BookIssueModel data)
        {
            try
            {
                var isNew = UDB.Library_Issue.Any(b => b.IssueId == data.IssueId);
                if (isNew == false)
                {
                    return AddNewBookIssue(data);
                }
                else
                {
                    return EditCurrentBookIssue(data);
                }
            }
            catch (Exception ex)
            {
                msg = "an error occurred :" + ex;
                return Json(msg, JsonRequestBehavior.AllowGet);
            }
        }

        // POST: LibraryModule/Book/Delete/5
        [HttpPost]
        public JsonResult Delete(int id)
        {
            try
            {
                Library_Issue i = UDB.Library_Issue.Find(id);
                UDB.Library_Issue.Remove(i);
                UDB.SaveChanges();
                don = UDB.SaveChanges();
                if (don > 0)
                {
                    msg = "The operation was successful";
                    return Json(msg, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    msg = "The operation was not successful";
                    return Json(msg, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                msg = "an error occurred :" + ex;
                return Json(msg, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult AddNewBookIssue(BookIssueModel data)
        {
            var isExist = UDB.Library_Issue.Any(b => b.MemberId == data.IssueId && b.Library_Book.BookName == data.BookName && b.IsReceived == false);
            if (isExist == true)
            {
                msg = "the record is Already entered";
                return Json(msg, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var issueDate = DateTime.UtcNow;
                var deliveryDate = issueDate.AddDays(5);
                Library_Issue Iss = new Library_Issue
                {
                    MemberId = data.MemberId,
                    BookNo = data.BookNo,
                    IssueDate = issueDate,
                    DeliveryDate = deliveryDate,
                    IsReceived = false
                };
                UDB.Library_Issue.Add(Iss);
                don = UDB.SaveChanges();
                if (don > 0)
                {
                    msg = "operation was successful";
                    return Json(msg, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    msg = "operation was not successful";
                    return Json(msg, JsonRequestBehavior.AllowGet);
                }
            }
        }

        public JsonResult EditCurrentBookIssue(BookIssueModel data)
        {
            var isExist = UDB.Library_Issue.Any(b => b.IssueId != data.IssueId && b.MemberId == data.MemberId && b.BookNo == data.BookNo);
            if (isExist == true)
            {
                msg = "the record is Already entered";
                return Json(msg, JsonRequestBehavior.AllowGet);
            }
            else
            {
                Library_Issue LB = UDB.Library_Issue.Find(data.IssueId);
                LB.MemberId = data.MemberId;
                LB.BookNo = data.BookNo;
                LB.IsReceived = true;
                UDB.Entry(LB).State = System.Data.Entity.EntityState.Modified;
                don = UDB.SaveChanges();
                if (don > 0)
                {
                    msg = "operation was successful";
                    return Json(msg, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    msg = "operation was successful";
                    return Json(msg, JsonRequestBehavior.AllowGet);
                }
            }
        }
    }
}
