﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UniversityManagementSystem.Areas.Fees.Models
{
    public class StudentInfo
    {
        public int Student_Id { get; set; }
        public string Student_Number { get; set; }
        public string Student_FullName { get; set; }
        public string Department { get; set; }
        public string Student_Email { get; set; }
    }
}