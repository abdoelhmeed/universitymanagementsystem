﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UniversityManagementSystem.Areas.Fees.Models
{
    public class TotalFeesPyDept
    {
        public string StdNumber { get; set; }
        public string StdName { get; set; }
        public string StdDept { get; set; }
        public decimal StdFees { get; set; }
        public string YearName { get; set; }

    }
}