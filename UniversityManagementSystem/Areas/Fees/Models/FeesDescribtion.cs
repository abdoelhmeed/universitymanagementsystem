﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UniversityManagementSystem.Areas.Fees.Models
{
    public class FeesDescribtion
    {
        public int TFD_Id { get; set; }
        public int Student_Id { get; set; }
        public string Student_Number { get; set; }
        public string Student_Name { get; set; }
        public string Depositnumber { get; set; }
        public decimal TFD_TuitionFee { get; set; }
        public DateTime TFD_FeesDate { get; set; }
        public Nullable<int> Year_Id { get; set; }
        public string Year_Name { get; set; }
        public int TF_Id { get; set; }
    }

    public class FeesReports
    {
        public int TFD_Id { get; set; }
        public int Student_Id { get; set; }
        public string Student_Number { get; set; }
        public string Student_Name { get; set; }
        public string Depositnumber { get; set; }
        public decimal TFD_TuitionFee { get; set; }
        public DateTime TFD_FeesDate { get; set; }
        public string Year_Name { get; set; }
        
    }

    public class FeesForOnSudentReports
    {
        public int TFD_Id { get; set; }
        public string Student_Number { get; set; }
        public string Dept { get; set; }
        public string Student_Name { get; set; }
        public string Depositnumber { get; set; }
        public decimal TFD_TuitionFee { get; set; }
        public DateTime TFD_FeesDate { get; set; }
        public string Year_Name { get; set; }

    }


    public class FeesReportsForAllSudent
    {
     
        public int Department_Id { get; set; }
        public int Year_Id { get; set; }
        public int batch_Id { get; set; }

        public int Semester_Id { get; set; }



    }


}