﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace UniversityManagementSystem.Areas.Fees.Models
{
    public class TuitionFees
    {
        public int TF_Id { get; set; }
        public int Student_Id { get; set; }
        public string Student_Number { get; set; }
        public string StudentName { get; set; }
        public string User_Id { get; set; }
        public Nullable<int> Department_Id { get; set; }
        public string Department_Name { get; set; }
        public Nullable<decimal> Fees_Department { get; set; }
        public int Payment_Id { get; set; }

        public string Payment_Name { get; set; }
        public Nullable<decimal> FeesPayment { get; set; }
        public decimal Fees_Total { get; set; }
        public System.DateTime FeesFeesDate { get; set; }
    }
}