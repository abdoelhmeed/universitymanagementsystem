﻿using System.Web.Mvc;

namespace UniversityManagementSystem.Areas.Fees
{
    public class FeesAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Fees";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Fees_default",
                "Fees/{controller}/{action}/{id}",
                new { controller = "Fees", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}