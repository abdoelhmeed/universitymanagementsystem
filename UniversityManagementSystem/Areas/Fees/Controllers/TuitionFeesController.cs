﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UniversityManagementSystem.Areas.Fees.Models;
using UniversityManagementSystem.Models;

namespace UniversityManagementSystem.Areas.Fees.Controllers
{
    [Authorize]
    public class TuitionFeesController : Controller
    {
        UniversityManagementSystemEntities db = new UniversityManagementSystemEntities();
        public ActionResult Index()
        {
            ViewBag.MainTitle = "Fees Module";
            ViewBag.SubTitle = "Student Fees Roles";
            List<TuitionFees> model = db.Fee_TuitionFees.Select(u => new TuitionFees {
                TF_Id = u.TF_Id,
                Department_Id = u.Department_Id,
                Department_Name = u.R_Departments_data.Department_Name,
                Student_Id = u.Student_Id,
                StudentName = u.R_StudentInfo_data.Student_FullName,
                Payment_Id = u.Payment_Id,
                Payment_Name = u.PaymentStatu.Payment_Name,
                Fees_Department = u.Fees_Department,
                FeesPayment = u.PaymentStatu.Payment_percent,
                Fees_Total = u.Fees_Total,
                FeesFeesDate = u.FeesFeesDate,
                User_Id = u.AspNetUser.Email,
                Student_Number = u.R_StudentInfo_data.Student_Number
            }).ToList();
            return View(model);
        }
       
        [HttpGet]
        public ActionResult CreateOrEdit(string Sudent_Id)
        {
            ViewBag.MainTitle = "Fees Module";
            ViewBag.SubTitle = "Create Student Fees Roles";

            TuitionFees model=new TuitionFees();
            
            R_StudentInfo_data StdInfo = db.R_StudentInfo_data.FirstOrDefault(u => u.Student_Number == Sudent_Id);
            Fee_TuitionFees tuitionFees = db.Fee_TuitionFees.FirstOrDefault(u => u.Student_Id== StdInfo.Student_Id);
            if (tuitionFees == null)
            {
                if (StdInfo != null)
                {
                    R_StudentRegistration_data StdRegistration = db.R_StudentRegistration_data.FirstOrDefault(x => x.Student_Id == StdInfo.Student_Id);

                    if (StdRegistration != null)
                    {
                        decimal Depart = db.R_Departments_data.FirstOrDefault(u => u.Department_Name == StdInfo.Department).Department_Fees;
                        model.Student_Id = StdInfo.Student_Id;
                        model.StudentName = StdInfo.Student_FullName;
                        model.FeesFeesDate = DateTime.Now;
                        model.Fees_Department = Depart;
                        model.Department_Name = StdRegistration.R_Departments_data.Department_Name;
                        model.Department_Id = StdRegistration.Department_Id;
                    }
                }
                ViewBag.Payment = db.PaymentStatus.ToList();
                return View(model);
            }
            else
            {
                ViewBag.Err = "Student Fees Is already exist";
                ViewBag.Payment = db.PaymentStatus.ToList();
                return View();
            }
        }
        [HttpPost]
        public ActionResult CreateOrEdit(TuitionFees model)
        {
            if (model != null)
            {
                PaymentStatu Payment = db.PaymentStatus.Find(model.Payment_Id);
                R_Departments_data Departments = db.R_Departments_data.Find(model.Department_Id);

                Fee_TuitionFees Fees = new Fee_TuitionFees()
                {
                    Department_Id = model.Department_Id,
                    Payment_Id = model.Payment_Id,
                    FeesPayment = Payment.Payment_Value,
                    Student_Id = model.Student_Id,
                    User_Id = User.Identity.GetUserId(),
                /*System.Security.Principal.WindowsIdentity.GetCurrent().User.ToString(),*/
                   Fees_Department = Departments.Department_Fees,
                    Fees_Total = (Departments.Department_Fees) - ((Departments.Department_Fees * Convert.ToDecimal( Payment.Payment_percent)) / 100) /*Departments.Department_Fees - Payment.Payment_Value*/,
                    FeesFeesDate = DateTime.Now,
                    
                };
                db.Fee_TuitionFees.Add(Fees);
                db.SaveChanges();
        }
            
            return RedirectToAction("Index");
        }
        public ActionResult  viewFoCreate()
        {
            List<R_StudentInfo_data> StudentIds = db.R_StudentInfo_data.ToList();
           
            List<R_StudentInfo_data> filter = new List<R_StudentInfo_data>();
            foreach (R_StudentInfo_data item in StudentIds)
             {
                 
                 if (item.Student_Id != 0)
                  {
                   R_StudentRegistration_data StuRegIds = db.R_StudentRegistration_data.SingleOrDefault(x => x.Student_Id == item.Student_Id);
                    if (StuRegIds != null)
                    {
                        Fee_TuitionFees Fee = db.Fee_TuitionFees.SingleOrDefault(u => u.Student_Id == StuRegIds.Student_Id);
                        if (Fee == null)
                        {
                            filter.Add(item);
                        }
                        
                    }
                  
                }
             }
            List<StudentInfo> model = filter.Select(u => new StudentInfo { Department=u.Department,Student_FullName=u.Student_FullName,Student_Id=u.Student_Id,Student_Number=u.Student_Number}).ToList();

            return View(model);
        }
        [HttpGet]
        public ActionResult Edit(int TF_Id)
        {
            Fee_TuitionFees model = db.Fee_TuitionFees.Find(TF_Id);
            TuitionFees Tfee = new TuitionFees() {
                Department_Id = model.Department_Id,
                Department_Name = model.R_Departments_data.Department_Name,
                FeesFeesDate = model.FeesFeesDate,
                FeesPayment = model.FeesPayment,
                Fees_Department=model.Fees_Department,
                Fees_Total=model.Fees_Total,
                Payment_Id=model.Payment_Id,
                StudentName=model.R_StudentInfo_data.Student_FullName,
                Student_Id=model.Student_Id,
                TF_Id=model.TF_Id,
                Student_Number=model.R_StudentInfo_data.Student_Number,
                User_Id=model.User_Id,
            };
            ViewBag.Payment = db.PaymentStatus.ToList();
            return View(Tfee);
        }

        [HttpPost]
        public ActionResult Edit(TuitionFees model)
        {
            if (model != null)
            {
                PaymentStatu Payment = db.PaymentStatus.Find(model.Payment_Id);
                R_Departments_data Departments = db.R_Departments_data.Find(model.Department_Id);
                Fee_TuitionFees Fees = db.Fee_TuitionFees.Find(model.TF_Id);
                var f = (Fees.Fees_Department) - ((Fees.Fees_Department * Convert.ToDecimal(Payment.Payment_percent)) / 100);
               Fees.User_Id = User.Identity.GetUserId();
               Fees.Fees_Total =Convert.ToDecimal(f);
               Fees.FeesFeesDate = DateTime.Now;
                Fees.Payment_Id = model.Payment_Id;
               db.Entry(Fees).State = EntityState.Modified;
               db.SaveChanges();
            }

            return RedirectToAction("Index");
        }
    }
}