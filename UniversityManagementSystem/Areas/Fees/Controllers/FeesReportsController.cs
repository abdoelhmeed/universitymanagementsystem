﻿using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UniversityManagementSystem.Models;
using UniversityManagementSystem.Areas.Fees.Models;
namespace UniversityManagementSystem.Areas.Fees.Controllers
{
    [Authorize]
    public class FeesReportsController : Controller
    {
        UniversityManagementSystemEntities db = new UniversityManagementSystemEntities();
        // GET: Fees/FeesReports
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult StudentAllFees(int studentID)
        {

            List<FeesReports> md = db.Fee_TuitionFeesDescribtion.Where(x => x.Student_Id == studentID).Select(
                u => new FeesReports {
                    Depositnumber =u.Depositnumber,
                    Student_Id=u.Student_Id,
                    Student_Name=u.R_StudentInfo_data.Student_FullName,
                    Student_Number=u.R_StudentInfo_data.Student_Number,
                    TFD_FeesDate=u.TFD_FeesDate,
                    TFD_Id=u.TFD_Id,
                    TFD_TuitionFee=u.TFD_TuitionFee,
                    Year_Name=u.R_StudyYear_data.Year_Name

            }).ToList();
            ReportDocument rd = new ReportDocument();
            rd.Load(Path.Combine(Server.MapPath("~/Areas/Fees/Reports/StudentAllFeesReport.rpt")));
            rd.SetDataSource(md.ToList());
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            return File(stream, "application/pdf", "StudentAllFeesReport.pdf");

        }
    


       
        [HttpPost]
        public ActionResult StudentPyDeptFees(FeesReportsForAllSudent model)
        {
           
           TotalFeesPyDept FTFD=new TotalFeesPyDept();
            List<TotalFeesPyDept> FTFDR = new List<TotalFeesPyDept>();
           
            List<Fee_TuitionFeesDescribtion>  FTFDT = new List<Fee_TuitionFeesDescribtion>();
            List<R_StudentRegistration_data> StudentIds = db.R_StudentRegistration_data.Where(u => u.Year_Id == model.Year_Id && u.Department_Id == model.Department_Id && u.batch_Id==model.batch_Id  && u.Semester_Id==model.Semester_Id).ToList();
           

            foreach (var item in StudentIds)
            {
                decimal Total=0;
                List<Fee_TuitionFeesDescribtion> TuitionFees = db.Fee_TuitionFeesDescribtion.Where(u => u.Student_Id == item.Student_Id && u.Year_Id == model.Year_Id).ToList();
                foreach (var item1 in TuitionFees)
                {
                    if (item1 != null)
                    {
                        Total = db.Fee_TuitionFeesDescribtion.Where(u => u.Student_Id == item.Student_Id && u.Year_Id == model.Year_Id).Sum(t => t.TFD_TuitionFee);

                    }

                   
                }
                    R_StudentRegistration_data Stdrig = db.R_StudentRegistration_data.FirstOrDefault(u => u.Student_Id == item.Student_Id && u.Year_Id == model.Year_Id && u.Semester_Id==model.Semester_Id );
                TotalFeesPyDept PyDept = new TotalFeesPyDept() {
                    StdDept= Stdrig.R_Departments_data.Department_Name,
                    StdFees=Total,
                    StdName= Stdrig.R_StudentInfo_data.Student_FullName,
                    StdNumber= Stdrig.R_StudentInfo_data.Student_Number,
                    YearName= Stdrig.R_StudyYear_data.Year_Name
                };
                FTFDR.Add(PyDept);
                
               

            }

            ReportDocument rd = new ReportDocument();
            rd.Load(Path.Combine(Server.MapPath("~/Areas/Fees/Reports/TotalFeesPyDeptReport.rpt")));
            rd.SetDataSource(FTFDR.ToList());
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            return File(stream, "application/pdf", "TotalFeesPyDeptReport.pdf");
            
        }
    }
}