﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using UniversityManagementSystem.Models;

namespace UniversityManagementSystem.Areas.Fees.Controllers
{
    [Authorize]
    public class PaymentStatusController : Controller
    {
        private UniversityManagementSystemEntities db = new UniversityManagementSystemEntities();

        // GET: RegistrationModule/PaymentStatus
        public ActionResult Index()
        {
            ViewBag.MainTitle = "Fees Module";
            ViewBag.SubTitle = "Special Case";
            return View(db.PaymentStatus.ToList());
        }
        [HttpPost]
        public JsonResult Create(PaymentStatu model)
        {
            if (model.Payment_Id <= 0)
            {
              
               try
                {
                    db.PaymentStatus.Add(model);
                    int val = db.SaveChanges();
                    if (val > 0)
                    {
                        return Json("Special Case is Saved", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json("payment Statut is Not Saved ,Please try again", JsonRequestBehavior.AllowGet);
                    }

                }
               catch
                {

                    return Json("Please try again", JsonRequestBehavior.AllowGet);
                }
            }
            else
            {

               try
                {
                    db.Entry(model).State = EntityState.Modified;
                    int val = db.SaveChanges();
                    if (val > 0)
                    {
                        return Json("Special Case is Updated ", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json("Special Case is Not Update ,Please try again", JsonRequestBehavior.AllowGet);
                    }

                }
               catch
                {

                    return Json("Please try again", JsonRequestBehavior.AllowGet);
                }

            }
          
        }
        
        // POST: RegistrationModule/PaymentStatus/Delete/5
        [HttpPost]
        public JsonResult Delete(int id)
        {
            try
            {
                PaymentStatu paymentStatu = db.PaymentStatus.Find(id);
                    db.PaymentStatus.Remove(paymentStatu);
                    db.SaveChanges();

                    return Json("Deleted", JsonRequestBehavior.AllowGet); 
            }
            catch (Exception)
            {

                return Json("Sorry, The Field Can Not Be Deleted", JsonRequestBehavior.AllowGet);
            }
           
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


       
    }
}
