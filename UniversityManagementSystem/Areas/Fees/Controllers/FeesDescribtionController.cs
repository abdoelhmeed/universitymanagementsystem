﻿using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UniversityManagementSystem.Areas.Fees.Models;
using UniversityManagementSystem.Models;


namespace UniversityManagementSystem.Areas.Fees.Controllers
{
    [Authorize]
    public class FeesDescribtionController : Controller
    {
        UniversityManagementSystemEntities db = new UniversityManagementSystemEntities();  
        public ActionResult Index()
        {
            ViewBag.MainTitle = "Fees Module";
            ViewBag.SubTitle = "Fees Describtion";
            List<int> StdIds = db.Fee_TuitionFees.Select(x => x.Student_Id).ToList();
            List<R_StudentInfo_data> ListStd = new List<R_StudentInfo_data>();
            R_StudentInfo_data SnigStd = new R_StudentInfo_data();
            foreach (var item in StdIds)
            {
                SnigStd = db.R_StudentInfo_data.Find(item);
                ListStd.Add(SnigStd);
            }
            return View(ListStd.ToList());  
        }

        public ActionResult Financial(int StudentID)
        {
            ViewBag.MainTitle = "Fees Module";
            ViewBag.SubTitle = "Financial Details";
            IEnumerable<FeesDescribtion> model = db.Fee_TuitionFeesDescribtion.Select(x=> new FeesDescribtion {
               Student_Name=x.R_StudentInfo_data.Student_FullName,
               Depositnumber=x.Depositnumber,
               Student_Id=x.Student_Id,
               TFD_FeesDate=x.TFD_FeesDate,
               TFD_Id=x.TFD_Id,
               TFD_TuitionFee=x.TFD_TuitionFee,
               TF_Id=x.TF_Id,
              Year_Id=x.Year_Id,
              Year_Name=x.R_StudyYear_data.Year_Name
           }).Where(u => u.Student_Id == StudentID).ToList();
            List<year> year= db.R_StudyYear_data.Select(x => new year {ID=x.Year_Id, Name=x.Year_Name}).ToList();
            List<year> Newyear = new List<year>();
            foreach (var item in year)
            {
                List<Fee_TuitionFeesDescribtion> y = db.Fee_TuitionFeesDescribtion.Where(u=>u.Year_Id ==item.ID && u.Student_Id== StudentID).ToList();
                if (y.Count > 0)
                {
                    Newyear.Add(item);
                }
            }
            R_StudentInfo_data studentInfo = db.R_StudentInfo_data.Find(StudentID);
            ViewBag.yearName = Newyear;
            ViewBag.Department = studentInfo.Department;
            ViewBag.Student_FullName = studentInfo.Student_FullName;
            ViewBag.Student_Id = studentInfo.Student_Id;
            ViewBag.Student_Number = studentInfo.Student_Number;
            ViewBag.fees = db.Fee_TuitionFees.Single(x => x.Student_Id == StudentID).Fees_Total;
            
            return View(model);
        }
        [HttpGet]
        public ActionResult Pay(int StuID)
        {
            R_StudentInfo_data StdInfo = db.R_StudentInfo_data.Find(StuID);
            Fee_TuitionFees TF = db.Fee_TuitionFees.FirstOrDefault(x => x.Student_Id == StuID);
            List<R_StudyYear_data> StudyYear = db.R_StudyYear_data.ToList();
            ViewBag.StudyYear = new SelectList(StudyYear, "Year_Id", "Year_Name");
            FeesDescribtion model = new FeesDescribtion();
            model.Student_Id = StdInfo.Student_Id;
            model.TF_Id = TF.TF_Id;
            return PartialView("Pay",model);
        }
        [HttpPost]
        public JsonResult PostPay(FeesDescribtion model)
        {
            Fee_TuitionFeesDescribtion FTD = new Fee_TuitionFeesDescribtion()
            {
                TFD_TuitionFee = model.TFD_TuitionFee,
                TF_Id = model.TF_Id,
                Student_Id = model.Student_Id,
                Year_Id = model.Year_Id,
                TFD_FeesDate = DateTime.Now,
                Depositnumber=model.Depositnumber,
            };
            db.Fee_TuitionFeesDescribtion.Add(FTD);
            int Val=    db.SaveChanges();
            if (Val > 0)
            {
                
              string  mss =Convert.ToString( model.Student_Id);
                return Json(mss, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string mss = "not Saved";
                return Json(mss, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public ActionResult EditPay(int TFD_Id)
        {
            Fee_TuitionFeesDescribtion fee = db.Fee_TuitionFeesDescribtion.Find(TFD_Id);
            List<R_StudyYear_data> StudyYear = db.R_StudyYear_data.ToList();
            ViewBag.StudyYear = new SelectList(StudyYear, "Year_Id", "Year_Name");
            FeesDescribtion model = new FeesDescribtion();
            if (fee != null)
            {
                model.Student_Id = fee.Student_Id;
                model.TFD_Id = fee.TFD_Id;
                model.TFD_TuitionFee = fee.TFD_TuitionFee;
                model.TF_Id = fee.TF_Id;
                model.Year_Id = fee.Year_Id;
                model.Depositnumber = fee.Depositnumber;
            }
            return PartialView("EditPay", model);
        }
        [HttpPost]
        public JsonResult PostEditPay(FeesDescribtion model)
        {
            if (model != null)
            {
                Fee_TuitionFeesDescribtion feed = db.Fee_TuitionFeesDescribtion.Find(model.TFD_Id);
                if (feed !=null)
                {
                    feed.TFD_Id = model.TFD_Id; 
                    feed.TFD_TuitionFee = model.TFD_TuitionFee;
                    feed.TF_Id = model.TF_Id;
                    feed.Student_Id = model.Student_Id;
                    feed.Year_Id = model.Year_Id;
                    feed.TFD_FeesDate = DateTime.Now;
                    feed.Depositnumber = model.Depositnumber;
                    db.Entry(feed).State = EntityState.Modified;
                    int val = db.SaveChanges();
                    if (val > 0)
                    {
                        string mss = Convert.ToString(model.Student_Id);
                        return Json(mss, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        string mss = "not Saved";
                        return Json(mss, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string mss = "not Saved";
                    return Json(mss, JsonRequestBehavior.AllowGet);
                }

            }
            else
            {
                string mss = "not Saved";
                return Json(mss, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult Del(int TFD_Id)
        {
            Fee_TuitionFeesDescribtion fee = db.Fee_TuitionFeesDescribtion.Find(TFD_Id);
            if (fee !=null)
            {
                db.Fee_TuitionFeesDescribtion.Remove(fee);
                int Val = db.SaveChanges();
                if (Val > 0)
                {
                    string mss = Convert.ToString(fee.Student_Id);
                    return Json(mss, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string mss = "Not Delete";
                    return Json(mss, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                string mss = "Not Delete";
                return Json(mss, JsonRequestBehavior.AllowGet);
            }
        }
     
        public ActionResult Preint(int FTDID)
        {
            Fee_TuitionFeesDescribtion ft = db.Fee_TuitionFeesDescribtion.Find(FTDID);
            List<FeesForOnSudentReports> PyPreintR = new List<FeesForOnSudentReports>();
            if (ft != null)
            {
                string Dept = db.R_StudentRegistration_data.FirstOrDefault(x => x.Student_Id == ft.Student_Id).R_Departments_data.Department_Name;
                List<FeesForOnSudentReports> PyPreint = new List<FeesForOnSudentReports>() {
                    new FeesForOnSudentReports{
                        Student_Name =ft.R_StudentInfo_data.Student_FullName,
                        TFD_TuitionFee=ft.TFD_TuitionFee,
                        Student_Number=ft.R_StudentInfo_data.Student_Number,
                        Dept=Dept,
                        Year_Name=ft.R_StudyYear_data.Year_Name,
                        Depositnumber=ft.Depositnumber,
                        TFD_FeesDate=ft.TFD_FeesDate,
                        TFD_Id=0,
                    }
                };

                PyPreintR.AddRange(PyPreint);
            }
           
            ReportDocument rd = new ReportDocument();
            rd.Load(Path.Combine(Server.MapPath("~/Areas/Fees/Reports/FeesForOnSudentReports.rpt")));
            rd.SetDataSource(PyPreintR.ToList());
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            return File(stream, "application/pdf", "FeesForOnSudentReports.pdf");
        }
        
    }
   
}