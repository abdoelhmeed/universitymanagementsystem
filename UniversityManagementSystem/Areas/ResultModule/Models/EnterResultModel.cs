﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UniversityManagementSystem.Areas.ResultModule.Models
{
    public class EnterResultModel
    {
        public int Year_Id { get; set; }
        public int Department_Id { get; set; }
        public int Semester_Id { get; set; }
        public int batch_Id { get; set; }
        public int Course_Id { get; set; }
        public List<GetStudantInfoModel> StdList {get; set;}
    }
    public class CalculationResultModel
    {
        public int Year_Id { get; set; }
        public int Department_Id { get; set; }
        public int Semester_Id { get; set; }
        public int batch_Id { get; set; }
        public int Course_Id { get; set; }
        
        public List<GetStudantIdsModel> StdList { get; set; }
    }
    public class GetStudantInfoModel
    {
        
        public int SR_Id { get; set; }
        public int StdId { get; set; }
        public string StdName { get; set; }
        public double StdDegree { get; set; }
        public string CourseStatus { get; set; }
    }
    public class GetStudantIdsModel
    {

        public int SR_Id { get; set; }
        public int StdId { get; set; }
    }
}