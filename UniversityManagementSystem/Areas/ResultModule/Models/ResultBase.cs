﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UniversityManagementSystem.Models;

namespace UniversityManagementSystem.Areas.ResultModule.Models
{
    public class ResultBase
    {
        private UniversityManagementSystemEntities db = new UniversityManagementSystemEntities();
        public string GetStudentgGrade(double ClassPoint)
        {
            string R = "";
            if (ClassPoint >= 3.50 && ClassPoint <= 4.00)
            {
                R = "Excellent";
            }
            else
            if (ClassPoint >= 3.00 && ClassPoint <= 3.49)
            {
                R = "Very good";
            }
            else
            if (ClassPoint >= 2.50 && ClassPoint <= 2.99)
            {
                R = "Good";
            }
            else
            if (ClassPoint >= 2.00 && ClassPoint <= 2.49)
            {
                R = "Passed";
            }
            else
            {
                R = "Failure";
            }
            return R;
        }
        public double RoundStudentgGrade(double Point)
        {
            double R = 0.0;
            if (Point >= 3.51 && Point <= 4.00)
            {
                R = 4.00;
            }
            else
            if (Point >= 3.01 && Point <= 3.50)
            {
                R = 3.50;
            }
            else
            if (Point >= 2.51 && Point <= 3.00)
            {
                R = 3.00;
            }
            else
            if (Point >= 2.01 && Point <= 2.50)
            {
                R = 2.50;
            }
            else
            if (Point == 2.00)
            {
                R = 2.50;
            }
            else
            {
                R = 0.0;
            }
            return R;
        }
    }
}