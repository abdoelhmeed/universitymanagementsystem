﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UniversityManagementSystem.Areas.ResultModule.Models
{
    public class CourseInfoModel
    {
        public int Course_Id { get; set; }
        public string Course_Name { get; set; }
        public Nullable<double> Course_BasicGrade { get; set; }
        public Nullable<double> Course_AppendixGrade { get; set; }
        public Nullable<double> Course_HoursNumber { get; set; }
        public Nullable<double> Course_PointsNumber { get; set; }
        public string Course_Descirption { get; set; }
    }
}