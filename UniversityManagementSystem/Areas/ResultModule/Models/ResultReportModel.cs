﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UniversityManagementSystem.Areas.ResultModule.Models
{
    public class ViewResultReportModel
    {
        public int Result_Id { get; set; }
        public string Student_FullName { get; set; }
        public string Col1_courseName { get; set; }
        public string Col2_courseHours { get; set; }

        public float Result_ClassHours { get; set; }
        public float Result_ClassPoints { get; set; }
        public float Result_CumulativeHours { get; set; }
        public float Result_CumulativePoints { get; set; }
        public string Result_AcademicStatus { get; set; }
        
    }
}