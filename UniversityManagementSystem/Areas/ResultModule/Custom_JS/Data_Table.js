﻿$(document).ready(function (e) {
    if ($("S_Year_Id").val() != null)
    {
        document.getElementById('Year_Id').value = $("S_Year_Id").val();
    }
    var myTable =
        $('#BordResult ,#Result_td ,#Result_datatables ,#Apprecition_datatables')
            .DataTable({
                bAutoWidth: false,
                "aaSorting": [],
                dom: 'Bfrtip',
                buttons: [
                    {
                        extend: 'pdfHtml5',
                        download: 'open'
                    }
                ]
            });
})
