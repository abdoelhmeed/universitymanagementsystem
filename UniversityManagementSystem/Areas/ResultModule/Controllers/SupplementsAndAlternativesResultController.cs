﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UniversityManagementSystem.Areas.ResultModule.Models;
using UniversityManagementSystem.Models;

namespace UniversityManagementSystem.Areas.ResultModule.Controllers
{
    [Authorize]
    public class SupplementsAndAlternativesResultController : Controller
    {
        private UniversityManagementSystemEntities db = new UniversityManagementSystemEntities();
        ResultController RC = new ResultController();
        private string msg;
        private int don;
        // GET: ResultModule/SupplementsAndAlternativesResult
        [HttpGet]
        public ActionResult Index()
        {
            ViewBag.MainTitle = "Result Module";
            ViewBag.SubTitle = "Supplements And AlternativesResult";

            DropDown();

            List<GetStudantInfoModel> StdList = new List<GetStudantInfoModel>();
            var data = db.Re_StudentResult_data.ToList();

            foreach (var item in data)
            {
                GetStudantInfoModel row = new GetStudantInfoModel
                {
                    SR_Id = item.SR_Id,
                    StdId = item.R_StudentRegistration_data.Student_Id,
                    StdName = item.R_StudentRegistration_data.R_StudentInfo_data.Student_EmerInSName,
                    StdDegree = 0.0,
                    CourseStatus = "null"
                };
                StdList.Add(row);
            }
            ViewBag.StdInfo = StdList.ToList().Take(0);
            return View();
        }
        //
        [HttpPost]
        public ActionResult Index(EnterResultModel search)
        {
            ViewBag.MainTitle = "Result Module";
            ViewBag.SubTitle = "Supplements And AlternativesResult";

            DropDown();

            List<GetStudantInfoModel> StdList = new List<GetStudantInfoModel>();
            var data = db.Re_StudentResult_data.Where(s => s.R_StudentRegistration_data.Year_Id == search.Year_Id && s.R_StudentRegistration_data.Department_Id == search.Department_Id && 
                       s.R_StudentRegistration_data.batch_Id == search.batch_Id && s.R_StudentRegistration_data.Semester_Id == search.Semester_Id).ToList();

            foreach (var item in data)
            {
                var StdResultInfo = db.Re_StudentResultDescribtion.FirstOrDefault(s => s.Result_Id == item.Result_Id && s.C_CourseAssignToYear_data.Course_Id == search.Course_Id);
                if (StdResultInfo != null)
                {
                    if (StdResultInfo.SRD_CourseGrade < 50)
                    {
                        GetStudantInfoModel row = new GetStudantInfoModel
                        {
                            SR_Id = item.SR_Id,
                            StdId = item.R_StudentRegistration_data.Student_Id,
                            StdName = item.R_StudentRegistration_data.R_StudentInfo_data.Student_FullName,
                            StdDegree = StdResultInfo.SRD_CourseGrade,
                            CourseStatus = StdResultInfo.SRD_Status
                        };
                        StdList.Add(row);
                    }
                }
            }
            ViewBag.StdInfo = StdList.ToList();
            return View();
        }
        //GET: ResultModule/CalculationSupplementsAndAlternatives
        [HttpPost]
        public JsonResult InsertSupplementsAndAlternativesData(EnterResultModel data)
        {
            try
            {
                var CourseIsRegistred = db.C_CourseAssignToYear_data.Any(c => c.Year_Id == data.Year_Id && c.Department_Id == data.Department_Id && c.Semester_Id == data.Semester_Id && c.Course_Id == data.Course_Id);
                if (CourseIsRegistred == false)
                {
                    msg = "The material was not registered in the academic year.";
                }
                else
                {
                    var GetSubjectDegree = db.C_Courses_data.Find(data.Course_Id).Course_AppendixGrade;
                    foreach (var item in data.StdList)
                    {
                        
                        //Get Course Assign To Year number
                        int CATYId = db.C_CourseAssignToYear_data.FirstOrDefault(c => c.Year_Id == data.Year_Id && c.Department_Id == data.Department_Id && c.Semester_Id == data.Semester_Id && c.Course_Id == data.Course_Id).CATYId;

                        int ResultId = db.Re_StudentResult_data.FirstOrDefault(s => s.SR_Id == item.SR_Id).Result_Id;
                        //Get Studant Result Describtion number 
                        int SRDId = db.Re_StudentResultDescribtion.FirstOrDefault(sd => sd.CATYId == CATYId && sd.Result_Id == ResultId).SRD_Id;

                        var currentStatus = db.Re_StudentResultDescribtion.Find(SRDId).SRD_Status;
                        var NewStatus = item.CourseStatus;

                        //Get Studant Result number 
                        bool ResultIdIsExist = db.Re_StudentSupplementsAndAlternativesResult_data.Any(s => s.SR_Id == item.SR_Id);
                        if (ResultIdIsExist == false)
                        {
                            don = NewSupplementsAndAlternativesResult_data(item.SR_Id,currentStatus );
                            if (don > 0)
                            {
                                NewSupplementsAndAlternativesResultDescribtion(CATYId, item.StdDegree,currentStatus, NewStatus, item.SR_Id, GetSubjectDegree);
                                EditRe_StudentResultDescribtion(CATYId, item.StdDegree,currentStatus, NewStatus, item.SR_Id, GetSubjectDegree);
                            }
                        }
                        else
                        {
                            don = EditSupplementsAndAlternativesResult_data(item.SR_Id, item.CourseStatus);
                            if (don > 0)
                            {
                                bool SRDIdIsExist = db.Re_StudentSupplementsAndAlternativesResultDescribtion_data.Any(sd => sd.Re_StudentSupplementsAndAlternativesResult_data.SR_Id == item.SR_Id && sd.CATYId == CATYId);
                                if (SRDIdIsExist)
                                {
                                    EditSupplementsAndAlternativesResultDescribtion(CATYId, item.StdDegree ,currentStatus ,NewStatus, item.SR_Id, GetSubjectDegree);
                                    EditRe_StudentResultDescribtion(CATYId, item.StdDegree,currentStatus ,NewStatus, item.SR_Id, GetSubjectDegree);
                                }
                                else
                                {
                                    NewSupplementsAndAlternativesResultDescribtion(CATYId, item.StdDegree,currentStatus ,NewStatus, item.SR_Id, GetSubjectDegree);
                                    EditRe_StudentResultDescribtion(CATYId, item.StdDegree ,currentStatus ,NewStatus, item.SR_Id, GetSubjectDegree);
                                }
                            }
                        }
                    }
                    msg = "Material grades have been introduced " + db.C_Courses_data.Find(data.Course_Id).Course_Name;
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }
        //
        public int NewSupplementsAndAlternativesResult_data(int SR_Id , string CurrentCourseStatus)
        {
            Re_StudentSupplementsAndAlternativesResult_data srd = new Re_StudentSupplementsAndAlternativesResult_data();
            srd.SSAR_ClassHours = 0;
            srd.SSAR_ClassPoints = 0;
            srd.SSAR_CumulativeHours = 0;
            srd.SSAR_CumulativePoints = 0;
            srd.SSAR_Type = CurrentCourseStatus == "Alternative" ? "Alternative" : "Supplements";
            srd.SR_Id = SR_Id;
            db.Re_StudentSupplementsAndAlternativesResult_data.Add(srd);
            return db.SaveChanges();
        }
        //
        public int EditSupplementsAndAlternativesResult_data(int SR_Id, string CurrentCourseStatus)
        {
            Re_StudentSupplementsAndAlternativesResult_data srd = db.Re_StudentSupplementsAndAlternativesResult_data.FirstOrDefault(s => s.SR_Id == SR_Id);
            srd.SSAR_ClassHours = 0;
            srd.SSAR_ClassPoints = 0;
            srd.SSAR_CumulativeHours = 0;
            srd.SSAR_CumulativePoints = 0;
            srd.SSAR_Type = CurrentCourseStatus == "Alternative" ? "Alternative" : "Supplements";
            srd.SR_Id = SR_Id;
            db.Entry(srd).State = System.Data.Entity.EntityState.Modified;
            return db.SaveChanges();
        }
        //
        public int NewSupplementsAndAlternativesResultDescribtion(int CATYId, double StdDegree, string CurrentCourseStatus, string NewCourseStatus, int SR_Id ,double GetSubjectDegree)
        {
            //
            int SSARId = db.Re_StudentSupplementsAndAlternativesResult_data.FirstOrDefault(s => s.SR_Id == SR_Id).SSAR_Id;

            //Calculation
            double pointNumber = ((StdDegree / 20) - 1);
            Re_StudentSupplementsAndAlternativesResultDescribtion_data srdd = new Re_StudentSupplementsAndAlternativesResultDescribtion_data();
            if (CurrentCourseStatus == "Alternative")
            {
                srdd.SSARD_PointsNumber = Math.Round(pointNumber, 2);
                srdd.SSARD_CourseGrade = StdDegree;
                srdd.SSARD_PointsLetter = RC.Pointsletter(StdDegree);
            }
            else
            {
                srdd.SSARD_PointsNumber = StdDegree >= GetSubjectDegree ? 1.5: Math.Round(pointNumber, 2);
                srdd.SSARD_CourseGrade = StdDegree >= GetSubjectDegree ? 50: StdDegree;
                srdd.SSARD_PointsLetter = StdDegree >= GetSubjectDegree?"C*":"F*";
            }
            srdd.SSARD_Status = NewCourseStatus;
            srdd.SSAR_Id = SSARId;
            srdd.CATYId = CATYId;
            db.Re_StudentSupplementsAndAlternativesResultDescribtion_data.Add(srdd);
            return db.SaveChanges();
        }
        //
        public int EditSupplementsAndAlternativesResultDescribtion(int CATYId, double StdDegree, string CurrentCourseStatus, string NewCourseStatus, int SR_Id ,double GetSubjectDegree)
        {
            
            //Calculation
            double pointNumber =((StdDegree / 20) - 1);
            //
            int SSARId = db.Re_StudentSupplementsAndAlternativesResult_data.FirstOrDefault(s => s.SR_Id == SR_Id).SSAR_Id;
            //Get Studant Result Describtion number 
            int SSARDId = db.Re_StudentSupplementsAndAlternativesResultDescribtion_data.FirstOrDefault(sd => sd.CATYId == CATYId && sd.SSAR_Id == SSARId).SSARD_Id;

            Re_StudentSupplementsAndAlternativesResultDescribtion_data srdd = db.Re_StudentSupplementsAndAlternativesResultDescribtion_data.Find(SSARDId);
            
            if (CurrentCourseStatus == "Alternative")
            {

                srdd.SSARD_CourseGrade = StdDegree;
                srdd.SSARD_PointsNumber = Math.Round(pointNumber, 2);
                srdd.SSARD_PointsLetter = RC.Pointsletter(StdDegree);
            }
            else
            {
                srdd.SSARD_PointsNumber = StdDegree >= GetSubjectDegree ? 1.5 : Math.Round(pointNumber, 2);
                srdd.SSARD_CourseGrade = StdDegree >= GetSubjectDegree ? 50 : StdDegree;
                srdd.SSARD_PointsLetter = StdDegree >= GetSubjectDegree ? "C*" : "F*";
            }
            srdd.SSARD_Status = NewCourseStatus;
            srdd.SSAR_Id = SSARId;
            srdd.CATYId = CATYId;
            db.Entry(srdd).State = System.Data.Entity.EntityState.Modified;

            return db.SaveChanges();
        }
        //
        public int EditRe_StudentResultDescribtion(int CATYId, double StdDegree, string CurrentCourseStatus, string NewCourseStatus, int SR_Id ,double GetSubjectDegree)
        {
            //Calculation
            double pointNumber = ((StdDegree / 20) - 1);
            //
            int ResultId = db.Re_StudentResult_data.FirstOrDefault(s => s.SR_Id == SR_Id).Result_Id;
            //Get Studant Result Describtion number 
            int SRDId = db.Re_StudentResultDescribtion.FirstOrDefault(sd => sd.CATYId == CATYId && sd.Result_Id == ResultId).SRD_Id;

            Re_StudentResultDescribtion srdd = db.Re_StudentResultDescribtion.Find(SRDId);
            
            if (CurrentCourseStatus == "Alternative")
            {
                srdd.SRD_CourseGrade = StdDegree;
                srdd.SRD_PointsNumber = Math.Round(pointNumber, 2);
                srdd.SRD_Pointsletter = RC.Pointsletter(StdDegree);
            }
            else
            {
                srdd.SRD_CourseGrade = StdDegree >= GetSubjectDegree ? 50 : StdDegree;
                srdd.SRD_PointsNumber = StdDegree >= GetSubjectDegree ? 1.5 : Math.Round(pointNumber, 2);
                srdd.SRD_Pointsletter  = StdDegree >= GetSubjectDegree ? "C*" : "F*";
            }
            srdd.SRD_ExamSit = 2;
            srdd.SRD_Status = NewCourseStatus;
            srdd.Result_Id = db.Re_StudentResult_data.FirstOrDefault(r => r.SR_Id == SR_Id).Result_Id;
            srdd.CATYId = CATYId;
            db.Entry(srdd).State = System.Data.Entity.EntityState.Modified;

            return db.SaveChanges();
        }
        //
        public void DropDown()
        {
            R_StudyYear_data sd = new R_StudyYear_data { Year_Id = 0, Year_Name = " -- select Year -- " };
            var Year_Data = db.R_StudyYear_data.ToList();
            Year_Data.Add(sd);
            R_Departments_data dd = new R_Departments_data { Department_Id = 0, Department_Name = " -- select Department -- " };
            var Department_Data = db.R_Departments_data.ToList();
            Department_Data.Add(dd);
            R_Semester_data sed = new R_Semester_data { Semester_Id = 0, Semester_Name = " -- select Semester -- " };
            var Semester_Data = db.R_Semester_data.ToList();
            Semester_Data.Add(sed);
            R_BatchNumber_data bn = new R_BatchNumber_data { batch_Id = 0, batch_Name = " -- select Batch --" };
            var BatchNumber_Data = db.R_BatchNumber_data.ToList();
            BatchNumber_Data.Add(bn);
            C_Courses_data cd = new C_Courses_data { Course_Id = 0, Course_Name = "-- select Course -- " };
            var Courses_Data = db.C_Courses_data.ToList();
            Courses_Data.Add(cd);
            ViewBag.Year_Id = new SelectList(Year_Data.OrderBy(y => y.Year_Id), "Year_Id", "Year_Name");
            ViewBag.Department_Id = new SelectList(Department_Data.OrderBy(d => d.Department_Id), "Department_Id", "Department_Name");
            ViewBag.Semester_Id = new SelectList(Semester_Data.OrderBy(s => s.Semester_Id), "Semester_Id", "Semester_Name");
            ViewBag.batch_Id = new SelectList(BatchNumber_Data.OrderBy(b => b.batch_Id), "batch_Id", "batch_Name");
            ViewBag.Course_Id = new SelectList(Courses_Data.OrderBy(c => c.Course_Id), "Course_Id", "Course_Name");
        }
    }
}