﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using UniversityManagementSystem.Models;

namespace UniversityManagementSystem.Areas.ResultModule.Controllers
{
    [Authorize]
    public class AppreciationController : Controller
    {
        private UniversityManagementSystemEntities db = new UniversityManagementSystemEntities();

        // GET: ResultModule/Appreciation
        public ActionResult Index()
        {
            ViewBag.MainTitle = "Result Module";
            ViewBag.SubTitle = "Percentage Setting";

            return View(db.Re_Appreciation.ToList());
        }

        // GET: ResultModule/Appreciation/Details/5
        public ActionResult Details(int? id)
        {
            ViewBag.MainTitle = "Result Module";
            ViewBag.SubTitle = "Percentage Setting";

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Re_Appreciation re_Appreciation = db.Re_Appreciation.Find(id);
            if (re_Appreciation == null)
            {
                return HttpNotFound();
            }
            return View(re_Appreciation);
        }

        // GET: ResultModule/Appreciation/Create
        public ActionResult Create()
        {
            ViewBag.MainTitle = "Result Module";
            ViewBag.SubTitle = "Percentage Setting";
            return View();
        }

        // POST: ResultModule/Appreciation/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "A_Id,A_Appreciation_Text,A_Appreciation_Point")] Re_Appreciation re_Appreciation)
        {
            ViewBag.MainTitle = "Result Module";
            ViewBag.SubTitle = "Percentage Setting";
            if (ModelState.IsValid)
            {
                db.Re_Appreciation.Add(re_Appreciation);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(re_Appreciation);
        }

        // GET: ResultModule/Appreciation/Edit/5
        public ActionResult Edit(int? id)
        {
            ViewBag.MainTitle = "Result Module";
            ViewBag.SubTitle = "Percentage Setting";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Re_Appreciation re_Appreciation = db.Re_Appreciation.Find(id);
            if (re_Appreciation == null)
            {
                return HttpNotFound();
            }
            return View(re_Appreciation);
        }

        // POST: ResultModule/Appreciation/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "A_Id,A_Appreciation_Text,A_Appreciation_Point")] Re_Appreciation re_Appreciation)
        {
            ViewBag.MainTitle = "Result Module";
            ViewBag.SubTitle = "Percentage Setting";
            if (ModelState.IsValid)
            {
                db.Entry(re_Appreciation).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(re_Appreciation);
        }

        // GET: ResultModule/Appreciation/Delete/5
        public ActionResult Delete(int? id)
        {
            ViewBag.MainTitle = "Result Module";
            ViewBag.SubTitle = "Percentage Setting";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Re_Appreciation re_Appreciation = db.Re_Appreciation.Find(id);
            if (re_Appreciation == null)
            {
                return HttpNotFound();
            }
            return View(re_Appreciation);
        }

        // POST: ResultModule/Appreciation/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ViewBag.MainTitle = "Result Module";
            ViewBag.SubTitle = "Percentage Setting";
            Re_Appreciation re_Appreciation = db.Re_Appreciation.Find(id);
            db.Re_Appreciation.Remove(re_Appreciation);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
