﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UniversityManagementSystem.Models;
using UniversityManagementSystem.Areas.ResultModule.Reports;
using System.IO;

namespace UniversityManagementSystem.Areas.ResultModule.Controllers
{
    [Authorize]
    public class ResultReportController : Controller
    {
        UniversityManagementSystemEntities db = new UniversityManagementSystemEntities();

        //GET: ResultModule/ResultReport
        public ActionResult Index()
        {
            ViewBag.MainTitle = "Result Module";
            ViewBag.SubTitle = "Reports";

            return View();
        }
        //GET: ResultModule/ResultReport
        public ActionResult GeneralResultForStudent(int Std_Id ,string DeanOfAcademicAffairs)
        {
            try
            {
                var stddata = db.Re_StudentResult_data.OrderByDescending(r => r.Result_Id).FirstOrDefault(r => r.R_StudentRegistration_data.Student_Id == Std_Id);
                //if (stddata.R_StudentRegistration_data.Semester_Id == 36 || stddata.R_StudentRegistration_data.Semester_Id == 38)
                //{
                //CreateCERTIFICATE(stddata.Result_Id);
                    var data = db.Re_StudentResult_data.OrderByDescending(r => r.Result_Id).Where(r => r.R_StudentRegistration_data.Student_Id == Std_Id && r.Result_Id == stddata.Result_Id).Select(r => new
                    {
                        r.Result_Id,
                        r.SR_Id,
                        r.Result_CumulativePoints,
                        r.Result_AcademicStatus,
                        Student_FullName = r.R_StudentRegistration_data.R_StudentInfo_data.Student_FullName,
                        Student_img = r.R_StudentRegistration_data.R_StudentInfo_data.Student_img,
                        Department_Name = r.R_StudentRegistration_data.R_Departments_data.Department_Name,
                        Coll_Name = r.R_StudentRegistration_data.R_Departments_data.R_Colleges_data.Coll_Name,
                        Student_Id = r.R_StudentRegistration_data.Student_Id,
                        Student_Number = r.R_StudentRegistration_data.R_StudentInfo_data.Student_Number,
                        Student_Nationality = r.R_StudentRegistration_data.R_StudentInfo_data.Student_Nationality,
                        CertificateId = r.Re_CERTIFICATE.FirstOrDefault().CertificateId
                    }).ToList();

                    CrystalReportGeneralResult P = new CrystalReportGeneralResult();
                    P.Load(Path.Combine(Server.MapPath("~/Areas/ResultModule/Reports"), "CrystalReportGeneralResult.rpt"));
                    P.SetDataSource(data);
                    P.SetParameterValue("stdId", Std_Id);
                    P.SetParameterValue("DAA", DeanOfAcademicAffairs);
                    Response.Buffer = false;
                    Response.ClearContent();
                    Response.ClearHeaders();

                    Stream stream = P.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    return File(stream, "application/pdf", "StudantGeneralResult_data.pdf");
                //}
                //else
                //{
                //    return Json("The student has not yet graduated", JsonRequestBehavior.AllowGet);
                //}
            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }
        //GET: ResultModule/ResultReport
        public ActionResult AllStudantResult(int Std_Id ,bool ar=false)
        {
            try
            {
                dynamic data;
                if (ar == true)
                {
                    data = db.GetStudantResultAr(Std_Id).ToList();
                    StudantResult_ReportAr P = new StudantResult_ReportAr();
                    P.SetParameterValue("StdNumber", Std_Id);

                    P.Load(Path.Combine(Server.MapPath("~/Areas/ResultModule/Reports"), "StudantResult_ReportAr.rpt"));
                    P.SetDataSource(data);
                    Response.Buffer = false;
                    Response.ClearContent();
                    Response.ClearHeaders();

                    Stream stream = P.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    return File(stream, "application/pdf", "StudantResult_ReportAr.pdf");
                }
                else
                {
                     data = db.GetStudantResult(Std_Id).ToList();
                    StudantResult_Report P = new StudantResult_Report();
                    P.SetParameterValue("StdNumber", Std_Id);

                    P.Load(Path.Combine(Server.MapPath("~/Areas/ResultModule/Reports"), "StudantResult_Report.rpt"));
                    P.SetDataSource(data);
                    Response.Buffer = false;
                    Response.ClearContent();
                    Response.ClearHeaders();

                    Stream stream = P.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    return File(stream, "application/pdf", "StudantResult_data.pdf");
                }
            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }
        //GET: ResultModule/ResultReport
        public ActionResult SemesterStudantResult(int Std_Id ,int Sem_Id, bool ar = false)
        {
            dynamic data;
            if (ar == true)
            {
                data = db.GetSemesterStudantResultAr(Std_Id, Sem_Id).ToList();
                StudantresultForSemesterAr P = new StudantresultForSemesterAr();
                P.SetParameterValue("StdNumber", Std_Id);
                P.SetParameterValue("SemesterId", Sem_Id);

                P.Load(Path.Combine(Server.MapPath("~/Areas/ResultModule/Reports"), "StudantresultForSemesterAr.rpt"));
                P.SetDataSource(data);
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                Stream stream = P.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                return File(stream, "application/pdf", "StudantresultForSemesterAr.pdf");
            }
            else
            {
                data = db.GetSemesterStudantResult(Std_Id, Sem_Id).ToList();
                StudantresultForSemester P = new StudantresultForSemester();
                P.SetParameterValue("StdNumber", Std_Id);
                P.SetParameterValue("SemesterId", Sem_Id);

                P.Load(Path.Combine(Server.MapPath("~/Areas/ResultModule/Reports"), "StudantresultForSemester.rpt"));
                P.SetDataSource(data);
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                Stream stream = P.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                return File(stream, "application/pdf", "StudantresultForSemester.pdf");
            }

            
        }
        //GET: ResultModule/ResultReport
        public ActionResult ViewBordResult(int YId, int DId, int SId)
        {
            ViewBag.MainTitle = "Result Module";
            ViewBag.SubTitle = "Bord Result";

            var StudentResults = db.BoardResultStudents(YId, SId, DId).GroupBy(x => x.Student_Id).Select(x => x.First()).ToList();
            var coursesOfstudents = db.CourseOfResult(YId, SId, DId).GroupBy(x => x.Course_ShortName).Select(x => x.First()).ToList();
            ViewBag.coursesOfstudents = coursesOfstudents;
            ViewBag.StudentResults = StudentResults;
            ViewBag.YId = YId;
            ViewBag.DId = DId;
            ViewBag.SId = SId;

            return View();
        }
        //
        public ActionResult GetFristBordPage(int YId, int DId, int SId)
        {
            List<BordFristPage_Result> List = new List<BordFristPage_Result>();
            var data = db.BordFristPage(YId, SId, DId).ToList();
            foreach (var item in data)
            {
                if (List.Any(r => r.Course_Id == item.Course_Id) == false)
                {
                    BordFristPage_Result row = new BordFristPage_Result
                    {
                        Result_Id = item.Result_Id,
                        Coll_Name = item.Coll_Name,
                        Department_Name = item.Department_Name,
                        batch_Name = item.batch_Name,
                        Year_Name = item.Year_Name,
                        Semester_Name = item.Semester_Name,
                        Result_CumulativePoints = item.Result_CumulativePoints,

                        Course_Id = item.Course_Id,
                        Course_Name = item.Course_Name,
                        Course_HoursNumber = item.Course_HoursNumber,
                    };
                    List.Add(row);
                }
            }
            var SuccessfulStudents = db.Re_StudentResult_data.Count(r=> r.R_StudentRegistration_data.Year_Id == YId && r.R_StudentRegistration_data.Department_Id == DId && r.R_StudentRegistration_data.Semester_Id == SId && r.Result_ClassPoints >= 2.00);
            var StudentsRetarded = db.Re_StudentResult_data.Count(r=> r.R_StudentRegistration_data.Year_Id == YId && r.R_StudentRegistration_data.Department_Id == DId && r.R_StudentRegistration_data.Semester_Id == SId && r.Result_ClassPoints < 2.00 && r.Result_ClassPoints >= 1.50);
            var SeparationCases = db.Re_StudentResult_data.Count(r=> r.R_StudentRegistration_data.Year_Id == YId && r.R_StudentRegistration_data.Department_Id == DId && r.R_StudentRegistration_data.Semester_Id == SId && r.Result_ClassPoints < 1.50);
            var TotalNumber = db.R_StudentRegistration_data.Count(r=> r.Year_Id == YId && r.Department_Id == DId && r.Semester_Id == SId);
            var NumberSittingList = db.Re_StudentResult_data.Where(r => r.R_StudentRegistration_data.Year_Id == YId && r.R_StudentRegistration_data.Department_Id == DId && r.R_StudentRegistration_data.Semester_Id == SId).ToList();
            var NumberSitting = db.Re_StudentResult_data.Count(r => r.R_StudentRegistration_data.Year_Id == YId && r.R_StudentRegistration_data.Department_Id == DId && r.R_StudentRegistration_data.Semester_Id == SId);
            var WhoNotCompleted = 0;
            foreach (var item in NumberSittingList)
            {

                if(db.Re_StudentResultDescribtion.FirstOrDefault(d=> d.Result_Id == item.Result_Id ).SRD_Status != "Present")
                {
                    WhoNotCompleted++;
                }
            }

            CrystalReportBord P = new CrystalReportBord();
            P.Load(Path.Combine(Server.MapPath("~/Areas/ResultModule/Reports"), "CrystalReportBord.rpt"));
            P.SetDataSource(List.ToList());
            P.SetParameterValue("YId", Convert.ToInt32(YId));
            P.SetParameterValue("SId", Convert.ToInt32(SId));
            P.SetParameterValue("DId", Convert.ToInt32(DId));
            P.SetParameterValue("SuccessfulStudents", Convert.ToInt32(SuccessfulStudents));
            P.SetParameterValue("StudentsRetarded", Convert.ToInt32(StudentsRetarded));
            P.SetParameterValue("SeparationCases", Convert.ToInt32(SeparationCases));
            P.SetParameterValue("TotalNumber", Convert.ToInt32(TotalNumber));
            P.SetParameterValue("NumberSitting", Convert.ToInt32(NumberSitting));
            P.SetParameterValue("WhoCompleted", Convert.ToInt32(NumberSitting - WhoNotCompleted));
            P.SetParameterValue("WhoNotCompleted", Convert.ToInt32(WhoNotCompleted));
            
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();

            Stream stream = P.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            return File(stream, "application/pdf", "CrystalReportBord_data.pdf");
        }
        //GET: ResultModule/Create CERTIFICATE
        public void CreateCERTIFICATE(int id)
        {
            var find = db.Re_CERTIFICATE.Any(c => c.Result_Id == id);
            if (find == false)
            {
                Re_CERTIFICATE r = new Re_CERTIFICATE
                {
                    Result_Id = id
                };
                db.Re_CERTIFICATE.Add(r);
                db.SaveChanges();
            }
        }
        ////GET: ResultModule/ResultReport
        public JsonResult SemestersList(int id)
        {

            var SemesterList = db.R_StudentRegistration_data.Where(r => r.Student_Id == id).Select(r => new { r.Semester_Id, Semester_Name = r.R_Semester_data.Semester_Name }).ToList();
            return Json(SemesterList, JsonRequestBehavior.AllowGet);
        }
    }
}