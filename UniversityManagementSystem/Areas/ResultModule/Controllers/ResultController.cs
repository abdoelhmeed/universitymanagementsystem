﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UniversityManagementSystem.Models;
using UniversityManagementSystem.Areas.ResultModule.Models;

namespace UniversityManagementSystem.Areas.ResultModule.Controllers
{
    [Authorize]
    public class ResultController : Controller
    {
        private UniversityManagementSystemEntities db = new UniversityManagementSystemEntities();
        public ResultBase _base = new ResultBase();
        private string msg;
        private int don;
        private double CumulativeHours;
        private double CumulativePoints;

        // GET: ResultModule/Result
        [HttpGet]
        public ActionResult Index()
        {
            ViewBag.MainTitle = "Result Module";
            ViewBag.SubTitle = "Enter Result";

            DropDown();

            List<GetStudantInfoModel> StdList = new List<GetStudantInfoModel>();
            var data = db.R_StudentRegistration_data.Select(s => new { s.SR_Id, s.Student_Id, Student_FullName = s.R_StudentInfo_data.Student_FullName }).ToList().Take(0);
            foreach (var item in data)
            {
                GetStudantInfoModel row = new GetStudantInfoModel
                {
                    SR_Id = item.SR_Id,
                    StdId = item.Student_Id,
                    StdName = item.Student_FullName,
                    StdDegree = 00,
                    CourseStatus = "Present"
                };
                StdList.Add(row);
            }
            ViewBag.StdInfo = StdList.ToList();
            return View();
        }

        // GET: ResultModule/Result
        [HttpPost]
        public ActionResult Index(EnterResultModel search)
        {
            ViewBag.MainTitle = "Result Module";
            ViewBag.SubTitle = "Enter Result";
            DropDown();

            List<GetStudantInfoModel> StdList = new List<GetStudantInfoModel>();
            var data = db.R_StudentRegistration_data.Where(s => s.Year_Id == search.Year_Id && s.Department_Id == search.Department_Id && s.batch_Id == search.batch_Id && s.Semester_Id == search.Semester_Id && s.Registered == true).
            Select(s => new { s.SR_Id, s.Student_Id, Student_FullName = s.R_StudentInfo_data.Student_FullName }).ToList();
            foreach (var item in data)
            {
                var StdDeg = 0.0;
                var CourseStatus = "";
                bool HaveResult = db.Re_StudentResult_data.Any(s => s.SR_Id == item.SR_Id);
                if (HaveResult == true)
                {
                    //Get Studant Result number 
                    int ResultId = db.Re_StudentResult_data.FirstOrDefault(s => s.SR_Id == item.SR_Id).Result_Id;
                    if (db.Re_StudentResultDescribtion.Any(s => s.Result_Id == ResultId && s.C_CourseAssignToYear_data.Course_Id == search.Course_Id) == true)
                    {
                        StdDeg = db.Re_StudentResultDescribtion.FirstOrDefault(s => s.Result_Id == ResultId && s.C_CourseAssignToYear_data.Course_Id == search.Course_Id).SRD_CourseGrade;
                        CourseStatus = db.Re_StudentResultDescribtion.FirstOrDefault(s => s.Result_Id == ResultId && s.C_CourseAssignToYear_data.Course_Id == search.Course_Id).SRD_Status;
                    }
                    else
                    {
                        StdDeg = 0.0;
                        CourseStatus = "Present";
                    }
                }
                GetStudantInfoModel row = new GetStudantInfoModel
                {
                    SR_Id = item.SR_Id,
                    StdId = item.Student_Id,
                    StdName = item.Student_FullName,
                    StdDegree = StdDeg,
                    CourseStatus = CourseStatus
                };
                StdList.Add(row);
            }
            ViewBag.StdInfo = StdList.ToList();

            return View();
        }

        //GET: ResultModule/ResultForm
        [HttpPost]
        public JsonResult InsertResultData(EnterResultModel data)
        {
            try
            {
                var CourseIsRegistred = db.C_CourseAssignToYear_data.Any(c => c.Year_Id == data.Year_Id && c.Department_Id == data.Department_Id && c.Semester_Id == data.Semester_Id && c.Course_Id == data.Course_Id);
                if (CourseIsRegistred == false)
                {
                    msg = "لم يتم تسجيل المادة في السنة الدراسيتة بعد";
                }
                else
                {
                    foreach (var item in data.StdList)
                    {
                        //Get Studant Result number 
                        bool ResultIdIsExist = db.Re_StudentResult_data.Any(s => s.SR_Id == item.SR_Id);
                        if (ResultIdIsExist == false)
                        {
                            don = NewRe_StudentResult_data(item.SR_Id);
                            if (don > 0)
                            {
                                NewRe_StudentResultDescribtion(data.Year_Id, data.Department_Id, data.Semester_Id, data.Course_Id, item.StdDegree, item.CourseStatus, item.SR_Id);
                            }
                        }
                        else
                        {
                            don = EditRe_StudentResult_data(item.SR_Id);
                            if (don > 0)
                            {
                                //Get Course Assign To Year number
                                int CATYId = db.C_CourseAssignToYear_data.FirstOrDefault(c => c.Year_Id == data.Year_Id && c.Department_Id == data.Department_Id && c.Semester_Id == data.Semester_Id && c.Course_Id == data.Course_Id).CATYId;
                                //
                                int ResultId = db.Re_StudentResult_data.FirstOrDefault(s => s.SR_Id == item.SR_Id).Result_Id;
                                bool SRDIdIsExist = db.Re_StudentResultDescribtion.Any(sd => sd.CATYId == CATYId && sd.Result_Id == ResultId);
                                if (SRDIdIsExist)
                                {
                                    EditRe_StudentResultDescribtion(data.Year_Id, data.Department_Id, data.Semester_Id, data.Course_Id, item.StdDegree, item.CourseStatus, item.SR_Id);
                                }
                                else
                                {
                                    NewRe_StudentResultDescribtion(data.Year_Id, data.Department_Id, data.Semester_Id, data.Course_Id, item.StdDegree, item.CourseStatus, item.SR_Id);
                                }
                            }
                        }
                    }
                    msg = "Material grades have been introduced :" + db.C_Courses_data.Find(data.Course_Id).Course_Name;
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }

        //GET: ResultModule/SemesterResult
        [HttpGet]
        public ActionResult SemesterResult()
        {
            ViewBag.MainTitle = "Result Module";
            ViewBag.SubTitle = "Calculate Result";

            DropDown();
            List<GetStudantInfoModel> StdList = new List<GetStudantInfoModel>();
            var data = db.R_StudentRegistration_data.Select(s => new { s.SR_Id, s.Student_Id, Student_FullName = s.R_StudentInfo_data.Student_FullName }).ToList().Take(0);
            foreach (var item in data)
            {
                GetStudantInfoModel row = new GetStudantInfoModel
                {
                    SR_Id = item.SR_Id,
                    StdId = item.Student_Id,
                    StdName = item.Student_FullName
                };
                StdList.Add(row);
            }
            ViewBag.StdInfo = StdList.ToList();
            return View();
        }

        //POST: ResultModule/SemesterResult
        [HttpPost]
        public ActionResult SemesterResult(EnterResultModel search)
        {
            ViewBag.MainTitle = "Result Module";
            ViewBag.SubTitle = "Calculate Result";

            DropDown();

            List<GetStudantInfoModel> StdList = new List<GetStudantInfoModel>();
            var data = db.R_StudentRegistration_data.Where(s => s.Year_Id == search.Year_Id && s.Department_Id == search.Department_Id && s.batch_Id == search.batch_Id && s.Semester_Id == search.Semester_Id && s.Registered == true).
                Select(s => new { s.SR_Id, s.Student_Id, Student_FullName = s.R_StudentInfo_data.Student_FullName }).ToList();
            foreach (var item in data)
            {
                GetStudantInfoModel row = new GetStudantInfoModel
                {
                    SR_Id = item.SR_Id,
                    StdId = item.Student_Id,
                    StdName = item.Student_FullName
                };
                StdList.Add(row);
            }
            ViewBag.StdInfo = StdList.ToList();
            return View();
        }

        //GET: ResultModule/Cal_SemesterResult
        [HttpPost]
        public JsonResult CalculationSemesterResult(CalculationResultModel data)
        {
            foreach (var item in data.StdList)
            {
                //Get Studant Id 
                var stdid = db.Re_StudentResult_data.FirstOrDefault(r => r.SR_Id == item.SR_Id).R_StudentRegistration_data.Student_Id;
                //Get Studant Result number 
                int ResultId = db.Re_StudentResult_data.FirstOrDefault(s => s.SR_Id == item.SR_Id).Result_Id;
                //Get Number Of semester
                int SemesterNumber = db.R_Semester_data.Find(data.Semester_Id).Semester_Number;
                //Get Count Of semester
                int SemesterCount = db.Re_StudentResult_data.Count(s => s.R_StudentRegistration_data.Student_Id == stdid);
                //Get result of all courses for studant
                double CalPointsNumber = 0, TotalPointsNumber = 0;
                int Hours = 0, TotalHours = 0;
                var IsCheating = db.Re_StudentResultDescribtion.Any(r => r.Result_Id == ResultId && r.SRD_Status == "Cheating");
                var GetValue = db.Re_StudentResultDescribtion.Where(s => s.Result_Id == ResultId).ToList();

                foreach (var item1 in GetValue)
                {
                    Hours = db.C_CourseAssignToYear_data.FirstOrDefault(c => c.CATYId == item1.CATYId).C_Courses_data.Course_HoursNumber;
                    CalPointsNumber = Convert.ToDouble(item1.SRD_PointsNumber * Hours);
                    TotalHours = TotalHours + Hours;
                    TotalPointsNumber = Convert.ToDouble(TotalPointsNumber + CalPointsNumber);
                }
                var ClassPoints = Math.Round(TotalPointsNumber / TotalHours, 2);


                if (db.Re_StudentResult_data.Any(s => s.Result_Id == ResultId) == true)
                {
                    var CH = db.Re_StudentResult_data.Where(s => s.R_StudentRegistration_data.Student_Id == stdid).Sum(s => s.Result_ClassHours);
                    var CP = db.Re_StudentResult_data.Where(s => s.R_StudentRegistration_data.Student_Id == stdid).Sum(s => s.Result_ClassPoints);
                    CumulativeHours = CH == 0.0 ? TotalHours : CH;
                    CumulativePoints = CP == 0.0? ClassPoints : CP;
                }
                else
                {
                    CumulativeHours = TotalHours;
                    CumulativePoints = TotalPointsNumber;
                }
                var RHours = _base.RoundStudentgGrade(TotalHours);
                var RPoints = _base.RoundStudentgGrade(ClassPoints);
                Re_StudentResult_data srd = db.Re_StudentResult_data.Find(ResultId);
                srd.Result_CumulativeHours = CumulativeHours;
                //srd.Result_CumulativePoints = Math.Round((CumulativePoints / TotalHours) / SemesterNumber,2);
                srd.Result_CumulativePoints = Math.Round(CumulativePoints  / SemesterCount, 2);
                srd.Result_ClassHours = RHours;
                srd.Result_ClassPoints = RPoints;
                srd.Result_calculationNumber = srd.Result_calculationNumber + 1;
                srd.SR_Id = item.SR_Id;
                //if (CumulativePoints / TotalHours >= 2.00 && IsCheating == false){ srd.Result_AcademicStatus = "successful"; } else if (CumulativePoints / TotalHours >= 1.25 && CumulativePoints / TotalHours < 200 && IsCheating == false) { srd.Result_AcademicStatus = "Separated "; } else if (IsCheating == true) { } else { srd.Result_AcademicStatus = "filed"; }
                if (IsCheating == true) { srd.Result_AcademicStatus = "Cheating"; } else if (ClassPoints >= 2.00 && IsCheating == false) { srd.Result_AcademicStatus = "Passed"; } else if (ClassPoints >= 1.25 && ClassPoints < 2.00 && IsCheating == false) { srd.Result_AcademicStatus = "Eleminated"; } else { srd.Result_AcademicStatus = "Failed"; }
                db.Entry(srd).State = System.Data.Entity.EntityState.Modified;
                int don = db.SaveChanges();
            }
            msg = "Result was calculated for" + db.R_BatchNumber_data.Find(data.batch_Id).batch_Name + " Department " + db.R_Departments_data.Find(data.Department_Id).Department_Name;
            return Json(msg, JsonRequestBehavior.AllowGet);
        }

        //NewRe Student Result data
        public int NewRe_StudentResult_data(int SR_Id)
        {
            Re_StudentResult_data srd = new Re_StudentResult_data();
            srd.Result_ClassHours = 0;
            srd.Result_ClassPoints = 0;
            srd.Result_CumulativeHours = 0;
            srd.Result_CumulativePoints = 0;
            srd.Result_calculationNumber = 0;
            srd.SR_Id = SR_Id;
            db.Re_StudentResult_data.Add(srd);
            return db.SaveChanges();
        }
        
        //Edit Student Result data
        public int EditRe_StudentResult_data(int SR_Id)
        {
            Re_StudentResult_data srd = db.Re_StudentResult_data.FirstOrDefault(s => s.SR_Id == SR_Id);
            srd.Result_ClassHours = 0;
            srd.Result_ClassPoints = 0;
            srd.Result_CumulativeHours = 0;
            srd.Result_CumulativePoints = 0;
            srd.Result_calculationNumber = 0;
            srd.SR_Id = SR_Id;
            db.Entry(srd).State = System.Data.Entity.EntityState.Modified;
            return db.SaveChanges();
        }
        
        //New Student Result Describtion
        public int NewRe_StudentResultDescribtion(int YearId, int DepartId, int SemId, int CId, double StdDegree, string CourseStatus, int SR_Id)
        {
            //Get Course Assign To Year number
            int CATYId = db.C_CourseAssignToYear_data.FirstOrDefault(c => c.Year_Id == YearId && c.Department_Id == DepartId && c.Semester_Id == SemId && c.Course_Id == CId).CATYId;
            //Get Number of courses
            int CoursesNumber = db.C_CourseAssignToYear_data.Count(c => c.Year_Id == YearId && c.Department_Id == DepartId && c.Semester_Id == SemId) - 1;
            //Calculation
            double PointNum = CourseStatus == "Present" ? ((StdDegree / 20) - 1) : 0;
            Re_StudentResultDescribtion srdd = new Re_StudentResultDescribtion();
            srdd.SRD_CourseGrade = StdDegree;
            srdd.SRD_PointsNumber = Math.Round(PointNum,2);
            srdd.SRD_Pointsletter = Pointsletter(StdDegree);
            srdd.SRD_ExamSit = 1;
            srdd.SRD_Status = CourseStatus;
            srdd.Result_Id = db.Re_StudentResult_data.FirstOrDefault(r => r.SR_Id == SR_Id).Result_Id;
            srdd.CATYId = CATYId;
            db.Re_StudentResultDescribtion.Add(srdd);
            return db.SaveChanges();
        }
        
        //Edit Student Result Describtion
        public int EditRe_StudentResultDescribtion(int YearId, int DepartId, int SemId, int CId, double StdDegree, string CourseStatus, int SR_Id)
        {
            //Get Course Assign To Year number
            int CATYId = db.C_CourseAssignToYear_data.FirstOrDefault(c => c.Year_Id == YearId && c.Department_Id == DepartId && c.Semester_Id == SemId && c.Course_Id == CId).CATYId;
            //Get Number of courses
            int CoursesNumber = db.C_CourseAssignToYear_data.Count(c => c.Year_Id == YearId && c.Department_Id == DepartId && c.Semester_Id == SemId) - 1;
            //Calculation
            double PointNum = CourseStatus == "Present" ? ((StdDegree / 20) - 1):0;
            //
            int ResultId = db.Re_StudentResult_data.FirstOrDefault(s => s.SR_Id == SR_Id).Result_Id;
            //Get Studant Result Describtion number 
            int SRDId = db.Re_StudentResultDescribtion.FirstOrDefault(sd => sd.CATYId == CATYId && sd.Result_Id == ResultId).SRD_Id;

            Re_StudentResultDescribtion srdd = db.Re_StudentResultDescribtion.Find(SRDId);
            srdd.SRD_CourseGrade = StdDegree;
            srdd.SRD_PointsNumber = Math.Round(PointNum,2);
            srdd.SRD_Pointsletter = Pointsletter(StdDegree);
            srdd.SRD_ExamSit = 1;
            srdd.SRD_Status = CourseStatus;
            srdd.Result_Id = db.Re_StudentResult_data.FirstOrDefault(r => r.SR_Id == SR_Id).Result_Id;
            srdd.CATYId = CATYId;
            db.Entry(srdd).State = System.Data.Entity.EntityState.Modified;
            return db.SaveChanges();
        }
        
        //Get Courses by Department Id
        public JsonResult GetCourses(int sid ,int did)
        {
            var BatchNumber_data = db.C_CourseAssignToYear_data.Where(p => p.Department_Id == did && p.Semester_Id ==sid).Select(p => new { p.Course_Id, p.C_Courses_data.Course_Name }).ToList();
            return Json(BatchNumber_data, JsonRequestBehavior.AllowGet);
        }
        
        // Points by letter
        public string Pointsletter(double? Degree)
        {
            string R = "";
            int Count = 0;
            var Appreciation_result = db.Re_Appreciation.ToList();
            foreach (var item in Appreciation_result)
            {
                if (Degree >= item.A_Appreciation_Point && Count < 1)
                {
                    R = item.A_Appreciation_Text;
                    Count++;
                }
            }
            return R;
        }
        
        //Get  Semester data
        public void DropDown()
        {
            R_StudyYear_data sd = new R_StudyYear_data{Year_Id = 0,Year_Name = " -- select Year -- "};
            var Year_Data = db.R_StudyYear_data.ToList();
            Year_Data.Add(sd);
            R_Departments_data dd = new R_Departments_data { Department_Id = 0,Department_Name = " -- select Department -- " };
            var Department_Data = db.R_Departments_data.ToList();
            Department_Data.Add(dd);
            R_Semester_data sed = new R_Semester_data { Semester_Id = 0, Semester_Name = " -- select Semester -- " };
            var Semester_Data = db.R_Semester_data.ToList();
            Semester_Data.Add(sed);
            R_BatchNumber_data bn = new R_BatchNumber_data { batch_Id = 0, batch_Name = " -- select Batch --" };
            var BatchNumber_Data = db.R_BatchNumber_data.ToList();
            BatchNumber_Data.Add(bn);
            C_Courses_data cd = new C_Courses_data { Course_Id = 0, Course_Name = "-- select Course -- " };
            var Courses_Data = db.C_Courses_data.ToList();
            Courses_Data.Add(cd);
            ViewBag.Year_Id = new SelectList(Year_Data.OrderBy(y=>y.Year_Id), "Year_Id", "Year_Name");
            ViewBag.Department_Id = new SelectList(Department_Data.OrderBy(d => d.Department_Id), "Department_Id", "Department_Name");
            ViewBag.Semester_Id = new SelectList(Semester_Data.OrderBy(s=> s.Semester_Id), "Semester_Id", "Semester_Name");
            ViewBag.batch_Id = new SelectList(BatchNumber_Data.OrderBy(b=> b.batch_Id), "batch_Id", "batch_Name");
            ViewBag.Course_Id = new SelectList(Courses_Data.OrderBy(c=> c.Course_Id), "Course_Id", "Course_Name");
        }
    }
}