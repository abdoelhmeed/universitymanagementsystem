﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace UniversityManagementSystem.Areas.MobilModule.Models
{
    public class ResearchModel
    {
        [Display(Name ="ID")]
        public int ResID { get; set; }
        [Display(Name = "Title")]
        [Required]
        public string ReTitle { get; set; }
        [Display(Name = "Publisher")]
        [Required]
        public string RePublisher { get; set; }
        [Display(Name = "About")]
        [Required]
        public string ReAbout { get; set; }
        [Display(Name = "Url")]
        [Required]
        [DataType(DataType.Url)]
        public string ResUrl { get; set; }
        [Display(Name = "Date")]
        [Required]
        [DataType(DataType.Date)]
        public DateTime Redate { get; set; }
        [Display(Name = "College Name")]
        [Required]
        public int CollegeID { get; set; }
    }
}