﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UniversityManagementSystem.Areas.MobilModule.Models;
using UniversityManagementSystem.Models;
using System.ComponentModel.DataAnnotations;


namespace UniversityManagementSystem.Areas.MobilModule.Models
{
    public class StdEvents
    {
        [Display(Name ="ID")]
       
        public int EventID { get; set; }
        [Display(Name ="Title")]
        [Required]
        public string EventTitle { get; set; }
        [Display(Name = "Body")]
        [Required]
        public string EventSubject { get; set; }
        [Display(Name = "Date")]
        [Required]
        public DateTime EventDate { get; set; }
        public HttpPostedFileBase[] img { get; set; }
    }
}