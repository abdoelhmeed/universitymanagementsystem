﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace UniversityManagementSystem.Areas.MobilModule.Models
{
    public class StudentTimeTableModel
    {
        [Display(Name ="ID")]
        public int TTId { get; set; }
        public string TTFile { get; set; }
        public HttpPostedFileBase File { get; set; }
        public Nullable<int> TSemID { get; set; }
        public Nullable<int> TDep { get; set; }
        public Nullable<int> Tyeary { get; set; }
        [Display(Name = "Describe")]
        [Required]
        public string TDiscrbions { get; set; }
    }
}