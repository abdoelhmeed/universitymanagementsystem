﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace UniversityManagementSystem.Areas.MobilModule.Models
{
    public class StudentSheetsModel
    {
        [Display(Name =("ID"))]
        public int SheetID { get; set; }
        [Display(Name = ("Subject Name"))]
        [Required(ErrorMessage = "Subject Name is Required")]
        public string SubjectName { get; set; }
        [Display(Name = ("Department"))]
        [Required(ErrorMessage = "Department is Required")]
        public Nullable<int> Department_Id { get; set; }
        [Display(Name = ("Sheet Semester"))]
        [Required(ErrorMessage = "Department is Required")]
        public Nullable<int> SheetSemester { get; set; }
        public HttpPostedFileBase PDF { get; set; }
        [Display(Name = ("Sheet Semester"))]
        [Required(ErrorMessage = "Describe is Required")]
        public string SheetDescribe { get; set; }
    }
}