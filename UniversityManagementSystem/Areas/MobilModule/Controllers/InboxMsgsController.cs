﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using UniversityManagementSystem.Models;

namespace UniversityManagementSystem.Areas.MobilModule.Controllers
{
    public class InboxMsgsController : Controller
    {
        private UniversityManagementSystemEntities db = new UniversityManagementSystemEntities();

        // GET: MobilModule/InboxMsgs
        public ActionResult Index()
        {
            ViewBag.MainTitle = "Mobil Module";
            ViewBag.SubTitle = "Message";
            
            var inboxMsgs = db.InboxMsgs.Include(i => i.R_Departments_data).Include(i => i.R_Semester_data);
            return View(inboxMsgs.ToList());
        }

        // GET: MobilModule/InboxMsgs/Details/5
  

        // GET: MobilModule/InboxMsgs/Create
        public ActionResult Create()
        {
            ViewBag.MainTitle = "Mobil Module";
            ViewBag.SubTitle = "Message";
            ViewBag.Department_Id = new SelectList(db.R_Departments_data, "Department_Id", "Department_Name");
            ViewBag.Semester_Id = new SelectList(db.R_Semester_data, "Semester_Id", "Semester_Name");
            return View();
        }

        // POST: MobilModule/InboxMsgs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "InMsgId,InMsgTitle,InMsgBody,Department_Id,Semester_Id")] InboxMsg inboxMsg)
        {
            ViewBag.MainTitle = "Mobil Module";
            ViewBag.SubTitle = "Message";

            if (ModelState.IsValid)
            {
                db.InboxMsgs.Add(inboxMsg);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Department_Id = new SelectList(db.R_Departments_data, "Department_Id", "Department_Name", inboxMsg.Department_Id);
            ViewBag.Semester_Id = new SelectList(db.R_Semester_data, "Semester_Id", "Semester_Name", inboxMsg.Semester_Id);
            return View(inboxMsg);
        }

        // GET: MobilModule/InboxMsgs/Delete/5
        public ActionResult Delete(int? id)
        {
            ViewBag.MainTitle = "Mobil Module";
            ViewBag.SubTitle = "Message";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            InboxMsg inboxMsg = db.InboxMsgs.Find(id);
            if (inboxMsg == null)
            {
                return HttpNotFound();
            }
            return View(inboxMsg);
        }

        // POST: MobilModule/InboxMsgs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            InboxMsg inboxMsg = db.InboxMsgs.Find(id);
            db.InboxMsgs.Remove(inboxMsg);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
