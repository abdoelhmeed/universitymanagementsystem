﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using UniversityManagementSystem.Models;
using UniversityManagementSystem.Areas.MobilModule.Models;
using System.IO;

namespace UniversityManagementSystem.Areas.MobilModule.Controllers
{
    public class ResearchesController : Controller
    {
        private UniversityManagementSystemEntities db = new UniversityManagementSystemEntities();

        // GET: MobilModule/Researches
        public ActionResult Index()
        {
            ViewBag.MainTitle = "Researches";
            ViewBag.SubTitle = "index";
            return View(db.Researches.ToList());
        }

       
        public ActionResult Create()
        {
            ViewBag.Colg = new SelectList(db.R_Colleges_data, "Coll_Id", "Coll_Name");
            ViewBag.MainTitle = "Researches";
            ViewBag.SubTitle = "Create";
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ResearchModel model)
        {
            if (ModelState.IsValid)
            {
              
                Research research = new Research
                {
                    RePublisher = model.RePublisher,
                    ResUrl= model.ResUrl,
                    ReTitle=model.ReTitle,
                    ReAbout=model.ReAbout,
                    CollegeID=model.CollegeID,
                    Redate=model.Redate
                   
                };
                db.Researches.Add(research);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Colg = new SelectList(db.R_Colleges_data, "Coll_Id", "Coll_Name",model.CollegeID);
            return View(model);
        }

     

        // GET: MobilModule/Researches/Delete/5
        public ActionResult Delete(int? id)
        {
            ViewBag.MainTitle = "Researches";
            ViewBag.SubTitle = "Delete";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Research research = db.Researches.Find(id);
            if (research == null)
            {
                return HttpNotFound();
            }
            return View(research);
        }

        // POST: MobilModule/Researches/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ViewBag.MainTitle = "Researches";
            ViewBag.SubTitle = "Delete";
            Research research = db.Researches.Find(id);
            db.Researches.Remove(research);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
