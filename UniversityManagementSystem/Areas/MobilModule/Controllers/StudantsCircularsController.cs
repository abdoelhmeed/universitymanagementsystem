﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using UniversityManagementSystem.Models;

namespace UniversityManagementSystem.Areas.MobilModule.Controllers
{
    public class StudantsCircularsController : Controller
    {
        private UniversityManagementSystemEntities db = new UniversityManagementSystemEntities();

        // GET: MobilModule/StudantsCirculars
        public ActionResult Index()
        {
            ViewBag.MainTitle = "Mobil Module";
            ViewBag.SubTitle = "Studants Circulars";
            return View(db.StudantsCirculars.ToList());
        }

        // GET: MobilModule/StudantsCirculars/Details/5
        public ActionResult Details(int? id)
        {
            ViewBag.MainTitle = "Mobil Module";
            ViewBag.SubTitle = "Studants Circulars";

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            StudantsCircular studantsCircular = db.StudantsCirculars.Find(id);
            if (studantsCircular == null)
            {
                return HttpNotFound();
            }
            return View(studantsCircular);
        }

        // GET: MobilModule/StudantsCirculars/Create
        public ActionResult Create()
        {
            ViewBag.MainTitle = "Mobil Module";
            ViewBag.SubTitle = "Studants Circulars";

            return View();
        }

        // POST: MobilModule/StudantsCirculars/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "StdCirId,StdCirTitle,StdCirBody,StdCirDate")] StudantsCircular studantsCircular)
        {
            ViewBag.MainTitle = "Mobil Module";
            ViewBag.SubTitle = "Studants Circulars";

            if (ModelState.IsValid)
            {
                db.StudantsCirculars.Add(studantsCircular);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(studantsCircular);
        }

        // GET: MobilModule/StudantsCirculars/Edit/5
        public ActionResult Edit(int? id)
        {
            ViewBag.MainTitle = "Mobil Module";
            ViewBag.SubTitle = "Studants Circulars";

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            StudantsCircular studantsCircular = db.StudantsCirculars.Find(id);
            if (studantsCircular == null)
            {
                return HttpNotFound();
            }
            return View(studantsCircular);
        }

        // POST: MobilModule/StudantsCirculars/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "StdCirId,StdCirTitle,StdCirBody,StdCirDate")] StudantsCircular studantsCircular)
        {
            ViewBag.MainTitle = "Mobil Module";
            ViewBag.SubTitle = "Studants Circulars";

            if (ModelState.IsValid)
            {
                db.Entry(studantsCircular).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(studantsCircular);
        }

        // GET: MobilModule/StudantsCirculars/Delete/5
        public ActionResult Delete(int? id)
        {
            ViewBag.MainTitle = "Mobil Module";
            ViewBag.SubTitle = "Studants Circulars";

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            StudantsCircular studantsCircular = db.StudantsCirculars.Find(id);
            if (studantsCircular == null)
            {
                return HttpNotFound();
            }
            return View(studantsCircular);
        }

        // POST: MobilModule/StudantsCirculars/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ViewBag.MainTitle = "Mobil Module";
            ViewBag.SubTitle = "Studants Circulars";

            StudantsCircular studantsCircular = db.StudantsCirculars.Find(id);
            db.StudantsCirculars.Remove(studantsCircular);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
