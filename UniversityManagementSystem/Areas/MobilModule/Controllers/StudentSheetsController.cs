﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using UniversityManagementSystem.Models;
using UniversityManagementSystem.Areas.MobilModule.Models;
using System.IO;

namespace UniversityManagementSystem.Areas.MobilModule.Controllers
{
    public class StudentSheetsController : Controller
    {
        private UniversityManagementSystemEntities db = new UniversityManagementSystemEntities();

        // GET: MobilModule/StudentSheets
        public ActionResult Index()
        {
            ViewBag.MainTitle = "Student Sheets";
            ViewBag.SubTitle = "index";
            var studentSheets = db.StudentSheets.Include(s => s.R_Departments_data).Include(s => s.R_Semester_data);
            return View(studentSheets.ToList());
        }


        // GET: MobilModule/StudentSheets/Create
        public ActionResult Create()
        {
            ViewBag.MainTitle = "Student Sheets";
            ViewBag.SubTitle = "Create";
            ViewBag.Department_Id = new SelectList(db.R_Departments_data, "Department_Id", "Department_Name");
            ViewBag.SheetSemester = new SelectList(db.R_Semester_data, "Semester_Id", "Semester_Name");
            return View();
        }

        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create( StudentSheetsModel model)
        {
            ViewBag.MainTitle = "Student Sheets";
            ViewBag.SubTitle = "Create";
            if (ModelState.IsValid)
            {
                string appFeilName = Path.GetFileNameWithoutExtension(model.PDF.FileName);
                string extension = Path.GetExtension(model.PDF.FileName);
                appFeilName = db.R_Departments_data.Find(model.Department_Id).Department_Name+"-"+db.R_Semester_data.Find(model.SheetSemester).Semester_Name+"-" + DateTime.Now.ToString("yymmssfff") + extension;
                string PDFName = appFeilName;
                appFeilName = Path.Combine(Server.MapPath("~/Attachments/"), appFeilName);
                model.PDF.SaveAs(appFeilName);
                StudentSheet studentSheet = new StudentSheet()
                {
                    Department_Id=model.Department_Id,
                    SheetSemester=model.SheetSemester,
                    SubjectName=model.SubjectName,
                    SheetUrl=PDFName,
                    SheetDescribe=model.SheetDescribe
                    
                };
                db.StudentSheets.Add(studentSheet);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Department_Id = new SelectList(db.R_Departments_data, "Department_Id", "Department_Name", model.Department_Id);
            ViewBag.SheetSemester = new SelectList(db.R_Semester_data, "Semester_Id", "Semester_Name", model.SheetSemester);
            return View(model);
        }



        // GET: MobilModule/StudentSheets/Delete/5
        public ActionResult Delete(int? id)
        {
            ViewBag.MainTitle = "Student Sheets";
            ViewBag.SubTitle = "Delete";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            StudentSheet studentSheet = db.StudentSheets.Find(id);
            if (studentSheet == null)
            {
                return HttpNotFound();
            }
            return View(studentSheet);
        }

        // POST: MobilModule/StudentSheets/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            
            StudentSheet studentSheet = db.StudentSheets.Find(id);
            db.StudentSheets.Remove(studentSheet);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
