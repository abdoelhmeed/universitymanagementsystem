﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using UniversityManagementSystem.Areas.MobilModule.Models;
using UniversityManagementSystem.Models;

namespace UniversityManagementSystem.Areas.MobilModule.Controllers
{
    [Authorize]
    public class StudentTimeTablesController : Controller
    {
        private UniversityManagementSystemEntities db = new UniversityManagementSystemEntities();

        // GET: MobilModule/StudentTimeTables
        public ActionResult Index()
        {
            ViewBag.MainTitle = "Mobil Module";
            ViewBag.SubTitle = "Time Tables";
            var studentTimeTables = db.StudentTimeTables.Include(s => s.R_Departments_data).Include(s => s.R_Semester_data).Include(s => s.R_StudyYear_data);
            return View(studentTimeTables.ToList());
        }

        // GET: MobilModule/StudentTimeTables/Details/5
        public ActionResult Details(int? id)
        {
            ViewBag.MainTitle = "Mobil Module";
            ViewBag.SubTitle = "Time Tables";

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            StudentTimeTable studentTimeTable = db.StudentTimeTables.Find(id);
            if (studentTimeTable == null)
            {
                return HttpNotFound();
            }
            return View(studentTimeTable);
        }

        // GET: MobilModule/StudentTimeTables/Create
        public ActionResult Create()
        {
            ViewBag.MainTitle = "Mobil Module";
            ViewBag.SubTitle = "Time Tables";

            ViewBag.TDep = new SelectList(db.R_Departments_data, "Department_Id", "Department_Name");
            ViewBag.TSemID = new SelectList(db.R_Semester_data, "Semester_Id", "Semester_Name");
            ViewBag.Tyeary = new SelectList(db.R_StudyYear_data, "Year_Id", "Year_Name");
            return View();
        }

        // POST: MobilModule/StudentTimeTables/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(StudentTimeTableModel model)
        {
            ViewBag.MainTitle = "Mobil Module";
            ViewBag.SubTitle = "Time Tables";

            if (ModelState.IsValid)
            {
                int ID = Convert.ToInt32(DateTime.Now.ToString("yymmssfff"));
                string appFeilName = Path.GetFileNameWithoutExtension(model.File.FileName);
                string extension = Path.GetExtension(model.File.FileName);
                appFeilName = model.TTFile + DateTime.Now.ToString("yymmssfff") + extension;
                string PDFName = appFeilName;
                appFeilName = Path.Combine("/Attachments", DateTime.Now.Ticks.ToString() + appFeilName); ;

                string FilePath = HostingEnvironment.MapPath(appFeilName);
                model.File.SaveAs(FilePath);
                StudentTimeTable StdTimeTable = new StudentTimeTable() {
                    TDep = model.TDep,
                    TSemID = model.TSemID,
                    TDiscrbions = model.TDiscrbions,
                    TTFile = appFeilName,
                    Tyeary = model.Tyeary,
                    TTId = ID
                };
                db.StudentTimeTables.Add(StdTimeTable);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.TDep = new SelectList(db.R_Departments_data, "Department_Id", "Department_Name", model.TDep);
            ViewBag.TSemID = new SelectList(db.R_Semester_data, "Semester_Id", "Semester_Name", model.TSemID);
            ViewBag.Tyeary = new SelectList(db.R_StudyYear_data, "Year_Id", "Year_Name", model.Tyeary);
            return View(model);
        }



        // GET: MobilModule/StudentTimeTables/Delete/5
        public ActionResult Delete(int? id)
        {
            ViewBag.MainTitle = "Mobil Module";
            ViewBag.SubTitle = "Time Tables";

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            StudentTimeTable studentTimeTable = db.StudentTimeTables.Find(id);
            if (studentTimeTable == null)
            {
                return HttpNotFound();
            }
            return View(studentTimeTable);
        }

        // POST: MobilModule/StudentTimeTables/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ViewBag.MainTitle = "Mobil Module";
            ViewBag.SubTitle = "Time Tables";

            StudentTimeTable studentTimeTable = db.StudentTimeTables.Find(id);
            db.StudentTimeTables.Remove(studentTimeTable);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
