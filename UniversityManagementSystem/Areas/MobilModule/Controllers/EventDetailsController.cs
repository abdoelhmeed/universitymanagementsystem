﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using UniversityManagementSystem.Models;
using UniversityManagementSystem.Areas.MobilModule.Models;
using System.IO;

namespace UniversityManagementSystem.Areas.MobilModule.Controllers
{
    public class EventDetailsController : Controller
    {
        private UniversityManagementSystemEntities db = new UniversityManagementSystemEntities();
        // GET: MobilModule/EventDetails
        public ActionResult Index()
        {
            ViewBag.MainTitle = "Mobil Module";
            ViewBag.SubTitle = "Event";
            return View(db.EventDetails.ToList());
        }
        // GET: MobilModule/EventDetails/Create
        public ActionResult Create()
        {
            ViewBag.MainTitle = "Mobil Module";
            ViewBag.SubTitle = "Event";
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(StdEvents model)
        {
            ViewBag.MainTitle = "Mobil Module";
            ViewBag.SubTitle = "Event";
            if (ModelState.IsValid)
            {
                EventDetail eventDetail = new EventDetail()
                {
                    EventDate=model.EventDate,
                    EventSubject=model.EventSubject,
                    EventTitle=model.EventTitle,
                   
                };
                db.EventDetails.Add(eventDetail);
                db.SaveChanges();
                foreach (var item in model.img.ToList())
                {
                    string appFeilName = Path.GetFileNameWithoutExtension(item.FileName);
                    string extension = Path.GetExtension(item.FileName);
                    appFeilName =model.EventTitle + DateTime.Now.ToString("yymmssfff") + extension;
                    string PDFName = appFeilName;
                    appFeilName = Path.Combine(Server.MapPath("~/Attachments/"), appFeilName);
                    item.SaveAs(appFeilName);
                    EventImg Img = new EventImg()
                    {
                        EvimgName = PDFName,
                        EventID = eventDetail.EventID

                       
                    };
                    db.EventImgs.Add(Img);
                    db.SaveChanges();
                   
                }
            
                return RedirectToAction("Index");
            }

            return View(model);
        }
        public ActionResult Delete(int? id)
        {
            ViewBag.MainTitle = "Mobil Module";
            ViewBag.SubTitle = "Event";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EventDetail eventDetail = db.EventDetails.Find(id);
            if (eventDetail == null)
            {
                return HttpNotFound();
            }
            return View(eventDetail);
        }
        // POST: MobilModule/EventDetails/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            List<EventImg> EventImgList = db.EventImgs.Where(u => u.EventID == id).ToList();
            foreach (var item in EventImgList)
            {
                EventImg EventImgSingle = db.EventImgs.Find(item.EvimgID);

                db.EventImgs.Remove(EventImgSingle);
                db.SaveChanges();
            }
            EventDetail eventDetail = db.EventDetails.Find(id);
            db.EventDetails.Remove(eventDetail);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
