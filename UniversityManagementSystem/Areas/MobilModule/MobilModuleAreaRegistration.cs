﻿using System.Web.Mvc;

namespace UniversityManagementSystem.Areas.MobilModule
{
    public class MobilModuleAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "MobilModule";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "MobilModule_default",
                "MobilModule/{controller}/{action}/{id}",
                new {Controller= "MobilModule", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}