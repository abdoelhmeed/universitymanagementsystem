﻿using System.Web.Mvc;

namespace UniversityManagementSystem.Areas.AcademicAffairsModule
{
    public class AcademicAffairsModuleAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "AcademicAffairsModule";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "AcademicAffairsModule_default",
                "AcademicAffairsModule/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}