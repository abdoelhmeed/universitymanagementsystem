﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using UniversityManagementSystem.Models;

namespace UniversityManagementSystem.Areas.AcademicAffairsModule.Controllers
{
    [Authorize]
    public class CoursesDataController : Controller
    {
        private UniversityManagementSystemEntities db = new UniversityManagementSystemEntities();

        // GET: AcademicAffairsModule/CoursesData
        public ActionResult Index()
        {
            ViewBag.MainTitle = "Academic Affairs Module";
            ViewBag.SubTitle  = "Coures Data";

            return View(db.C_Courses_data.ToList());
        }

        // GET: AcademicAffairsModule/CoursesData/Details/5
        public ActionResult Details(int? id)
        {
            ViewBag.MainTitle = "Academic Affairs Module";
            ViewBag.SubTitle = "View Coures Data";

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            C_Courses_data c_Courses_data = db.C_Courses_data.Find(id);
            if (c_Courses_data == null)
            {
                return HttpNotFound();
            }
            return View(c_Courses_data);
        }

        // GET: AcademicAffairsModule/CoursesData/Create
        public ActionResult Create()
        {
            ViewBag.MainTitle = "Academic Affairs Module";
            ViewBag.SubTitle = "Enter Coures Data";

            return View();
        }

        // POST: AcademicAffairsModule/CoursesData/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Course_Id,Course_Name,Course_ShortName,Course_BasicGrade,Course_AppendixGrade,Course_HoursNumber,Course_PointsNumber,Course_Descirption ,Course_ShortNameAR , Course_NameAR")] C_Courses_data c_Courses_data)
        {
            ViewBag.MainTitle = "Academic Affairs Module";
            ViewBag.SubTitle = "Enter Coures Data";

            if (ModelState.IsValid)
            {
                db.C_Courses_data.Add(c_Courses_data);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(c_Courses_data);
        }

        // GET: AcademicAffairsModule/CoursesData/Edit/5
        public ActionResult Edit(int? id)
        {
            ViewBag.MainTitle = "Academic Affairs Module";
            ViewBag.SubTitle  = "Edit Coures Data";

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            C_Courses_data c_Courses_data = db.C_Courses_data.Find(id);
            if (c_Courses_data == null)
            {
                return HttpNotFound();
            }
            return View(c_Courses_data);
        }

        // POST: AcademicAffairsModule/CoursesData/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Course_Id,Course_Name,Course_ShortName,Course_BasicGrade,Course_AppendixGrade,Course_HoursNumber,Course_PointsNumber,Course_Descirption ,Course_ShortNameAR , Course_NameAR")] C_Courses_data c_Courses_data)
        {
            ViewBag.MainTitle = "Academic Affairs Module";
            ViewBag.SubTitle = "Edit Coures Data";
            if (ModelState.IsValid)
            {
                db.Entry(c_Courses_data).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(c_Courses_data);
        }

        // GET: AcademicAffairsModule/CoursesData/Delete/5
        public ActionResult Delete(int? id)
        {
            ViewBag.MainTitle = "Academic Affairs Module";
            ViewBag.SubTitle = "Delete Coures Data";

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            C_Courses_data c_Courses_data = db.C_Courses_data.Find(id);
            if (c_Courses_data == null)
            {
                return HttpNotFound();
            }
            return View(c_Courses_data);
        }

        // POST: AcademicAffairsModule/CoursesData/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ViewBag.MainTitle = "Academic Affairs Module";
            ViewBag.SubTitle = "Delete Coures Data";

            C_Courses_data c_Courses_data = db.C_Courses_data.Find(id);
            db.C_Courses_data.Remove(c_Courses_data);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
