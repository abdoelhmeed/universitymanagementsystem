﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using UniversityManagementSystem.Models;

namespace UniversityManagementSystem.Areas.AcademicAffairsModule.Controllers
{
    [Authorize]
    public class TeachersDataController : Controller
    {
        private UniversityManagementSystemEntities db = new UniversityManagementSystemEntities();

        // GET: AcademicAffairsModule/TeachersData
        public ActionResult Index()
        {
            ViewBag.MainTitle = "Academic Affairs Module";
            ViewBag.SubTitle = "Teachers Data";
            var c_Teachers_data = db.C_Teachers_data.Include(c => c.Designation).Include(c => c.R_Departments_data);
            return View(c_Teachers_data.ToList());
        }

        // GET: AcademicAffairsModule/TeachersData/Details/5
        public ActionResult Details(int? id)
        {
            ViewBag.MainTitle = "Academic Affairs Module";
            ViewBag.SubTitle = "View Teachers Data";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            C_Teachers_data c_Teachers_data = db.C_Teachers_data.Find(id);
            if (c_Teachers_data == null)
            {
                return HttpNotFound();
            }
            return View(c_Teachers_data);
        }

        // GET: AcademicAffairsModule/TeachersData/Create
        public ActionResult Create()
        {
            ViewBag.MainTitle = "Academic Affairs Module";
            ViewBag.SubTitle = "Create Teachers Data";
            ViewBag.Designation_Id = new SelectList(db.Designations, "Designation_Id", "Title");
            ViewBag.Department_Id = new SelectList(db.R_Departments_data, "Department_Id", "Department_Name");
            return View();
        }

        // POST: AcademicAffairsModule/TeachersData/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Teacher_Id,Teacher_Name,Teacher_Address,Teacher_Email,Teacher_Phone,Department_Id,Designation_Id")] C_Teachers_data c_Teachers_data)
        {
            ViewBag.MainTitle = "Academic Affairs Module";
            ViewBag.SubTitle = "Create Teachers Data";
            if (ModelState.IsValid)
            {
                db.C_Teachers_data.Add(c_Teachers_data);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Designation_Id = new SelectList(db.Designations, "Designation_Id", "Title", c_Teachers_data.Designation_Id);
            ViewBag.Department_Id = new SelectList(db.R_Departments_data, "Department_Id", "Department_Name", c_Teachers_data.Department_Id);
            return View(c_Teachers_data);
        }

        // GET: AcademicAffairsModule/TeachersData/Edit/5
        public ActionResult Edit(int? id)
        {
            ViewBag.MainTitle = "Academic Affairs Module";
            ViewBag.SubTitle = "Edit Teachers Data";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            C_Teachers_data c_Teachers_data = db.C_Teachers_data.Find(id);
            if (c_Teachers_data == null)
            {
                return HttpNotFound();
            }
            ViewBag.Designation_Id = new SelectList(db.Designations, "Designation_Id", "Title", c_Teachers_data.Designation_Id);
            ViewBag.Department_Id = new SelectList(db.R_Departments_data, "Department_Id", "Department_Name", c_Teachers_data.Department_Id);
            return View(c_Teachers_data);
        }

        // POST: AcademicAffairsModule/TeachersData/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Teacher_Id,Teacher_Name,Teacher_Address,Teacher_Email,Teacher_Phone,Department_Id,Designation_Id")] C_Teachers_data c_Teachers_data)
        {
            ViewBag.MainTitle = "Academic Affairs Module";
            ViewBag.SubTitle = "Edit Teachers Data";
            if (ModelState.IsValid)
            {
                db.Entry(c_Teachers_data).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Designation_Id = new SelectList(db.Designations, "Designation_Id", "Title", c_Teachers_data.Designation_Id);
            ViewBag.Department_Id = new SelectList(db.R_Departments_data, "Department_Id", "Department_Name", c_Teachers_data.Department_Id);
            return View(c_Teachers_data);
        }

        // GET: AcademicAffairsModule/TeachersData/Delete/5
        public ActionResult Delete(int? id)
        {
            ViewBag.MainTitle = "Academic Affairs Module";
            ViewBag.SubTitle = "Delete Teachers Data";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            C_Teachers_data c_Teachers_data = db.C_Teachers_data.Find(id);
            if (c_Teachers_data == null)
            {
                return HttpNotFound();
            }
            return View(c_Teachers_data);
        }

        // POST: AcademicAffairsModule/TeachersData/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ViewBag.MainTitle = "Academic Affairs Module";
            ViewBag.SubTitle = "Dalete Teachers Data";
            C_Teachers_data c_Teachers_data = db.C_Teachers_data.Find(id);
            db.C_Teachers_data.Remove(c_Teachers_data);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
