﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UniversityManagementSystem.Areas.AcademicAffairsModule.Models;
using UniversityManagementSystem.Models;

namespace UniversityManagementSystem.Areas.AcademicAffairsModule.Controllers
{
    [Authorize]
    public class AcademicSittingController : Controller
    {
        private UniversityManagementSystemEntities db = new UniversityManagementSystemEntities();
        private string msg;
        private int don;

        // GET: AcademicAffairsModule/AcademicSitting
        public ActionResult TransferStudent()
        {
            ViewBag.MainTitle = "Academic Affairs Module";
            ViewBag.SubTitle = "Transfer Student";
            DropDown();

            List<GetStudantIdsModel> StdList = new List<GetStudantIdsModel>();
            var data = db.R_StudentRegistration_data.Select(s => new { s.SR_Id, s.Student_Id, Student_FullName = s.R_StudentInfo_data.Student_FullName }).ToList().Take(0);
            foreach (var item in data)
            {
                GetStudantIdsModel row = new GetStudantIdsModel
                {
                    SR_Id = item.SR_Id,
                    StdId = item.Student_Id,
                    StdName = item.Student_FullName
                };
                StdList.Add(row);
            }
            ViewBag.StdInfo = StdList.ToList();
            return View();
        }

        //GET: ResultModule/TransferStudent
        [HttpPost]
        public ActionResult TransferStudent(GetStudantModel search)
        {
            ViewBag.MainTitle = "Academic Affairs Module";
            ViewBag.SubTitle = "Transfer Student";

            DropDown();

            List<GetStudantIdsModel> StdList = new List<GetStudantIdsModel>();
            var data = db.R_StudentRegistration_data.Where(s => s.Year_Id == search.Year_Id && s.Department_Id == search.Department_Id && s.batch_Id == search.batch_Id && s.Semester_Id == search.Semester_Id).
                Select(s => new { s.SR_Id, s.Student_Id, Student_FullName = s.R_StudentInfo_data.Student_FullName }).ToList().Take(20);
            foreach (var item in data)
            {
                GetStudantIdsModel row = new GetStudantIdsModel
                {
                    SR_Id = item.SR_Id,
                    StdId = item.Student_Id,
                    StdName = item.Student_FullName
                };
                StdList.Add(row);
            }
            ViewBag.StdInfo = StdList.ToList();
            return View();
        }

        [HttpPost]
        public JsonResult TransferCurrentStudent(SaveStudantModel data)
        {
            var IsFalid = db.R_StudentRegistration_data.Any(r => r.Year_Id == data.YearId && r.batch_Id == data.batchId && r.Department_Id == data.DepartmentId && r.Semester_Id == data.SemesterId);
            if (IsFalid == true)
            {
                msg = "Is Exist record";
            }
            else
            {
                int DeptIdTo = db.R_StudentRegistration_data.Find(data.SR_Id).Department_Id;
                R_TransferStudent ts = new R_TransferStudent
                {
                    Student_Id = data.StudentId,
                    Department_IdTo = DeptIdTo,
                    Department_IdFrom = data.DepartmentId,
                    TSDate = DateTime.Now.Date
                };
                db.R_TransferStudent.Add(ts);
                don = db.SaveChanges();
                if (don > 0)
                {
                    R_StudentRegistration_data sr = new R_StudentRegistration_data
                    {
                        Student_Id = data.StudentId,
                        Year_Id = data.YearId,
                        Semester_Id = data.SemesterId,
                        Department_Id = data.DepartmentId,
                        User_Id = User.Identity.GetUserId(),
                        batch_Id = data.batchId,
                        Registered = false,
                        Registr_Status = "Formal"
                    };
                    db.R_StudentRegistration_data.Add(sr);
                    don = db.SaveChanges();
                    int stdid = sr.Student_Id;
                    if (don > 0)
                    {
                        //var sr_edit = db.R_StudentRegistration_data.Find(data.SR_Id);
                        //sr_edit.Registr_Status = "";
                        //db.Entry(sr).State = System.Data.Entity.EntityState.Modified;
                        //don = db.SaveChanges();
                        //if (don2 > 0)
                        //{
                        //}

                        //abdo
                        R_StudentInfo_data std = db.R_StudentInfo_data.Find(stdid);
                        if (std != null)
                        {
                            R_Departments_data dept = db.R_Departments_data.Find(data.DepartmentId);
                            R_Colleges_data Cologe = db.R_Colleges_data.Find(dept.Coll_Id);
                            std.Department = dept.Department_Name;
                            std.Student_College = Cologe.Coll_Name;
                            db.Entry(std).State = EntityState.Modified;
                            db.SaveChanges();
                            
                            Fee_TuitionFees feesPyStudent = db.Fee_TuitionFees.Single(x=>x.Student_Id==std.Student_Id);
                            if (feesPyStudent != null)
                            {
                                Fee_TuitionFees UpFees = db.Fee_TuitionFees.Find(feesPyStudent.TF_Id);
                                PaymentStatu Payment = db.PaymentStatus.Find(UpFees.Payment_Id);
                                UpFees.Department_Id = data.DepartmentId;
                                UpFees.Payment_Id = Payment.Payment_Id;
                                UpFees.FeesPayment = Payment.Payment_Value;
                                UpFees.Student_Id = std.Student_Id;
                                UpFees.User_Id = User.Identity.GetUserId();
                                UpFees.Fees_Department = dept.Department_Fees;
                                UpFees.Fees_Total = (dept.Department_Fees) - ((dept.Department_Fees * Convert.ToDecimal(Payment.Payment_percent)) / 100);
                                UpFees.FeesFeesDate = DateTime.Now;
                                db.Entry(UpFees).State = EntityState.Modified;
                                db.SaveChanges();

                            }

                        }
                         msg = "The operation was successful";
                    }
                }
            }
            return Json( msg,JsonRequestBehavior.AllowGet);
        }
        public void DropDown()
        {
            R_StudyYear_data sd = new R_StudyYear_data { Year_Id = 0, Year_Name = " -- select Year -- " };
            var Year_Data = db.R_StudyYear_data.ToList();
            Year_Data.Add(sd);
            R_Departments_data dd = new R_Departments_data { Department_Id = 0, Department_Name = " -- select Department -- " };
            var Department_Data = db.R_Departments_data.ToList();
            Department_Data.Add(dd);
            R_Semester_data sed = new R_Semester_data { Semester_Id = 0, Semester_Name = " -- select Semester -- " };
            var Semester_Data = db.R_Semester_data.ToList();
            Semester_Data.Add(sed);
            R_BatchNumber_data bn = new R_BatchNumber_data { batch_Id = 0, batch_Name = " -- select Batch --" };
            var BatchNumber_Data = db.R_BatchNumber_data.ToList();
            BatchNumber_Data.Add(bn);
            C_Courses_data cd = new C_Courses_data { Course_Id = 0, Course_Name = "-- select Course -- " };
            var Courses_Data = db.C_Courses_data.ToList();
            Courses_Data.Add(cd);
            ViewBag.Year_Id = new SelectList(Year_Data.OrderBy(y => y.Year_Id), "Year_Id", "Year_Name");
            ViewBag.Department_Id = new SelectList(Department_Data.OrderBy(d => d.Department_Id), "Department_Id", "Department_Name");
            ViewBag.Semester_Id = new SelectList(Semester_Data.OrderBy(s => s.Semester_Id), "Semester_Id", "Semester_Name");
            ViewBag.batch_Id = new SelectList(BatchNumber_Data.OrderBy(b => b.batch_Id), "batch_Id", "batch_Name");
            ViewBag.Course_Id = new SelectList(Courses_Data.OrderBy(c => c.Course_Id), "Course_Id", "Course_Name");
        }
        public JsonResult BatchByDeptId(int id)
        {
            var BatchNumber_data = db.R_BatchNumber_data.Where(p=> p.Department_Id == id).Select(p=> new { p.batch_Id ,p.batch_Name }).ToList();
            return Json(BatchNumber_data , JsonRequestBehavior.AllowGet);
        }
    }
}