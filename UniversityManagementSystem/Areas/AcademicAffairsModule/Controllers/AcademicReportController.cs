﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UniversityManagementSystem.Areas.AcademicAffairsModule.Report;
using UniversityManagementSystem.Areas.ResultModule.Reports;
using UniversityManagementSystem.Models;

namespace UniversityManagementSystem.Areas.AcademicAffairsModule.Controllers
{
    [Authorize]
    public class AcademicReportController : Controller
    {
        UniversityManagementSystemEntities db = new UniversityManagementSystemEntities();

        // GET: AcademicAffairsModule/AcademicReport
        public ActionResult Index()
        {
            ViewBag.MainTitle = "Academic Affairs Module";
            ViewBag.SubTitle = "Academic Report";
            return View();
        }
        public ActionResult GetTransformStudent(DateTime From, DateTime To)
        {
            var data = db.R_TransferStudent.Where(t => t.TSDate >= From && t.TSDate <= To).Select(t => new { t.TSId, t.Student_Id, Student_Number = t.R_StudentInfo_data.Student_Number, t.R_StudentInfo_data.Student_FullName, DNameFrom = t.R_Departments_data.Department_Name, DNameTo = t.R_Departments_data1.Department_Name, t.TSDate }).ToList();

            TransferStudentReport P = new TransferStudentReport();
            P.SetParameterValue("dateFrom ", From);
            P.SetParameterValue("dateTo", To);

            P.Load(Path.Combine(Server.MapPath("~/Areas/AcademicAffairsModule/Report"), "TransferStudentReport.rpt"));
            P.SetDataSource(data);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();

            Stream stream = P.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            return File(stream, "application/pdf", "TransferStudent.pdf");
        }

        public ActionResult ExaminationsLists(int YearId, int DepatId, int SemId)
        {
            var data = db.R_StudentRegistration_data.Where(t => t.Year_Id == YearId && t.Department_Id == DepatId && t.Semester_Id == SemId && t.Registered == true).Select(t => new { t.SR_Id, t.R_StudentInfo_data.Student_Number, t.R_StudentInfo_data.Student_FullName, t.R_StudyYear_data.Year_Name, t.R_Semester_data.Semester_Name, t.R_Departments_data.Department_Name }).ToList();

            ExaminationsListsReport exa = new ExaminationsListsReport();
            //exa.SetParameterValue("YId ", YearId);
            //exa.SetParameterValue("DId", DepatId);
            //exa.SetParameterValue("SId", SemId);

            exa.Load(Path.Combine(Server.MapPath("~/Areas/AcademicAffairsModule/Report"), "ExaminationsListsCrystalReport.rpt"));
            exa.SetDataSource(data);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();

            Stream stream = exa.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            return File(stream, "application/pdf", "ExaminationsLists.pdf");
        }

        public ActionResult YearTansitionersLists(int S_YearId, int S_DepatId, int S_SemId)
        {
            bool FindData = db.Re_StudentResult_data.Any(t => t.R_StudentRegistration_data.Year_Id == S_YearId && t.R_StudentRegistration_data.Department_Id == S_DepatId && t.R_StudentRegistration_data.Semester_Id == S_SemId && t.Result_AcademicStatus == "successful");
            if (FindData == true)
            {
                var data = db.Re_StudentResult_data.Where(t => t.R_StudentRegistration_data.Year_Id == S_YearId && t.R_StudentRegistration_data.Department_Id == S_DepatId && t.R_StudentRegistration_data.Semester_Id == S_SemId && t.Result_AcademicStatus == "successful").
                Select(t => new
                {
                    t.SR_Id,
                    t.R_StudentRegistration_data.R_StudentInfo_data.Student_Number,
                    t.R_StudentRegistration_data.R_StudentInfo_data.Student_FullName,
                    t.R_StudentRegistration_data.R_StudyYear_data.Year_Name,
                    t.R_StudentRegistration_data.R_Semester_data.Semester_Name,
                    t.R_StudentRegistration_data.R_Departments_data.Department_Name,
                    t.Result_AcademicStatus,
                    t.Re_StudentResultDescribtion.SingleOrDefault().C_CourseAssignToYear_data.C_Courses_data.Course_Name,
                    t.Re_StudentResultDescribtion.SingleOrDefault().C_CourseAssignToYear_data.C_Courses_data.Course_HoursNumber,
                    t.Re_StudentResultDescribtion.FirstOrDefault().SRD_Pointsletter
                }).ToList();
                YearTansitionersListsCrystalReport exa = new YearTansitionersListsCrystalReport();
                exa.Load(Path.Combine(Server.MapPath("~/Areas/AcademicAffairsModule/Report"), "YearTansitionersListsCrystalReport.rpt"));
                exa.SetDataSource(data);
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();

                Stream stream = exa.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                return File(stream, "application/pdf", "YearTansitionersLists.pdf");
            }
            else
            {
                return RedirectToAction("Index" , "AcademicReport" ,new {msg = "There is no data." });
            }
        }
    }
}