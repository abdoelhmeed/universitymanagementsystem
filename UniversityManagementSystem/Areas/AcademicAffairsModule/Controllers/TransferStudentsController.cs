﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UniversityManagementSystem.Areas.AcademicAffairsModule.Models;
using UniversityManagementSystem.Areas.ResultModule.Models;
using UniversityManagementSystem.Models;

namespace UniversityManagementSystem.Areas.AcademicAffairsModule.Controllers
{
    [Authorize]
    public class TransferStudentsController : Controller
    {
        private UniversityManagementSystemEntities db = new UniversityManagementSystemEntities();
        private string msg;

        // GET: AcademicAffairsModule/TransferStudents
        [HttpGet]
        public ActionResult SetSemester()
        {
            ViewBag.MainTitle = "Academic Affairs Module";
            ViewBag.SubTitle = "Set Semester";

            DropDown();
            List<GetStudantInfoModel> StdList = new List<GetStudantInfoModel>();
            var data = db.R_StudentRegistration_data.Select(s => new { s.SR_Id, s.Student_Id, Student_FullName = s.R_StudentInfo_data.Student_FullName }).ToList().Take(0);
            foreach (var item in data)
            {
                GetStudantInfoModel row = new GetStudantInfoModel
                {
                    SR_Id = item.SR_Id,
                    StdId = item.Student_Id,
                    StdName = item.Student_FullName
                };
                StdList.Add(row);
            }
            ViewBag.StdInfo = StdList.ToList();
            return View();
        }

        //POST: ResultModule/SemesterResult
        [HttpPost]
        public ActionResult SetSemester(RetSemesterModel search)
        {
            ViewBag.MainTitle = "Academic Affairs Module";
            ViewBag.SubTitle = "Set Semester";

            DropDown();
            List<GetStudantInfoModel> StdList = new List<GetStudantInfoModel>();
            var data = db.Re_StudentResult_data.Where(s => s.R_StudentRegistration_data.Year_Id == search.Year_Id && s.R_StudentRegistration_data.Department_Id == search.Department_Id && s.R_StudentRegistration_data.batch_Id == search.batch_Id && s.R_StudentRegistration_data.Semester_Id == search.Semester_Id).
                Select(s => new { s.SR_Id, s.R_StudentRegistration_data.Student_Id, Student_FullName = s.R_StudentRegistration_data.R_StudentInfo_data.Student_FullName }).ToList();
            foreach (var item in data)
            {
                GetStudantInfoModel row = new GetStudantInfoModel
                {
                    SR_Id = item.SR_Id,
                    StdId = item.Student_Id,
                    StdName = item.Student_FullName
                };
                StdList.Add(row);
            }
            ViewBag.StdInfo = StdList.ToList();
            return View();
        }
        
        // POST: AcademicAffairsModule/TransferStudents
        [HttpPost]
        public JsonResult SetSemesterForStudants(RetSemesterModel data)
        {
            var ChickSemesterType = db.R_Semester_data.Find(data.Semester_Id).Semester_Type;
            if (ChickSemesterType == 2)
            {
                msg = "The operation can not be performed Go to set New Year.";
            }
            else
            {
                foreach (var item in data.StdList)
                {
                    var ResultIsCal = db.Re_StudentResult_data.FirstOrDefault(s => s.SR_Id == item.SR_Id).Result_calculationNumber;
                    if (ResultIsCal == 0)
                    {
                        msg = "The result has not yet been calculated.";
                    }
                    else
                    {
                        var NextSemester = data.Semester_Id + 1;
                        //Get user semester
                        var InSemester = db.R_StudentRegistration_data.Any(s => 
                        s.Year_Id == data.Year_Id && 
                        s.Department_Id == data.Department_Id &&
                        s.batch_Id == data.batch_Id && 
                        s.Semester_Id == NextSemester &&
                        s.Student_Id == item.StdId);

                        if (InSemester == true)
                        {
                            msg = "Student data already exists.";
                        }
                        else
                        {
                            //Get Studant Result number 
                            int ResultId = db.Re_StudentResult_data.FirstOrDefault(s => s.SR_Id == item.SR_Id).Result_Id;
                            R_StudentRegistration_data StdReg = new R_StudentRegistration_data()
                            {
                                Department_Id = data.Department_Id,
                                batch_Id = data.batch_Id,
                                Semester_Id = NextSemester,
                                Year_Id = data.Year_Id,
                                Student_Id = item.StdId,
                                Registered = false,
                                User_Id = User.Identity.GetUserId(),
                                Registr_Status = "Formal",
                            };
                            db.R_StudentRegistration_data.Add(StdReg);
                            db.SaveChanges();
                            msg = "Students of  " + db.R_BatchNumber_data.Find(data.batch_Id).batch_Name + "Batch have been transferred to the new semester  " + NextSemester + " successfully .";
                        }
                    }
                }
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }

        // GET: AcademicAffairsModule/transfer students to next year
        [HttpGet]
        public JsonResult SetYearForStudants(RetSemesterModel data)
        {
            var ChickSemesterType = db.R_Semester_data.Find(data.Semester_Id).Semester_Type;
            if (ChickSemesterType == 1)
            {
                msg = "The Operation can not be Performed Go to set New Semester.";
            }
            else
            {
                foreach (var item in data.StdList)
                {
                    var ResultIsCal = db.Re_StudentResult_data.FirstOrDefault(s => s.SR_Id == item.SR_Id).Result_calculationNumber;
                    if (ResultIsCal == 0)
                    {
                        msg = "The Result has not yet been Calculated.";
                    }
                    else
                    {
                        //Get Next Year Id
                        var currentYear = db.R_StudyYear_data.Find(data.Year_Id).Year_Number;
                        var NextYear = GetNextYear(currentYear);
                        //Get Next Semester Id
                        var CurrentNumber = db.R_Semester_data.Find(data.Semester_Id).Semester_Number;
                        var NextSemester = db.R_Semester_data.FirstOrDefault(y => y.Semester_Number == CurrentNumber + 1).Semester_Id;
                        //Get Bast Semester Id
                        var BastSemester = db.R_Semester_data.FirstOrDefault(y => y.Semester_Number == CurrentNumber - 1).Semester_Id;
                        //Get Studant Result number 
                        int ResultId = db.Re_StudentResult_data.FirstOrDefault(s => s.SR_Id == item.SR_Id).Result_Id;
                        //Get New Batch Number
                        var CurrentBatch = db.R_BatchNumber_data.FirstOrDefault(b => b.Department_Id == data.Department_Id);
                        //var NewBatch =db.R_BatchNumber_data.FirstOrDefault(b=> b.Department_Id == da)

                        //Get user semester
                        var InSemester = db.R_StudentRegistration_data.Any(s =>
                        s.Year_Id == data.Year_Id &&
                        s.Department_Id == data.Department_Id &&
                        s.batch_Id == data.batch_Id &&
                        s.Semester_Id == NextSemester &&
                        s.Student_Id == item.StdId);

                        if (InSemester == true)
                        {
                            msg = "Student data already exists.";
                        }
                        else
                        {
                            var CumulativePoints = db.Re_StudentResult_data.Find(item.SR_Id).Result_CumulativePoints;
                            if (CumulativePoints >= 2.00)
                            {
                                TransferStudantToNextSemester(item.SR_Id, item.StdId, data.Department_Id, NextYear, NextSemester, data.batch_Id);
                                AcademicStatus(ResultId,"Success");
                            }
                            else
                            if (CumulativePoints > 1.25 && CumulativePoints < 2.00)
                            {
                                int B_Id = NewBatch(NextYear, data.Department_Id);
                                TransferStudantToNextSemester(item.SR_Id, item.StdId, data.Department_Id, NextYear, BastSemester, B_Id);
                                AcademicStatus(ResultId, "Repeat");
                            }
                            else
                            {
                                AcademicStatus(ResultId, "Detained");
                            }
                            msg = "Students of " + db.R_BatchNumber_data.Find(data.batch_Id).batch_Name + " Batch have been transferred to the new Year successfully .";
                        }
                    }
                }
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }

        // TransferStudents
        public void TransferStudantToNextSemester(int SR_Id, int StdId, int Dept_Id, int YearIdTo, int SemYearIdTo, int batch_Id)
        {
            //Get Studant Result Number 
            int ResultId = db.Re_StudentResult_data.FirstOrDefault(s => s.SR_Id == SR_Id).Result_Id;
            R_StudentRegistration_data StdReg = new R_StudentRegistration_data()
            {
                Department_Id = Dept_Id,
                Year_Id = YearIdTo,
                Semester_Id = SemYearIdTo,
                batch_Id = batch_Id,
                Student_Id = StdId,
                Registered = false,
                User_Id = User.Identity.GetUserId(),
                Registr_Status = "Formal",
            };
            db.R_StudentRegistration_data.Add(StdReg);
            db.SaveChanges();
        }
        
        //add new Batch 
        public int NewBatch(int YearId ,int Dept_Id)
        {

            var FindBatch = db.R_BatchNumber_data.Any(b=> b.Year_Id == YearId && b.Department_Id == Dept_Id);
            if (FindBatch != true)
            {
                int NewNumber = Convert.ToInt32(db.R_BatchNumber_data.FirstOrDefault(b => b.Year_Id == YearId && b.Department_Id == Dept_Id).batch_Number + 1);
                R_BatchNumber_data bn = new R_BatchNumber_data
                {
                    batch_Number = NewNumber,
                    Year_Id = GetIds(YearId, Dept_Id, null).Item1,
                    Department_Id = Dept_Id,
                    batch_Name = db.R_Departments_data.FirstOrDefault(d=> d.Department_Id==Dept_Id).Department_Name.ToString() + " Batch " + NewNumber,
                };
                db.R_BatchNumber_data.Add(bn);
                db.SaveChanges();

                int BId = db.R_BatchNumber_data.FirstOrDefault(b => b.batch_Number == NewNumber).batch_Id;
                return BId;
            }
            else
            {
                int BId = db.R_BatchNumber_data.FirstOrDefault(b => b.Year_Id == YearId && b.Department_Id == Dept_Id).batch_Id;
                return BId;
            }
        }
        
        //Next year
        public int GetNextYear(int currentYear)
        {
            var FindNextYear = db.R_StudyYear_data.Any(y => y.Year_Number == currentYear + 1);
            if (FindNextYear == false)
            {
                var nextdate = DateTime.Now.AddYears(currentYear + 1);
                R_StudyYear_data SYD = new R_StudyYear_data()
                {
                    Year_Number = currentYear + 1,
                    Year_Name = Convert.ToString(currentYear) + "-" + Convert.ToString(currentYear + 1),
                    YearStart = nextdate.Date,
                    YearEnd = nextdate.Date.AddMonths(12),
                };
                db.R_StudyYear_data.Add(SYD);
                db.SaveChanges();
                return SYD.Year_Id;
            }
            else
            {
                return db.R_StudyYear_data.FirstOrDefault(y => y.Year_Number == currentYear + 1).Year_Id;
            }
        }
        public Tuple<int , int ,int> GetIds(int YearId, int Dept_Id ,int? Semester_Id)
        {
            //Get Next Year Id
            var currentYear = db.R_StudyYear_data.Find(YearId).Year_Number;
            var NextYear = db.R_StudyYear_data.FirstOrDefault(y => y.Year_Number == currentYear + 1).Year_Id;
            //Get Next Semester Id
            var CurrentNumber = db.R_Semester_data.Find(Semester_Id).Semester_Number;
            var NextSemester = db.R_Semester_data.FirstOrDefault(y => y.Semester_Number == CurrentNumber + 1).Semester_Id;
            //Get Bast Semester Id
            var BastSemester = db.R_Semester_data.FirstOrDefault(y => y.Semester_Number == CurrentNumber - 1).Semester_Id;

            return Tuple.Create(NextYear, NextSemester, BastSemester);   
        }

        public void AcademicStatus(int ResultId ,string AcademicStatus)
        {
            Re_StudentResult_data srd = db.Re_StudentResult_data.Find(ResultId);
            srd.Result_AcademicStatus = AcademicStatus;
            db.Entry(srd).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }
        //
        public void DropDown()
        {
            R_StudyYear_data sd = new R_StudyYear_data { Year_Id = 0, Year_Name = " -- select Year -- " };
            var Year_Data = db.R_StudyYear_data.ToList();
            Year_Data.Add(sd);
            R_Departments_data dd = new R_Departments_data { Department_Id = 0, Department_Name = " -- select Department -- " };
            var Department_Data = db.R_Departments_data.ToList();
            Department_Data.Add(dd);
            R_Semester_data sed = new R_Semester_data { Semester_Id = 0, Semester_Name = " -- select Semester -- " };
            var Semester_Data = db.R_Semester_data.ToList();
            Semester_Data.Add(sed);
            R_BatchNumber_data bn = new R_BatchNumber_data { batch_Id = 0, batch_Name = " -- select Batch --" };
            var BatchNumber_Data = db.R_BatchNumber_data.ToList();
            BatchNumber_Data.Add(bn);
            C_Courses_data cd = new C_Courses_data { Course_Id = 0, Course_Name = "-- select Course -- " };
            var Courses_Data = db.C_Courses_data.ToList();
            Courses_Data.Add(cd);
            ViewBag.Year_Id = new SelectList(Year_Data.OrderBy(y => y.Year_Id), "Year_Id", "Year_Name");
            ViewBag.Department_Id = new SelectList(Department_Data.OrderBy(d => d.Department_Id), "Department_Id", "Department_Name");
            ViewBag.Semester_Id = new SelectList(Semester_Data.OrderBy(s => s.Semester_Id), "Semester_Id", "Semester_Name");
            ViewBag.batch_Id = new SelectList(BatchNumber_Data.OrderBy(b => b.batch_Id), "batch_Id", "batch_Name");
            ViewBag.Course_Id = new SelectList(Courses_Data.OrderBy(c => c.Course_Id), "Course_Id", "Course_Name");
        }
    }
}