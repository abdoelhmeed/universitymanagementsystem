﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using UniversityManagementSystem.Models;

namespace UniversityManagementSystem.Areas.AcademicAffairsModule.Views
{
    public class CourseAssignDataController : Controller
    {
        private UniversityManagementSystemEntities db = new UniversityManagementSystemEntities();

        // GET: AcademicAffairsModule/CourseAssignData
        public ActionResult Index()
        {
            var c_CourseAssignToYear_data = db.C_CourseAssignToYear_data.Include(c => c.C_Courses_data).Include(c => c.C_Teachers_data).Include(c => c.R_Departments_data).Include(c => c.R_Semester_data).Include(c => c.R_StudyYear_data);
            return View(c_CourseAssignToYear_data.ToList());
        }

        // GET: AcademicAffairsModule/CourseAssignData/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            C_CourseAssignToYear_data c_CourseAssignToYear_data = db.C_CourseAssignToYear_data.Find(id);
            if (c_CourseAssignToYear_data == null)
            {
                return HttpNotFound();
            }
            return View(c_CourseAssignToYear_data);
        }

        // GET: AcademicAffairsModule/CourseAssignData/Create
        public ActionResult Create()
        {
            ViewBag.Course_Id = new SelectList(db.C_Courses_data, "Course_Id", "Course_Name");
            ViewBag.Teacher_Id = new SelectList(db.C_Teachers_data, "Teacher_Id", "Teacher_Name");
            ViewBag.Department_Id = new SelectList(db.R_Departments_data, "Department_Id", "Department_Name");
            ViewBag.Semester_Id = new SelectList(db.R_Semester_data, "Semester_Id", "Semester_Name");
            ViewBag.Year_Id = new SelectList(db.R_StudyYear_data, "Year_Id", "Year_Name");
            return View();
        }

        // POST: AcademicAffairsModule/CourseAssignData/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CATYId,Course_Id,Year_Id,Department_Id,Semester_Id,Teacher_Id")] C_CourseAssignToYear_data c_CourseAssignToYear_data)
        {
            if (ModelState.IsValid)
            {
                db.C_CourseAssignToYear_data.Add(c_CourseAssignToYear_data);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Course_Id = new SelectList(db.C_Courses_data, "Course_Id", "Course_Name", c_CourseAssignToYear_data.Course_Id);
            ViewBag.Teacher_Id = new SelectList(db.C_Teachers_data, "Teacher_Id", "Teacher_Name", c_CourseAssignToYear_data.Teacher_Id);
            ViewBag.Department_Id = new SelectList(db.R_Departments_data, "Department_Id", "Department_Name", c_CourseAssignToYear_data.Department_Id);
            ViewBag.Semester_Id = new SelectList(db.R_Semester_data, "Semester_Id", "Semester_Name", c_CourseAssignToYear_data.Semester_Id);
            ViewBag.Year_Id = new SelectList(db.R_StudyYear_data, "Year_Id", "Year_Name", c_CourseAssignToYear_data.Year_Id);
            return View(c_CourseAssignToYear_data);
        }

        // GET: AcademicAffairsModule/CourseAssignData/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            C_CourseAssignToYear_data c_CourseAssignToYear_data = db.C_CourseAssignToYear_data.Find(id);
            if (c_CourseAssignToYear_data == null)
            {
                return HttpNotFound();
            }
            ViewBag.Course_Id = new SelectList(db.C_Courses_data, "Course_Id", "Course_Name", c_CourseAssignToYear_data.Course_Id);
            ViewBag.Teacher_Id = new SelectList(db.C_Teachers_data, "Teacher_Id", "Teacher_Name", c_CourseAssignToYear_data.Teacher_Id);
            ViewBag.Department_Id = new SelectList(db.R_Departments_data, "Department_Id", "Department_Name", c_CourseAssignToYear_data.Department_Id);
            ViewBag.Semester_Id = new SelectList(db.R_Semester_data, "Semester_Id", "Semester_Name", c_CourseAssignToYear_data.Semester_Id);
            ViewBag.Year_Id = new SelectList(db.R_StudyYear_data, "Year_Id", "Year_Name", c_CourseAssignToYear_data.Year_Id);
            return View(c_CourseAssignToYear_data);
        }

        // POST: AcademicAffairsModule/CourseAssignData/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CATYId,Course_Id,Year_Id,Department_Id,Semester_Id,Teacher_Id")] C_CourseAssignToYear_data c_CourseAssignToYear_data)
        {
            if (ModelState.IsValid)
            {
                db.Entry(c_CourseAssignToYear_data).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Course_Id = new SelectList(db.C_Courses_data, "Course_Id", "Course_Name", c_CourseAssignToYear_data.Course_Id);
            ViewBag.Teacher_Id = new SelectList(db.C_Teachers_data, "Teacher_Id", "Teacher_Name", c_CourseAssignToYear_data.Teacher_Id);
            ViewBag.Department_Id = new SelectList(db.R_Departments_data, "Department_Id", "Department_Name", c_CourseAssignToYear_data.Department_Id);
            ViewBag.Semester_Id = new SelectList(db.R_Semester_data, "Semester_Id", "Semester_Name", c_CourseAssignToYear_data.Semester_Id);
            ViewBag.Year_Id = new SelectList(db.R_StudyYear_data, "Year_Id", "Year_Name", c_CourseAssignToYear_data.Year_Id);
            return View(c_CourseAssignToYear_data);
        }

        // GET: AcademicAffairsModule/CourseAssignData/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            C_CourseAssignToYear_data c_CourseAssignToYear_data = db.C_CourseAssignToYear_data.Find(id);
            if (c_CourseAssignToYear_data == null)
            {
                return HttpNotFound();
            }
            return View(c_CourseAssignToYear_data);
        }

        // POST: AcademicAffairsModule/CourseAssignData/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            C_CourseAssignToYear_data c_CourseAssignToYear_data = db.C_CourseAssignToYear_data.Find(id);
            db.C_CourseAssignToYear_data.Remove(c_CourseAssignToYear_data);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
