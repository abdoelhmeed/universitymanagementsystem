﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UniversityManagementSystem.Areas.AcademicAffairsModule.Models
{
    public class GetStudantModel
    {

        public int Year_Id { get; set; }
        public int Department_Id { get; set; }
        public int Semester_Id { get; set; }
        public int batch_Id { get; set; }
        public int StdId { get; set; }
    }
    public class RetSemesterModel
    {
        public int Year_Id { get; set; }
        public int Department_Id { get; set; }
        public int Semester_Id { get; set; }
        public int batch_Id { get; set; }
        public int Course_Id { get; set; }

        public List<GetStudantIdsModel> StdList { get; set; }
    }
    public class GetStudantIdsModel
    {
        public int SR_Id { get; set; }
        public int StdId { get; set; }
        public string StdName { get; set; }
    }

    public class SaveStudantModel
    {
        public int YearId { get; set; }
        public int DepartmentId { get; set; }
        public int SemesterId { get; set; }
        public int batchId { get; set; }
        public int StudentId { get; set; }
        public int SR_Id { get; set; }
    }

}