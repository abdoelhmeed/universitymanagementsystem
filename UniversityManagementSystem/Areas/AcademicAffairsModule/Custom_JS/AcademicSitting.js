﻿$(document).ready(function (e) {

    //Supplements And Alternatives Result
    $(document).on('click', '.update', function () {

        var id = $(this).attr("id");
        var SR_Id = $('#' + id).children('td[data-target=SR_Id]').text();
        var StdId = $('#' + id).children('td[data-target=StdId]').text();
        var StdName = $('#' + id).children('td[data-target=StdName]').text();

        $("#SR_Id").val(SR_Id);
        $("#StudentId").val(StdId);
        $("#StdName").val(StdName);

        $(".modal-title").text("Edit Current Data");
    });

    //Transfer Student
    $("#TSform_data").submit(function (event) {
        event.preventDefault();
        $('#TSform_data').validate({
            rules: {
                YearId: {
                    required: true
                }, DepartmentId: {
                    required: true
                }, SemesterId: {
                    required: true
                }, batchId: {
                    required: true
                }
            },
            messages: {
                Year_Id: "year name is Required",
                Department_Id: "department name is Required",
                Semester_Id: "semester number is Required",
                batch_Id: "batch number is Required",
            },
            errorPlacement: function (error, element) { error.addClass("help-block"); element.parents(".form-group").addClass("has-feedback"); error.insertAfter(element); },
            highlight: function (element) { $(element).parents(".form-group").addClass("has-error").removeClass("has-success"); },
            unhighlight: function (element, errorClass, validClass) { $(element).parents(".form-group").addClass("has-success").removeClass("has-error"); },
            submitHandler: function (form) {
                $("#Loding").show();
                var data = new FormData(form);
                $.ajax({
                    type: 'POST',
                    url: '/AcademicAffairsModule/AcademicSitting/TransferCurrentStudent',
                    data: data,
                    contentType: false,
                    processData: false,
                    success: function (data)
                    {
                        if(data === "The operation was successful")
                        {
                            $("#TSform_data")[0].reset();
                            $("#Modal_View").modal('hide');
                            $("#Loding").hide();
                            alert(data);
                            location.reload();
                        }
                        else
                        {
                            $("#Loding").hide();
                            alert(data);
                        }
                    },
                    error: function (data) {
                        alert(data);
                    }
                });
            }
        });
    });

    //
    $("#SetSemesterBtn").click(function () {
        debugger
        var SubjectArr = [];
        SubjectArr.length = 0;
        $("#Loding").show();

        $.each($("#SetSemester_td tbody tr"), function () {
            SubjectArr.push({
                StdId: $(this).find('td:eq(0)').html(),
                SR_Id: $(this).find('td:eq(1)').html()
            });
        });

        var data = {
            Year_Id: $("#Year_Id").val(),
            Department_Id: $("#Department_Id").val(),
            Semester_Id: $("#Semester_Id").val(),
            Semester_IdTo: $("#Semester_IdTo").val(),
            batch_Id: $("#batch_Id").val(),
            Course_Id: $("#Course_Id").val(),
            StdList: SubjectArr
        };

        $.ajax({
            type: "POST",
            url: "/AcademicAffairsModule/TransferStudents/SetSemesterForStudants",
            data: data,
            success: function (data) {
                $("#Loding").hide();
                alert(data)
                alert(dara);
            },
            error: function (data) {
                $("#Loding").hide();
                alert(data);
            }
        })

    });

    //
    $("#SetYearBtn").click(function () {
        debugger
        var SubjectArr = [];
        SubjectArr.length = 0;
        $("#YLoding").show();

        $.each($("#SetSemester_td tbody tr"), function () {
            SubjectArr.push({
                StdId: $(this).find('td:eq(0)').html(),
                SR_Id: $(this).find('td:eq(1)').html()
            });
        });

        var data = {
            Year_Id: $("#Year_Id").val(),
            Department_Id: $("#Department_Id").val(),
            Semester_Id: $("#Semester_Id").val(),
            Semester_IdTo: $("#YearIdTo").val(),
            SemesterYearIdTo: $("#SemesterYearIdTo").val(),
            batch_Id: $("#batch_Id").val(),
            Course_Id: $("#Course_Id").val(),
            StdList: SubjectArr
        };

        $.ajax({
            type: "POST",
            url: "/AcademicAffairsModule/TransferStudents/SetSemesterForStudants",
            data: data,
            success: function (data) {
                $("#YLoding").hide();
                alert(data);

            },
            error: function (data) {
                $("#YLoding").hide();
                alert(data);
            }
        })

    });

    //
    $(document).on('change', '#DepartmentId', function () {
        var id = $(this).val();
        $.ajax({
            url: "/AcademicAffairsModule/AcademicSitting/BatchByDeptId",
            type: "GET",
            data: { Id: id },
            success: function (data) {
                $('#batchId').html("");
                var _options = ""
                _options += ('<option value=" ">-- select batch number --</option>');
                $.each(data, function (i, value) {
                    _options += ('<option value="' + value.batch_Id + '">' + value.batch_Name + '</option>');
                });
                $('#batchId').append(_options);
            },
            error: function (data) {
                alert(data);
            }
        });
    });
    //
    $(document).on('change', '#Department_Id', function () {
        var id = $(this).val();
        $.ajax({
            url: "/AcademicAffairsModule/AcademicSitting/BatchByDeptId",
            type: "GET",
            data: { Id: id },
            success: function (data) {
                $('#batch_Id').html("");
                var _options = ""
                _options +=  ('<option value=" ">-- select batch number --</option>');
                $.each(data, function (i, value) {
                    _options += ('<option value="' + value.batch_Id + '">' + value.batch_Name + '</option>');
                });
                $('#batch_Id').append(_options);
            },
            error: function (data) {
                alert(data);
            }
        });
    });
});