﻿using System.Web.Mvc;

namespace UniversityManagementSystem.Areas.FinancialAccountsModule
{
    public class FinancialAccountsModuleAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "FinancialAccountsModule";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "FinancialAccountsModule_default",
                "FinancialAccountsModule/{controller}/{action}/{id}",
                new { controller= "FinancialAccountsModule", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}