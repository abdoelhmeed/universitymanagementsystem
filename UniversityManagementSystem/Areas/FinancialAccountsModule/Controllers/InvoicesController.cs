﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UniversityManagementSystem.Models;
using UniversityManagementSystem.Areas.FinancialAccountsModule.Models;
using UniversityManagementSystem.Areas.FinancialAccountsModule.Reports;
using System.IO;
using System.Data.Entity;

namespace UniversityManagementSystem.Areas.FinancialAccountsModule.Controllers
{
    [Authorize]
    public class InvoicesController : Controller
    {
        UniversityManagementSystemEntities db = new UniversityManagementSystemEntities();
        public ActionResult Index()
        {
            ViewBag.MainTitle = "Financial Accounts Module";
            ViewBag.SubTitle = "Invoices";

            List<Order> model = db.Orders.Where(x => x.orderCase == true && x.orderAdmin == true && x.orderFinished== false).ToList();
            return View(model);
        }
        //public ActionResult Invoices()
        //{

        //}
        public ActionResult AddInvoices(int orID)
        {
            ViewBag.MainTitle = "Financial Accounts Module";
            ViewBag.SubTitle = "Invoices Details";
            Invoice invoices = db.Invoices.Single(x=>x.orID== orID);
            List<InvoicesDetail> model = db.InvoicesDetails.Where(u => u.INnumber == invoices.INnumber).ToList();
            ViewBag.Nnumber = invoices.INnumber;
            ViewBag.orID = orID;
            return View(model);

        }

        [HttpPost]
        public JsonResult newInvoices(InvoicesDetailModel model)
        {
            InvoicesDetail IVD = new InvoicesDetail()
            {
                 IdProductName=model.IdProductName,
                 IdQuantity=model.IdQuantity,
                 IdSupplier=model.IdSupplier,
                 IdUnitPrice=model.IdUnitPrice,
                 INnumber=model.INnumber,
                 IdNumber=model.IdNumber
            };
            db.InvoicesDetails.Add(IVD);
            db.SaveChanges();
            return Json(model.orID,JsonRequestBehavior.AllowGet);


        }


        public ActionResult PrenitOrderDetail(int orID)
        {
            List<OrderDetail> Detail = db.OrderDetails.Where(u => u.orID == orID).ToList();
            Order or = db.Orders.Find(orID);
           List<PrenitOrderDetails >OD = new List<PrenitOrderDetails>();

            foreach (var item in Detail)
            {
                PrenitOrderDetails orderdt = new PrenitOrderDetails()
                {
                    orID =or.orID,
                    Applicant = or.orderApplicant,
                    History = Convert.ToDateTime(or.orderHistory),
                    productName = item.odProductDetails,
                    Notes = item.orNotes,
                    Quantity = Convert.ToInt32(item.odQuantity)
                };
                OD.Add(orderdt);

            }

            CrReOrderDetails rd = new CrReOrderDetails();
            rd.Load(Path.Combine(Server.MapPath("~/Areas/FinancialAccountsModule/Reports/CrReOrderDetails.rpt")));
            rd.SetDataSource(OD.ToList());
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            return File(stream, "application/pdf","OrderDetails.pdf");
        }



        public ActionResult ActivateOrder(int orID)
        {
            Order order = db.Orders.Find(orID);
            if (order != null)
            {
                order.ordestatus = "Finished";
                order.orderFinished = true;
                db.Entry(order).State = EntityState.Modified;
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }
    }
}