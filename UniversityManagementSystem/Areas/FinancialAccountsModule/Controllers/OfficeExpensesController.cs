﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UniversityManagementSystem.Areas.FinancialAccountsModule.Models;

namespace UniversityManagementSystem.Areas.FinancialAccountsModule.Controllers
{
    [Authorize]
    public class OfficeExpensesController : Controller
    {
        // GET: FinancialAccountsModule/OfficeExpenses
        public ActionResult Index()
        {
            ViewBag.MainTitle = "Financial Accounts Module";
            ViewBag.SubTitle = "Office Expenses";
            List<OfficeExpensesModel> model = new List<OfficeExpensesModel>()
            {
                //new OfficeExpensesModel{amount=1222,billnumber="22ee",expenseType="exee",History=DateTime.Now,ID=1,Notes="no",Organization="no",Photoinvoice="",UserID=""}
            };
            return View(model);
        }
    }
}