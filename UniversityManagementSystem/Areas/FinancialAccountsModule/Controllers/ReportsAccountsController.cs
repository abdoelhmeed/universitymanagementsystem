﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UniversityManagementSystem.Areas.FinancialAccountsModule.Models;
using UniversityManagementSystem.Areas.FinancialAccountsModule.Reports;
using UniversityManagementSystem.Models;

namespace UniversityManagementSystem.Areas.FinancialAccountsModule.Controllers
{
    [Authorize]
    public class ReportsAccountsController : Controller
    {
        UniversityManagementSystemEntities db = new UniversityManagementSystemEntities();
        // GET: FinancialAccountsModule/ReportsAccounts
        public ActionResult Index()
        {
            ViewBag.MainTitle = "Financial Accounts Module";
            ViewBag.SubTitle = "Reports";
            return View();
        }

        public ActionResult ReportsCollectedFees(ReportsAccounts Rmodel)
        {
            var depid = db.R_Departments_data.Select(x => x.Department_Id).ToList();
            List<FeesAndDetp> FeesDept = new List<FeesAndDetp>();
            List<FeesAndDetp> FeesDeptList = new List<FeesAndDetp>();
            foreach (var item in depid)
            {
                 FeesDept = db.Fee_TuitionFeesDescribtion.Where(u => u.Fee_TuitionFees.Department_Id == item && u.TFD_FeesDate > Rmodel.dateForm && u.TFD_FeesDate < Rmodel.dateTO).Select(x=> new FeesAndDetp {Fees=x.TFD_TuitionFee,deptID=x.Fee_TuitionFees.Department_Id,FeesData=x.TFD_FeesDate } ).ToList();
                FeesDeptList.AddRange(FeesDept);
            }


            List<FeesAndDetp> FeesAndDetpList = new List<FeesAndDetp>();
            foreach (var item in depid)
            {
               
                if (FeesDeptList.Count > 0)
                {
                    FeesAndDetp TotalFss = new FeesAndDetp()
                    {
                        deptID = item,
                        Fees = FeesDeptList.Where(x => x.deptID == item).Sum(i => i.Fees),

                    };
                    FeesAndDetpList.Add(TotalFss);
                }
            }

            List<FeesAndDetp> ReFeesAndDetp = FeesAndDetpList.ToList();
            List<ReportsFeesAndDetp> ReportsFeesAndDetpList = new List<ReportsFeesAndDetp>();
            foreach (var item in ReFeesAndDetp)
            {

                ReportsFeesAndDetp reportsFeesAndDetp = new ReportsFeesAndDetp()
                {
                    DatForm = Rmodel.dateForm,
                    DatTo = Rmodel.dateTO,
                    Fees = item.Fees,
                    deptName = db.R_Departments_data.Find(item.deptID).Department_Name
                };
                ReportsFeesAndDetpList.Add(reportsFeesAndDetp);
            }


            CrReCollectedFees rd = new CrReCollectedFees();
            rd.Load(Path.Combine(Server.MapPath("~/Areas/FinancialAccountsModule/Reports/CrReCollectedFees.rpt")));
            rd.SetDataSource(ReportsFeesAndDetpList.ToList());
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            return File(stream, "application/pdf", "CollectedFees.pdf");
        }

        public ActionResult ReportsAddFundFees(ReportsAccounts Rmodel)
        {
            DateTime DTo = Rmodel.dateTO;
            DateTime DForm = Rmodel.dateForm;
            List<ReAddFund> ReAddFundList = new List<ReAddFund>();
            List<AddFund> model = db.AddFunds.Where(u => u.fundHistory > DForm && u.fundHistory < DTo).ToList();
            foreach (var item in model)
            {
                ReAddFund addFund = new ReAddFund()
                {
                    funAmount =Convert.ToInt32( item.funAmount),
                    fundHistory = Convert.ToDateTime(item.fundHistory),
                    fundID = Convert.ToInt32(item.fundID),
                    funNameOfOrganization = item.funNameOfOrganization,
                    funNotes = item.funNotes,
                    dateForm = DForm,
                    dateTO = DTo
                };
                ReAddFundList.Add(addFund);
            }

            CrReAddFund rd = new CrReAddFund();
            rd.Load(Path.Combine(Server.MapPath("~/Areas/FinancialAccountsModule/Reports/CrReAddFund.rpt")));
            rd.SetDataSource(ReAddFundList.ToList());
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            return File(stream, "application/pdf", "CrReAddFund.pdf");
        }


        public ActionResult ReportsInvoicesFees(ReportsAccounts Rmodel)
        {
            var Inv = db.Invoices.Where(u => u.INDate > Rmodel.dateForm && u.INDate < Rmodel.dateTO).ToList();
            List<OrderTotalModel> model = new List<OrderTotalModel>();
            foreach (var item in Inv)
            {
                decimal uQTotal = 0;
              var  InvoicesDetails = db.InvoicesDetails.Where(x => x.INnumber == item.INnumber);
                foreach (var Ditem in InvoicesDetails)
                {
                    var TIN = db.InvoicesDetails.Find(Ditem.idID);
                    decimal UnitPrice = Convert.ToDecimal(TIN.IdUnitPrice);
                    int Quantity = Convert.ToInt32(TIN.IdQuantity);
                    uQTotal = uQTotal + (UnitPrice * Quantity);
                }
                OrderTotalModel OTM = new OrderTotalModel() {
                    Date = Convert.ToDateTime(item.INDate),
                    dateForm = Rmodel.dateForm,
                    dateTO = Rmodel.dateTO,
                    ID = item.INnumber,
                    Name = item.Order.orderApplicant,
                    Money = uQTotal

                };
                model.Add(OTM);
            }
            CrReOrderTotal rd = new CrReOrderTotal();
            rd.Load(Path.Combine(Server.MapPath("~/Areas/FinancialAccountsModule/Reports/CrReOrderTotal.rpt")));
            rd.SetDataSource(model.ToList());
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            return File(stream, "application/pdf", "CrReOrderTotal.pdf");
        }
        //public ActionResult ReportsAddFundFees()
        //{

        //}
    }
}