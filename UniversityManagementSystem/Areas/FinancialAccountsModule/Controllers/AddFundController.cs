﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using UniversityManagementSystem.Areas.FinancialAccountsModule.Models;
using UniversityManagementSystem.Models;

namespace UniversityManagementSystem.Areas.FinancialAccountsModule.Controllers
{
    [Authorize]
    public class AddFundController : Controller
    {
        private UniversityManagementSystemEntities db = new UniversityManagementSystemEntities();

        // GET: FinancialAccountsModule/AddFund
        public ActionResult Index()
        {
            ViewBag.MainTitle = "Financial Accounts Module";
            ViewBag.SubTitle = "AddFund";

            return View(db.AddFunds.ToList());
        }

      
        // GET: FinancialAccountsModule/AddFund/Create
        public ActionResult Create()
        {
            ViewBag.MainTitle = "Accounts Module";
            ViewBag.SubTitle = "Add Fund";
            return View();
        }

        // POST: FinancialAccountsModule/AddFund/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(AddFundModel model)
        {
            
            if (ModelState.IsValid)
            {
                AddFund addFund = new AddFund()
                {
                    funAmount=model.funAmount,
                    fundHistory=DateTime.Now,
                    funNameOfOrganization=model.funNameOfOrganization,
                    funNotes=model.funNotes,

                };
                db.AddFunds.Add(addFund);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(model);
        }
        
        [HttpGet]
        public ActionResult Edit(int? id)
        {

            ViewBag.MainTitle = "Accounts Module";
            ViewBag.SubTitle = "Add Fund";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AddFund addFund = db.AddFunds.Find(id);
            if (addFund == null)
            {
                return HttpNotFound();
            }
            AddFundModel model = new AddFundModel()
            {
                funAmount=addFund.funAmount,
                fundHistory=Convert.ToDateTime( addFund.fundHistory),
                fundID=addFund.fundID,
                funNameOfOrganization=addFund.funNameOfOrganization,
                funNotes=addFund.funNotes

            };
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(AddFundModel model)
        {
            if (ModelState.IsValid)
            {
                AddFund addFund = new AddFund() {
                    funAmount=model.funAmount,
                    funNotes=model.funNotes,
                    fundID=model.fundID,
                    funNameOfOrganization=model.funNameOfOrganization,
                    fundHistory=model.fundHistory
                };
                db.Entry(addFund).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(model);
        }

      
  

        // POST: FinancialAccountsModule/AddFund/Delete/5
        [HttpPost]
        public JsonResult DeleteConfirmed(int id)
        {
            string mss = "";
            AddFund addFund = db.AddFunds.Find(id);
            db.AddFunds.Remove(addFund);
           int val= db.SaveChanges();
            if (val > 0)
            {
                mss = "del";
                return Json(mss,JsonRequestBehavior.AllowGet);
            }
            else
            {
                mss = "Sorry, try again";
                return Json(mss, JsonRequestBehavior.AllowGet);
            }

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
