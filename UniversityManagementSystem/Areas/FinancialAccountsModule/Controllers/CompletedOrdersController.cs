﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UniversityManagementSystem.Areas.FinancialAccountsModule.Models;
using UniversityManagementSystem.Areas.FinancialAccountsModule.Reports;
using UniversityManagementSystem.Models;

namespace UniversityManagementSystem.Areas.FinancialAccountsModule.Controllers
{
    [Authorize]
    public class CompletedOrdersController : Controller
    {
        UniversityManagementSystemEntities db = new UniversityManagementSystemEntities();
        // GET: FinancialAccountsModule/CompletedOrders
        public ActionResult Index()
        {
            ViewBag.MainTitle = "Financial Accounts Module";
            ViewBag.SubTitle = " Completed Orders";

            List<Order> model = db.Orders.Where(x => x.orderCase == true && x.orderAdmin == true && x.orderFinished == true).ToList();
            return View(model);
            
        }

        public ActionResult PreintInvoice(int orID)
        {
            Order or= db.Orders.Find(orID);
            Invoice invoice = db.Invoices.Single(u=> u.orID == orID);

            List<InvoicesDetail> PreintInvoices = db.InvoicesDetails.Where(u => u.INnumber == invoice.INnumber).ToList();
            List<PreintInvoices> PIDList = new List<PreintInvoices>();
            foreach (var item in PreintInvoices)
            {
                PreintInvoices PID = new PreintInvoices() {
                    Applicant = or.orderApplicant,
                    History = Convert.ToDateTime(or.orderHistory),
                    INnumber = invoice.INnumber,
                    ProductName = item.IdProductName,
                    Quantity = Convert.ToInt32(item.IdQuantity),
                    Supplier = item.IdSupplier,
                    UnitPrice = Convert.ToInt32(item.IdUnitPrice)

                };

                PIDList.Add(PID);
            }


            CryRePreintInvoice rd = new CryRePreintInvoice();
            rd.Load(Path.Combine(Server.MapPath("~/Areas/FinancialAccountsModule/Reports/CryRePreintInvoice.rpt")));
            rd.SetDataSource(PIDList.ToList());
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            return File(stream, "application/pdf", "CompletedOrdersInvoice.pdf");
        }
    }
}