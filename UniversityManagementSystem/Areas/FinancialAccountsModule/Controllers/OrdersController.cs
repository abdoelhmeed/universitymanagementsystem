﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UniversityManagementSystem.Areas.FinancialAccountsModule.Models;
using UniversityManagementSystem.Models;

namespace UniversityManagementSystem.Areas.FinancialAccountsModule.Controllers
{
    [Authorize]
    public class OrdersController : Controller
    {
        UniversityManagementSystemEntities db = new UniversityManagementSystemEntities();
        // GET: FinancialAccountsModule/Orders
        public ActionResult Index()
        {
            ViewBag.MainTitle = "Financial Accounts Module";
            ViewBag.SubTitle = "Orders";

            List<Order> model = db.Orders.ToList();
            return View(model);
        }
        [HttpGet]
        public ActionResult AddOrders()
        {

            return PartialView("AddOrders");
        }
        [HttpPost]
        public JsonResult AddnewOrders(string orderApplicant)
        {
            string mss = "";
            if (orderApplicant != null)
            {
                Order order = new Order()
                {
                    orderCase = false,
                    orderAdmin = false,
                    orderHistory = DateTime.Now,
                    orderFinished = false,
                    orderApplicant = orderApplicant,
                    ordestatus = "Not Sent"

                };
                db.Orders.Add(order);
                int val = db.SaveChanges();
                if (val > 0)
                {
                    mss = Convert.ToString(order.orID);
                    return Json(mss, JsonRequestBehavior.AllowGet);
                }
            }
            mss = "not";
            return Json(mss, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult AddOrderDetail(int OId)
        {
            ViewBag.MainTitle = "Financial Accounts Module";
            ViewBag.SubTitle = "Add Order Detail";
            List<OrderDetail> model = db.OrderDetails.Where(u => u.orID == OId).ToList();
            Order order = db.Orders.Find(OId);
            ViewBag.orID = order.orID;
            ViewBag.orderApplicant = order.orderApplicant;
            ViewBag.orderHistory = order.orderHistory;
            return View(model);
        }

        [HttpPost]
        public JsonResult AddDetail(OrderDetailModel model)
        {
            string mss = Convert.ToString(model.orID);
            OrderDetail OD = new OrderDetail() {
                odProductDetails = model.odProductDetails,
                odproductName = model.odproductName,
                odQuantity = model.odQuantity,
                orNotes = model.orNotes,
                orID = model.orID
            };
            db.OrderDetails.Add(OD);
            db.SaveChanges();
            return Json(mss, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DetailDelete(int OrDID)
        {
            var orDt = db.OrderDetails.Find(OrDID);
            int OId = Convert.ToInt32( orDt.orID);
            if (orDt != null)
            {
                db.OrderDetails.Remove(orDt);
                db.SaveChanges();
            }
            return RedirectToAction("AddOrderDetail", new { OId = OId });
        }

        public ActionResult OrderDel(int OId)
        {
            var orderDetail = db.OrderDetails.Where(u => u.orID == OId).ToList();
            if (orderDetail.Count > 0)
            {
                foreach (var item in orderDetail)
                {
                    db.OrderDetails.Remove(item);
                    db.SaveChanges();
                }
            }
            var order = db.Orders.Find(OId);
            if (order !=null)
            {
                db.Orders.Remove(order);
                db.SaveChanges();
            }

            return RedirectToAction("Index");
        }
       public ActionResult ActivateOrder(int orID)
        {
            Order order = db.Orders.Find(orID);
            if (order !=null)
            {
                order.orderCase = true;
                order.ordestatus = "Pending";
                db.Entry(order).State = EntityState.Modified;
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }

    }
}