﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UniversityManagementSystem.Models;
using UniversityManagementSystem.Areas.FinancialAccountsModule.Models;
using System.Data.Entity;

namespace UniversityManagementSystem.Areas.FinancialAccountsModule.Controllers
{
    [Authorize]
    public class OrderAuthenticateController : Controller
    {
        UniversityManagementSystemEntities db = new UniversityManagementSystemEntities();
        public ActionResult Index()
        {
            ViewBag.MainTitle = "Financial Accounts Module";
            ViewBag.SubTitle = "Authenticate Order";

            List<Order> model = db.Orders.Where(x => x.orderCase == true && x.orderAdmin==false).ToList();
            return View(model);
        }
       
        public ActionResult OrderDel(int OId)
        {
            var orderDetail = db.OrderDetails.Where(u => u.orID == OId).ToList();
            if (orderDetail.Count > 0)
            {
                foreach (var item in orderDetail)
                {
                    db.OrderDetails.Remove(item);
                    db.SaveChanges();
                }
            }
            var order = db.Orders.Find(OId);
            if (order != null)
            {
                db.Orders.Remove(order);
                db.SaveChanges();
            }

            return RedirectToAction("Index");
        }


        public ActionResult ActivateOrderAdmin(int orID)
        {
            Order order = db.Orders.Find(orID);
            if (order != null)
            {
                order.orderAdmin = true;
                order.ordestatus = "Approved";
                db.Entry(order).State = EntityState.Modified;
                db.SaveChanges();
            }

            int Nnumber = Convert.ToInt32(DateTime.Now.ToString("yymmssfff"));
            Invoice invoice = new Invoice()
            {
                INnumber = Nnumber,
                INDate = DateTime.Now,
                orID = orID,
                INDone = false,

            };
            db.Invoices.Add(invoice);
            db.SaveChanges();

            return RedirectToAction("Index");
        }
    }
}