﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UniversityManagementSystem.Areas.FinancialAccountsModule.Models;
using UniversityManagementSystem.Models;

namespace UniversityManagementSystem.Areas.FinancialAccountsModule.Controllers
{
    [Authorize]
    public class CollectedFeesController : Controller
    {
        UniversityManagementSystemEntities db = new UniversityManagementSystemEntities();
        // GET: FinancialAccountsModule/CollectedFees
        [HttpGet]
        public ActionResult Index()
        {
           
            var depid = db.R_Departments_data.Select(x => x.Department_Id).ToList();
            List<FeesAndDetp> model = new List<FeesAndDetp>();
            ViewBag.MainTitle = "Financial Accounts Module";
            ViewBag.SubTitle = "Collected Fees";
            var lidt = from FT in db.Fee_TuitionFees
                       join FD in db.Fee_TuitionFeesDescribtion
                       on FT.TF_Id equals FD.TF_Id 
                       select new FeesAndDetp {deptID=FT.Department_Id ,Fees=FD.TFD_TuitionFee,FeesData=FD.TFD_FeesDate};

            foreach (var item in depid)
            {
                var getlist = db.Fee_TuitionFeesDescribtion.Where(x => x.Fee_TuitionFees.Department_Id == item ).ToList();
                if (getlist.Count > 0)
                {
                    FeesAndDetp TotalFss = new FeesAndDetp()
                    {
                        deptID = item,
                        Fees = lidt.Where(x => x.deptID == item).Sum(i => i.Fees)

                    };
                    model.Add(TotalFss);

                }
              

            }
            ViewBag.StudyYear = new SelectList(db.R_StudyYear_data, "Year_Id", "Year_Name");
            ViewBag.FeesDept = model.ToList();
            return View();
        }
        [HttpPost]
        public ActionResult Index(CollectedYears collectedYears)
        {

            var depid = db.R_Departments_data.Select(x => x.Department_Id).ToList();
            List<FeesAndDetp> model = new List<FeesAndDetp>();
            ViewBag.MainTitle = "Financial Accounts Module";
            ViewBag.SubTitle = "Collected Fees";
            var lidt = from FT in db.Fee_TuitionFees
                       join FD in db.Fee_TuitionFeesDescribtion.Where(u=> u.Year_Id== collectedYears.Year_Id)
                       on FT.TF_Id equals FD.TF_Id
                       select new FeesAndDetp { deptID = FT.Department_Id, Fees = FD.TFD_TuitionFee, FeesData = FD.TFD_FeesDate };
            if (depid.Count > 0)
            {
                foreach (var item in depid)
                {
                    var getlist = db.Fee_TuitionFeesDescribtion.Where(x => x.Fee_TuitionFees.Department_Id == item).ToList();
                    if (getlist.Count > 0)
                    {
                        try
                        {
                            FeesAndDetp TotalFss = new FeesAndDetp()
                            {
                                deptID = item,
                                Fees = lidt.Where(x => x.deptID == item).Sum(i => i.Fees)

                            };
                            model.Add(TotalFss);
                        }
                        catch (Exception)
                        {

                           
                        }
                    

                    }


                }
            }
            
            
            ViewBag.StudyYear = new SelectList(db.R_StudyYear_data, "Year_Id", "Year_Name");
            ViewBag.FeesDept = model.ToList();
            return View();
        }
    }
}