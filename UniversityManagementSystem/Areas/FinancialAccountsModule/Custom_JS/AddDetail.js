﻿jQuery(function ($) {


    // new Player And Update current selecte Player
    $("#form_data").submit(function (event) {
        event.preventDefault();
        $('#form_data').validate({
            rules: {
                odproductName: {
                    required: true
                }, odProductDetails: {
                    required: true
                }, odQuantity: {
                    required: true,
                    number: true
                },
                orNotes: {
                    required: true
                }
            },
            messages: {
                odproductName: "product Name is Required",
                odProductDetails: "Product Details is Required",
                odQuantity: "Quantity is Required and Type Number",
                orNotes: "Notes is Required",

            },
            errorPlacement: function (error, element) { error.addClass("help-block"); element.parents(".form-group").addClass("has-feedback"); error.insertAfter(element); },
            highlight: function (element) { $(element).parents(".form-group").addClass("has-error").removeClass("has-success"); },
            unhighlight: function (element, errorClass, validClass) { $(element).parents(".form-group").addClass("has-success").removeClass("has-error"); },
            submitHandler: function (form) {
                $("#Loding").show();
                var data = new FormData(form);
                $.ajax({
                    type: 'POST',
                    url: '/FinancialAccountsModule/Orders/AddDetail',
                    data: data,
                    contentType: false,
                    processData: false,
                    success: function (mss) {
                        location.href = "/FinancialAccountsModule/Orders/AddOrderDetail?OId=" + mss;
                    },
                    error: function (data) {
                        alert("Sorry, try again");
                    }
                });
            }
        });
    });

   
});

   