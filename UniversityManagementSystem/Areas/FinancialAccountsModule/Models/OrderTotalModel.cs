﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UniversityManagementSystem.Areas.FinancialAccountsModule.Models
{
    public class OrderTotalModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public decimal Money { get; set; }
        public DateTime Date { get; set; }
        public DateTime dateForm { get; set; }
        public DateTime dateTO { get; set; }

    }
}