﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UniversityManagementSystem.Areas.FinancialAccountsModule.Models
{
    public class FeesAndDetp
    {
        public int? deptID { get; set; }
        public decimal Fees { get; set; }
        public DateTime FeesData { get; set; }
    }
}