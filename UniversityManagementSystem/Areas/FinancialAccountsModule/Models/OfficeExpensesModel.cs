﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace UniversityManagementSystem.Areas.FinancialAccountsModule.Models
{
    public class OfficeExpensesModel
    {
        [Display(Name = "ID")]
        public int ID { get; set; }
        public string Organization { get; set; }
        [Required]
        [Display(Name = "Amount")]
        public decimal amount { get; set; }
        public DateTime History { get; set; }
        [Required]
        [Display(Name = "Expense Type")]
        public string expenseType { get; set; }
        [Required]
        [Display(Name = "Bill Number")]
        public string billnumber { get; set; }
        [Required]
        [Display(Name = "Photo invoice")]
        public string Photoinvoice { get; set; }
        public string Notes { get; set; }
        [Display(Name = "User ")]
        public string UserID { get; set; }
    }
}