﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UniversityManagementSystem.Areas.FinancialAccountsModule.Models
{
    public class OrderDetailModel
    {
       
        public string odproductName { get; set; }
        public string odProductDetails { get; set; }
        public int odQuantity { get; set; }
        public string orNotes { get; set; }
        public int orID { get; set; }
    }
}