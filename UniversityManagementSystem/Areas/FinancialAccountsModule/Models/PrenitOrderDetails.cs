﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UniversityManagementSystem.Areas.FinancialAccountsModule.Models
{
    public class PrenitOrderDetails
    {
        public int orID { get; set; }
        public string Applicant { get; set; }
        public DateTime History { get; set; }
        public string productName { get; set; }
        public int Quantity { get; set; }
        public string Notes { get; set; }
      
    }
}