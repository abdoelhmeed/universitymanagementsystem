﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UniversityManagementSystem.Areas.FinancialAccountsModule.Models
{
    public class PreintInvoices
    {
       
        public int INnumber { get; set; }
        public string Applicant { get; set; }
        public DateTime History { get; set; }
        public string ProductName { get; set; }
        public string Supplier { get; set; }
        public int Quantity { get; set; }
        public decimal UnitPrice { get; set; }
        

    }
}