﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UniversityManagementSystem.Areas.FinancialAccountsModule.Models
{
    public class InvoicesDetailModel
    {
        public string IdProductName { get; set; }
        public string IdSupplier { get; set; }
        public int IdQuantity { get; set; }
        public decimal IdUnitPrice { get; set; }
        public int INnumber { get; set; }
        public int  orID { get; set; }
        public string IdNumber { get; set; }
    }
}