﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace UniversityManagementSystem.Areas.FinancialAccountsModule.Models
{
    public class SalariesModel
    {
        [Display(Name = "ID")]
        public int ID { get; set; }
        [Required]
        [Display(Name = "Amount")]
        public decimal amount { get; set; }
        public DateTime History { get; set; }
        [Required]
        [Display(Name = "Employee ID")]
        public int EmpID {get;set;}
        public string Notes { get; set; }
        [Display(Name = "User ")]
        public string UserID { get; set; }
    }
}