﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace UniversityManagementSystem.Areas.FinancialAccountsModule.Models
{
    public class AddFundModel
    {
        [Display(Name ="ID")]
        public int fundID { get; set; }
        [Display(Name = "Date")]
        public DateTime fundHistory { get; set; }
        [Display(Name = "Organization")]
        [Required]
        public string funNameOfOrganization { get; set; }
        [Display(Name = "Amount")]
        [Required]
        public Nullable<decimal> funAmount { get; set; }
        [Display(Name = "Notes")]
        [Required]
        public string funNotes { get; set; }


    }
}