﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UniversityManagementSystem.Areas.FinancialAccountsModule.Models
{
    public class ReportsFeesAndDetp
    {
        public string deptName { get; set; }
        public decimal Fees { get; set; }
        public DateTime DatForm { get; set; }
        public DateTime DatTo { get; set; }
    }
}