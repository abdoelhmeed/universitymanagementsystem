﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UniversityManagementSystem.Areas.FinancialAccountsModule.Models
{
    public class ReAddFund
    {
        
        public int fundID { get; set; }
        public DateTime fundHistory { get; set; }
        public string funNameOfOrganization { get; set; }
        public decimal funAmount { get; set; }
        public string funNotes { get; set; }
        public DateTime dateForm { get; set; }
        public DateTime dateTO { get; set; }
    }
}