﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace UniversityManagementSystem.Areas.FinancialAccountsModule.Models
{
    public class OrderModel
    {
        [Display(Name ="ID")]
        public int orID { get; set; }
        [Display(Name = "Applicant")]
        public string orderApplicant { get; set; }
        [Display(Name = "History")]
        public DateTime orderHistory { get; set; }
        [Display(Name = "Case")]
        public bool orderCase { get; set; }
        [Display(Name = "Admin")]
        public bool orderAdmin { get; set; }
    }
}