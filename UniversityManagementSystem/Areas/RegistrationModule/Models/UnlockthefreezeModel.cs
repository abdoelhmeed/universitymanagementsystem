﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace UniversityManagementSystem.Areas.RegistrationModule.Models
{
    public class UnlockthefreezeModel
    {

        public int RegistrationID { get; set; }
        public int SR_Id { get; set; }
        public int Student_Id { get; set; }
        public string Student_Name { get; set; }
        public string Student_Number { get; set; }
        public int Year_Id { get; set; }
        public string Year_Name { get; set; }
        [Required(ErrorMessage = "Semester is Required")]
        public int Semester_Id { get; set; }
        public string Semester_Name { get; set; }
        public int Department_Id { get; set; }
        public string Department_Name { get; set; }
        public string User_Id { get; set; }
        [Required(ErrorMessage = "Batch is Required")]
        public int batch_Id { get; set; }
        public string batch_Name { get; set; }
        public Nullable<bool> Registered { get; set; }
        public string Registr_Status { get; set; }
    }
}