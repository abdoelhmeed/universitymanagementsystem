﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace UniversityManagementSystem.Areas.RegistrationModule.Models
{
    public class Attachment
    {
    
        public int attachment_Id { get; set; }
        public Nullable<int> Student_id { get; set; }
        [Display(Name = "Student Number")]
        public string Student_Number { get; set; }
        [Display(Name = "Student FullName")]
        public string Student_FullName { get; set; }
        [Display(Name = "College")]
        public string Student_College { get; set; }
        [Display(Name = "Department")]
        public string Department { get; set; }
        public string description { get; set; }
        public byte[] attachfile { get; set; }
    }
}