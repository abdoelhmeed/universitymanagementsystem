﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace UniversityManagementSystem.Areas.RegistrationModule.Models
{
    public class StudentInfoDataModel
    {
        public int Student_Id { get; set; }

        [Required(ErrorMessage = "Required is Student Number")]
        public string Student_Number { get; set; }
        [Required(ErrorMessage = "Required is Full Name")]
        public string Student_FullName { get; set; }
        [Required(ErrorMessage = "Required is Nationality")]
        public string Student_Nationality { get; set; }
        [Required(ErrorMessage = "Required is Passport Or National No")]
        public string Student_PassportOrNationalNo { get; set; }
        [Required(ErrorMessage = "Required is Gender")]
        public string Student_Sex { get; set; }
        [Required(ErrorMessage = "Required is Place Of Birth")]
        public string Student_PlaceOfBirth { get; set; }
        [Required(ErrorMessage = " Required is Native Language")]
        public string Student_NativeLanguage { get; set; }
        [Required(ErrorMessage = "Required is Present Address")]
        public string Student_PresentAddress { get; set; }
        [Required(ErrorMessage = "Required is Student Tel")]
        [DataType(DataType.PhoneNumber)]
        public int Student_Tel { get; set; }
        [Required(ErrorMessage = "Required is Student Email")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        public string Student_Email { get; set; }
        [Required(ErrorMessage = "Required is  Permanant Address")]
        public string Student_PermanantAddress { get; set; }
        [Required(ErrorMessage = "Required is  Name")]
        public string Student_EmerInSName { get; set; }
        [Required(ErrorMessage = "Required is  Email")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        public string Student_EmerInSEmail { get; set; }
        [Required(ErrorMessage = "Required is  Email")]
        [DataType(DataType.PhoneNumber)]
        public Nullable<int> Student_EmerInSTel { get; set; }
        public string Student_EmerOutSName { get; set; }
        public Nullable<int> Student_EmerOutSTel { get; set; }
        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        public string Student_EmerOutSEmail { get; set; }
        [Required(ErrorMessage = "Required is  Name certificate")]
        public string Student_Nameofcertificate { get; set; }
        [Required(ErrorMessage = "Required is  Certificate Date")]
      
        public System.DateTime Student_CertificateDate { get; set; }
        [Required(ErrorMessage = "Required is  General Percentage")]
        public int Student_GeneralPercentage { get; set; }
        [Required(ErrorMessage = "Required is  College")]
        public string Student_College { get; set; }
        [Required(ErrorMessage = "Required is  Department")]
        public string Department { get; set; }
        [Required(ErrorMessage = "Required is  Student Status")]
        public Nullable<bool> Student_Status { get; set; }
        public string Student_img { get; set; }
        public HttpPostedFileBase CertificateImage { get; set; }
        public HttpPostedFileBase Image { get; set; }
    }
}