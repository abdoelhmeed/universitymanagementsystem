﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace UniversityManagementSystem.Areas.RegistrationModule.Models
{
    public class StudentRegistration
    {
        public int RegistrationID { get; set; }
        public int SR_Id { get; set; }
        public int Student_Id { get; set; }
        public string Student_Name { get; set; }
        public int Year_Id { get; set; }
        public string Year_Name { get; set; }
        public int Semester_Id { get; set; }
        public string Semester_Name{ get; set; }
        public int Department_Id { get; set; }
        public string Department_Name { get; set; }
        public string User_Id { get; set; }
        public int batch_Id { get; set; }
        public string batch_Name { get; set; }
        public Nullable<bool> Registered { get; set; }
        public string Registr_Status { get; set; }
    }


    public class StudentRegistrationModel
    {
        public int RegistrationID { get; set; }
        public int SR_Id { get; set; }
        public string SudNumber { get; set; }
        public int Student_Id { get; set; }
        public string Student_Name { get; set; }
        public int Year_Id { get; set; }
        public string Year_Name { get; set; }
        public int Semester_Id { get; set; }
        public string Semester_Name { get; set; }
        public int Department_Id { get; set; }
        public string Department_Name { get; set; }
        public string User_Id { get; set; }
        public int batch_Id { get; set; }
        public string batch_Name { get; set; }
        public Nullable<bool> Registered { get; set; }
        public string Registr_Status { get; set; }
    }

    public class ForNewStudentRegistration
    {
        public int SR_Id { get; set; }
        public int Student_Id { get; set; }
        public string Student_Name { get; set; }
        [Required(ErrorMessage = "Batch is Year")]
        public int Year_Id { get; set; }
        public string Year_Name { get; set; }
        [Required(ErrorMessage = "Batch is Semester")]
        public int Semester_Id { get; set; }
        public string Semester_Name { get; set; }
        public int Department_Id { get; set; }
        public string Department_Name { get; set; }
        public string User_Id { get; set; }
        [Required(ErrorMessage = "Batch is Required")]
        public int batch_Id { get; set; }
        public string batch_Name { get; set; }
        public bool Registered { get; set; }
        public string Registr_Status { get; set; }

        public Nullable<bool> Student_Status { get; set; }
    }




}