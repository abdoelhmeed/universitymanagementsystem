﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UniversityManagementSystem.Areas.RegistrationModule.Models
{
    public class SmpModel
    {
        public int smpid { get; set; }
        public string smpname { get; set; }
        public int smpNumber { get; set; }
    }
}