﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace UniversityManagementSystem.Areas.RegistrationModule.Models
{
    public class DepartmentsModel
    {
        [Display(Name ="ID")]
        public int Department_Id { get; set; }
        [Display(Name = "Department Name")]
        [Required]
        public string Department_Name { get; set; }
        [Display(Name = "Fees")]
        [Required]
        public decimal Department_Fees { get; set; }
        [Display(Name = "Semester Number")]
        [Required]
        public int Semester_number { get; set; }
        [Display(Name = "College")]
        [Required]
        public int Coll_Id { get; set; }

        public HttpPostedFileBase DepImg { get; set; }
        public string DepImgName { get; set; }
        [Display(Name = "Details")]
        [Required]
        public string DeptDtails { get; set; }
        [Display(Name = "Requirements")]
        [Required]
        public string DepRequirements { get; set; }
    }
}