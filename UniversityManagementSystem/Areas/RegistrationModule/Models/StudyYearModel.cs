﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace UniversityManagementSystem.Areas.RegistrationModule.Models
{
    public class StudyYearModel
    {
        [Display(Name ="ID")]
        public int Year_Id { get; set; }
        [Display(Name = "Year Number")]
        public int Year_Number { get; set; }
        [Display(Name = "Year Name")]
        public string Year_Name { get; set; }
        [Display(Name = "Year Start")]
        [Required]
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> YearStart { get; set; }
        [Display(Name = "Year End")]
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> YearEnd { get; set; }
    }
}