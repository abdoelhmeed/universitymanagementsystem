﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UniversityManagementSystem.Areas.RegistrationModule.Models
{
    public class FreezingModel
    {
        public int fid { get; set; }
        public Nullable<int> fStudent_Id { get; set; }
        public string fStudentnumber { get; set; }
        public string FullName { get; set; }
        public string fStudentDept{ get; set; }
        public Nullable<System.DateTime> freezingstartDate { get; set; }
        public Nullable<bool> fStatus { get; set; }
        public string fStatusName { get; set; }
        public Nullable<System.DateTime> freezingdoneDate { get; set; }

    }
}