﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UniversityManagementSystem.Areas.RegistrationModule.Models
{
    public class NoticePaymentModel
    {
        public int StdID { get; set; }
        public string StdName { get; set; }

        public string stdNumber { get; set; }
        public string CollegeName { get; set; }

        public decimal money { get; set; }

        public string years { get; set; }
    }
}