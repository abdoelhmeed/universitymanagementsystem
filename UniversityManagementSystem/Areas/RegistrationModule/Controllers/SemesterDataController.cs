﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using UniversityManagementSystem.Models;

namespace UniversityManagementSystem.Areas.RegistrationModule.Controllers
{
    public class SemesterDataController : Controller
    {
        private UniversityManagementSystemEntities db = new UniversityManagementSystemEntities();

        // GET: RegistrationModule/SemesterData
        public ActionResult Index()
        {
            ViewBag.MainTitle = "University Setting Module";
            ViewBag.SubTitle = "Semester Setting";
            return View(db.R_Semester_data.ToList());
        }

        // GET: RegistrationModule/SemesterData/Details/5
        public ActionResult Details(int? id)
        {
            ViewBag.MainTitle = "University Setting Module";
            ViewBag.SubTitle = "Semester Setting";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            R_Semester_data r_Semester_data = db.R_Semester_data.Find(id);
            if (r_Semester_data == null)
            {
                return HttpNotFound();
            }
            return View(r_Semester_data);
        }

        // GET: RegistrationModule/SemesterData/Create
        public ActionResult Create()
        {
           
            ViewBag.MainTitle = "University Setting Module";
            ViewBag.SubTitle = "Semester Setting";
            List<SemesterType> SeTye = new List<SemesterType>()
            {
                new SemesterType{Number =1,TypeName="Somebody"},
                new SemesterType{Number =2 ,TypeName="even"}
            };
            ViewBag.SemesterType = new SelectList(SeTye, "Number", "Somebody");
            return View();
        }

        // POST: RegistrationModule/SemesterData/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Semester_Id,Semester_Number,Semester_Name,Semester_Type")] R_Semester_data r_Semester_data)
        {
            ViewBag.MainTitle = "University Setting Module";
            ViewBag.SubTitle = "Semester Setting";
            List<SemesterType> SeTye = new List<SemesterType>()
            {
                new SemesterType{Number =1,TypeName="Somebody"},
                new SemesterType{Number =2 ,TypeName="even"}
            };
            ViewBag.SemesterType = new SelectList(SeTye, "Number", "Somebody");
            if (ModelState.IsValid)
            {
                R_Semester_data sd = new R_Semester_data
                {
                    Semester_Number = db.R_Semester_data.Any() == true? db.R_Semester_data.OrderByDescending(s => s.Semester_Number).FirstOrDefault().Semester_Number + 1 : 1,
                    Semester_Name = r_Semester_data.Semester_Name,
                    Semester_Type = r_Semester_data.Semester_Type
                };
                db.R_Semester_data.Add(sd);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(r_Semester_data);
        }

        // GET: RegistrationModule/SemesterData/Edit/5
        public ActionResult Edit(int? id)
        {
            List<SemesterType> SeTye = new List<SemesterType>()
            {
                new SemesterType{Number =1,TypeName="Somebody"},
                new SemesterType{Number =2 ,TypeName="even"}
            };
            
            ViewBag.SemesterType = new SelectList(SeTye, "Number", "Somebody");
            ViewBag.MainTitle = "University Setting Module";
            ViewBag.SubTitle = "Semester Setting";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            R_Semester_data r_Semester_data = db.R_Semester_data.Find(id);
            if (r_Semester_data == null)
            {
                return HttpNotFound();
            }
            return View(r_Semester_data);
        }

        // POST: RegistrationModule/SemesterData/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Semester_Id,Semester_Number,Semester_Name,Semester_Type")] R_Semester_data r_Semester_data)
        {
            List<SemesterType> SeTye = new List<SemesterType>()
            {
                new SemesterType{Number =1,TypeName="Somebody"},
                new SemesterType{Number =2 ,TypeName="even"}
            };
            ViewBag.SemesterType = new SelectList(SeTye, "Number", "Somebody");
            if (ModelState.IsValid)
            {
                R_Semester_data sd = db.R_Semester_data.Find(r_Semester_data.Semester_Id);
                sd.Semester_Name = r_Semester_data.Semester_Name;
                sd.Semester_Type = r_Semester_data.Semester_Type;
                db.Entry(sd).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(r_Semester_data);
        }

        // GET: RegistrationModule/SemesterData/Delete/5
        public ActionResult Delete(int? id)
        {
            ViewBag.MainTitle = "University Setting Module";
            ViewBag.SubTitle = "Semester Setting";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            R_Semester_data r_Semester_data = db.R_Semester_data.Find(id);
            if (r_Semester_data == null)
            {
                return HttpNotFound();
            }
            return View(r_Semester_data);
        }

        // POST: RegistrationModule/SemesterData/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            R_Semester_data r_Semester_data = db.R_Semester_data.Find(id);
            db.R_Semester_data.Remove(r_Semester_data);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
    class SemesterType
    {
        public int Number { get; set; }
        public string TypeName { get; set; }
    }
}
