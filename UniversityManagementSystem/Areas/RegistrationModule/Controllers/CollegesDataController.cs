﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using UniversityManagementSystem.Models;

namespace UniversityManagementSystem.Areas.RegistrationModule.Controllers
{
    public class CollegesDataController : Controller
    {
        private UniversityManagementSystemEntities db = new UniversityManagementSystemEntities();

        // GET: RegistrationModule/CollegesData
        public ActionResult Index()
        {
            ViewBag.MainTitle = "University Setting Module";
            ViewBag.SubTitle = "Colleges Setting";
            return View(db.R_Colleges_data.ToList());
        }

        // GET: RegistrationModule/CollegesData/Details/5
        public ActionResult Details(int? id)
        {
            ViewBag.MainTitle = "University Setting Module";
            ViewBag.SubTitle = "Colleges Setting";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            R_Colleges_data r_Colleges_data = db.R_Colleges_data.Find(id);
            if (r_Colleges_data == null)
            {
                return HttpNotFound();
            }
            return View(r_Colleges_data);
        }

        // GET: RegistrationModule/CollegesData/Create
        public ActionResult Create()
        {
            ViewBag.MainTitle = "University Setting Module";
            ViewBag.SubTitle = "Colleges Setting";
            return View();
        }

        // POST: RegistrationModule/CollegesData/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Coll_Id,Coll_Name")] R_Colleges_data r_Colleges_data)
        {
            ViewBag.MainTitle = "University Setting Module";
            ViewBag.SubTitle = "Colleges Setting";
            if (ModelState.IsValid)
            {
                db.R_Colleges_data.Add(r_Colleges_data);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(r_Colleges_data);
        }

        // GET: RegistrationModule/CollegesData/Edit/5
        public ActionResult Edit(int? id)
        {
            ViewBag.MainTitle = "University Setting Module";
            ViewBag.SubTitle = "Colleges Setting";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            R_Colleges_data r_Colleges_data = db.R_Colleges_data.Find(id);
            if (r_Colleges_data == null)
            {
                return HttpNotFound();
            }
            return View(r_Colleges_data);
        }

        // POST: RegistrationModule/CollegesData/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Coll_Id,Coll_Name")] R_Colleges_data r_Colleges_data)
        {
            ViewBag.MainTitle = "University Setting Module";
            ViewBag.SubTitle = "Colleges Setting";
            if (ModelState.IsValid)
            {
                db.Entry(r_Colleges_data).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(r_Colleges_data);
        }

        // GET: RegistrationModule/CollegesData/Delete/5
        public ActionResult Delete(int? id)
        {
            ViewBag.MainTitle = "University Setting Module";
            ViewBag.SubTitle = "Colleges Setting";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            R_Colleges_data r_Colleges_data = db.R_Colleges_data.Find(id);
            if (r_Colleges_data == null)
            {
                return HttpNotFound();
            }
            return View(r_Colleges_data);
        }

        // POST: RegistrationModule/CollegesData/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                ViewBag.MainTitle = "University Setting Module";
                ViewBag.SubTitle = "Colleges Setting";
                R_Colleges_data r_Colleges_data = db.R_Colleges_data.Find(id);
                db.R_Colleges_data.Remove(r_Colleges_data);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch (Exception)
            {

                return RedirectToAction("Index");
            }
            
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
