﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using UniversityManagementSystem.Models;
using UniversityManagementSystem.Areas.RegistrationModule.Models;
using UniversityManagementSystem.Areas.Fees.Models;
using UniversityManagementSystem.Areas.RegistrationModule.Reports;

using System.IO;
using CrystalDecisions.CrystalReports.Engine;
using Microsoft.AspNet.Identity;

namespace UniversityManagementSystem.Areas.RegistrationModule.Controllers
{
    [Authorize]
    public class StudentRegistrationController : Controller
    {
        private UniversityManagementSystemEntities db = new UniversityManagementSystemEntities();

        // GET: RegistrationModule/StudentRegistration
        [HttpGet]
        public ActionResult Index()
        {
            ViewBag.MainTitle = "Registration Module";
            ViewBag.SubTitle = "Registration";
            List<R_BatchNumber_data> BatchNumber = db.R_BatchNumber_data.ToList();
            List<R_Semester_data> Semester = db.R_Semester_data.ToList();
            List<R_StudyYear_data> StudyYear = db.R_StudyYear_data.ToList();
            List<R_Departments_data> Departments = db.R_Departments_data.ToList();
            ViewBag.BatchNumber =new SelectList(BatchNumber, "batch_Id", "batch_Name");
            ViewBag.Semester = new SelectList(Semester, "Semester_Id", "Semester_Name");
            ViewBag.StudyYear = new SelectList(StudyYear, "Year_Id", "Year_Name");
            ViewBag.Departments = new SelectList(Departments, "Department_Id", "Department_Name");
            List<StudentRegistration> studentRegistration = db.R_StudentRegistration_data.Where(u => u.Registered == false ).Select(x => new StudentRegistration {
                Student_Name = x.R_StudentInfo_data.Student_FullName,
                Student_Id = x.Student_Id,
                RegistrationID=x.SR_Id,
                Semester_Name = x.R_Semester_data.Semester_Name,
                Semester_Id = x.Semester_Id,
                Year_Name = x.R_StudyYear_data.Year_Name,
                Year_Id = x.Year_Id,
                Department_Name = x.R_Departments_data.Department_Name,
                Department_Id=x.Department_Id,
                batch_Name = x.R_BatchNumber_data.batch_Name,
                batch_Id=x.batch_Id,
                Registered = x.Registered,
                Registr_Status = x.Registr_Status,
               
            }).ToList();
            ViewBag.StudentRegistration = studentRegistration;
         
            return View();
        }

        public ActionResult Studentinfo()
        {
            ViewBag.MainTitle = "Registration Module";
            ViewBag.SubTitle = "Studentinfo";
            //List<R_BatchNumber_data> BatchNumber = db.R_BatchNumber_data.ToList();
            //List<R_Semester_data> Semester = db.R_Semester_data.ToList();
            //List<R_StudyYear_data> StudyYear = db.R_StudyYear_data.ToList();
            //List<R_Departments_data> Departments = db.R_Departments_data.ToList();
            //ViewBag.BatchNumber = new SelectList(BatchNumber, "batch_Id", "batch_Name");
            //ViewBag.Semester = new SelectList(Semester, "Semester_Id", "Semester_Name");
            //ViewBag.StudyYear = new SelectList(StudyYear, "Year_Id", "Year_Name");
            //ViewBag.Departments = new SelectList(Departments, "Department_Id", "Department_Name");
            List<StudentRegistrationModel> studentRegistration = db.R_StudentRegistration_data.Where(u => u.Registered == true).Select(x => new StudentRegistrationModel
            {
                SudNumber=x.R_StudentInfo_data.Student_Number,
                Student_Name = x.R_StudentInfo_data.Student_FullName,
                Student_Id = x.Student_Id,
                RegistrationID = x.SR_Id,
                Semester_Name = x.R_Semester_data.Semester_Name,
                Semester_Id = x.Semester_Id,
                Year_Name = x.R_StudyYear_data.Year_Name,
                Year_Id = x.Year_Id,
                Department_Name = x.R_Departments_data.Department_Name,
                Department_Id = x.Department_Id,
                batch_Name = x.R_BatchNumber_data.batch_Name,
                batch_Id = x.batch_Id,
                Registered = x.Registered,
                Registr_Status = x.Registr_Status,

            }).ToList();
            ViewBag.StudentRegistration = studentRegistration;

            return View(studentRegistration);
        }

        public JsonResult setRegistrationture(int SR_Id)
        {
            ViewBag.MainTitle = "Registration Module";
            ViewBag.SubTitle = "Registration";
            R_StudentRegistration_data StudentRegistration = db.R_StudentRegistration_data.Find(SR_Id);
            if (StudentRegistration !=null)
            {
                StudentRegistration.Registered = true;
                db.Entry(StudentRegistration).State = EntityState.Modified;
                int val= db.SaveChanges();
                if (val > 0)
                {
                    string mss = "Registration Complet";
                    return Json(mss, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string mss = "Pre-registered";
                    return Json(mss, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                string mss = "Pre-registered";
                return Json(mss, JsonRequestBehavior.AllowGet);
            }

           
        }
        [HttpPost]
        public ActionResult Index(StudentRegistration model)
        {
            ViewBag.MainTitle = "Registration Module";
            ViewBag.SubTitle = "Registration";
            List<R_BatchNumber_data> BatchNumber = db.R_BatchNumber_data.ToList();
            List<R_Semester_data> Semester = db.R_Semester_data.ToList();
            List<R_StudyYear_data> StudyYear = db.R_StudyYear_data.ToList();
            List<R_Departments_data> Departments = db.R_Departments_data.ToList();
            ViewBag.BatchNumber = new SelectList(BatchNumber, "batch_Id", "batch_Name");
            ViewBag.Semester = new SelectList(Semester, "Semester_Id", "Semester_Name");
            ViewBag.StudyYear = new SelectList(StudyYear, "Year_Id", "Year_Name");
            ViewBag.Departments = new SelectList(Departments, "Department_Id", "Department_Name");
              List<StudentRegistration> studentRegistration = db.R_StudentRegistration_data.Where(u=> u.Year_Id==model.Year_Id && u.Semester_Id==model.Semester_Id && u.Department_Id == model.Department_Id && u.Registr_Status != "freezing").Select(x => new StudentRegistration
            {
                Student_Name = x.R_StudentInfo_data.Student_FullName,
                Student_Id = x.Student_Id,
                Semester_Name = x.R_Semester_data.Semester_Name,
                Semester_Id = x.Semester_Id,
                Year_Name = x.R_StudyYear_data.Year_Name,
                Year_Id = x.Year_Id,
                Department_Name = x.R_Departments_data.Department_Name,
                Department_Id = x.Department_Id,
                batch_Name = x.R_BatchNumber_data.batch_Name,
                batch_Id = x.batch_Id,
                Registered = x.Registered,
                Registr_Status = x.Registr_Status
            }).ToList();
            ViewBag.StudentRegistration = studentRegistration;

            return View();
        }

        // GET: RegistrationModule/StudentRegistration/Details/5
        public ActionResult Details(int? id)
        {
            ViewBag.MainTitle = "Registration Module";
            ViewBag.SubTitle = "Registration";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            R_StudentRegistration_data r_StudentRegistration_data = db.R_StudentRegistration_data.Find(id);
            if (r_StudentRegistration_data == null)
            {
                return HttpNotFound();
            }
            return View(r_StudentRegistration_data);
        }

        // GET: RegistrationModule/StudentRegistration/Create
        public ActionResult Create()
        {

            ViewBag.MainTitle = "Registration Module";
            ViewBag.SubTitle = "Registration";
            ViewBag.User_Id = new SelectList(db.AspNetUsers, "Id", "Email");
            ViewBag.batch_Id = new SelectList(db.R_BatchNumber_data, "batch_Id", "batch_Name");
            ViewBag.Department_Id = new SelectList(db.R_Departments_data, "Department_Id", "Department_Name");
            ViewBag.Semester_Id = new SelectList(db.R_Semester_data, "Semester_Id", "Semester_Name");
            ViewBag.Year_Id = new SelectList(db.R_StudyYear_data, "Year_Id", "Year_Name");
            ViewBag.Student_Id = new SelectList(db.R_StudentInfo_data, "Student_Id", "Student_Number");
            return View();
        }


        // تسخدم مرة واحدة فقط عند تسجيل طالب جديد في Student Info
        [HttpGet]
        public JsonResult NewRegistration(int StudentID)
        {
            ViewBag.MainTitle = "Registration Module";
            ViewBag.SubTitle = "Registration";
            string mss = "";

            R_StudentRegistration_data StudentRegistration = db.R_StudentRegistration_data.FirstOrDefault(u => u.Student_Id == StudentID);
            if (StudentRegistration==null)
            {

            
                    ForNewStudentRegistration model = new ForNewStudentRegistration();
                    List<R_Departments_data> Departments = db.R_Departments_data.ToList();
                    ViewBag.Departments = new SelectList(Departments, "Department_Id", "Department_Name");
                    R_StudentInfo_data StudentInfo = db.R_StudentInfo_data.Find(StudentID);
                    if (StudentInfo != null)
                    {
                        string Colleges = StudentInfo.Student_College;
                        string Department = StudentInfo.Department;
                        R_Colleges_data Collg = db.R_Colleges_data.First(u => u.Coll_Name == Colleges);
                        R_Departments_data Dept = db.R_Departments_data.First(u => u.Department_Name == Department);
                        model.Student_Name = StudentInfo.Student_FullName;
                        model.Student_Id = StudentInfo.Student_Id;
                        model.Department_Id = Dept.Department_Id;
                        //model.Department_Name = Dept.Department_Name;
                }


                mss = NewRegistration(model);
                return Json(mss,JsonRequestBehavior.AllowGet);
            }
            else
            {
                mss = "Student in Registration Is already exist";
                return Json(mss, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public string NewRegistration(ForNewStudentRegistration model)
        {
            ViewBag.MainTitle = "Registration Module";
            ViewBag.SubTitle = "Registration";
            string mss = " ";
           
            List<R_Departments_data> Departments = db.R_Departments_data.ToList();
            ViewBag.Departments = new SelectList(Departments, "Department_Id", "Department_Name");
            string studentName = db.R_StudentInfo_data.Find(model.Student_Id).Student_FullName;
            model.Student_Name = studentName;
            List<R_StudentRegistration_data> STR = db.R_StudentRegistration_data.Where(u => u.Semester_Id == model.Semester_Id&&u.Student_Id==model.Student_Id && u.Year_Id == model.Year_Id).ToList();
            if (STR.Count == 0)
            {        
                
                    if (Vald(model))
                    {
                    
                    int Year = DateTime.Now.Year;
                    R_StudyYear_data Yeardat = db.R_StudyYear_data.SingleOrDefault(u=>u.Year_Number==Year);
                    if (Yeardat == null)
                    {
                        R_StudyYear_data SYD = new R_StudyYear_data()
                        {
                            Year_Number = Year,
                            Year_Name = Convert.ToString(Year) + "-" + Convert.ToString(Year + 1),
                            YearStart = DateTime.Now,
                            YearEnd = DateTime.Now.AddMonths(12),
                        };
                        db.R_StudyYear_data.Add(SYD);
                        db.SaveChanges();
                        model.Year_Id = 0;
                        model.Year_Id = SYD.Year_Id;
                    }
                    else
                    {
                        model.Year_Id = db.R_StudyYear_data.First(x=>x.Year_Number==Year).Year_Id;
                    }

                    R_BatchNumber_data Batch = db.R_BatchNumber_data.SingleOrDefault(u => u.Department_Id == model.Department_Id && u.Year_Id==model.Year_Id);
                    if (Batch == null)
                    {
                       List< R_BatchNumber_data> BatchNumbers = db.R_BatchNumber_data.Where(u => u.Department_Id == model.Department_Id).ToList();
                        int bber = BatchNumbers.Count + 1;
                        R_BatchNumber_data NewBatch = new R_BatchNumber_data()
                        {
                            Year_Id = model.Year_Id,
                            Department_Id = model.Department_Id,
                            batch_Number = bber,
                            batch_Name= db.R_Departments_data.SingleOrDefault(x=>x.Department_Id==model.Department_Id).Department_Name.ToString()+" -Batch "+ Convert.ToString( BatchNumbers.Count + 1),

                        };
                        db.R_BatchNumber_data.Add(NewBatch);
                        db.SaveChanges();
                        model.batch_Id = NewBatch.batch_Id;
                    }
                    else
                    {
                        model.batch_Id = Batch.batch_Id;
                    }

                    R_Semester_data Set = db.R_Semester_data.FirstOrDefault(u=> u.Semester_Number==1);
                    if (Set !=null)
                    {
                        model.Semester_Id = Set.Semester_Id;
                    }
                    else
                    {
                        R_Semester_data Sem = new R_Semester_data()
                        {
                           Semester_Name= "Semester 1",
                           Semester_Number=1,
                           Semester_Type=1,

                        };
                        db.R_Semester_data.Add(Sem);
                        db.SaveChanges();
                        model.Semester_Id = Sem.Semester_Id;
                    }
                  

                        R_StudentRegistration_data StdReg = new R_StudentRegistration_data()
                    {
                        Department_Id = model.Department_Id,
                        batch_Id = model.batch_Id,
                        Semester_Id = model.Semester_Id,
                        Year_Id = model.Year_Id,
                        Student_Id = model.Student_Id,
                        Registered = false,
                        User_Id = User.Identity.GetUserId(),
                        Registr_Status = "Formal",
                        };
                        db.R_StudentRegistration_data.Add(StdReg);
                       int val= db.SaveChanges();
                        if (val > 0)
                        {

                        return mss = "Registered";
                    }
                        else
                        {

                        return mss = "Is not Saved,peals try again";
                    }
                    }
                    else
                    {

                    return mss = "select all items if form,peals try again";
                }
             }
            else
            {

                return mss = "Student is already exists";
            }


        }

      


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "SR_Id,Student_Id,Year_Id,Semester_Id,Department_Id,User_Id,batch_Id,Registered,Registr_Status")] R_StudentRegistration_data r_StudentRegistration_data)
        {
            ViewBag.MainTitle = "Registration Module";
            ViewBag.SubTitle = "Registration";
            if (ModelState.IsValid)
            {
                db.R_StudentRegistration_data.Add(r_StudentRegistration_data);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.User_Id = new SelectList(db.AspNetUsers, "Id", "Email", r_StudentRegistration_data.User_Id);
            ViewBag.batch_Id = new SelectList(db.R_BatchNumber_data, "batch_Id", "batch_Name", r_StudentRegistration_data.batch_Id);
            ViewBag.Department_Id = new SelectList(db.R_Departments_data, "Department_Id", "Department_Name", r_StudentRegistration_data.Department_Id);
            ViewBag.Semester_Id = new SelectList(db.R_Semester_data, "Semester_Id", "Semester_Name", r_StudentRegistration_data.Semester_Id);
            ViewBag.Year_Id = new SelectList(db.R_StudyYear_data, "Year_Id", "Year_Name", r_StudentRegistration_data.Year_Id);
            ViewBag.Student_Id = new SelectList(db.R_StudentInfo_data, "Student_Id", "Student_Number", r_StudentRegistration_data.Student_Id);
            return View(r_StudentRegistration_data);
        }

        // GET: RegistrationModule/StudentRegistration/Edit/5
        public ActionResult Edit(int? id)
        {
            ViewBag.MainTitle = "Registration Module";
            ViewBag.SubTitle = "Registration";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            R_StudentRegistration_data r_StudentRegistration_data = db.R_StudentRegistration_data.Find(id);
            if (r_StudentRegistration_data == null)
            {
                return HttpNotFound();
            }
            ViewBag.User_Id = new SelectList(db.AspNetUsers, "Id", "Email", r_StudentRegistration_data.User_Id);
            ViewBag.batch_Id = new SelectList(db.R_BatchNumber_data, "batch_Id", "batch_Name", r_StudentRegistration_data.batch_Id);
            ViewBag.Department_Id = new SelectList(db.R_Departments_data, "Department_Id", "Department_Name", r_StudentRegistration_data.Department_Id);
            ViewBag.Semester_Id = new SelectList(db.R_Semester_data, "Semester_Id", "Semester_Name", r_StudentRegistration_data.Semester_Id);
            ViewBag.Year_Id = new SelectList(db.R_StudyYear_data, "Year_Id", "Year_Name", r_StudentRegistration_data.Year_Id);
            ViewBag.Student_Id = new SelectList(db.R_StudentInfo_data, "Student_Id", "Student_Number", r_StudentRegistration_data.Student_Id);
            return View(r_StudentRegistration_data);
        }

       
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "SR_Id,Student_Id,Year_Id,Semester_Id,Department_Id,User_Id,batch_Id,Registered,Registr_Status")] R_StudentRegistration_data r_StudentRegistration_data)
        {
            ViewBag.MainTitle = "Registration Module";
            ViewBag.SubTitle = "Registration";
            if (ModelState.IsValid)
            {
                db.Entry(r_StudentRegistration_data).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.User_Id = new SelectList(db.AspNetUsers, "Id", "Email", r_StudentRegistration_data.User_Id);
            ViewBag.batch_Id = new SelectList(db.R_BatchNumber_data, "batch_Id", "batch_Name", r_StudentRegistration_data.batch_Id);
            ViewBag.Department_Id = new SelectList(db.R_Departments_data, "Department_Id", "Department_Name", r_StudentRegistration_data.Department_Id);
            ViewBag.Semester_Id = new SelectList(db.R_Semester_data, "Semester_Id", "Semester_Name", r_StudentRegistration_data.Semester_Id);
            ViewBag.Year_Id = new SelectList(db.R_StudyYear_data, "Year_Id", "Year_Name", r_StudentRegistration_data.Year_Id);
            ViewBag.Student_Id = new SelectList(db.R_StudentInfo_data, "Student_Id", "Student_Number", r_StudentRegistration_data.Student_Id);
            return View(r_StudentRegistration_data);
        }

        // GET: RegistrationModule/StudentRegistration/Delete/5
        public ActionResult Delete(int? id)
        {
            ViewBag.MainTitle = "Registration Module";
            ViewBag.SubTitle = "Registration";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            R_StudentRegistration_data r_StudentRegistration_data = db.R_StudentRegistration_data.Find(id);
            if (r_StudentRegistration_data == null)
            {
                return HttpNotFound();
            }
            return View(r_StudentRegistration_data);
        }

        // POST: RegistrationModule/StudentRegistration/Delete/5
        [HttpPost, ActionName("Delete")]
       
        public ActionResult DeleteConfirmed(int id)
        {
            ViewBag.MainTitle = "Registration Module";
            ViewBag.SubTitle = "Registration";
            R_StudentRegistration_data r_StudentRegistration_data = db.R_StudentRegistration_data.Find(id);
            db.R_StudentRegistration_data.Remove(r_StudentRegistration_data);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        [HttpGet]
        public ActionResult PayInfoForReg(int Student_Id)
        {
            ViewBag.MainTitle = "Registration Module";
            ViewBag.SubTitle = "Registration";
            IEnumerable<FeesDescribtion> model = db.Fee_TuitionFeesDescribtion.Select(x => new FeesDescribtion
            {
                Student_Name = x.R_StudentInfo_data.Student_FullName,
                Depositnumber = x.Depositnumber,
                Student_Id = x.Student_Id,
                TFD_FeesDate = x.TFD_FeesDate,
                TFD_Id = x.TFD_Id,
                TFD_TuitionFee = x.TFD_TuitionFee,
                TF_Id = x.TF_Id,
                Year_Id = x.Year_Id,
                Year_Name = x.R_StudyYear_data.Year_Name
            }).Where(u => u.Student_Id == Student_Id).ToList();
            List<year> year = db.R_StudyYear_data.Select(x => new year { ID = x.Year_Id, Name = x.Year_Name }).ToList();
            List<year> Newyear = new List<year>();
            foreach (var item in year)
            {
                List<Fee_TuitionFeesDescribtion> y = db.Fee_TuitionFeesDescribtion.Where(u => u.Year_Id == item.ID && u.Student_Id == Student_Id).ToList();
                if (y.Count > 0)
                {
                    Newyear.Add(item);
                }
            }
            R_StudentInfo_data studentInfo = db.R_StudentInfo_data.Find(Student_Id);
            ViewBag.yearName = Newyear;
            ViewBag.Department = studentInfo.Department;
            ViewBag.Student_FullName = studentInfo.Student_FullName;
            ViewBag.Student_Id = studentInfo.Student_Id;
            ViewBag.Student_Number = studentInfo.Student_Number;

            return PartialView("PayInfoForReg", model);
        }
        [HttpGet]
        public ActionResult NoticePayment(int Student_Id)
        {
            ViewBag.MainTitle = "Registration Module";
            ViewBag.SubTitle = "Registration";
            NoticePaymentModel model = new NoticePaymentModel();
            List<string> year = db.R_StudyYear_data.Select(x => x.Year_Name).ToList();
            R_StudentInfo_data studentInfo = db.R_StudentInfo_data.Find(Student_Id);
            ViewBag.year = year;
            model.CollegeName = studentInfo.Department;
            model.StdName = studentInfo.Student_FullName;
            model.StdID = studentInfo.Student_Id;
            model.stdNumber = studentInfo.Student_Number;
            return PartialView("NoticePayment", model);
        }
        [HttpPost]
        public ActionResult NoticePayment(NoticePaymentModel modle)
        {
            ViewBag.MainTitle = "Registration Module";
            ViewBag.SubTitle = "Registration";
            List<NoticePaymentModel> md = new List<NoticePaymentModel>() {

               new NoticePaymentModel {CollegeName=modle.CollegeName,money=modle.money,
               StdID=modle.StdID,StdName=modle.StdName,stdNumber=modle.stdNumber,years=DateTime.Now.Year.ToString()
               }

            };
            
            ReportDocument rd = new ReportDocument();
            rd.Load(Path.Combine(Server.MapPath("~/Areas/RegistrationModule/Reports/NoticePaymentReport.rpt")));
            rd.SetDataSource(md.ToList());
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            return File(stream, "application/pdf", "NoticePaymentReport.pdf");


        }


        //[HttpGet]
        //public ActionResult Pay(int StuID)
        //{
        //    R_StudentInfo_data StdInfo = db.R_StudentInfo_data.Find(StuID);
        //    Fee_TuitionFees TF = db.Fee_TuitionFees.FirstOrDefault(x => x.Student_Id == StuID);
        //    List<R_StudyYear_data> StudyYear = db.R_StudyYear_data.ToList();
        //    ViewBag.StudyYear = new SelectList(StudyYear, "Year_Id", "Year_Name");
        //    FeesDescribtion model = new FeesDescribtion();
        //    model.Student_Id = StdInfo.Student_Id;
        //    model.TF_Id = TF.TF_Id;

        //    return PartialView("PayInfoForReg", model);
        //}



        [HttpGet]
        public ActionResult OldRegistration(int StudentID)
        {
            ViewBag.MainTitle = "Registration Module";
            ViewBag.SubTitle = " Registration For Old Student";
            R_StudentRegistration_data StudentRegistration = db.R_StudentRegistration_data.FirstOrDefault(u => u.Student_Id == StudentID);
            if (StudentRegistration == null)
            {
                string DeptName = db.R_StudentInfo_data.Find(StudentID).Department;
                int deptid = db.R_Departments_data.Single(x => x.Department_Name == DeptName).Department_Id;

                ForNewStudentRegistration model = new ForNewStudentRegistration();
                List<R_Semester_data> Semester = db.R_Semester_data.ToList();
                List<R_Departments_data> Departments = db.R_Departments_data.ToList();
                List<R_StudyYear_data> Year = db.R_StudyYear_data.ToList();
                List<R_BatchNumber_data> Batch = db.R_BatchNumber_data.Where(x => x.Department_Id == deptid).ToList();
                ViewBag.Semester = new SelectList(Semester, "Semester_Id", "Semester_Name");
                ViewBag.Departments = new SelectList(Departments, "Department_Id", "Department_Name");
                ViewBag.Year = new SelectList(Year, "Year_Id", "Year_Name");
                ViewBag.Batch = new SelectList(Batch, "batch_Id", "batch_Name");

                R_StudentInfo_data StudentInfo = db.R_StudentInfo_data.Find(StudentID);
                if (StudentInfo != null)
                {
                    string Colleges = StudentInfo.Student_College;
                    string Department = StudentInfo.Department;
                    R_Colleges_data Collg = db.R_Colleges_data.First(u => u.Coll_Name == Colleges);
                    R_Departments_data Dept = db.R_Departments_data.First(u => u.Department_Name == Department);
                    model.Student_Name = StudentInfo.Student_FullName;
                    model.Student_Id = StudentInfo.Student_Id;
                    model.Department_Id = Dept.Department_Id;
                    model.Department_Name = Dept.Department_Name;

                }
             
                return PartialView(model);
            }
            else
            {
                ViewBag.Err = "Student in Registration Is already exist";
                return PartialView();
            }
        }
        [HttpPost]
        public ActionResult OldRegistration(ForNewStudentRegistration model)
        {
            ViewBag.MainTitle = "Registration Module";
            ViewBag.SubTitle = "Registration";
            string mss = "";
            string DeptName = db.R_StudentInfo_data.Find(model.Student_Id).Department;
            int deptid = db.R_Departments_data.Single(x => x.Department_Name == DeptName).Department_Id;
            List<R_Semester_data> Semester = db.R_Semester_data.ToList();
            List<R_Departments_data> Departments = db.R_Departments_data.ToList();
            List<R_StudyYear_data> Year = db.R_StudyYear_data.ToList();
            List<R_BatchNumber_data> Batch = db.R_BatchNumber_data.ToList();
            ViewBag.Semester = new SelectList(Semester, "Semester_Id", "Semester_Name");
            ViewBag.Departments = new SelectList(Departments, "Department_Id", "Department_Name");
            ViewBag.Year = new SelectList(Year, "Year_Id", "Year_Name");

            string studentName = db.R_StudentInfo_data.Find(model.Student_Id).Student_FullName;
            model.Student_Name = studentName;
            List<R_StudentRegistration_data> STR = db.R_StudentRegistration_data.Where(u => u.Semester_Id == model.Semester_Id && u.Student_Id == model.Student_Id && u.Year_Id == model.Year_Id).ToList();
            if (STR.Count == 0)
            {

                if (Vald(model))
                {
                    R_StudentRegistration_data StdReg = new R_StudentRegistration_data()
                    {
                        Department_Id = model.Department_Id,
                        batch_Id = model.batch_Id,
                        Semester_Id = model.Semester_Id,
                        Year_Id = model.Year_Id,
                        Student_Id = model.Student_Id,
                        Registered = false,
                        User_Id = User.Identity.GetUserId(),
                        Registr_Status = "Formal",
                    };
                    db.R_StudentRegistration_data.Add(StdReg);
                    int val = db.SaveChanges();
                    if (val > 0)
                    {

                        return RedirectToAction("Index");
                    }
                    else
                    {
                        ViewBag.Err = "Is not Saved,peals try again";
                        return View(model);
                    }
                }
                else
                {
                    ViewBag.Err = "select all items if form,peals try again";
                    return View(model);
                }
            }
            else
            {
                ViewBag.Err = "Student is already exists";
                return View(model);
            }

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public bool Vald(ForNewStudentRegistration model)
        {
            if (model.Department_Id == 0)
            {
                return false;
            }
            else if (model.Student_Id == 0)
            {
                return false;
            }
            else 
            {
                return true;
            }

        }




    }
}
