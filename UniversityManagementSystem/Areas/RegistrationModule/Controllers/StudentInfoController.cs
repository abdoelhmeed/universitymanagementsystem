﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using UniversityManagementSystem.Areas.RegistrationModule.Models;
using UniversityManagementSystem.Models;


namespace UniversityManagementSystem.Areas.RegistrationModule.Controllers
{
    [Authorize]
    public class StudentInfoController : Controller
    {
        private UniversityManagementSystemEntities db = new UniversityManagementSystemEntities();
        
        // GET: RegistrationModule/StudentInfo
        public ActionResult Index()
        {
            ViewBag.MainTitle = "Registration Module";
            ViewBag.SubTitle = "New Student Data";
            return View(db.R_StudentInfo_data.Where(u=>u.Student_Status==true).ToList());
        }

        // GET: RegistrationModule/StudentInfo/Details/5
        public ActionResult Details(int? id)
        {
            ViewBag.MainTitle = "Registration Module";
            ViewBag.SubTitle = "Details Student";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            R_StudentInfo_data r_StudentInfo_data = db.R_StudentInfo_data.Find(id);
            if (r_StudentInfo_data == null)
            {
                return HttpNotFound();
            }
            ViewBag.Img = r_StudentInfo_data.Student_img;
            ViewBag.StdId = id;
            return View(r_StudentInfo_data);
        }

        // GET: RegistrationModule/StudentInfo/Create
        public ActionResult Create()
        {
            ViewBag.MainTitle = "Registration Module";
            ViewBag.SubTitle = "Create Student";
            List< Stauts> stauts = new List<Stauts>() {
                new Stauts{boolStauts=false,StautsName="Transfered Student"},
                new  Stauts{boolStauts=true,StautsName="New Sudent"}

            };
           

            List<string> Gender = new List<string>();
            Gender.Add("male");
             Gender.Add("female");
            Gender.Add("Other");
            List<string> Dept = db.R_Colleges_data.Select(u=>u.Coll_Name).ToList();
            ViewBag.Dept = Dept;
            ViewBag.Gender = Gender.ToList();
            ViewBag.Satuts = new SelectList(stauts, "boolStauts", "StautsName");
            return View();
        }
        public JsonResult GetDepartment(string Colleges)
        {
            int College = db.R_Colleges_data.FirstOrDefault(u=> u.Coll_Name == Colleges).Coll_Id;
            List<Department> Departments = db.R_Departments_data.Where(u => u.Coll_Id == College).Select(x=> new Department {DeptName=x.Department_Name}).ToList();
            return Json(Departments, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Create(StudentInfoDataModel model)
        {
            ViewBag.MainTitle = "Registration Module";
            ViewBag.SubTitle = "Create Student";

            List<Stauts> stauts = new List<Stauts>() {
                new Stauts{boolStauts=false,StautsName="Transfered Student"},
                new  Stauts{boolStauts=true,StautsName="New Sudent"}

            };


            List<string> Gender = new List<string>();
            Gender.Add("male");
             Gender.Add("female");
            Gender.Add("Other");
            List<string> Dept = db.R_Colleges_data.Select(u => u.Coll_Name).ToList();
            ViewBag.Dept = Dept;
            ViewBag.Gender = Gender.ToList();
            ViewBag.Satuts = new SelectList(stauts, "boolStauts", "StautsName");

            if (ModelState.IsValid)
            {

                List<R_StudentInfo_data> Info = db.R_StudentInfo_data.Where(u => u.Student_Number == model.Student_Number).ToList();
                if (Info.Count <= 0)
                {

               
                string fileName = Path.GetFileNameWithoutExtension(model.Image.FileName);
                string extension = Path.GetExtension(model.Image.FileName);
                fileName = model.Student_FullName + DateTime.Now.ToString("yymmssfff") + extension;
                string imgname = fileName;
                model.Student_img = "~/Image/" + fileName;
                fileName = Path.Combine(Server.MapPath("~/Image/"), fileName);
                model.Image.SaveAs(fileName);
                R_StudentInfo_data std = new R_StudentInfo_data() {
                    Student_Tel = model.Student_Tel,
                    Student_FullName = model.Student_FullName,
                    Student_img = imgname,
                    Student_CertificateDate = model.Student_CertificateDate,
                    Student_College = model.Student_College,
                    Student_Email = model.Student_Email,
                    Student_EmerInSEmail = model.Student_EmerInSEmail,
                    Student_EmerInSName = model.Student_EmerInSName,
                    Student_EmerInSTel = model.Student_EmerInSTel,
                    Student_EmerOutSEmail = model.Student_EmerOutSEmail,
                    Student_Status = model.Student_Status,
                    Student_Sex = model.Student_Sex,
                    Student_EmerOutSName = model.Student_EmerOutSName,
                    Student_EmerOutSTel = model.Student_EmerOutSTel,
                    Student_GeneralPercentage = model.Student_GeneralPercentage,
                    Student_Nameofcertificate = model.Student_Nameofcertificate,
                    Student_Nationality = model.Student_Nationality,
                    Student_PassportOrNationalNo = model.Student_PassportOrNationalNo,
                    Student_PermanantAddress = model.Student_PermanantAddress,
                    Student_PlaceOfBirth = model.Student_PlaceOfBirth,
                    Student_PresentAddress = model.Student_PresentAddress,
                    Student_NativeLanguage = model.Student_NativeLanguage,
                    Student_Number = model.Student_Number,
                    Department=model.Department
                    
                };
                db.R_StudentInfo_data.Add(std);
                int val =db.SaveChanges();
                if (val >0)
                {

                    byte[] CertificateImage = new byte[model.CertificateImage.ContentLength];
                    model.CertificateImage.InputStream.Read(CertificateImage, 0, model.CertificateImage.ContentLength);
                    attachment Cimg = new attachment()
                    {
                        Student_id = std.Student_Id,
                        attachfile = CertificateImage,
                        description = "CI" + std.Student_Number.ToString()
                    };
                    db.attachments.Add(Cimg);
                    db.SaveChanges();

                    return RedirectToAction("Create");
                }
                else
                {
                    ViewBag.msgError = "No Save Try again";
                    return View(model);
                }

                }
                else
                {
                    ViewBag.msgError = "Student Number is Already exists";
                    return View(model);
                }
            }
            else
            {
                ViewBag.msgError = "Data input is correct";
                return View(model);
            }
            
        }

        // GET: RegistrationModule/StudentInfo/Edit/5
        public ActionResult Edit(int? id)
        {
          
            ViewBag.MainTitle = "Registration Module";
            ViewBag.SubTitle = "Edit Student";

            List<Stauts> stauts = new List<Stauts>() {
                new Stauts{boolStauts=false,StautsName="Transfered Student"},
                new  Stauts{boolStauts=true,StautsName="New Sudent"}

            };


            List<string> Gender = new List<string>();
            Gender.Add("male");
             Gender.Add("female");
            Gender.Add("Other");
            List<string> Dept = db.R_Colleges_data.Select(u => u.Coll_Name).ToList();
            ViewBag.Dept = Dept;
            ViewBag.Gender = Gender.ToList();
            ViewBag.Satuts = new SelectList(stauts, "boolStauts", "StautsName");
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            R_StudentInfo_data model = db.R_StudentInfo_data.Find(id);
            if (model == null)
            {
                return HttpNotFound();
            }
             
            StudentInfoDataModel std = new StudentInfoDataModel();
            std.Student_Tel = model.Student_Tel;
            std.Student_Id = model.Student_Id;
            std.Student_FullName = model.Student_FullName;
            std.Student_img = model.Student_img;
            std.Student_CertificateDate = model.Student_CertificateDate;
            std.Student_College = model.Student_College;
            std.Student_Email = model.Student_Email;
            std.Student_EmerInSEmail = model.Student_EmerInSEmail;
            std.Student_EmerInSName = model.Student_EmerInSName;
            std.Student_EmerInSTel = model.Student_EmerInSTel;
            std.Student_EmerOutSEmail = model.Student_EmerOutSEmail;
            std.Student_Status = model.Student_Status;
            std.Student_Sex = model.Student_Sex;
            std.Student_EmerOutSName = model.Student_EmerOutSName;
            std.Student_EmerOutSTel = model.Student_EmerOutSTel;
            std.Student_GeneralPercentage = model.Student_GeneralPercentage;
            std.Student_Nameofcertificate = model.Student_Nameofcertificate;
            std.Student_Nationality = model.Student_Nationality;
            std.Student_PassportOrNationalNo = model.Student_PassportOrNationalNo;
            std.Student_PermanantAddress = model.Student_PermanantAddress;
            std.Student_PlaceOfBirth = model.Student_PlaceOfBirth;
            std.Student_PresentAddress = model.Student_PresentAddress;
            std.Student_NativeLanguage = model.Student_NativeLanguage;
            std.Student_Number = model.Student_Number;
            std.Department = model.Department;
            ViewBag.StdId = model.Student_Id;
            ViewBag.Img = model.Student_img;

            return View(std);
        }

        [HttpPost]
        public ActionResult Edit(StudentInfoDataModel model)
        {

            ViewBag.MainTitle = "Registration Module";
            ViewBag.SubTitle = "Create Student";

            List<Stauts> stauts = new List<Stauts>() {
                new Stauts{boolStauts=false,StautsName="Transfered Student"},
                new  Stauts{boolStauts=true,StautsName="New Sudent"}

            };
            ViewBag.StdId = model.Student_Id;

            List<string> Gender = new List<string>();
            Gender.Add("male");
             Gender.Add("female");
            Gender.Add("Other");
            List<string> Dept = db.R_Colleges_data.Select(u => u.Coll_Name).ToList();
            ViewBag.Dept = Dept;
            ViewBag.Gender = Gender.ToList();
            ViewBag.Satuts = new SelectList(stauts, "boolStauts", "StautsName");
            if (ModelState.IsValid)
            {
                List<R_StudentInfo_data> Info = db.R_StudentInfo_data.Where(u => u.Student_Number == model.Student_Number).ToList();
                if (Info.Count > 0)
                {


                    var fullPath = Server.MapPath("~/Image/" + model.Student_img);
                    if (System.IO.File.Exists(fullPath))
                    {
                        System.IO.File.Delete(fullPath);

                    }
                    string fileName = Path.GetFileNameWithoutExtension(model.Image.FileName);
                    string extension = Path.GetExtension(model.Image.FileName);
                    fileName = model.Student_FullName + DateTime.Now.ToString("yymmssfff") + extension;
                    string imgname = fileName;
                    model.Student_img = "~/Image/" + fileName;
                    fileName = Path.Combine(Server.MapPath("~/Image/"), fileName);
                    model.Image.SaveAs(fileName);
                    R_StudentInfo_data std = db.R_StudentInfo_data.Find(model.Student_Id);

                    if (std != null)
                    {

                        std.Student_Tel = model.Student_Tel;
                        std.Student_FullName = model.Student_FullName;
                        std.Student_img = imgname;
                        std.Student_Number = model.Student_Number;
                        std.Student_CertificateDate = model.Student_CertificateDate;
                        std.Student_College = model.Student_College;
                        std.Student_Email = model.Student_Email;
                        std.Student_EmerInSEmail = model.Student_EmerInSEmail;
                        std.Student_EmerInSName = model.Student_EmerInSName;
                        std.Student_EmerInSTel = model.Student_EmerInSTel;
                        std.Student_EmerOutSEmail = model.Student_EmerOutSEmail;
                        std.Student_Status = model.Student_Status;
                        std.Student_Sex = model.Student_Sex;
                        std.Student_EmerOutSName = model.Student_EmerOutSName;
                        std.Student_EmerOutSTel = model.Student_EmerOutSTel;
                        std.Student_GeneralPercentage = model.Student_GeneralPercentage;
                        std.Student_Nameofcertificate = model.Student_Nameofcertificate;
                        std.Student_Nationality = model.Student_Nationality;
                        std.Student_PassportOrNationalNo = model.Student_PassportOrNationalNo;
                        std.Student_PermanantAddress = model.Student_PermanantAddress;
                        std.Student_PlaceOfBirth = model.Student_PlaceOfBirth;
                        std.Student_PresentAddress = model.Student_PresentAddress;
                        std.Student_NativeLanguage = model.Student_NativeLanguage;
                        std.Department = model.Department;
                        db.Entry(std).State = EntityState.Modified;
                        int val = db.SaveChanges();
                        if (val > 0)
                        {

                            byte[] CertificateImage = new byte[model.CertificateImage.ContentLength];
                            model.CertificateImage.InputStream.Read(CertificateImage, 0, model.CertificateImage.ContentLength);

                            List<attachment> at = db.attachments.Where(x => x.Student_id == model.Student_Id).ToList();
                            if (at.Count > 0)
                            {
                                foreach (var item in at)
                                {
                                    db.Entry(item).State = EntityState.Deleted;
                                    db.SaveChanges();
                                }

                            }

                            attachment Cimg = new attachment()
                            {
                                Student_id = std.Student_Id,
                                attachfile = CertificateImage,
                                description = "CI" + std.Student_Number.ToString()
                            };
                            db.attachments.Add(Cimg);
                            db.SaveChanges();
                            return RedirectToAction("Index");
                        }
                        else
                        {
                            ViewBag.msgError = "No Save Try again";
                            return View(model);
                        }

                    }
                    else
                    {
                        ViewBag.msgError = "Data input is correct";
                        return View(model);
                    }
                }
                else
                {
                    ViewBag.msgError = "Student Number is Already exists";
                    return View(model);
                }

            }
            else
            {
                ViewBag.msgError = "Data input is correct";
                return View(model);
            }
        }

        // GET: RegistrationModule/StudentInfo/Delete/5
        public ActionResult Delete(int? id)
        {
            ViewBag.MainTitle = "Registration Module";
            ViewBag.SubTitle = "Delete Student";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            R_StudentInfo_data r_StudentInfo_data = db.R_StudentInfo_data.Find(id);
            if (r_StudentInfo_data == null)
            {
                return HttpNotFound();
            }
            return View(r_StudentInfo_data);
        }

        // POST: RegistrationModule/StudentInfo/Delete/5
        [HttpPost]
        public JsonResult DeleteConfirmed(int Student_Id)
        {
            string mss;
            try
            {

                List<attachment> att = db.attachments.Where(x=>x.Student_id==Student_Id).ToList();
                foreach (var item in att)
                {
                    attachment attachments = db.attachments.Find(item.attachment_Id);
                    db.attachments.Remove(attachments);
                    db.SaveChanges();
                }


                R_StudentInfo_data r_StudentInfo_data = db.R_StudentInfo_data.Find(Student_Id);
                db.R_StudentInfo_data.Remove(r_StudentInfo_data);
                db.SaveChanges();
                mss = "Del";
                return Json(mss,JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                mss = e.Message + "Sorry, the request can not be deleted after registration";
                return Json(mss, JsonRequestBehavior.AllowGet);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

     
    }
    class Department
    {
        public string DeptName { get; set; }
      
    }

    class Stauts
    {
        public bool boolStauts { get; set; }
        public string StautsName { get; set; }
    }

    
}
