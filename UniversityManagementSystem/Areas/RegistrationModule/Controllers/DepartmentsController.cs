﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using UniversityManagementSystem.Models;
using UniversityManagementSystem.Areas.RegistrationModule.Models;
using System.IO;

namespace UniversityManagementSystem.Areas.RegistrationModule.Controllers
{
    public class DepartmentsController : Controller
    {
        private UniversityManagementSystemEntities db = new UniversityManagementSystemEntities();

        // GET: RegistrationModule/Departments
        public ActionResult Index()
        {
            ViewBag.MainTitle = "University Setting Module";
            ViewBag.SubTitle = "Departments Setting";
            var r_Departments_data = db.R_Departments_data.Include(r => r.R_Colleges_data);
            return View(r_Departments_data.ToList());
        }

        // GET: RegistrationModule/Departments/Details/5
        public ActionResult Details(int? id)
        {
            ViewBag.MainTitle = "University Setting Module";
            ViewBag.SubTitle = "Departments Setting";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            R_Departments_data r_Departments_data = db.R_Departments_data.Find(id);
            if (r_Departments_data == null)
            {
                return HttpNotFound();
            }
            return View(r_Departments_data);
        }

        // GET: RegistrationModule/Departments/Create
        public ActionResult Create()
        {
            ViewBag.MainTitle = "University Setting Module";
            ViewBag.SubTitle = "Departments Setting";
            ViewBag.Coll_Id = new SelectList(db.R_Colleges_data, "Coll_Id", "Coll_Name");

            return View();
        }

       
    
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(DepartmentsModel model)
        {
            ViewBag.MainTitle = "University Setting Module";
            ViewBag.SubTitle = "Departments Setting";
            if (ModelState.IsValid)
            {
                string appFeilName = Path.GetFileNameWithoutExtension(model.DepImg.FileName);
                string extension = Path.GetExtension(model.DepImg.FileName);
                appFeilName = model.Department_Name + DateTime.Now.ToString("yymmssfff") + extension;
                string PDFName = appFeilName;
                appFeilName = Path.Combine(Server.MapPath("~/Attachments/"), appFeilName);
                model.DepImg.SaveAs(appFeilName);
                R_Departments_data Dep = new R_Departments_data()
                {
                    Department_Name = model.Department_Name,
                    Coll_Id = model.Coll_Id,
                    Department_Fees = model.Department_Fees,
                    DepImg = PDFName,
                    DeptDtails = model.DeptDtails,
                    DepRequirements = model.DepRequirements,
                    Semester_number = model.Semester_number
                };
                db.R_Departments_data.Add(Dep);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Coll_Id = new SelectList(db.R_Colleges_data, "Coll_Id", "Coll_Name", model.Coll_Id);
            return View(model);
        }

        // GET: RegistrationModule/Departments/Edit/5
        public ActionResult Edit(int? id)
        {
            ViewBag.MainTitle = "University Setting Module";
            ViewBag.SubTitle = "Departments Setting";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            R_Departments_data r_Departments_data = db.R_Departments_data.Find(id);
            DepartmentsModel model = new DepartmentsModel()
            {
                Department_Id = r_Departments_data.Department_Id,
                Department_Name = r_Departments_data.Department_Name,
                Coll_Id = r_Departments_data.Coll_Id,
                Department_Fees = r_Departments_data.Department_Fees,
                DepImgName = r_Departments_data.DepImg,
                DeptDtails = r_Departments_data.DeptDtails,
                DepRequirements = r_Departments_data.DepRequirements,
                Semester_number = r_Departments_data.Semester_number
            };
            if (model == null)
            {
                return HttpNotFound();
            }
            ViewBag.Coll_Id = new SelectList(db.R_Colleges_data, "Coll_Id", "Coll_Name", model.Coll_Id);
            return View(model);
        }

        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(DepartmentsModel model)
        {
            ViewBag.MainTitle = "University Setting Module";
            ViewBag.SubTitle = "Departments Setting";
            if (ModelState.IsValid)
            {
                string appFeilName = Path.GetFileNameWithoutExtension(model.DepImg.FileName);
                string extension = Path.GetExtension(model.DepImg.FileName);
                appFeilName = model.Department_Name + DateTime.Now.ToString("yymmssfff") + extension;
                string PDFName = appFeilName;
                appFeilName = Path.Combine(Server.MapPath("~/Attachments/"), appFeilName);
                model.DepImg.SaveAs(appFeilName);
                R_Departments_data r_Departments_data = new R_Departments_data()
                {
                    Department_Id = model.Department_Id,
                    Department_Name = model.Department_Name,
                    Coll_Id = model.Coll_Id,
                    Department_Fees = model.Department_Fees,
                    DepImg = PDFName,
                    DeptDtails = model.DeptDtails,
                    DepRequirements = model.DepRequirements,
                    Semester_number = model.Semester_number
                };
                db.Entry(r_Departments_data).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Coll_Id = new SelectList(db.R_Colleges_data, "Coll_Id", "Coll_Name", model.Coll_Id);
            return View(model);
        }

        // GET: RegistrationModule/Departments/Delete/5
        public ActionResult Delete(int? id)
        {
            ViewBag.MainTitle = "University Setting Module";
            ViewBag.SubTitle = "Departments Setting";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            R_Departments_data r_Departments_data = db.R_Departments_data.Find(id);
            if (r_Departments_data == null)
            {
                return HttpNotFound();
            }
            return View(r_Departments_data);
        }

       
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                R_Departments_data r_Departments_data = db.R_Departments_data.Find(id);
                db.R_Departments_data.Remove(r_Departments_data);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch (Exception)
            {

                return RedirectToAction("Index");
            }
           
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
