﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UniversityManagementSystem.Areas.RegistrationModule.Report;
using UniversityManagementSystem.Models;

namespace UniversityManagementSystem.Areas.RegistrationModule.Controllers
{
    public class RegistrationReportsController : Controller
    {
        UniversityManagementSystemEntities db = new UniversityManagementSystemEntities();

        // GET: RegistrationModule/RegistrationReports
        public ActionResult Index()
        {
            ViewBag.MainTitle = "Registration Module";
            ViewBag.SubTitle = "Registration Reports";
            return View();
        }
        public ActionResult IdCards(int YearId, int DepatId, int SemId)
        {
            var data = db.R_StudentRegistration_data.Where(t => t.Year_Id == YearId && t.Department_Id == DepatId && t.Semester_Id == SemId && t.Registered == true).
                Select(t => new { t.SR_Id, t.R_StudentInfo_data.Student_Number, t.R_StudentInfo_data.Student_FullName, t.R_StudyYear_data.Year_Name, t.R_Semester_data.Semester_Name,
                t.R_Departments_data.Department_Name ,t.R_BatchNumber_data.batch_Name ,t.R_StudentInfo_data.Student_PresentAddress ,t.R_StudentInfo_data.Student_Nationality 
                ,t.R_Departments_data.R_Colleges_data.Coll_Name , t.R_StudentInfo_data.Student_img}).ToList();

            StudentIdCardReport exa = new StudentIdCardReport();
            //exa.SetParameterValue("YId ", YearId);
            //exa.SetParameterValue("DId", DepatId);
            //exa.SetParameterValue("SId", SemId);

            exa.Load(Path.Combine(Server.MapPath("~/Areas/RegistrationModule/Report"), "StudentIdCardReport.rpt"));
            exa.SetDataSource(data);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();

            Stream stream = exa.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            return File(stream, "application/pdf", "StudentIdCard.pdf");
        }
    }
}