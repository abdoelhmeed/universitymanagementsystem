﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UniversityManagementSystem.Models;
using UniversityManagementSystem.Areas.RegistrationModule.Models;

namespace UniversityManagementSystem.Areas.RegistrationModule.Controllers
{
    [Authorize]
    public class attachmentController : Controller
    {
        UniversityManagementSystemEntities db = new UniversityManagementSystemEntities();
        // GET: RegistrationModule/attachment
        public ActionResult Index(int StudentID)
        {
            R_StudentInfo_data Info = db.R_StudentInfo_data.Find(StudentID);
            Attachment model = new Attachment();

            model.Student_id = Info.Student_Id;
            model.Student_FullName = Info.Student_FullName;
            model.Student_College = Info.Student_College;
            model.Department = Info.Department;
            return View(model);
        }
        [HttpPost]
        public ActionResult Index(Attachment att, HttpPostedFileBase File1)
        {
            if (File1 != null && File1.ContentLength > 0)
            {

                att.attachfile = new byte[File1.ContentLength]; 
                File1.InputStream.Read(att.attachfile, 0, File1.ContentLength);
                attachment atta = new attachment()
                {
                    Student_id = att.Student_id,
                    attachfile = att.attachfile,
                  description=att.description
                };
                db.attachments.Add(atta);
                db.SaveChanges();
                return RedirectToAction("Index", "StudentInfo");
            }
            else
            {
                ViewBag.Stuats = "Select File";
                return View();
            }
            
        }
        [HttpGet]
        public ActionResult Show(int ID)
        {
            List<Attachment> model = db.attachments.Where(x=>x.Student_id==ID).Select(u => new Attachment {
                attachfile=u.attachfile,
                attachment_Id=u.attachment_Id,
                description=u.description

            }).ToList();
            ViewBag.img = ID;
            return PartialView("Show", model);
        }
        public ActionResult Del()
        {
            return View();
        }
    }
}