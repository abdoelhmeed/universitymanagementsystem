﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using UniversityManagementSystem.Areas.RegistrationModule.Models;
using UniversityManagementSystem.Models;

namespace UniversityManagementSystem.Areas.RegistrationModule.Controllers
{
    [Authorize]
    public class FreezingController : Controller
    {
        private UniversityManagementSystemEntities db = new UniversityManagementSystemEntities();

        // GET: RegistrationModule/Freezing
        public ActionResult Index()
        {
            ViewBag.MainTitle = "Registration Module";
            ViewBag.SubTitle = "Freezing";
            var r_Freezing = db.R_Freezing.Include(r => r.R_StudentInfo_data).Select(x=> new FreezingModel {
               freezingdoneDate=x.freezingdoneDate,
               freezingstartDate=x.freezingstartDate,
               fid=x.fid,
               fStudentnumber=x.R_StudentInfo_data.Student_Number,
                FullName = x.R_StudentInfo_data.Student_FullName,
               fStudent_Id=x.R_StudentInfo_data.Student_Id,
               fStatus=x.fStatus,
               fStatusName=x.fStatusName,
               fStudentDept=x.R_StudentInfo_data.Department
            });
            return View(r_Freezing.ToList());
        }
        public JsonResult AddStudentForFreezing(AddNewStudentForFreezing model)
        {
            List<SmpModel> smpList=new List<SmpModel>();
            try
            {
                
                string mss = "Sorry, the student number does not exist Or alreagy Freezing";
                int Sid = db.R_StudentInfo_data.Single(x => x.Student_Number == model.StudentNumbar).Student_Id;
                bool s = db.R_Freezing.Any(x => x.fStudent_Id == Sid);
                if (!s && Sid > 0)
                {

                    R_StudentRegistration_data Reg = db.R_StudentRegistration_data.First(x => x.Student_Id == Sid && x.Registered==false);

                    R_Freezing freezing = new R_Freezing()
                    {
                        freezingstartDate = DateTime.Now,
                        fStatus = true,
                        fStatusName = "freezing",
                        fStudent_Id = Sid
                    };
                    db.R_Freezing.Add(freezing);
                    int val = db.SaveChanges();
                    if (val > 0)
                    {
                        List<R_StudentRegistration_data> RegList = db.R_StudentRegistration_data.Where(x => x.Student_Id == Sid && x.Registered==false).ToList();
                        foreach (var item in RegList)
                        {
                            var Dep= item.Semester_Id;
                            R_Semester_data mepData = db.R_Semester_data.Find(Dep);
                            SmpModel smp = new SmpModel() {
                                smpid = mepData.Semester_Id,
                                smpname= mepData.Semester_Name,
                                smpNumber= mepData.Semester_Number
                            };
                            smpList.Add(smp);
                        }
                      SmpModel Listsmp= FindMaxAge(smpList.ToList());
                        List<R_StudentRegistration_data> StuReg = db.R_StudentRegistration_data.Where(x=>x.Student_Id==Sid && x.Semester_Id==Listsmp.smpid && x.Registered==false).ToList();
                       
                        foreach (var item in StuReg)
                        {
                            item.Registr_Status = "freezing";
                            db.Entry(item).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                       

                        mss = "student is freezing";
                    }
                    else
                    {
                        mss = "Sorry, try another one";
                    }
                }
                return Json(mss, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json("Sorry, the student number does not exist", JsonRequestBehavior.AllowGet);
            }

        }
        [HttpGet]
        public ActionResult Unlockthefreeze(int sid)
        {
            ViewBag.MainTitle = "Registration Module";
            ViewBag.SubTitle = "Unlock The Freeze";
            List<SmpModel> smpList = new List<SmpModel>();
            List<R_StudentRegistration_data> RegList = db.R_StudentRegistration_data.Where(x => x.Student_Id == sid && x.Registered== false).ToList();
            foreach (var item in RegList)
            {
                var Dep = item.Semester_Id;
                R_Semester_data mepData = db.R_Semester_data.Find(Dep);
                SmpModel smp = new SmpModel()
                {
                    smpid = mepData.Semester_Id,
                    smpname = mepData.Semester_Name,
                    smpNumber = mepData.Semester_Number
                };
                smpList.Add(smp);
            }
            SmpModel Listsmp = FindMaxAge(smpList.ToList());
            List<R_StudentRegistration_data> StuReg = db.R_StudentRegistration_data.Where(x => x.Student_Id == sid && x.Semester_Id == Listsmp.smpid && x.Registered == false&& x.Registr_Status == "freezing").ToList();
            R_StudentRegistration_data rig = StuReg.Last();
            UnlockthefreezeModel model = new UnlockthefreezeModel() {
                Department_Id = rig.Department_Id,
                Department_Name = rig.R_Departments_data.Department_Name,
                Registered = rig.Registered,
                Registr_Status = rig.Registr_Status,
                User_Id = rig.User_Id,
                Student_Id = rig.Student_Id,
                Student_Name = rig.R_StudentInfo_data.Student_FullName,
                Semester_Id = rig.Semester_Id,
                //RegistrationID=rig.SR_Id,
                Semester_Name = rig.R_Semester_data.Semester_Name,
                SR_Id = rig.SR_Id,
                Year_Id = rig.Year_Id,
                Year_Name = rig.R_StudyYear_data.Year_Name,
                batch_Id = rig.batch_Id,
                Student_Number=rig.R_StudentInfo_data.Student_Number

            };
            var Depstudent = db.R_Departments_data.Find(rig.Department_Id);
            ViewBag.Semester = new SelectList(db.R_Semester_data, "Semester_Id", "Semester_Name", model.Semester_Id);
            ViewBag.BatchNumber = new SelectList(db.R_BatchNumber_data.Where(x=>x.Department_Id==rig.Department_Id), "batch_Id", "batch_Name", model.batch_Id);
            return View(model);
        }
        [HttpPost]
        public ActionResult Unlockthefreeze(UnlockthefreezeModel model)
        {
            int Year = DateTime.Now.Year;
            R_StudyYear_data Yeardat = db.R_StudyYear_data.SingleOrDefault(u => u.Year_Number == Year);
            if (Yeardat == null)
            {
                R_StudyYear_data SYD = new R_StudyYear_data()
                {
                    Year_Number = Year,
                    Year_Name = Convert.ToString(Year) + "-" + Convert.ToString(Year + 1),
                    YearStart = DateTime.Now,
                    YearEnd = DateTime.Now.AddMinutes(12),
                };
                db.R_StudyYear_data.Add(SYD);
                db.SaveChanges();
                model.Year_Id = 0;
                model.Year_Id = SYD.Year_Id;
            }
            else
            {
                model.Year_Id = db.R_StudyYear_data.First(x => x.Year_Number == Year).Year_Id;
            }
            R_StudentRegistration_data StdReg = new R_StudentRegistration_data()
            {
                Department_Id = model.Department_Id,
                batch_Id = model.batch_Id,
                Semester_Id = model.Semester_Id,
                Year_Id = model.Year_Id,
                Student_Id = model.Student_Id,
                Registered = false,
                User_Id = User.Identity.GetUserId(),
                Registr_Status = "Formal",
            };
            db.R_StudentRegistration_data.Add(StdReg);
            int val = db.SaveChanges();
            if (val > 0)
            {
                List<R_Freezing> free = db.R_Freezing.Where(x=> x.fStudent_Id==model.Student_Id).ToList();
                foreach (var item in free)
                {
                    R_Freezing fr = db.R_Freezing.Find(item.fid);

                    fr.fid = item.fid;
                    fr.freezingdoneDate = DateTime.Now;

                    fr.fStatus = !item.fStatus;
                    fr.fStatusName = "UnFreezing";
                       
                    
                    db.Entry(fr).State = EntityState.Modified;
                    db.SaveChanges();
                    
                }
            }
            return RedirectToAction("Index");

        }
        public SmpModel FindMaxAge(List<SmpModel> list)
        {
            if (list.Count == 0)
            {
                throw new InvalidOperationException("Empty list");
            }
            int maxAge = int.MinValue;
            
            foreach (SmpModel type in list)
            {
                if (type.smpNumber > maxAge)
                {
                    maxAge = type.smpNumber;
                }
            }
           SmpModel smp= list.First(x => x.smpNumber == maxAge);
            return smp;
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
