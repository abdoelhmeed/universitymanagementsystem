﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using UniversityManagementSystem.Models;

namespace UniversityManagementSystem.Areas.RegistrationModule.Controllers
{
    public class BatchNumberDataController : Controller
    {
        private UniversityManagementSystemEntities db = new UniversityManagementSystemEntities();

        // GET: RegistrationModule/BatchNumberData
        public ActionResult Index()
        {
            ViewBag.MainTitle = "University Setting Module";
            ViewBag.SubTitle = "Batch Number Setting";
            var r_BatchNumber_data = db.R_BatchNumber_data.Include(r => r.R_Departments_data);
         
            return View(r_BatchNumber_data.ToList());
        }

        // GET: RegistrationModule/BatchNumberData/Details/5
        public ActionResult Details(int? id)
        {
            ViewBag.MainTitle = "University Setting Module";
            ViewBag.SubTitle = "Batch Number Setting";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            R_BatchNumber_data r_BatchNumber_data = db.R_BatchNumber_data.Find(id);
            if (r_BatchNumber_data == null)
            {
                return HttpNotFound();
            }
            return View(r_BatchNumber_data);
        }

        // GET: RegistrationModule/BatchNumberData/Create
        public ActionResult Create()
        {
            ViewBag.MainTitle = "University Setting Module";
            ViewBag.SubTitle = "Batch Number Setting";
            ViewBag.Department_Id = new SelectList(db.R_Departments_data, "Department_Id", "Department_Name");
            ViewBag.Year_Id = new SelectList(db.R_StudyYear_data, "Year_Id", "Year_Name");
            return View();
        }

        // POST: RegistrationModule/BatchNumberData/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(R_BatchNumber_data model)
        {
            ViewBag.MainTitle = "University Setting Module";
            ViewBag.SubTitle = "Batch Number Setting";

            bool Batch = db.R_BatchNumber_data.Any(u => u.Department_Id == model.Department_Id && u.Year_Id == model.Year_Id);
            if (Batch == false)
            {
                List<R_BatchNumber_data> BatchNumbers = db.R_BatchNumber_data.Where(u => u.Department_Id == model.Department_Id).ToList();
                int bber = BatchNumbers.Count + 1;
                R_BatchNumber_data NewBatch = new R_BatchNumber_data()
                {
                    Year_Id = model.Year_Id,
                    Department_Id = model.Department_Id,
                    batch_Number = bber,
                    batch_Name = db.R_Departments_data.SingleOrDefault(x => x.Department_Id == model.Department_Id).Department_Name.ToString() + " Batch " + Convert.ToString(BatchNumbers.Count + 1),

                };
                db.R_BatchNumber_data.Add(NewBatch);
                db.SaveChanges();

            }
           
            return RedirectToAction("Index");
        }

        // GET: RegistrationModule/BatchNumberData/Delete/5
        public ActionResult Delete(int? id)
        {
            ViewBag.MainTitle = "University Setting Module";
            ViewBag.SubTitle = "Batch Number Setting";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            R_BatchNumber_data r_BatchNumber_data = db.R_BatchNumber_data.Find(id);
            if (r_BatchNumber_data == null)
            {
                return HttpNotFound();
            }
            return View(r_BatchNumber_data);
        }

        // POST: RegistrationModule/BatchNumberData/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                R_BatchNumber_data r_BatchNumber_data = db.R_BatchNumber_data.Find(id);
                db.R_BatchNumber_data.Remove(r_BatchNumber_data);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch (Exception)
            {

                return RedirectToAction("Index");
            }
           
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
