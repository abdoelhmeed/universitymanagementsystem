﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UniversityManagementSystem.Areas.RegistrationModule;
using UniversityManagementSystem.Models;
using System.Web.Mvc;
using UniversityManagementSystem.Areas.RegistrationModule.Models;
using Microsoft.AspNet.Identity;

namespace UniversityManagementSystem.Areas.RegistrationModule.Controllers
{
    [Authorize]
    public class RegistrationNewTransformStudentController : Controller
    {
        UniversityManagementSystemEntities db = new UniversityManagementSystemEntities();
        public ActionResult Index()
        {
            ViewBag.MainTitle = "Registration Module";
            ViewBag.SubTitle = "Transfered Student Details";
            return View(db.R_StudentInfo_data.Where(u => u.Student_Status == false).ToList());
        }


        [HttpGet]
        public ActionResult RegistrationTransformStudent(int StudentID)
        {
            ViewBag.MainTitle = "Registration Module";
            ViewBag.SubTitle = "Transfered Student Details Registration ";
            R_StudentRegistration_data StudentRegistration = db.R_StudentRegistration_data.FirstOrDefault(u => u.Student_Id == StudentID);
            if (StudentRegistration == null)
            {
                string DeptName = db.R_StudentInfo_data.Find(StudentID).Department;
                int deptid = db.R_Departments_data.Single(x => x.Department_Name == DeptName).Department_Id;

                ForNewStudentRegistration model = new ForNewStudentRegistration();
                List<R_Semester_data> Semester = db.R_Semester_data.ToList();
                List<R_Departments_data> Departments = db.R_Departments_data.ToList();
                List<R_StudyYear_data> Year = db.R_StudyYear_data.ToList();
                List<R_BatchNumber_data> Batch = db.R_BatchNumber_data.Where(x=>x.Department_Id== deptid).ToList();
                ViewBag.Semester = new SelectList(Semester, "Semester_Id", "Semester_Name");
                ViewBag.Departments = new SelectList(Departments, "Department_Id", "Department_Name");
                ViewBag.Year = new SelectList(Year, "Year_Id", "Year_Name");
                ViewBag.Batch = new SelectList(Batch, "batch_Id", "batch_Name");

                R_StudentInfo_data StudentInfo = db.R_StudentInfo_data.Find(StudentID);
                if (StudentInfo != null)
                {
                    string Colleges = StudentInfo.Student_College;
                    string Department = StudentInfo.Department;
                    R_Colleges_data Collg = db.R_Colleges_data.First(u => u.Coll_Name == Colleges);
                    R_Departments_data Dept = db.R_Departments_data.First(u => u.Department_Name == Department);
                    model.Student_Name = StudentInfo.Student_FullName;
                    model.Student_Id = StudentInfo.Student_Id;
                    model.Department_Id = Dept.Department_Id;
                    model.Department_Name = Dept.Department_Name;

                }
                ViewBag.Err = null;
                return View(model);
            }
            else
            {
                ViewBag.Err = "Student in Registration Is already exist";
                return View();
            }
        }
        [HttpPost]
        public ActionResult RegistrationTransformStudent(ForNewStudentRegistration model)
        {
            string DeptName = db.R_StudentInfo_data.Find(model.Student_Id).Department;
            int deptid = db.R_Departments_data.Single(x => x.Department_Name == DeptName).Department_Id;
            List<R_Semester_data> Semester = db.R_Semester_data.ToList();
            List<R_Departments_data> Departments = db.R_Departments_data.ToList();
            List<R_StudyYear_data> Year = db.R_StudyYear_data.ToList();
            List<R_BatchNumber_data> Batch = db.R_BatchNumber_data.ToList();
            ViewBag.Semester = new SelectList(Semester, "Semester_Id", "Semester_Name");
            ViewBag.Departments = new SelectList(Departments, "Department_Id", "Department_Name");
            ViewBag.Year = new SelectList(Year, "Year_Id", "Year_Name");
          
            string studentName = db.R_StudentInfo_data.Find(model.Student_Id).Student_FullName;
            model.Student_Name = studentName;
            List<R_StudentRegistration_data> STR = db.R_StudentRegistration_data.Where(u => u.Semester_Id == model.Semester_Id && u.Student_Id == model.Student_Id && u.Year_Id == model.Year_Id).ToList();
            if (STR.Count == 0)
            {

                if (Vald(model))
                {
                    R_StudentRegistration_data StdReg = new R_StudentRegistration_data()
                    {
                        Department_Id = model.Department_Id,
                        batch_Id = model.batch_Id,
                        Semester_Id = model.Semester_Id,
                        Year_Id = model.Year_Id,
                        Student_Id = model.Student_Id,
                        Registered = false,
                        User_Id = User.Identity.GetUserId(),
                        Registr_Status = "Formal",
                    };
                    db.R_StudentRegistration_data.Add(StdReg);
                    int val = db.SaveChanges();
                    if (val > 0)
                    {
                      
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        ViewBag.Err = "Is not Saved,peals try again";
                        return View(model);
                    }
                }
                else
                {
                    ViewBag.Err = "select all items if form,peals try again";
                    return View(model);
                }
            }
            else
            {
                ViewBag.Err = "Student is already exists";
                return View(model);
            }


        }


        public bool Vald(ForNewStudentRegistration model)
        {
            if (model.Department_Id == 0)
            {
                return false;
            }
            else if (model.batch_Id == 0)
            {
                return false;
            }
            else if (model.Semester_Id == 0)
            {
                return false;
            }
            else if (model.Year_Id == 0)
            {
                return false;
            }
            else if (model.Student_Id == 0)
            {
                return false;
            }
            else
            {
                return true;
            }

        }
    }
}