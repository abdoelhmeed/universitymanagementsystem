﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using UniversityManagementSystem.Areas.RegistrationModule.Models;
using UniversityManagementSystem.Models;

namespace UniversityManagementSystem.Areas.RegistrationModule.Controllers
{
    public class YearDataController : Controller
    {
        private UniversityManagementSystemEntities db = new UniversityManagementSystemEntities();

        // GET: RegistrationModule/YearData
        public ActionResult Index()
        {
            ViewBag.MainTitle = "University Setting Module";
            ViewBag.SubTitle = "Years Setting";
            return View(db.R_StudyYear_data.ToList());
        }

        // GET: RegistrationModule/YearData/Details/5
        public ActionResult Details(int? id)
        {
            ViewBag.MainTitle = "University Setting Module";
            ViewBag.SubTitle = "Years Setting";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            R_StudyYear_data r_StudyYear_data = db.R_StudyYear_data.Find(id);
            if (r_StudyYear_data == null)
            {
                return HttpNotFound();
            }
            return View(r_StudyYear_data);
        }

        // GET: RegistrationModule/YearData/Create
        public ActionResult Create()
        {
            List<string> Year = new List<string>();
            int NowYear = DateTime.Now.Year;
            int yy = NowYear - 19;
            for (int i = yy; i <= NowYear; i++)

            {
                Year.Add((i - 1).ToString() + "-" + i.ToString());
            }
            ViewBag.Year = Year;
            return View();
        }

        // POST: RegistrationModule/YearData/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]

        public ActionResult Create(R_StudyYear_data r_StudyYear_data)
        {
            ViewBag.MainTitle = "University Setting Module";
            ViewBag.SubTitle = "Years Setting";
          int  yenumber =Convert.ToInt32( r_StudyYear_data.Year_Name.Substring(0, 4));
            R_StudyYear_data SYD = new R_StudyYear_data()
            {
                Year_Number = yenumber,
                Year_Name = r_StudyYear_data.Year_Name,
                YearEnd= r_StudyYear_data.YearEnd,
                YearStart= r_StudyYear_data.YearStart
            };
            db.R_StudyYear_data.Add(SYD);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // GET: RegistrationModule/YearData/Edit/5
        public ActionResult Edit(int? id)
        {
            ViewBag.MainTitle = "University Setting Module";
            ViewBag.SubTitle = "Years Setting";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            R_StudyYear_data model = db.R_StudyYear_data.Find(id);
            StudyYearModel r_StudyYear_data = new StudyYearModel()
            {
                Year_Id = model.Year_Id,
                YearEnd = model.YearEnd,
                YearStart = model.YearStart,
                Year_Name = model.Year_Name,
                Year_Number = model.Year_Number
            };
            if (r_StudyYear_data == null)
            {
                return HttpNotFound();
            }
            return View(r_StudyYear_data);
        }

        // POST: RegistrationModule/YearData/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(StudyYearModel model)
        {
            ViewBag.MainTitle = "University Setting Module";
            ViewBag.SubTitle = "Years Setting";

            if (ModelState.IsValid)
            {
                R_StudyYear_data r_StudyYear_data = new R_StudyYear_data()
                {
                 Year_Id=model.Year_Id,
                 YearEnd=model.YearEnd,
                 YearStart=model.YearStart,
                 Year_Name=model.Year_Name,
                 Year_Number=model.Year_Number
                };
                db.Entry(r_StudyYear_data).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(model);
        }

        // GET: RegistrationModule/YearData/Delete/5
        public ActionResult Delete(int? id)
        {
            ViewBag.MainTitle = "University Setting Module";
            ViewBag.SubTitle = "Years Setting";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
           
            R_StudyYear_data r_StudyYear_data = db.R_StudyYear_data.Find(id);
            List<R_BatchNumber_data> Bt = db.R_BatchNumber_data.Where(x => x.Year_Id == r_StudyYear_data.Year_Id).ToList();
            if (r_StudyYear_data == null)
            {
                return HttpNotFound();
            }
            return View(r_StudyYear_data);
        }

        // POST: RegistrationModule/YearData/Delete/5
        [HttpPost]
        public JsonResult DeleteConfirmed(int id)
        {
            try
            {
                R_StudyYear_data r_StudyYear_data = db.R_StudyYear_data.Find(id);
                db.R_StudyYear_data.Remove(r_StudyYear_data);
                db.SaveChanges();
                return Json("Deleted", JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                   return Json(e.Message + "Not Deleted", JsonRequestBehavior.AllowGet);
            }
           
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
