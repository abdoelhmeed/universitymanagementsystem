﻿using System.Web.Mvc;

namespace UniversityManagementSystem.Areas.RegistrationModule
{
    public class RegistrationModuleAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "RegistrationModule";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "RegistrationModule_default",
                "RegistrationModule/{controller}/{action}/{id}",
                new { controller = "RegistrationModule", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}