﻿using System.Web.Mvc;

namespace UniversityManagementSystem.Areas.HRModule
{
    public class HRModuleAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "HRModule";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "HRModule_default",
                "HRModule/{controller}/{action}/{id}",
                new { controller = "HRModule", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}