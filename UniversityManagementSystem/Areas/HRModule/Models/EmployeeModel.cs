﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace UniversityManagementSystem.Areas.HRModule.Models
{
    public class EmployeeModel
    {
        [Display(Name ="ID")]
        public int empID { get; set; }
        [Required]
        [Display(Name = "Full Name")]
        public string empName { get; set; }
        [Required]
        [Display(Name = "Gender")]
        public string empGender { get; set; }
        [Required]
        [Display(Name = "Nationality")]
        public string empNationality { get; set; }
        [Required]
        [Display(Name = "Identification No")]
        public string empIdentificationNo { get; set; }
        [Required]
        [Display(Name = "Passport No")]
        public string empPassportNo { get; set; }
        [Required]
        [Display(Name = "Bank Account")]
        public string empBankAccount { get; set; }
        [Required]
        [Display(Name = "Address")]
        public string empAddress { get; set; }
        [Required]
        [Display(Name = "Private Address")]
        public string empPrivateAddress { get; set; }
        [Required]
        [Display(Name = "Emergency Phone")]
        public string empEmergencyPhone { get; set; }
        [Required]
        [Display(Name = "Data Of Birth")]
        [DataType(DataType.DateTime)]
        public DateTime empDataOfBirth { get; set; }
        [Required]
        [Display(Name = "Place Of Birth")]
        public string empPlaceOfBirth { get; set; }
        [Required]
        [Display(Name = "Marital Status")]
        public string empMaritalStatus { get; set; }
        [Required]
        [Display(Name = "Picture")]
        public HttpPostedFileBase empPicture { get; set; }
        public string UserID { get; set; }
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 4)]
        [Display(Name = "Name")]
        public string UserName { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }
        [DataType(DataType.Password)]
        [Display(Name = " Confirm Password ")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Required]
        public string Phone { get; set; }
        [Required]
        [Display(Name ="Employee Type")]
        public int EmpTId { get; set; }

    }
}