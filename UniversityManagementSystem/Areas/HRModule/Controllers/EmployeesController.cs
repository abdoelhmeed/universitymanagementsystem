﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using UniversityManagementSystem.Models;
using UniversityManagementSystem.Areas.HRModule.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.IO;

namespace UniversityManagementSystem.Areas.HRModule.Controllers
{
    [Authorize]
    public class EmployeesController : Controller
    {
        private UniversityManagementSystemEntities db = new UniversityManagementSystemEntities();

        // GET: HRModule/Employees
        public ActionResult Index()
        {
            ViewBag.MainTitle = "HR Module";
            ViewBag.SubTitle = "Employee ";
            var employees = db.Employees.Include(e => e.AspNetUser);
            return View(employees.ToList());
        }

        // GET: HRModule/Employees/Details/5
        public ActionResult Details(int? id)
        {
            ViewBag.MainTitle = "HR Module";
            ViewBag.SubTitle = "Employee ";

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = db.Employees.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        // GET: HRModule/Employees/Create
        public ActionResult Create()
        {
            ViewBag.MainTitle = "HR Module";
            ViewBag.SubTitle = "Employee ";
            List<string> Gender = new List<string>();
            Gender.Add("male");
            Gender.Add("female");
            Gender.Add("Other");
            ViewBag.Gender = Gender.ToList();
            List<string> SocialStatus = new List<string>();
            SocialStatus.Add("Unmarried");
            SocialStatus.Add("Married");
            ViewBag.SocialStatus = SocialStatus;

            List<EmpType> empType = db.EmpTypes.ToList();
            ViewBag.empType = new SelectList(empType, "EmpTId", "EmpTName");
            //ViewBag.UserID = new SelectList(db.AspNetUsers, "Id", "Email");
            return View();
        }

       
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(EmployeeModel model)
        {
            ViewBag.MainTitle = "HR Module";
            ViewBag.SubTitle = "Employee ";
            List<string> Gender = new List<string>();
            Gender.Add("male");
            Gender.Add("female");
            Gender.Add("Other");
            ViewBag.Gender = Gender.ToList();
            List<string> SocialStatus = new List<string>();
            SocialStatus.Add("Unmarried");
            SocialStatus.Add("Married");
            ViewBag.SocialStatus = SocialStatus;

            List<EmpType> empType = db.EmpTypes.ToList();
            ViewBag.empType = new SelectList(empType, "EmpTId", "EmpTName");
            if (ModelState.IsValid)
            {
                ApplicationDbContext dbUser = new ApplicationDbContext();
                var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(dbUser));
                var UserAdmin = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(dbUser));
                var User = new ApplicationUser();
                User.Email = model.Email;
                User.UserName = model.UserName;
                var check = UserAdmin.Create(User, model.Password);
                if (check.Succeeded)
                {
                    string appFeilName = Path.GetFileNameWithoutExtension(model.empPicture.FileName);
                    string extension = Path.GetExtension(model.empPicture.FileName);
                    appFeilName = model.empName + DateTime.Now.ToString("yymmssfff") + extension;
                    string PDFName = appFeilName;
                    appFeilName = Path.Combine(Server.MapPath("~/Attachments/"), appFeilName);
                    model.empPicture.SaveAs(appFeilName);
                   
                    Employee emp = new Employee();
                    emp.UserID = User.Id;
                    emp.empPicture = PDFName;
                    emp.empAddress = model.empAddress;
                    emp.empBankAccount = model.empBankAccount;
                    emp.empDataOfBirth = model.empDataOfBirth;
                    emp.empEmail = model.Email;
                    emp.empEmergencyPhone = model.empEmergencyPhone;
                    emp.empGender = model.empGender;
                    emp.empIdentificationNo = model.empIdentificationNo;
                    emp.empMaritalStatus = model.empMaritalStatus;
                    emp.empName = model.empName;
                    emp.empNationality = model.empNationality;
                    emp.empPassportNo = model.empPassportNo;
                    emp.empPlaceOfBirth = model.empPlaceOfBirth;
                    emp.empPrivateAddress = model.empPrivateAddress;
                    emp.EmpTId = model.EmpTId;
                    db.Employees.Add(emp);
                    db.SaveChanges();
                    return RedirectToAction("Index");

                }
                    
            }
            return View(model);
        }

        // GET: HRModule/Employees/Edit/5
        [HttpGet]
        public ActionResult Edit(int? id)
        {
            ViewBag.MainTitle = "HR Module";
            ViewBag.SubTitle = "Employee ";
            List<string> Gender = new List<string>();
            Gender.Add("male");
            Gender.Add("female");
            Gender.Add("Other");
            ViewBag.Gender = Gender.ToList();
            List<string> SocialStatus = new List<string>();
            SocialStatus.Add("Unmarried");
            SocialStatus.Add("Married");
            ViewBag.SocialStatus = SocialStatus;

            List<EmpType> empType = db.EmpTypes.ToList();
            ViewBag.empType = new SelectList(empType, "EmpTId", "EmpTName");
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee model = db.Employees.Find(id);
            if (model == null)
            {
                return HttpNotFound();
            }
            EmployeeModel emp = new EmployeeModel();
            emp.empID = model.empID;
            emp.UserID = model.UserID;
            emp.empAddress = model.empAddress;
            emp.empBankAccount = model.empBankAccount;
            emp.empDataOfBirth =Convert.ToDateTime( model.empDataOfBirth);
            emp.EmpTId = model.EmpTId;
            emp.empEmergencyPhone = model.empEmergencyPhone;
            emp.empGender = model.empGender;
            emp.empIdentificationNo = model.empIdentificationNo;
            emp.empMaritalStatus = model.empMaritalStatus;
            emp.empName = model.empName;
            emp.empNationality = model.empNationality;
            emp.empPassportNo = model.empPassportNo;
            emp.empPlaceOfBirth = model.empPlaceOfBirth;
            emp.empPrivateAddress = model.empPrivateAddress;
            

            return View(emp);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(EmployeeModel model)
        {
            ViewBag.MainTitle = "HR Module";
            ViewBag.SubTitle = "Employee ";
            List<string> Gender = new List<string>();
            Gender.Add("male");
            Gender.Add("female");
            Gender.Add("Other");
            ViewBag.Gender = Gender.ToList();
            List<string> SocialStatus = new List<string>();
            SocialStatus.Add("Unmarried");
            SocialStatus.Add("Married");
            ViewBag.SocialStatus = SocialStatus;

            List<EmpType> empType = db.EmpTypes.ToList();
            ViewBag.empType = new SelectList(empType, "EmpTId", "EmpTName");
            if (ModelState.IsValid)
            {
                string appFeilName = Path.GetFileNameWithoutExtension(model.empPicture.FileName);
                string extension = Path.GetExtension(model.empPicture.FileName);
                appFeilName = model.empName + DateTime.Now.ToString("yymmssfff") + extension;
                string PDFName = appFeilName;
                appFeilName = Path.Combine(Server.MapPath("~/Attachments/"), appFeilName);
                model.empPicture.SaveAs(appFeilName);

                Employee emp = new Employee();
                emp.empID = model.empID;
                emp.UserID = model.UserID;
                emp.empAddress = model.empAddress;
                emp.empBankAccount = model.empBankAccount;
                emp.empDataOfBirth = Convert.ToDateTime(model.empDataOfBirth);
                emp.EmpTId = model.EmpTId;
                emp.empEmergencyPhone = model.empEmergencyPhone;
                emp.empGender = model.empGender;
                emp.empIdentificationNo = model.empIdentificationNo;
                emp.empMaritalStatus = model.empMaritalStatus;
                emp.empName = model.empName;
                emp.empNationality = model.empNationality;
                emp.empPassportNo = model.empPassportNo;
                emp.empPlaceOfBirth = model.empPlaceOfBirth;
                emp.empPrivateAddress = model.empPrivateAddress;
                emp.empPicture = PDFName;
                db.Entry(emp).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(model);
        }

        // GET: HRModule/Employees/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = db.Employees.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        // POST: HRModule/Employees/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Employee employee = db.Employees.Find(id);
            db.Employees.Remove(employee);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
