﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using UniversityManagementSystem.Models;

namespace UniversityManagementSystem.Areas.HRModule.Controllers
{
    [Authorize]
    public class EmpTypesController : Controller
    {
        private UniversityManagementSystemEntities db = new UniversityManagementSystemEntities();

        // GET: HRModule/EmpTypes
        public ActionResult Index()
        {
            ViewBag.MainTitle = "HR Module";
            ViewBag.SubTitle = "Employee Type";
            return View(db.EmpTypes.ToList());
        }

        // GET: HRModule/EmpTypes/Details/5
        public ActionResult Details(int? id)
        {
            ViewBag.MainTitle = "HR Module";
            ViewBag.SubTitle = "Employee Type";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EmpType empType = db.EmpTypes.Find(id);
            if (empType == null)
            {
                return HttpNotFound();
            }
            return View(empType);
        }

        // GET: HRModule/EmpTypes/Create
        public ActionResult Create()
        {
            ViewBag.MainTitle = "HR Module";
            ViewBag.SubTitle = "Employee Type";
            return View();
        }

        // POST: HRModule/EmpTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "EmpTId,EmpTName")] EmpType empType)
        {
            ViewBag.MainTitle = "HR Module";
            ViewBag.SubTitle = "Employee Type";
            if (ModelState.IsValid)
            {
                db.EmpTypes.Add(empType);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(empType);
        }

        // GET: HRModule/EmpTypes/Edit/5
        public ActionResult Edit(int? id)
        {
            ViewBag.MainTitle = "HR Module";
            ViewBag.SubTitle = "Employee Type";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EmpType empType = db.EmpTypes.Find(id);
            if (empType == null)
            {
                return HttpNotFound();
            }
            return View(empType);
        }

        // POST: HRModule/EmpTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "EmpTId,EmpTName")] EmpType empType)
        {
            ViewBag.MainTitle = "HR Module";
            ViewBag.SubTitle = "Employee Type";
            if (ModelState.IsValid)
            {
                db.Entry(empType).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(empType);
        }

        // GET: HRModule/EmpTypes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EmpType empType = db.EmpTypes.Find(id);
            if (empType == null)
            {
                return HttpNotFound();
            }
            return View(empType);
        }

        // POST: HRModule/EmpTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            EmpType empType = db.EmpTypes.Find(id);
            db.EmpTypes.Remove(empType);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
