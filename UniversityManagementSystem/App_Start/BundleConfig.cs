﻿using System.Web;
using System.Web.Optimization;

namespace UniversityManagementSystem
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/asset/css/bootstrap.min.css",
                "~/asset/css/plugins/font-awesome.min.css",
                "~/asset/css/plugins/imple-line-icons.css",
                "~/asset/css/plugins/animate.min.css",
                "~/asset/css/style.css"
            ));
            bundles.Add(new StyleBundle("~/datatable_plugins/css").Include(
                "~/asset/css/plugins/datatables.bootstrap.min.css"
            ));
            bundles.Add(new ScriptBundle("~/Content/jquery").Include(
             "~/asset/js/jquery.min.js",
             "~/asset/js/jquery.ui.min.js",
             "~/asset/js/bootstrap.min.js"
            ));
            bundles.Add(new ScriptBundle("~/plugins/jquery").Include(
              "~/asset/js/plugins/moment.min.js",
              "~/asset/js/plugins/jquery.nicescroll.js",
              "~/asset/js/main.js",
             "~/asset/js/plugins/jquery.validate.min.js"

            ));

            bundles.Add(new ScriptBundle("~/datatable_plugins/jquery").Include(
             "~/asset/js/plugins/jquery.datatables.min.js",
             "~/asset/js/plugins/datatables.bootstrap.min.js"
           ));
        }
        
    }
   
}
