USE [master]
GO
/****** Object:  Database [UniversityManagementSystem]    Script Date: 18/6/2019 4:56:39 PM ******/
CREATE DATABASE [UniversityManagementSystem]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'UniversityManagementSystem', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\UniversityManagementSystem.mdf' , SIZE = 6144KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'UniversityManagementSystem_log', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\UniversityManagementSystem_log.ldf' , SIZE = 2816KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [UniversityManagementSystem] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [UniversityManagementSystem].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [UniversityManagementSystem] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [UniversityManagementSystem] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [UniversityManagementSystem] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [UniversityManagementSystem] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [UniversityManagementSystem] SET ARITHABORT OFF 
GO
ALTER DATABASE [UniversityManagementSystem] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [UniversityManagementSystem] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [UniversityManagementSystem] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [UniversityManagementSystem] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [UniversityManagementSystem] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [UniversityManagementSystem] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [UniversityManagementSystem] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [UniversityManagementSystem] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [UniversityManagementSystem] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [UniversityManagementSystem] SET  DISABLE_BROKER 
GO
ALTER DATABASE [UniversityManagementSystem] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [UniversityManagementSystem] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [UniversityManagementSystem] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [UniversityManagementSystem] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [UniversityManagementSystem] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [UniversityManagementSystem] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [UniversityManagementSystem] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [UniversityManagementSystem] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [UniversityManagementSystem] SET  MULTI_USER 
GO
ALTER DATABASE [UniversityManagementSystem] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [UniversityManagementSystem] SET DB_CHAINING OFF 
GO
ALTER DATABASE [UniversityManagementSystem] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [UniversityManagementSystem] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [UniversityManagementSystem]
GO
/****** Object:  Table [dbo].[__MigrationHistory]    Script Date: 18/6/2019 4:56:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[__MigrationHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ContextKey] [nvarchar](300) NOT NULL,
	[Model] [varbinary](max) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK_dbo.__MigrationHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC,
	[ContextKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AddFund]    Script Date: 18/6/2019 4:56:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AddFund](
	[fundID] [int] IDENTITY(1,1) NOT NULL,
	[fundHistory] [date] NULL,
	[funNameOfOrganization] [nvarchar](50) NULL,
	[funAmount] [money] NULL,
	[funNotes] [nvarchar](50) NULL,
 CONSTRAINT [PK_AddFund] PRIMARY KEY CLUSTERED 
(
	[fundID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 18/6/2019 4:56:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoles](
	[Id] [nvarchar](128) NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 18/6/2019 4:56:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.AspNetUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 18/6/2019 4:56:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserLogins](
	[LoginProvider] [nvarchar](128) NOT NULL,
	[ProviderKey] [nvarchar](128) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC,
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 18/6/2019 4:56:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId] [nvarchar](128) NOT NULL,
	[RoleId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 18/6/2019 4:56:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUsers](
	[Id] [nvarchar](128) NOT NULL,
	[Email] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEndDateUtc] [datetime] NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[UserName] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[attachment]    Script Date: 18/6/2019 4:56:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[attachment](
	[attachment_Id] [int] IDENTITY(1,1) NOT NULL,
	[Student_id] [int] NULL,
	[attachfile] [varbinary](max) NULL,
	[description] [nvarchar](100) NULL,
 CONSTRAINT [PK_attachment] PRIMARY KEY CLUSTERED 
(
	[attachment_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[C_CourseAssignToYear_data]    Script Date: 18/6/2019 4:56:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[C_CourseAssignToYear_data](
	[CATYId] [int] IDENTITY(1,1) NOT NULL,
	[Course_Id] [int] NOT NULL,
	[Year_Id] [int] NOT NULL,
	[Department_Id] [int] NOT NULL,
	[Semester_Id] [int] NOT NULL,
	[Teacher_Id] [int] NULL,
 CONSTRAINT [PK_CourseAssignToTeacher] PRIMARY KEY CLUSTERED 
(
	[CATYId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[C_Courses_data]    Script Date: 18/6/2019 4:56:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[C_Courses_data](
	[Course_Id] [int] IDENTITY(1,1) NOT NULL,
	[Course_Name] [nvarchar](50) NOT NULL,
	[Course_ShortName] [nchar](10) NULL,
	[Course_BasicGrade] [float] NOT NULL,
	[Course_AppendixGrade] [float] NOT NULL,
	[Course_HoursNumber] [int] NOT NULL,
	[Course_PointsNumber] [float] NOT NULL,
	[Course_Descirption] [nvarchar](100) NULL,
 CONSTRAINT [PK_Course] PRIMARY KEY CLUSTERED 
(
	[Course_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[C_Teachers_data]    Script Date: 18/6/2019 4:56:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[C_Teachers_data](
	[Teacher_Id] [int] IDENTITY(1,1) NOT NULL,
	[Teacher_Name] [nvarchar](50) NOT NULL,
	[Teacher_Address] [nvarchar](50) NOT NULL,
	[Teacher_Email] [nvarchar](50) NULL,
	[Teacher_Phone] [int] NOT NULL,
	[Department_Id] [int] NOT NULL,
	[Designation_Id] [int] NULL,
	[UserId] [nvarchar](128) NULL,
 CONSTRAINT [PK_Teacher] PRIMARY KEY CLUSTERED 
(
	[Teacher_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Designation]    Script Date: 18/6/2019 4:56:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Designation](
	[Designation_Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](50) NULL,
 CONSTRAINT [PK_Designation] PRIMARY KEY CLUSTERED 
(
	[Designation_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Employee]    Script Date: 18/6/2019 4:56:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Employee](
	[empID] [int] IDENTITY(1,1) NOT NULL,
	[empName] [nvarchar](150) NULL,
	[empGender] [nvarchar](50) NULL,
	[empNationality] [nvarchar](50) NULL,
	[empIdentificationNo] [nvarchar](50) NULL,
	[empPassportNo] [nvarchar](50) NULL,
	[empBankAccount] [nvarchar](10) NULL,
	[empAddress] [nvarchar](50) NULL,
	[empEmail] [nvarchar](50) NULL,
	[empPrivateAddress] [nvarchar](50) NULL,
	[empEmergencyPhone] [nvarchar](50) NULL,
	[empDataOfBirth] [date] NULL,
	[empPlaceOfBirth] [nvarchar](50) NULL,
	[empMaritalStatus] [nvarchar](50) NULL,
	[empPicture] [nvarchar](50) NULL,
	[UserID] [nvarchar](128) NULL,
	[EmpTId] [int] NOT NULL,
 CONSTRAINT [PK_Employee] PRIMARY KEY CLUSTERED 
(
	[empID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[EmpType]    Script Date: 18/6/2019 4:56:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmpType](
	[EmpTId] [int] IDENTITY(1,1) NOT NULL,
	[EmpTName] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_EmpType] PRIMARY KEY CLUSTERED 
(
	[EmpTId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[EventDetails]    Script Date: 18/6/2019 4:56:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EventDetails](
	[EventID] [int] IDENTITY(1,1) NOT NULL,
	[EventTitle] [nvarchar](50) NULL,
	[EventSubject] [nvarchar](max) NULL,
	[EventDate] [date] NULL,
 CONSTRAINT [PK_EventDetails] PRIMARY KEY CLUSTERED 
(
	[EventID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[EventImg]    Script Date: 18/6/2019 4:56:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EventImg](
	[EvimgID] [int] IDENTITY(1,1) NOT NULL,
	[EvimgName] [nvarchar](100) NULL,
	[EventID] [int] NULL,
 CONSTRAINT [PK_EventImg] PRIMARY KEY CLUSTERED 
(
	[EvimgID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Expenses]    Script Date: 18/6/2019 4:56:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Expenses](
	[ExpensesID] [int] IDENTITY(1,1) NOT NULL,
	[ProductName] [nvarchar](50) NULL,
	[Quantity] [int] NULL,
	[UnitPrice] [money] NULL,
	[UserID] [nvarchar](128) NULL,
	[BillNumber] [nvarchar](50) NULL,
	[ExpensesDate] [date] NULL,
 CONSTRAINT [PK_Expenses] PRIMARY KEY CLUSTERED 
(
	[ExpensesID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Fee_TuitionFees]    Script Date: 18/6/2019 4:56:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Fee_TuitionFees](
	[TF_Id] [int] IDENTITY(1,1) NOT NULL,
	[Student_Id] [int] NOT NULL,
	[User_Id] [nvarchar](128) NOT NULL,
	[Department_Id] [int] NULL,
	[Fees_Department] [money] NULL,
	[Payment_Id] [int] NOT NULL,
	[FeesPayment] [money] NULL,
	[Fees_Total] [money] NOT NULL,
	[FeesFeesDate] [date] NOT NULL,
 CONSTRAINT [PK_Tuitionfees] PRIMARY KEY CLUSTERED 
(
	[TF_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Fee_TuitionFeesDescribtion]    Script Date: 18/6/2019 4:56:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Fee_TuitionFeesDescribtion](
	[TFD_Id] [int] IDENTITY(1,1) NOT NULL,
	[Student_Id] [int] NOT NULL,
	[Depositnumber] [nvarchar](50) NOT NULL,
	[TFD_TuitionFee] [money] NOT NULL,
	[TFD_FeesDate] [date] NOT NULL,
	[Year_Id] [int] NULL,
	[TF_Id] [int] NOT NULL,
 CONSTRAINT [PK_TuitionfeesDescribtion] PRIMARY KEY CLUSTERED 
(
	[TFD_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[InboxMsg]    Script Date: 18/6/2019 4:56:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[InboxMsg](
	[InMsgId] [int] IDENTITY(1,1) NOT NULL,
	[InMsgTitle] [nvarchar](50) NOT NULL,
	[InMsgBody] [ntext] NOT NULL,
	[Department_Id] [int] NOT NULL,
	[Semester_Id] [int] NOT NULL,
 CONSTRAINT [PK_InboxMsg] PRIMARY KEY CLUSTERED 
(
	[InMsgId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Invoices]    Script Date: 18/6/2019 4:56:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Invoices](
	[INnumber] [int] NOT NULL,
	[INCase] [nvarchar](50) NULL,
	[INDate] [date] NULL,
	[INDone] [bit] NULL,
	[orID] [int] NULL,
 CONSTRAINT [PK_Invoices] PRIMARY KEY CLUSTERED 
(
	[INnumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[InvoicesDetails]    Script Date: 18/6/2019 4:56:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[InvoicesDetails](
	[idID] [int] IDENTITY(1,1) NOT NULL,
	[IdProductName] [nvarchar](50) NULL,
	[IdSupplier] [nvarchar](50) NULL,
	[IdQuantity] [int] NULL,
	[IdUnitPrice] [money] NULL,
	[INnumber] [int] NULL,
	[IdNumber] [nvarchar](50) NULL,
 CONSTRAINT [PK_InvoicesDetails] PRIMARY KEY CLUSTERED 
(
	[idID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[InvoicesImg]    Script Date: 18/6/2019 4:56:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[InvoicesImg](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Iimgname] [nvarchar](50) NULL,
	[INnumber] [int] NULL,
 CONSTRAINT [PK_InvoicesImg] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Library_Book]    Script Date: 18/6/2019 4:56:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Library_Book](
	[BookNo] [int] IDENTITY(1,1) NOT NULL,
	[BookName] [nvarchar](150) NOT NULL,
	[BookAuthor] [nvarchar](50) NOT NULL,
	[BookPublisher] [nvarchar](50) NOT NULL,
	[BookisAvail] [bit] NOT NULL,
	[BookNumberOfCopy] [int] NOT NULL,
 CONSTRAINT [PK_Library_Book] PRIMARY KEY CLUSTERED 
(
	[BookNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Library_Issue]    Script Date: 18/6/2019 4:56:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Library_Issue](
	[IssueId] [int] IDENTITY(1,1) NOT NULL,
	[BookNo] [int] NOT NULL,
	[MemberId] [int] NOT NULL,
	[IssueDate] [datetime] NOT NULL,
	[DeliveryDate] [datetime] NULL,
	[IsReceived] [bit] NULL,
 CONSTRAINT [PK_Library_Issue] PRIMARY KEY CLUSTERED 
(
	[IssueId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Library_Member]    Script Date: 18/6/2019 4:56:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Library_Member](
	[MemberId] [int] IDENTITY(1,1) NOT NULL,
	[MemberName] [nvarchar](50) NOT NULL,
	[MemberPhone] [nvarchar](50) NOT NULL,
	[MemberEmail] [nvarchar](50) NULL,
 CONSTRAINT [PK_Library_Member] PRIMARY KEY CLUSTERED 
(
	[MemberId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Order]    Script Date: 18/6/2019 4:56:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Order](
	[orID] [int] IDENTITY(1,1) NOT NULL,
	[orderApplicant] [nvarchar](50) NULL,
	[orderHistory] [date] NULL,
	[orderCase] [bit] NULL,
	[orderAdmin] [bit] NULL,
	[orderFinished] [bit] NULL,
	[ordestatus] [nvarchar](50) NULL,
 CONSTRAINT [PK_Order] PRIMARY KEY CLUSTERED 
(
	[orID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[OrderDetails]    Script Date: 18/6/2019 4:56:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderDetails](
	[odID] [int] IDENTITY(1,1) NOT NULL,
	[odproductName] [nvarchar](50) NULL,
	[odProductDetails] [nvarchar](max) NULL,
	[odQuantity] [int] NULL,
	[orNotes] [nvarchar](max) NULL,
	[orID] [int] NULL,
 CONSTRAINT [PK_OrderDetails] PRIMARY KEY CLUSTERED 
(
	[odID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PaymentStatus]    Script Date: 18/6/2019 4:56:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PaymentStatus](
	[Payment_Id] [int] IDENTITY(1,1) NOT NULL,
	[Payment_Name] [nvarchar](50) NOT NULL,
	[Payment_Value] [money] NOT NULL,
	[Payment_percent] [int] NULL,
 CONSTRAINT [PK_PaymentStatus] PRIMARY KEY CLUSTERED 
(
	[Payment_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[R_BatchNumber_data]    Script Date: 18/6/2019 4:56:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[R_BatchNumber_data](
	[batch_Id] [int] IDENTITY(1,1) NOT NULL,
	[batch_Number] [int] NULL,
	[batch_Name] [nvarchar](50) NOT NULL,
	[Department_Id] [int] NOT NULL,
	[Year_Id] [int] NOT NULL,
 CONSTRAINT [PK_BathcNumber] PRIMARY KEY CLUSTERED 
(
	[batch_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[R_Colleges_data]    Script Date: 18/6/2019 4:56:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[R_Colleges_data](
	[Coll_Id] [int] IDENTITY(1,1) NOT NULL,
	[Coll_Name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Colleges] PRIMARY KEY CLUSTERED 
(
	[Coll_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[R_Departments_data]    Script Date: 18/6/2019 4:56:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[R_Departments_data](
	[Department_Id] [int] IDENTITY(1,1) NOT NULL,
	[Department_Name] [nvarchar](50) NOT NULL,
	[Department_Fees] [decimal](18, 0) NOT NULL,
	[Semester_number] [int] NOT NULL,
	[Coll_Id] [int] NOT NULL,
	[DepImg] [nvarchar](max) NULL,
	[DeptDtails] [nvarchar](max) NULL,
	[DepRequirements] [nvarchar](max) NULL,
 CONSTRAINT [PK_Departments] PRIMARY KEY CLUSTERED 
(
	[Department_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[R_HealthStatus_data]    Script Date: 18/6/2019 4:56:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[R_HealthStatus_data](
	[Health_Id] [int] IDENTITY(1,1) NOT NULL,
	[Health_BloodType] [nvarchar](50) NULL,
	[Health_StudentHeight] [int] NULL,
	[Health_StudentWeight] [int] NULL,
	[Health_DiseasesAndDisorders] [nvarchar](100) NULL,
	[Health_BloodPressure] [int] NULL,
	[Health_Vision] [nvarchar](50) NULL,
	[Health_CorrectedVision] [nvarchar](50) NULL,
	[Health_Eyes] [nvarchar](50) NULL,
	[Student_Id] [int] NOT NULL,
 CONSTRAINT [PK_HealthStatus] PRIMARY KEY CLUSTERED 
(
	[Health_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[R_Semester_data]    Script Date: 18/6/2019 4:56:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[R_Semester_data](
	[Semester_Id] [int] IDENTITY(1,1) NOT NULL,
	[Semester_Number] [int] NOT NULL,
	[Semester_Name] [nvarchar](50) NOT NULL,
	[Semester_Type] [int] NOT NULL,
 CONSTRAINT [PK_Semester] PRIMARY KEY CLUSTERED 
(
	[Semester_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[R_StudentInfo_data]    Script Date: 18/6/2019 4:56:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[R_StudentInfo_data](
	[Student_Id] [int] IDENTITY(1,1) NOT NULL,
	[Student_Number] [nvarchar](50) NOT NULL,
	[Student_FullName] [nvarchar](200) NOT NULL,
	[Student_img] [nvarchar](max) NULL,
	[Student_Nationality] [nvarchar](50) NOT NULL,
	[Student_PassportOrNationalNo] [nvarchar](20) NOT NULL,
	[Student_Sex] [nvarchar](20) NOT NULL,
	[Student_PlaceOfBirth] [nvarchar](50) NOT NULL,
	[Student_NativeLanguage] [nvarchar](50) NOT NULL,
	[Student_PresentAddress] [nvarchar](100) NULL,
	[Student_Tel] [int] NOT NULL,
	[Student_Email] [nvarchar](50) NULL,
	[Student_PermanantAddress] [nvarchar](100) NOT NULL,
	[Student_EmerInSName] [nvarchar](50) NULL,
	[Student_EmerInSEmail] [nvarchar](50) NULL,
	[Student_EmerInSTel] [int] NULL,
	[Student_EmerOutSName] [nvarchar](50) NULL,
	[Student_EmerOutSTel] [int] NULL,
	[Student_EmerOutSEmail] [nvarchar](50) NULL,
	[Student_Nameofcertificate] [nvarchar](50) NULL,
	[Student_CertificateDate] [date] NOT NULL,
	[Student_GeneralPercentage] [int] NOT NULL,
	[Student_College] [nvarchar](50) NULL,
	[Department] [nvarchar](50) NULL,
	[Student_Status] [bit] NULL,
	[UserId] [nvarchar](128) NULL,
 CONSTRAINT [PK_R_StudentInfo_data] PRIMARY KEY CLUSTERED 
(
	[Student_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[R_StudentRegistration_data]    Script Date: 18/6/2019 4:56:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[R_StudentRegistration_data](
	[SR_Id] [int] IDENTITY(1,1) NOT NULL,
	[Student_Id] [int] NOT NULL,
	[Year_Id] [int] NOT NULL,
	[Semester_Id] [int] NOT NULL,
	[Department_Id] [int] NOT NULL,
	[User_Id] [nvarchar](128) NOT NULL,
	[batch_Id] [int] NOT NULL,
	[Registered] [bit] NULL,
	[Registr_Status] [nchar](10) NULL,
 CONSTRAINT [PK_StudentRegulation] PRIMARY KEY CLUSTERED 
(
	[SR_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[R_StudyYear_data]    Script Date: 18/6/2019 4:56:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[R_StudyYear_data](
	[Year_Id] [int] IDENTITY(1,1) NOT NULL,
	[Year_Number] [int] NOT NULL,
	[Year_Name] [nvarchar](50) NOT NULL,
	[YearStart] [date] NULL,
	[YearEnd] [date] NULL,
 CONSTRAINT [PK_studyYear] PRIMARY KEY CLUSTERED 
(
	[Year_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[R_TransferStudent]    Script Date: 18/6/2019 4:56:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[R_TransferStudent](
	[TSId] [int] IDENTITY(1,1) NOT NULL,
	[Student_Id] [int] NOT NULL,
	[Department_IdFrom] [int] NOT NULL,
	[Department_IdTo] [int] NOT NULL,
	[TSDate] [date] NOT NULL,
 CONSTRAINT [PK_R_TransferStudent] PRIMARY KEY CLUSTERED 
(
	[TSId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Re_Appreciation]    Script Date: 18/6/2019 4:56:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Re_Appreciation](
	[A_Id] [int] IDENTITY(1,1) NOT NULL,
	[A_Appreciation_Text] [nchar](10) NOT NULL,
	[A_Appreciation_Point] [float] NOT NULL,
 CONSTRAINT [PK_Re_Appreciation] PRIMARY KEY CLUSTERED 
(
	[A_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Re_CERTIFICATE]    Script Date: 18/6/2019 4:56:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Re_CERTIFICATE](
	[CertificateId] [int] IDENTITY(1,1) NOT NULL,
	[Result_Id] [int] NOT NULL,
 CONSTRAINT [PK_Re_CERTIFICATE] PRIMARY KEY CLUSTERED 
(
	[CertificateId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Re_StudentResult_data]    Script Date: 18/6/2019 4:56:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Re_StudentResult_data](
	[Result_Id] [int] IDENTITY(1,1) NOT NULL,
	[Result_ClassHours] [float] NOT NULL,
	[Result_ClassPoints] [float] NOT NULL,
	[Result_CumulativeHours] [float] NOT NULL,
	[Result_CumulativePoints] [float] NOT NULL,
	[Result_calculationNumber] [int] NULL,
	[Result_AcademicStatus] [nvarchar](50) NULL,
	[SR_Id] [int] NOT NULL,
 CONSTRAINT [PK_StudentResult] PRIMARY KEY CLUSTERED 
(
	[Result_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Re_StudentResultDescribtion]    Script Date: 18/6/2019 4:56:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Re_StudentResultDescribtion](
	[SRD_Id] [int] IDENTITY(1,1) NOT NULL,
	[SRD_CourseGrade] [float] NOT NULL,
	[SRD_PointsNumber] [float] NOT NULL,
	[SRD_Pointsletter] [nvarchar](50) NOT NULL,
	[SRD_ExamSit] [int] NOT NULL,
	[SRD_Status] [nvarchar](50) NULL,
	[Result_Id] [int] NOT NULL,
	[CATYId] [int] NOT NULL,
 CONSTRAINT [PK_Re_StudentResultDescribtion] PRIMARY KEY CLUSTERED 
(
	[SRD_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Re_StudentSupplementsAndAlternativesResult_data]    Script Date: 18/6/2019 4:56:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Re_StudentSupplementsAndAlternativesResult_data](
	[SSAR_Id] [int] IDENTITY(1,1) NOT NULL,
	[SSAR_ClassHours] [float] NOT NULL,
	[SSAR_ClassPoints] [float] NOT NULL,
	[SSAR_CumulativeHours] [float] NOT NULL,
	[SSAR_CumulativePoints] [float] NOT NULL,
	[SSAR_Type] [nvarchar](50) NOT NULL,
	[SR_Id] [int] NOT NULL,
 CONSTRAINT [PK_StudentSupplementsandalternativesResult] PRIMARY KEY CLUSTERED 
(
	[SSAR_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Re_StudentSupplementsAndAlternativesResultDescribtion_data]    Script Date: 18/6/2019 4:56:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Re_StudentSupplementsAndAlternativesResultDescribtion_data](
	[SSARD_Id] [int] IDENTITY(1,1) NOT NULL,
	[SSARD_CourseGrade] [float] NOT NULL,
	[SSARD_PointsNumber] [float] NOT NULL,
	[SSARD_PointsLetter] [nvarchar](50) NOT NULL,
	[SSARD_Status] [nvarchar](50) NOT NULL,
	[SSAR_Id] [int] NOT NULL,
	[CATYId] [int] NOT NULL,
 CONSTRAINT [PK_StudentSupplementsandalternativesResultDescribtion] PRIMARY KEY CLUSTERED 
(
	[SSARD_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Research]    Script Date: 18/6/2019 4:56:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Research](
	[ResID] [int] IDENTITY(1,1) NOT NULL,
	[ReTitle] [nvarchar](50) NULL,
	[ReAbout] [nvarchar](max) NULL,
	[RePublisher] [nvarchar](50) NULL,
	[ResUrl] [nvarchar](max) NULL,
	[Redate] [date] NULL,
	[CollegeID] [int] NULL,
 CONSTRAINT [PK_Research] PRIMARY KEY CLUSTERED 
(
	[ResID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[StudantsCircular]    Script Date: 18/6/2019 4:56:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StudantsCircular](
	[StdCirId] [int] IDENTITY(1,1) NOT NULL,
	[StdCirTitle] [nvarchar](50) NOT NULL,
	[StdCirBody] [ntext] NOT NULL,
	[StdCirDate] [datetime] NOT NULL,
 CONSTRAINT [PK_StudantsCircular] PRIMARY KEY CLUSTERED 
(
	[StdCirId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[StudentSheet]    Script Date: 18/6/2019 4:56:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StudentSheet](
	[SheetID] [int] IDENTITY(1,1) NOT NULL,
	[SubjectName] [nvarchar](50) NULL,
	[Department_Id] [int] NULL,
	[SheetSemester] [int] NULL,
	[SheetDescribe] [nvarchar](150) NULL,
	[SheetUrl] [nvarchar](max) NULL,
 CONSTRAINT [PK_StudentSheet] PRIMARY KEY CLUSTERED 
(
	[SheetID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[StudentTimeTable]    Script Date: 18/6/2019 4:56:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StudentTimeTable](
	[TTId] [int] NOT NULL,
	[TTFile] [ntext] NOT NULL,
	[TSemID] [int] NULL,
	[TDep] [int] NULL,
	[Tyeary] [int] NULL,
	[TDiscrbions] [nvarchar](50) NULL,
 CONSTRAINT [PK_StudentTimeTable] PRIMARY KEY CLUSTERED 
(
	[TTId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[attachment]  WITH CHECK ADD  CONSTRAINT [FK_attachment_R_StudentInfo_data] FOREIGN KEY([Student_id])
REFERENCES [dbo].[R_StudentInfo_data] ([Student_Id])
GO
ALTER TABLE [dbo].[attachment] CHECK CONSTRAINT [FK_attachment_R_StudentInfo_data]
GO
ALTER TABLE [dbo].[C_CourseAssignToYear_data]  WITH CHECK ADD  CONSTRAINT [FK_C_CourseAssignToTeacher_data_C_Courses_data] FOREIGN KEY([Course_Id])
REFERENCES [dbo].[C_Courses_data] ([Course_Id])
GO
ALTER TABLE [dbo].[C_CourseAssignToYear_data] CHECK CONSTRAINT [FK_C_CourseAssignToTeacher_data_C_Courses_data]
GO
ALTER TABLE [dbo].[C_CourseAssignToYear_data]  WITH CHECK ADD  CONSTRAINT [FK_C_CourseAssignToTeacher_data_C_Teachers_data] FOREIGN KEY([Teacher_Id])
REFERENCES [dbo].[C_Teachers_data] ([Teacher_Id])
GO
ALTER TABLE [dbo].[C_CourseAssignToYear_data] CHECK CONSTRAINT [FK_C_CourseAssignToTeacher_data_C_Teachers_data]
GO
ALTER TABLE [dbo].[C_CourseAssignToYear_data]  WITH CHECK ADD  CONSTRAINT [FK_C_CourseAssignToTeacher_data_R_Departments_data] FOREIGN KEY([Department_Id])
REFERENCES [dbo].[R_Departments_data] ([Department_Id])
GO
ALTER TABLE [dbo].[C_CourseAssignToYear_data] CHECK CONSTRAINT [FK_C_CourseAssignToTeacher_data_R_Departments_data]
GO
ALTER TABLE [dbo].[C_CourseAssignToYear_data]  WITH CHECK ADD  CONSTRAINT [FK_C_CourseAssignToYear_data_R_Semester_data] FOREIGN KEY([Semester_Id])
REFERENCES [dbo].[R_Semester_data] ([Semester_Id])
GO
ALTER TABLE [dbo].[C_CourseAssignToYear_data] CHECK CONSTRAINT [FK_C_CourseAssignToYear_data_R_Semester_data]
GO
ALTER TABLE [dbo].[C_CourseAssignToYear_data]  WITH CHECK ADD  CONSTRAINT [FK_C_CourseAssignToYear_data_R_StudyYear_data] FOREIGN KEY([Year_Id])
REFERENCES [dbo].[R_StudyYear_data] ([Year_Id])
GO
ALTER TABLE [dbo].[C_CourseAssignToYear_data] CHECK CONSTRAINT [FK_C_CourseAssignToYear_data_R_StudyYear_data]
GO
ALTER TABLE [dbo].[C_Teachers_data]  WITH CHECK ADD  CONSTRAINT [FK_C_Teachers_data_AspNetUsers] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[C_Teachers_data] CHECK CONSTRAINT [FK_C_Teachers_data_AspNetUsers]
GO
ALTER TABLE [dbo].[C_Teachers_data]  WITH CHECK ADD  CONSTRAINT [FK_C_Teachers_data_Designation] FOREIGN KEY([Designation_Id])
REFERENCES [dbo].[Designation] ([Designation_Id])
GO
ALTER TABLE [dbo].[C_Teachers_data] CHECK CONSTRAINT [FK_C_Teachers_data_Designation]
GO
ALTER TABLE [dbo].[C_Teachers_data]  WITH CHECK ADD  CONSTRAINT [FK_C_Teachers_data_R_Departments_data] FOREIGN KEY([Department_Id])
REFERENCES [dbo].[R_Departments_data] ([Department_Id])
GO
ALTER TABLE [dbo].[C_Teachers_data] CHECK CONSTRAINT [FK_C_Teachers_data_R_Departments_data]
GO
ALTER TABLE [dbo].[Employee]  WITH CHECK ADD  CONSTRAINT [FK_Employee_AspNetUsers] FOREIGN KEY([UserID])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[Employee] CHECK CONSTRAINT [FK_Employee_AspNetUsers]
GO
ALTER TABLE [dbo].[Employee]  WITH CHECK ADD  CONSTRAINT [FK_Employee_EmpType] FOREIGN KEY([EmpTId])
REFERENCES [dbo].[EmpType] ([EmpTId])
GO
ALTER TABLE [dbo].[Employee] CHECK CONSTRAINT [FK_Employee_EmpType]
GO
ALTER TABLE [dbo].[EventImg]  WITH CHECK ADD  CONSTRAINT [FK_EventImg_EventDetails] FOREIGN KEY([EventID])
REFERENCES [dbo].[EventDetails] ([EventID])
GO
ALTER TABLE [dbo].[EventImg] CHECK CONSTRAINT [FK_EventImg_EventDetails]
GO
ALTER TABLE [dbo].[Fee_TuitionFees]  WITH CHECK ADD  CONSTRAINT [FK_Fee_TuitionFees_AspNetUsers] FOREIGN KEY([User_Id])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[Fee_TuitionFees] CHECK CONSTRAINT [FK_Fee_TuitionFees_AspNetUsers]
GO
ALTER TABLE [dbo].[Fee_TuitionFees]  WITH CHECK ADD  CONSTRAINT [FK_Fee_TuitionFees_PaymentStatus] FOREIGN KEY([Payment_Id])
REFERENCES [dbo].[PaymentStatus] ([Payment_Id])
GO
ALTER TABLE [dbo].[Fee_TuitionFees] CHECK CONSTRAINT [FK_Fee_TuitionFees_PaymentStatus]
GO
ALTER TABLE [dbo].[Fee_TuitionFees]  WITH CHECK ADD  CONSTRAINT [FK_Fee_TuitionFees_R_Departments_data] FOREIGN KEY([Department_Id])
REFERENCES [dbo].[R_Departments_data] ([Department_Id])
GO
ALTER TABLE [dbo].[Fee_TuitionFees] CHECK CONSTRAINT [FK_Fee_TuitionFees_R_Departments_data]
GO
ALTER TABLE [dbo].[Fee_TuitionFees]  WITH CHECK ADD  CONSTRAINT [FK_Fee_TuitionFees_R_StudentInfo_data] FOREIGN KEY([Student_Id])
REFERENCES [dbo].[R_StudentInfo_data] ([Student_Id])
GO
ALTER TABLE [dbo].[Fee_TuitionFees] CHECK CONSTRAINT [FK_Fee_TuitionFees_R_StudentInfo_data]
GO
ALTER TABLE [dbo].[Fee_TuitionFeesDescribtion]  WITH CHECK ADD  CONSTRAINT [FK_Fee_TuitionFeesDescribtion_Fee_TuitionFees] FOREIGN KEY([TF_Id])
REFERENCES [dbo].[Fee_TuitionFees] ([TF_Id])
GO
ALTER TABLE [dbo].[Fee_TuitionFeesDescribtion] CHECK CONSTRAINT [FK_Fee_TuitionFeesDescribtion_Fee_TuitionFees]
GO
ALTER TABLE [dbo].[Fee_TuitionFeesDescribtion]  WITH CHECK ADD  CONSTRAINT [FK_Fee_TuitionFeesDescribtion_R_StudentInfo_data] FOREIGN KEY([Student_Id])
REFERENCES [dbo].[R_StudentInfo_data] ([Student_Id])
GO
ALTER TABLE [dbo].[Fee_TuitionFeesDescribtion] CHECK CONSTRAINT [FK_Fee_TuitionFeesDescribtion_R_StudentInfo_data]
GO
ALTER TABLE [dbo].[Fee_TuitionFeesDescribtion]  WITH CHECK ADD  CONSTRAINT [FK_Fee_TuitionFeesDescribtion_R_StudyYear_data] FOREIGN KEY([Year_Id])
REFERENCES [dbo].[R_StudyYear_data] ([Year_Id])
GO
ALTER TABLE [dbo].[Fee_TuitionFeesDescribtion] CHECK CONSTRAINT [FK_Fee_TuitionFeesDescribtion_R_StudyYear_data]
GO
ALTER TABLE [dbo].[InboxMsg]  WITH CHECK ADD  CONSTRAINT [FK_InboxMsg_R_Departments_data] FOREIGN KEY([Department_Id])
REFERENCES [dbo].[R_Departments_data] ([Department_Id])
GO
ALTER TABLE [dbo].[InboxMsg] CHECK CONSTRAINT [FK_InboxMsg_R_Departments_data]
GO
ALTER TABLE [dbo].[InboxMsg]  WITH CHECK ADD  CONSTRAINT [FK_InboxMsg_R_Semester_data] FOREIGN KEY([Semester_Id])
REFERENCES [dbo].[R_Semester_data] ([Semester_Id])
GO
ALTER TABLE [dbo].[InboxMsg] CHECK CONSTRAINT [FK_InboxMsg_R_Semester_data]
GO
ALTER TABLE [dbo].[Invoices]  WITH CHECK ADD  CONSTRAINT [FK_Invoices_Order] FOREIGN KEY([orID])
REFERENCES [dbo].[Order] ([orID])
GO
ALTER TABLE [dbo].[Invoices] CHECK CONSTRAINT [FK_Invoices_Order]
GO
ALTER TABLE [dbo].[InvoicesDetails]  WITH CHECK ADD  CONSTRAINT [FK_InvoicesDetails_Invoices] FOREIGN KEY([INnumber])
REFERENCES [dbo].[Invoices] ([INnumber])
GO
ALTER TABLE [dbo].[InvoicesDetails] CHECK CONSTRAINT [FK_InvoicesDetails_Invoices]
GO
ALTER TABLE [dbo].[InvoicesImg]  WITH CHECK ADD  CONSTRAINT [FK_InvoicesImg_Invoices] FOREIGN KEY([INnumber])
REFERENCES [dbo].[Invoices] ([INnumber])
GO
ALTER TABLE [dbo].[InvoicesImg] CHECK CONSTRAINT [FK_InvoicesImg_Invoices]
GO
ALTER TABLE [dbo].[Library_Issue]  WITH CHECK ADD  CONSTRAINT [FK_Library_Issue_Library_Book] FOREIGN KEY([BookNo])
REFERENCES [dbo].[Library_Book] ([BookNo])
GO
ALTER TABLE [dbo].[Library_Issue] CHECK CONSTRAINT [FK_Library_Issue_Library_Book]
GO
ALTER TABLE [dbo].[Library_Issue]  WITH CHECK ADD  CONSTRAINT [FK_Library_Issue_Library_Member] FOREIGN KEY([MemberId])
REFERENCES [dbo].[Library_Member] ([MemberId])
GO
ALTER TABLE [dbo].[Library_Issue] CHECK CONSTRAINT [FK_Library_Issue_Library_Member]
GO
ALTER TABLE [dbo].[OrderDetails]  WITH CHECK ADD  CONSTRAINT [FK_OrderDetails_Order] FOREIGN KEY([orID])
REFERENCES [dbo].[Order] ([orID])
GO
ALTER TABLE [dbo].[OrderDetails] CHECK CONSTRAINT [FK_OrderDetails_Order]
GO
ALTER TABLE [dbo].[R_BatchNumber_data]  WITH CHECK ADD  CONSTRAINT [FK_R_BatchNumber_data_R_Departments_data] FOREIGN KEY([Department_Id])
REFERENCES [dbo].[R_Departments_data] ([Department_Id])
GO
ALTER TABLE [dbo].[R_BatchNumber_data] CHECK CONSTRAINT [FK_R_BatchNumber_data_R_Departments_data]
GO
ALTER TABLE [dbo].[R_Departments_data]  WITH CHECK ADD  CONSTRAINT [FK_R_Departments_data_R_Colleges_data] FOREIGN KEY([Coll_Id])
REFERENCES [dbo].[R_Colleges_data] ([Coll_Id])
GO
ALTER TABLE [dbo].[R_Departments_data] CHECK CONSTRAINT [FK_R_Departments_data_R_Colleges_data]
GO
ALTER TABLE [dbo].[R_HealthStatus_data]  WITH CHECK ADD  CONSTRAINT [FK_R_HealthStatus_data_R_StudentInfo_data] FOREIGN KEY([Student_Id])
REFERENCES [dbo].[R_StudentInfo_data] ([Student_Id])
GO
ALTER TABLE [dbo].[R_HealthStatus_data] CHECK CONSTRAINT [FK_R_HealthStatus_data_R_StudentInfo_data]
GO
ALTER TABLE [dbo].[R_StudentInfo_data]  WITH CHECK ADD  CONSTRAINT [FK_R_StudentInfo_data_AspNetUsers] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[R_StudentInfo_data] CHECK CONSTRAINT [FK_R_StudentInfo_data_AspNetUsers]
GO
ALTER TABLE [dbo].[R_StudentRegistration_data]  WITH CHECK ADD  CONSTRAINT [FK_R_StudentRegistration_data_AspNetUsers] FOREIGN KEY([User_Id])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[R_StudentRegistration_data] CHECK CONSTRAINT [FK_R_StudentRegistration_data_AspNetUsers]
GO
ALTER TABLE [dbo].[R_StudentRegistration_data]  WITH CHECK ADD  CONSTRAINT [FK_R_StudentRegistration_data_R_BatchNumber_data] FOREIGN KEY([batch_Id])
REFERENCES [dbo].[R_BatchNumber_data] ([batch_Id])
GO
ALTER TABLE [dbo].[R_StudentRegistration_data] CHECK CONSTRAINT [FK_R_StudentRegistration_data_R_BatchNumber_data]
GO
ALTER TABLE [dbo].[R_StudentRegistration_data]  WITH CHECK ADD  CONSTRAINT [FK_R_StudentRegistration_data_R_Departments_data] FOREIGN KEY([Department_Id])
REFERENCES [dbo].[R_Departments_data] ([Department_Id])
GO
ALTER TABLE [dbo].[R_StudentRegistration_data] CHECK CONSTRAINT [FK_R_StudentRegistration_data_R_Departments_data]
GO
ALTER TABLE [dbo].[R_StudentRegistration_data]  WITH CHECK ADD  CONSTRAINT [FK_R_StudentRegistration_data_R_Semester_data] FOREIGN KEY([Semester_Id])
REFERENCES [dbo].[R_Semester_data] ([Semester_Id])
GO
ALTER TABLE [dbo].[R_StudentRegistration_data] CHECK CONSTRAINT [FK_R_StudentRegistration_data_R_Semester_data]
GO
ALTER TABLE [dbo].[R_StudentRegistration_data]  WITH CHECK ADD  CONSTRAINT [FK_R_StudentRegistration_data_R_StudentInfo_data] FOREIGN KEY([Student_Id])
REFERENCES [dbo].[R_StudentInfo_data] ([Student_Id])
GO
ALTER TABLE [dbo].[R_StudentRegistration_data] CHECK CONSTRAINT [FK_R_StudentRegistration_data_R_StudentInfo_data]
GO
ALTER TABLE [dbo].[R_StudentRegistration_data]  WITH CHECK ADD  CONSTRAINT [FK_R_StudentRegistration_data_R_StudyYear_data] FOREIGN KEY([Year_Id])
REFERENCES [dbo].[R_StudyYear_data] ([Year_Id])
GO
ALTER TABLE [dbo].[R_StudentRegistration_data] CHECK CONSTRAINT [FK_R_StudentRegistration_data_R_StudyYear_data]
GO
ALTER TABLE [dbo].[R_TransferStudent]  WITH CHECK ADD  CONSTRAINT [FK_R_TransferStudent_R_Departments_data] FOREIGN KEY([Department_IdFrom])
REFERENCES [dbo].[R_Departments_data] ([Department_Id])
GO
ALTER TABLE [dbo].[R_TransferStudent] CHECK CONSTRAINT [FK_R_TransferStudent_R_Departments_data]
GO
ALTER TABLE [dbo].[R_TransferStudent]  WITH CHECK ADD  CONSTRAINT [FK_R_TransferStudent_R_Departments_data1] FOREIGN KEY([Department_IdTo])
REFERENCES [dbo].[R_Departments_data] ([Department_Id])
GO
ALTER TABLE [dbo].[R_TransferStudent] CHECK CONSTRAINT [FK_R_TransferStudent_R_Departments_data1]
GO
ALTER TABLE [dbo].[R_TransferStudent]  WITH CHECK ADD  CONSTRAINT [FK_R_TransferStudent_R_StudentInfo_data] FOREIGN KEY([Student_Id])
REFERENCES [dbo].[R_StudentInfo_data] ([Student_Id])
GO
ALTER TABLE [dbo].[R_TransferStudent] CHECK CONSTRAINT [FK_R_TransferStudent_R_StudentInfo_data]
GO
ALTER TABLE [dbo].[Re_CERTIFICATE]  WITH CHECK ADD  CONSTRAINT [FK_Re_CERTIFICATE_Re_StudentResult_data] FOREIGN KEY([Result_Id])
REFERENCES [dbo].[Re_StudentResult_data] ([Result_Id])
GO
ALTER TABLE [dbo].[Re_CERTIFICATE] CHECK CONSTRAINT [FK_Re_CERTIFICATE_Re_StudentResult_data]
GO
ALTER TABLE [dbo].[Re_StudentResult_data]  WITH CHECK ADD  CONSTRAINT [FK_Re_StudentResult_data_R_StudentRegistration_data] FOREIGN KEY([SR_Id])
REFERENCES [dbo].[R_StudentRegistration_data] ([SR_Id])
GO
ALTER TABLE [dbo].[Re_StudentResult_data] CHECK CONSTRAINT [FK_Re_StudentResult_data_R_StudentRegistration_data]
GO
ALTER TABLE [dbo].[Re_StudentResultDescribtion]  WITH CHECK ADD  CONSTRAINT [FK_Re_StudentResultDescribtion_C_CourseAssignToYear_data] FOREIGN KEY([CATYId])
REFERENCES [dbo].[C_CourseAssignToYear_data] ([CATYId])
GO
ALTER TABLE [dbo].[Re_StudentResultDescribtion] CHECK CONSTRAINT [FK_Re_StudentResultDescribtion_C_CourseAssignToYear_data]
GO
ALTER TABLE [dbo].[Re_StudentResultDescribtion]  WITH CHECK ADD  CONSTRAINT [FK_Re_StudentResultDescribtion_Re_StudentResult_data] FOREIGN KEY([Result_Id])
REFERENCES [dbo].[Re_StudentResult_data] ([Result_Id])
GO
ALTER TABLE [dbo].[Re_StudentResultDescribtion] CHECK CONSTRAINT [FK_Re_StudentResultDescribtion_Re_StudentResult_data]
GO
ALTER TABLE [dbo].[Re_StudentSupplementsAndAlternativesResult_data]  WITH CHECK ADD  CONSTRAINT [FK_Re_StudentSupplementsAndAlternativesResult_data_R_StudentRegistration_data] FOREIGN KEY([SR_Id])
REFERENCES [dbo].[R_StudentRegistration_data] ([SR_Id])
GO
ALTER TABLE [dbo].[Re_StudentSupplementsAndAlternativesResult_data] CHECK CONSTRAINT [FK_Re_StudentSupplementsAndAlternativesResult_data_R_StudentRegistration_data]
GO
ALTER TABLE [dbo].[Re_StudentSupplementsAndAlternativesResultDescribtion_data]  WITH CHECK ADD  CONSTRAINT [FK_Re_StudentSupplementsAndAlternativesResultDescribtion_data_C_CourseAssignToYear_data] FOREIGN KEY([CATYId])
REFERENCES [dbo].[C_CourseAssignToYear_data] ([CATYId])
GO
ALTER TABLE [dbo].[Re_StudentSupplementsAndAlternativesResultDescribtion_data] CHECK CONSTRAINT [FK_Re_StudentSupplementsAndAlternativesResultDescribtion_data_C_CourseAssignToYear_data]
GO
ALTER TABLE [dbo].[Re_StudentSupplementsAndAlternativesResultDescribtion_data]  WITH CHECK ADD  CONSTRAINT [FK_Re_StudentSupplementsAndAlternativesResultDescribtion_data_Re_StudentSupplementsAndAlternativesResult_data] FOREIGN KEY([SSAR_Id])
REFERENCES [dbo].[Re_StudentSupplementsAndAlternativesResult_data] ([SSAR_Id])
GO
ALTER TABLE [dbo].[Re_StudentSupplementsAndAlternativesResultDescribtion_data] CHECK CONSTRAINT [FK_Re_StudentSupplementsAndAlternativesResultDescribtion_data_Re_StudentSupplementsAndAlternativesResult_data]
GO
ALTER TABLE [dbo].[Research]  WITH CHECK ADD  CONSTRAINT [FK_Research_R_Colleges_data] FOREIGN KEY([CollegeID])
REFERENCES [dbo].[R_Colleges_data] ([Coll_Id])
GO
ALTER TABLE [dbo].[Research] CHECK CONSTRAINT [FK_Research_R_Colleges_data]
GO
ALTER TABLE [dbo].[StudentSheet]  WITH CHECK ADD  CONSTRAINT [FK_StudentSheet_R_Departments_data] FOREIGN KEY([Department_Id])
REFERENCES [dbo].[R_Departments_data] ([Department_Id])
GO
ALTER TABLE [dbo].[StudentSheet] CHECK CONSTRAINT [FK_StudentSheet_R_Departments_data]
GO
ALTER TABLE [dbo].[StudentSheet]  WITH CHECK ADD  CONSTRAINT [FK_StudentSheet_R_Semester_data] FOREIGN KEY([SheetSemester])
REFERENCES [dbo].[R_Semester_data] ([Semester_Id])
GO
ALTER TABLE [dbo].[StudentSheet] CHECK CONSTRAINT [FK_StudentSheet_R_Semester_data]
GO
ALTER TABLE [dbo].[StudentTimeTable]  WITH CHECK ADD  CONSTRAINT [FK_StudentTimeTable_R_Departments_data] FOREIGN KEY([TDep])
REFERENCES [dbo].[R_Departments_data] ([Department_Id])
GO
ALTER TABLE [dbo].[StudentTimeTable] CHECK CONSTRAINT [FK_StudentTimeTable_R_Departments_data]
GO
ALTER TABLE [dbo].[StudentTimeTable]  WITH CHECK ADD  CONSTRAINT [FK_StudentTimeTable_R_Semester_data] FOREIGN KEY([TSemID])
REFERENCES [dbo].[R_Semester_data] ([Semester_Id])
GO
ALTER TABLE [dbo].[StudentTimeTable] CHECK CONSTRAINT [FK_StudentTimeTable_R_Semester_data]
GO
ALTER TABLE [dbo].[StudentTimeTable]  WITH CHECK ADD  CONSTRAINT [FK_StudentTimeTable_R_StudyYear_data] FOREIGN KEY([Tyeary])
REFERENCES [dbo].[R_StudyYear_data] ([Year_Id])
GO
ALTER TABLE [dbo].[StudentTimeTable] CHECK CONSTRAINT [FK_StudentTimeTable_R_StudyYear_data]
GO
/****** Object:  StoredProcedure [dbo].[BoardResultStudents]    Script Date: 18/6/2019 4:56:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[BoardResultStudents]
	-- Add the parameters for the stored procedure here
		@YId int,@SId int,@DId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

-- Insert statements for procedure here
	SELECT        R_StudentRegistration_data.SR_Id,R_StudentInfo_data.Student_Number ,R_StudentInfo_data.Student_Id, R_StudentInfo_data.Student_FullName, 
                         Re_StudentResult_data.Result_ClassHours, Re_StudentResult_data.Result_ClassPoints, Re_StudentResult_data.Result_CumulativeHours, 
                         Re_StudentResult_data.Result_CumulativePoints, Re_StudentResult_data.Result_AcademicStatus, R_StudentRegistration_data.Year_Id, R_StudentRegistration_data.Semester_Id, 
                         R_StudentRegistration_data.Department_Id
FROM            Re_StudentResult_data INNER JOIN
                         Re_StudentResultDescribtion ON Re_StudentResult_data.Result_Id = Re_StudentResultDescribtion.Result_Id INNER JOIN
                         R_StudentRegistration_data ON Re_StudentResult_data.SR_Id = R_StudentRegistration_data.SR_Id INNER JOIN
                         R_StudentInfo_data ON R_StudentRegistration_data.Student_Id = R_StudentInfo_data.Student_Id INNER JOIN
                         C_CourseAssignToYear_data ON Re_StudentResultDescribtion.CATYId = C_CourseAssignToYear_data.CATYId INNER JOIN
                         C_Courses_data ON C_CourseAssignToYear_data.Course_Id = C_Courses_data.Course_Id
WHERE        (R_StudentRegistration_data.Year_Id = @YId) AND (R_StudentRegistration_data.Semester_Id = @SId) AND (R_StudentRegistration_data.Department_Id = @DId)

END




GO
/****** Object:  StoredProcedure [dbo].[BordFristPage]    Script Date: 18/6/2019 4:56:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[BordFristPage]
	-- Add the parameters for the stored procedure here
	@YId int,@SId int,@DId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT        R_Colleges_data.Coll_Name, R_Departments_data.Department_Name, R_BatchNumber_data.batch_Name, R_StudyYear_data.Year_Name, R_Semester_data.Semester_Name, C_Courses_data.Course_Name, 
                         C_Courses_data.Course_HoursNumber, C_Courses_data.Course_Id,Re_StudentResult_data.Result_Id, Re_StudentResult_data.Result_CumulativePoints
FROM            Re_StudentResult_data INNER JOIN
                         R_StudentRegistration_data ON Re_StudentResult_data.SR_Id = R_StudentRegistration_data.SR_Id INNER JOIN
                         R_Departments_data ON R_StudentRegistration_data.Department_Id = R_Departments_data.Department_Id INNER JOIN
                         R_StudyYear_data ON R_StudentRegistration_data.Year_Id = R_StudyYear_data.Year_Id INNER JOIN
                         R_Colleges_data ON R_Departments_data.Coll_Id = R_Colleges_data.Coll_Id INNER JOIN
                         R_Semester_data ON R_StudentRegistration_data.Semester_Id = R_Semester_data.Semester_Id INNER JOIN
                         R_BatchNumber_data ON R_StudentRegistration_data.batch_Id = R_BatchNumber_data.batch_Id AND R_Departments_data.Department_Id = R_BatchNumber_data.Department_Id INNER JOIN
                         C_CourseAssignToYear_data ON R_Departments_data.Department_Id = C_CourseAssignToYear_data.Department_Id AND R_StudyYear_data.Year_Id = C_CourseAssignToYear_data.Year_Id AND 
                         R_Semester_data.Semester_Id = C_CourseAssignToYear_data.Semester_Id INNER JOIN
                         C_Courses_data ON C_CourseAssignToYear_data.Course_Id = C_Courses_data.Course_Id
WHERE        (R_StudentRegistration_data.Year_Id = @YId) AND (R_StudentRegistration_data.Semester_Id = @SId) AND (R_StudentRegistration_data.Department_Id = @DId)
END



GO
/****** Object:  StoredProcedure [dbo].[CourseOfResult]    Script Date: 18/6/2019 4:56:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[CourseOfResult]
	-- Add the parameters for the stored procedure here
	@YId int,@SId int,@DId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT        R_StudentRegistration_data.SR_Id, Re_StudentResult_data.Result_Id, Re_StudentResultDescribtion.SRD_CourseGrade, Re_StudentResultDescribtion.SRD_Pointsletter, C_Courses_data.Course_ShortName
FROM            Re_StudentResult_data INNER JOIN
                         R_StudentRegistration_data ON Re_StudentResult_data.SR_Id = R_StudentRegistration_data.SR_Id INNER JOIN
                         Re_StudentResultDescribtion ON Re_StudentResult_data.Result_Id = Re_StudentResultDescribtion.Result_Id INNER JOIN
                         C_CourseAssignToYear_data ON Re_StudentResultDescribtion.CATYId = C_CourseAssignToYear_data.CATYId INNER JOIN
                         C_Courses_data ON C_CourseAssignToYear_data.Course_Id = C_Courses_data.Course_Id
WHERE        (R_StudentRegistration_data.Year_Id = @YId) AND (R_StudentRegistration_data.Semester_Id = @SId) AND (R_StudentRegistration_data.Department_Id = @DId)
END




GO
/****** Object:  StoredProcedure [dbo].[CourseOfResultOfStudent]    Script Date: 18/6/2019 4:56:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[CourseOfResultOfStudent] 
	-- Add the parameters for the stored procedure here
	@YId int,@SId int,@DId int ,@RSId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT        R_StudentRegistration_data.SR_Id, Re_StudentResult_data.Result_Id, Re_StudentResultDescribtion.SRD_CourseGrade, Re_StudentResultDescribtion.SRD_Pointsletter
FROM            Re_StudentResult_data INNER JOIN
                         R_StudentRegistration_data ON Re_StudentResult_data.SR_Id = R_StudentRegistration_data.SR_Id INNER JOIN
                         Re_StudentResultDescribtion ON Re_StudentResult_data.Result_Id = Re_StudentResultDescribtion.Result_Id INNER JOIN
                         C_CourseAssignToYear_data ON Re_StudentResultDescribtion.CATYId = C_CourseAssignToYear_data.CATYId INNER JOIN
                         C_Courses_data ON C_CourseAssignToYear_data.Course_Id = C_Courses_data.Course_Id
WHERE        (R_StudentRegistration_data.Year_Id = @YId) AND (R_StudentRegistration_data.Semester_Id = @SId) AND (R_StudentRegistration_data.Department_Id = @DId) AND(R_StudentRegistration_data.SR_Id = @RSId)

END




GO
/****** Object:  StoredProcedure [dbo].[GetSemesterStudantResult]    Script Date: 18/6/2019 4:56:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetSemesterStudantResult]
	@StdNumber int ,@SemesterId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT        R_StudentRegistration_data.SR_Id, R_StudentInfo_data.Student_Number, R_StudentInfo_data.Student_FullName, R_Departments_data.Department_Name, R_Semester_data.Semester_Name, 
                         R_BatchNumber_data.batch_Name, Re_StudentResult_data.Result_Id, Re_StudentResult_data.Result_ClassHours, Re_StudentResult_data.Result_ClassPoints, Re_StudentResult_data.Result_AcademicStatus, 
                         Re_StudentResultDescribtion.SRD_CourseGrade, Re_StudentResultDescribtion.SRD_PointsNumber, Re_StudentResultDescribtion.SRD_Pointsletter, Re_StudentResultDescribtion.SRD_ExamSit, C_Courses_data.Course_Name,
                          R_StudentInfo_data.Student_Id
FROM            Re_StudentResult_data INNER JOIN
                         Re_StudentResultDescribtion ON Re_StudentResult_data.Result_Id = Re_StudentResultDescribtion.Result_Id INNER JOIN
                         C_CourseAssignToYear_data ON Re_StudentResultDescribtion.CATYId = C_CourseAssignToYear_data.CATYId INNER JOIN
                         C_Courses_data ON C_CourseAssignToYear_data.Course_Id = C_Courses_data.Course_Id INNER JOIN
                         R_StudentRegistration_data ON Re_StudentResult_data.SR_Id = R_StudentRegistration_data.SR_Id INNER JOIN
                         R_StudentInfo_data ON R_StudentRegistration_data.Student_Id = R_StudentInfo_data.Student_Id INNER JOIN
                         R_Departments_data ON R_StudentRegistration_data.Department_Id = R_Departments_data.Department_Id INNER JOIN
                         R_Semester_data ON R_StudentRegistration_data.Semester_Id = R_Semester_data.Semester_Id INNER JOIN
                         R_BatchNumber_data ON R_StudentRegistration_data.batch_Id = R_BatchNumber_data.batch_Id
WHERE        (R_StudentInfo_data.Student_Id = @StdNumber)  AND (C_CourseAssignToYear_data.Semester_Id = @SemesterId)
END





GO
/****** Object:  StoredProcedure [dbo].[GetStudantResult]    Script Date: 18/6/2019 4:56:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetStudantResult]
	-- Add the parameters for the stored procedure here
	@StdNumber int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT        R_StudentRegistration_data.SR_Id, R_StudentInfo_data.Student_Number, R_StudentInfo_data.Student_FullName, R_Departments_data.Department_Name, R_Semester_data.Semester_Name, 
                         R_BatchNumber_data.batch_Name, Re_StudentResult_data.Result_Id, Re_StudentResult_data.Result_ClassHours, Re_StudentResult_data.Result_ClassPoints, Re_StudentResult_data.Result_AcademicStatus, 
                         Re_StudentResultDescribtion.SRD_CourseGrade, Re_StudentResultDescribtion.SRD_PointsNumber, Re_StudentResultDescribtion.SRD_Pointsletter, Re_StudentResultDescribtion.SRD_ExamSit, C_Courses_data.Course_Name,
                          R_StudentInfo_data.Student_Id
FROM            Re_StudentResult_data INNER JOIN
                         Re_StudentResultDescribtion ON Re_StudentResult_data.Result_Id = Re_StudentResultDescribtion.Result_Id INNER JOIN
                         C_CourseAssignToYear_data ON Re_StudentResultDescribtion.CATYId = C_CourseAssignToYear_data.CATYId INNER JOIN
                         C_Courses_data ON C_CourseAssignToYear_data.Course_Id = C_Courses_data.Course_Id INNER JOIN
                         R_StudentRegistration_data ON Re_StudentResult_data.SR_Id = R_StudentRegistration_data.SR_Id INNER JOIN
                         R_StudentInfo_data ON R_StudentRegistration_data.Student_Id = R_StudentInfo_data.Student_Id INNER JOIN
                         R_Departments_data ON R_StudentRegistration_data.Department_Id = R_Departments_data.Department_Id INNER JOIN
                         R_Semester_data ON R_StudentRegistration_data.Semester_Id = R_Semester_data.Semester_Id INNER JOIN
                         R_BatchNumber_data ON R_StudentRegistration_data.batch_Id = R_BatchNumber_data.batch_Id
WHERE        (R_StudentInfo_data.Student_Id = @StdNumber)
END





GO
/****** Object:  StoredProcedure [dbo].[QueryGeneralReslt]    Script Date: 18/6/2019 4:56:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE  [dbo].[QueryGeneralReslt]
	-- Add the parameters for the stored procedure here
	@stdId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT        Re_StudentResult_data.Result_Id, Re_StudentResult_data.SR_Id, Re_StudentResult_data.Result_CumulativePoints, Re_StudentResult_data.Result_AcademicStatus, R_StudentInfo_data.Student_FullName, 
                         R_StudentInfo_data.Student_Nationality, R_StudentInfo_data.Student_img, R_Departments_data.Department_Name, R_Colleges_data.Coll_Name, R_StudentRegistration_data.Student_Id, 
                         R_StudentInfo_data.Student_Number
FROM            Re_StudentResult_data INNER JOIN
                         R_StudentRegistration_data ON Re_StudentResult_data.SR_Id = R_StudentRegistration_data.SR_Id INNER JOIN
                         R_StudentInfo_data ON R_StudentRegistration_data.Student_Id = R_StudentInfo_data.Student_Id INNER JOIN
                         R_Departments_data ON R_StudentRegistration_data.Department_Id = R_Departments_data.Department_Id INNER JOIN
                         R_Colleges_data ON R_Departments_data.Coll_Id = R_Colleges_data.Coll_Id
WHERE        (R_StudentRegistration_data.Student_Id = @stdId)
END



GO
USE [master]
GO
ALTER DATABASE [UniversityManagementSystem] SET  READ_WRITE 
GO
